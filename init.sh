git checkout 22-gelfand
git submodule update --init --recursive
mkdir build
cd build
cmake ..
make -j
mpirun -n 8 Gelfand lambda=0.35 initvalue_left=1 initvalue_right=1
mv data/vtu/u.vtu u_1.vtu
mpirun -n 8 Gelfand lambda=0.35 initvalue_left=1 initvalue_right=4
mv data/vtu/u.vtu u_2.vtu
mpirun -n 8 Gelfand lambda=0.35 initvalue_left=4 initvalue_right=1
mv data/vtu/u.vtu u_3.vtu
mpirun -n 8 Gelfand lambda=0.35 initvalue_left=4 initvalue_right=4
mv data/vtu/u.vtu u_4.vtu
paraview u_1.vtu
