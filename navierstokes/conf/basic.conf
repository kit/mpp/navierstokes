// file:   basic.conf
// author: Jonathan Wunderlich
//-----------------------------------

logfile = UseConfigEntries[Mesh, NormboundStrategy, Sigma, Reynolds];

Reynolds = 1.0;
#ReynoldsLength = 0.125;
Sigma = 0.25;

BarycentricRefinement = 1;

VerifiedComputation = 1;
precision = 10;
LatexDigits = 8;

//Compute Approximations
//-----------------------------------
ApproximationVelocity = 0;
ApproximationPressure = 0;
ApproximationV = 0;

//Plotting
//-----------------------------------
PlotVelocity = 1;
PlotPressure = 1;

//NormboundStrategy
//-----------------------------------
NormboundStrategy = Wieners;
#NormboundStrategy = PlumSimple;
#NormboundStrategy = PlumExtended;

//DefectStrategy
//-----------------------------------
VelocityDefectDiscretization = RT2;
#VelocityDefectDiscretization = P2;
PressureDefectDiscretization = RT2;
#PressureDefectDiscretization = P2;

//Homotopy / Goerisch
//-----------------------------------
#gamma1 = 0.60;
gamma1 = -1.0;
#Adjointgamma1 = 0.60;
Adjointgamma1 = -1.0;

HomotopyStartRho = 0.3;
AdjointHomotopyStartRho = 0.2;

AdjointDivergenceHomotopyNearZero = 0.1;

MinimizationStrategy = RTLagrange_Liu;
#MinimizationStrategy = RTLagrange;
#MinimizationStrategy = RTLagrange_all;
#MinimizationStrategy = Lagrange;
#MinimizationStrategy = Lagrange_all;

DivergenceHomotopyESolver = LOBPCGSelectiveVeryFast;
DivergenceHomotopyESolverMaxStep = 150;
DivergenceHomotopyESolverEpsilon = 1e-7;
DivergenceHomotopyESolverVerbose = 2;
DivergenceHomotopyCoarseESolver = LOBPCGSelectiveVeryFast;
DivergenceHomotopyCoarseESolverMaxStep = 150;
DivergenceHomotopyCoarseESolverEpsilon = 1e-7;
DivergenceHomotopyCoarseESolverVerbose = 2;
DivergenceHomotopyEVDegree = 2;
DivergenceHomotopyGoerischDegree = 2;
DivergenceHomotopySeparationRho = 5e-4;
DivergenceHomotopyNearRho = 1e-3;
DivergenceHomotopySeparationCluster = 5e-3;
DivergenceHomotopyStepSize = 0.001;
DivergenceHomotopyStepMax = 0.998;

AdjointDivergenceHomotopyESolver = LOBPCGSelectiveVeryFast;
AdjointDivergenceHomotopyESolverMaxStep = 150;
AdjointDivergenceHomotopyESolverEpsilon = 1e-7;
AdjointDivergenceHomotopyESolverVerbose = 2;
AdjointDivergenceHomotopyCoarseESolver = LOBPCGSelectiveVeryFast;
AdjointDivergenceHomotopyCoarseESolverMaxStep = 150;
AdjointDivergenceHomotopyCoarseESolverEpsilon = 1e-7;
AdjointDivergenceHomotopyCoarseESolverVerbose = 2;
AdjointDivergenceHomotopyEVDegree = 2;
AdjointDivergenceHomotopyGoerischDegree = 2;
AdjointDivergenceHomotopySeparationRho = 5e-4;
AdjointDivergenceHomotopyNearRho = 1e-3;
AdjointDivergenceHomotopySeparationCluster = 5e-3;
AdjointDivergenceHomotopyStepSize = 0.001;
AdjointDivergenceHomotopyStepMax = 0.998;

//Newton
//-----------------------------------
#NewtonSteps = 20;
NewtonReduction = 1e-10;

//LinearSolver
//-----------------------------------
LinearSolver = GMRES;
LinearSteps = 800;
#LinearSteps = 20;

//Preconditioner
//-----------------------------------
Preconditioner = PS;

ESolverPreconditioner = PS;

//Distribution
//-----------------------------------
#Distribution = RCB;
Distribution = RCB2D;
#Distribution = Stripes;

//Verbose
//-----------------------------------
ProblemVerbose = 9;
NavierStokesVerbose = 4;
AssembleVerbose = 2;
RitzGoerischAssembleVerbose = 2;
RayleighRitzVerbose = 7;
LehmannGoerischVerbose = 10;
DivergenceHomotopyVerbose = 5;
AdjointDivergenceHomotopyVerbose = 5;
OutputVerbose = -1;
NewtonVerbose = 1;
LinearVerbose = 1;
MultigridVerbose = -1;
PreconditionerVerbose = -1;
BaseSolverVerbose = -1;
MeshVerbose = 2;
ConfigVerbose = 1;
