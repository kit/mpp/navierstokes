#include "TestEnvironment.hpp"

#include "bernstein/MonomialBernstein.hpp"
#include "bernstein/BernsteinCoefficients.hpp"
#include "problem/NavierStokesPoiseuille.hpp"
#include "MeshesCreator.hpp"

using namespace ::testing;

struct Param {
  int expo_x;
  int expo_y;
  std::vector<double> expected;
};

class B8CoefficientTest : public TestWithParam<Param> {
protected:
  std::vector<double> coeff{};

  B8CoefficientTest() {
    coeff = getCoeff();
  }

  void OUTPUT(const std::vector<double> &coeff) {
    mout << "F0(A)=       " << coeff[0] << endl
         << "F1(A, B)=    " << coeff[1] << endl
         << "F2(A, B)=    " << coeff[3] << endl
         << "F3(A, B)=    " << coeff[6] << endl
         << "F4(A, B)=    " << coeff[10] << endl
         << "F5(A, B, C)= " << coeff[4] << endl
         << "F6(A, B, C)= " << coeff[7] << endl
         << "F7(A, B, C)= " << coeff[11] << endl
         << "F8(A, B, C)= " << coeff[12] << endl
         << "F9(A, B, C)= " << coeff[17] << endl
         << endl;
  }

private:
  std::vector<double> getCoeff() {
    VALUES<double> A(3.741, 2.675);
    VALUES<double> B(9.583, 1.876);
    VALUES<double> C(2.984, 6.592);

    switch (GetParam().expo_x) {
      case 0:
        switch (GetParam().expo_y) {
          case 0:
            return B8_Xp0_Yp0(A, B, C);
          case 1:
            return B8_Xp0_Yp1(A, B, C);
          case 2:
            return B8_Xp0_Yp2(A, B, C);
          case 3:
            return B8_Xp0_Yp3(A, B, C);
          case 4:
            return B8_Xp0_Yp4(A, B, C);
          case 5:
            return B8_Xp0_Yp5(A, B, C);
        }
      case 1:
        switch (GetParam().expo_y) {
          case 0:
            return B8_Xp1_Yp0(A, B, C);
          case 1:
            return B8_Xp1_Yp1(A, B, C);
          case 2:
            return B8_Xp1_Yp2(A, B, C);
          case 3:
            return B8_Xp1_Yp3(A, B, C);
          case 4:
            return B8_Xp1_Yp4(A, B, C);
          case 5:
            return B8_Xp1_Yp5(A, B, C);
        }
      case 2:
        switch (GetParam().expo_y) {
          case 0:
            return B8_Xp2_Yp0(A, B, C);
          case 1:
            return B8_Xp2_Yp1(A, B, C);
          case 2:
            return B8_Xp2_Yp2(A, B, C);
          case 3:
            return B8_Xp2_Yp3(A, B, C);
          case 4:
            return B8_Xp2_Yp4(A, B, C);
          case 5:
            return B8_Xp2_Yp5(A, B, C);
        }
      case 3:
        switch (GetParam().expo_y) {
          case 0:
            return B8_Xp3_Yp0(A, B, C);
          case 1:
            return B8_Xp3_Yp1(A, B, C);
          case 2:
            return B8_Xp3_Yp2(A, B, C);
          case 3:
            return B8_Xp3_Yp3(A, B, C);
          case 4:
            return B8_Xp3_Yp4(A, B, C);
          case 5:
            return B8_Xp3_Yp5(A, B, C);
        }
      case 4:
        switch (GetParam().expo_y) {
          case 0:
            return B8_Xp4_Yp0(A, B, C);
          case 1:
            return B8_Xp4_Yp1(A, B, C);
          case 2:
            return B8_Xp4_Yp2(A, B, C);
          case 3:
            return B8_Xp4_Yp3(A, B, C);
          case 4:
            return B8_Xp4_Yp4(A, B, C);
        }
      case 5:
        switch (GetParam().expo_y) {
          case 0:
            return B8_Xp5_Yp0(A, B, C);
          case 1:
            return B8_Xp5_Yp1(A, B, C);
          case 2:
            return B8_Xp5_Yp2(A, B, C);
          case 3:
            return B8_Xp5_Yp3(A, B, C);
        }
    }
    Exit("Not implemented")
  }
};

TEST_P(B8CoefficientTest, F0Test) {
  EXPECT_NEAR(coeff[0], GetParam().expected[0], 5e-4);
}

TEST_P(B8CoefficientTest, F1Test) {
  EXPECT_NEAR(coeff[1], GetParam().expected[1], 5e-4);
}

TEST_P(B8CoefficientTest, F2Test) {
  EXPECT_NEAR(coeff[3], GetParam().expected[2], 5e-4);
}

TEST_P(B8CoefficientTest, F3Test) {
  EXPECT_NEAR(coeff[6], GetParam().expected[3], 5e-4);
}

TEST_P(B8CoefficientTest, F4Test) {
  EXPECT_NEAR(coeff[10], GetParam().expected[4], 5e-4);
}

TEST_P(B8CoefficientTest, F5Test) {
  EXPECT_NEAR(coeff[4], GetParam().expected[5], 5e-4);
}

TEST_P(B8CoefficientTest, F6Test) {
  EXPECT_NEAR(coeff[7], GetParam().expected[6], 5e-4);
}

TEST_P(B8CoefficientTest, F7Test) {
  EXPECT_NEAR(coeff[11], GetParam().expected[7], 5e-4);
}

TEST_P(B8CoefficientTest, F8Test) {
  EXPECT_NEAR(coeff[12], GetParam().expected[8], 5e-4);
}

TEST_P(B8CoefficientTest, F9Test) {
  EXPECT_NEAR(coeff[17], GetParam().expected[9], 5e-4);
}

INSTANTIATE_TEST_SUITE_P(
    BernsteinMonomialsTest, B8CoefficientTest,
    Values(Param{0, 0, {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0}},
           Param{0,
                 1,
                 {2.675, 2.575125000, 2.475250000, 2.375375000, 2.275500000,
                  3.064750000, 2.964875000, 2.865000000, 3.454500000,
                  3.354625000}},
           Param{1,
                 0,
                 {3.741, 4.471250000, 5.201500000, 5.931750000, 6.662000000,
                  4.376625000, 5.106875000, 5.837125000, 5.012250000,
                  5.742500000}},
           Param{0,
                 2,
                 {7.155625, 6.621293750, 6.109762536, 5.621031357, 5.155100214,
                  9.129013107, 8.505707500, 7.905201928, 11.44961279,
                  10.73733282}},
           Param{1,
                 1,
                 {10.007175, 11.58696138, 13.00004211, 14.24641720, 15.32608664,
                  13.58495443, 15.41746296, 17.08326586, 17.72898493,
                  19.81421563}},
           Param{2,
                 0,
                 {13.995081, 19.45881150, 26.14143357, 34.04294721, 43.16335243,
                  18.59288461, 25.11756403, 32.86113504, 24.11416053,
                  31.69978889}},
           Param{0,
                 3,
                 {19.14129688, 16.99729274, 15.03625888, 13.24908670,
                  11.62666758, 26.61102191, 23.79765242, 21.20279847,
                  36.51860723, 32.89716138}},
           Param{1,
                 2,
                 {26.76919312, 29.99565508, 32.41553677, 34.09543711,
                  35.10195501, 40.94387092, 44.96900476, 48.03361864,
                  60.12930348, 65.14036595}},
           Param{2,
                 1,
                 {37.43684168, 50.65456204, 65.88552577, 82.64278559,
                  100.4393945, 58.32870797, 77.11313237, 98.26165136,
                  87.18525844, 112.5160908}},
           Param{3,
                 0,
                 {52.35559802, 83.01532172, 127.3546655, 188.9340117,
                  271.3137426, 77.26987783, 119.3752809, 178.2593359,
                  111.7451492, 167.9936942}},
           Param{0,
                 4,
                 {51.20296914, 43.55602105, 36.88796398, 31.10133576,
                  26.10449647, 76.24535715, 65.25620264, 55.59773066,
                  112.6056830, 97.01288331}},
           Param{1,
                 3,
                 {71.60759163, 77.56480417, 80.62785937, 81.29713811,
                  80.03045122, 120.7223240, 127.8307735, 131.1005741,
                  196.0638661, 204.5730292}},
           Param{2,
                 2,
                 {100.1435515, 131.7619489, 165.7483555, 199.9958969,
                  232.7089552, 177.4505861, 228.3638241, 281.5872338,
                  302.1050195, 380.1482227}},
           Param{3,
                 1,
                 {140.0512247, 216.8369703, 323.2165226, 463.2488964,
                  638.7173092, 244.7122956, 372.1284468, 544.3762932,
                  412.0964834, 612.8908186}},
           Param{4,
                 0,
                 {195.8622922, 348.7929940, 604.0746134, 1014.984711,
                  1651.440649, 315.7138798, 550.8293303, 932.5134436,
                  501.3711186, 855.2826018}},
           Param{0,
                 5,
                 {136.9679425, 111.3984597, 90.19319956, 72.70038361,
                  58.34610589, 215.3546603, 175.9495951, 143.0704784,
                  337.1354525, 276.8089503}},
           Param{1,
                 4,
                 {191.5503076, 200.3340430, 200.0159608, 193.0907961,
                  181.6195649, 349.8933366, 356.0550482, 349.5113133,
                  617.6113277, 617.2380530}},
           Param{2,
                 3,
                 {267.8840003, 342.4613762, 416.1551480, 482.3834076,
                  536.7435712, 527.7977944, 658.6494000, 782.3004397,
                  1005.568004, 1224.743558}},
           Param{3,
                 2,
                 {374.6370261, 566.0512792, 819.0998769, 1132.817957,
                  1497.558380, 750.4444979, 1116.912758, 1589.110463,
                  1455.716152, 2127.224610}},
           Param{4,
                 1,
                 {523.9316316, 913.4595124, 1541.864203, 2510.773988,
                  3932.273601, 1008.709530, 1741.357636, 2905.673420,
                  1882.610778, 3202.079639}},
           Param{5,
                 0,
                 {732.7208351, 1447.863030, 2801.163195, 5290.899716,
                  9726.598486, 1272.503867, 2478.545848, 4716.126701,
                  2188.602556, 4194.886575}},
           Param{1,
                 5,
                 {512.3970729, 516.7624781, 494.7753204, 456.7451907,
                  410.2268504, 1000.081777, 975.1749446, 913.8352264,
                  1888.119193, 1799.073209}},
           Param{2,
                 4,
                 {716.5897006, 889.3292670, 1042.680242, 1159.389023,
                  1232.238853, 1542.754865, 1861.120826, 2121.453425,
                  3231.673599, 3785.003538}},
           Param{3,
                 3,
                 {1002.154045, 1476.770299, 2072.561286, 2762.323078,
                  3496.243394, 2248.154500, 3262.125605, 4491.834706,
                  4934.353707, 7029.543909}},
           Param{4,
                 2,
                 {1401.517115, 2391.176525, 3930.907750, 6197.135164,
                  9329.293356, 3115.573470, 5289.230393, 8629.987710,
                  6767.305587, 11401.02294}},
           Param{5,
                 1,
                 {1960.028234, 3799.853110, 7183.493814, 13188.90091,
                  23408.11251, 4100.276246, 7939.386139, 14979.60045,
                  8356.347380, 16087.97822}},
           Param{2,
                 5,
                 {1916.877450, 2307.386392, 2606.596882, 2776.101262,
                  2815.368966, 4447.072862, 5172.001846, 5639.882643,
                  10077.92401, 11285.77538}},
           Param{3,
                 4,
                 {2680.762070, 3850.270416, 5235.573057, 6715.490188,
                  8125.753406, 6615.852342, 9331.206318, 12387.53133,
                  16136.11839, 22255.89836}},
           Param{4,
                 3,
                 {3749.058283, 6256.420681, 10009.30009, 15259.87799,
                  22048.61779, 9391.808791, 15616.90094, 24786.96168,
                  23312.52858, 38585.89757}},
           Param{5,
                 2,
                 {5243.075526, 9968.849250, 18404.32458, 32816.26484,
                  56157.59160, 12749.01800, 24377.10243, 45205.73675,
                  30526.73983, 58667.56234}},
           Param{3,
                 5,
                 {7171.038539, 10031.73225, 13202.69366, 16273.55696,
                  18796.15246, 19195.84433, 26251.17577, 33488.13755,
                  51182.37188, 67936.50227}},
           Param{4,
                 4,
                 {10028.73090, 16361.48813, 25453.60831, 37482.09295,
                  51896.40312, 27795.64789, 45137.62149, 69423.13785,
                  77396.41313, 124956.9506}},
           Param{5,
                 3,
                 {14025.22703, 26143.01959, 47105.43726, 81494.04630,
                  134276.9707, 38648.12614, 72667.73796, 131729.7239,
                  106715.5439, 202983.4408}}));

class B9CoefficientTest : public TestWithParam<Param> {
protected:
  std::vector<double> coeff{};

  B9CoefficientTest() {
    coeff = getCoeff();
  }

  void OUTPUT(const std::vector<double> &coeff) {
    mout << "F0(A)=        " << coeff[0] << endl
         << "F1(A, B)=     " << coeff[1] << endl
         << "F2(A, B)=     " << coeff[3] << endl
         << "F3(A, B)=     " << coeff[6] << endl
         << "F4(A, B)=     " << coeff[10] << endl
         << "F5(A, B, C)=  " << coeff[4] << endl
         << "F6(A, B, C)=  " << coeff[7] << endl
         << "F7(A, B, C)=  " << coeff[11] << endl
         << "F8(A, B, C)=  " << coeff[12] << endl
         << "F9(A, B, C)=  " << coeff[16] << endl
         << "F10(A, B, C)= " << coeff[17] << endl
         << "F11(A, B, C)= " << coeff[24] << endl
         << endl;
  }

private:
  std::vector<double> getCoeff() {
    VALUES<double> A(3.741, 2.675);
    VALUES<double> B(9.583, 1.876);
    VALUES<double> C(2.984, 6.592);

    switch (GetParam().expo_x) {
      case 0:
        switch (GetParam().expo_y) {
          case 0:
            return B9_Xp0_Yp0(A, B, C);
          case 1:
            return B9_Xp0_Yp1(A, B, C);
          case 2:
            return B9_Xp0_Yp2(A, B, C);
          case 3:
            return B9_Xp0_Yp3(A, B, C);
          case 4:
            return B9_Xp0_Yp4(A, B, C);
          case 5:
            return B9_Xp0_Yp5(A, B, C);
        }
      case 1:
        switch (GetParam().expo_y) {
          case 0:
            return B9_Xp1_Yp0(A, B, C);
          case 1:
            return B9_Xp1_Yp1(A, B, C);
          case 2:
            return B9_Xp1_Yp2(A, B, C);
          case 3:
            return B9_Xp1_Yp3(A, B, C);
          case 4:
            return B9_Xp1_Yp4(A, B, C);
          case 5:
            return B9_Xp1_Yp5(A, B, C);
        }
      case 2:
        switch (GetParam().expo_y) {
          case 0:
            return B9_Xp2_Yp0(A, B, C);
          case 1:
            return B9_Xp2_Yp1(A, B, C);
          case 2:
            return B9_Xp2_Yp2(A, B, C);
          case 3:
            return B9_Xp2_Yp3(A, B, C);
          case 4:
            return B9_Xp2_Yp4(A, B, C);
          case 5:
            return B9_Xp2_Yp5(A, B, C);
        }
      case 3:
        switch (GetParam().expo_y) {
          case 0:
            return B9_Xp3_Yp0(A, B, C);
          case 1:
            return B9_Xp3_Yp1(A, B, C);
          case 2:
            return B9_Xp3_Yp2(A, B, C);
          case 3:
            return B9_Xp3_Yp3(A, B, C);
          case 4:
            return B9_Xp3_Yp4(A, B, C);
          case 5:
            return B9_Xp3_Yp5(A, B, C);
        }
      case 4:
        switch (GetParam().expo_y) {
          case 0:
            return B9_Xp4_Yp0(A, B, C);
          case 1:
            return B9_Xp4_Yp1(A, B, C);
          case 2:
            return B9_Xp4_Yp2(A, B, C);
          case 3:
            return B9_Xp4_Yp3(A, B, C);
          case 4:
            return B9_Xp4_Yp4(A, B, C);
          case 5:
            return B9_Xp4_Yp5(A, B, C);
        }
      case 5:
        switch (GetParam().expo_y) {
          case 0:
            return B9_Xp5_Yp0(A, B, C);
          case 1:
            return B9_Xp5_Yp1(A, B, C);
          case 2:
            return B9_Xp5_Yp2(A, B, C);
          case 3:
            return B9_Xp5_Yp3(A, B, C);
          case 4:
            return B9_Xp5_Yp4(A, B, C);
        }
    }
    Exit("Not implemented")
  }
};

TEST_P(B9CoefficientTest, F0Test) {
  EXPECT_NEAR(coeff[0], GetParam().expected[0], 5e-4);
}

TEST_P(B9CoefficientTest, F1Test) {
  EXPECT_NEAR(coeff[1], GetParam().expected[1], 5e-4);
}

TEST_P(B9CoefficientTest, F2Test) {
  EXPECT_NEAR(coeff[3], GetParam().expected[2], 5e-4);
}

TEST_P(B9CoefficientTest, F3Test) {
  EXPECT_NEAR(coeff[6], GetParam().expected[3], 5e-4);
}

TEST_P(B9CoefficientTest, F4Test) {
  EXPECT_NEAR(coeff[10], GetParam().expected[4], 5e-4);
}

TEST_P(B9CoefficientTest, F5Test) {
  EXPECT_NEAR(coeff[4], GetParam().expected[5], 5e-4);
}

TEST_P(B9CoefficientTest, F6Test) {
  EXPECT_NEAR(coeff[7], GetParam().expected[6], 5e-4);
}

TEST_P(B9CoefficientTest, F7Test) {
  EXPECT_NEAR(coeff[11], GetParam().expected[7], 5e-4);
}

TEST_P(B9CoefficientTest, F8Test) {
  EXPECT_NEAR(coeff[12], GetParam().expected[8], 5e-4);
}

TEST_P(B9CoefficientTest, F9Test) {
  EXPECT_NEAR(coeff[16], GetParam().expected[9], 5e-4);
}

TEST_P(B9CoefficientTest, F10Test) {
  EXPECT_NEAR(coeff[17], GetParam().expected[10], 5e-4);
}

TEST_P(B9CoefficientTest, F11Test) {
  EXPECT_NEAR(coeff[24], GetParam().expected[11], 5e-4);
}

INSTANTIATE_TEST_SUITE_P(
    BernsteinMonomialsTest, B9CoefficientTest,
    Values(Param{0,
                 0,
                 {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0}},
           Param{0,
                 1,
                 {2.675, 2.586222222, 2.497444445, 2.408666666, 2.319888889,
                  3.021444444, 2.932666666, 2.843888888, 3.367888889,
                  2.755111111, 3.279111111, 3.714333333}},
           Param{1,
                 0,
                 {3.741, 4.390111111, 5.039222223, 5.688333333, 6.337444444,
                  4.306000001, 4.955111112, 5.604222222, 4.871000000,
                  6.253333334, 5.520111111, 5.436000000}},
           Param{0,
                 2,
                 {7.155625, 6.680663889, 6.223436138, 5.783941750, 5.362180722,
                  8.922167138, 8.378003750, 7.851573722, 10.95876272,
                  7.342877056, 10.34539706, 13.26541175}},
           Param{1,
                 1,
                 {10.007175, 11.41142956, 12.68602416, 13.83095883, 14.84623356,
                  13.14082029, 14.74163653, 16.21279282, 16.71488308,
                  17.55428917, 18.51226100, 20.72936337}},
           Param{2,
                 0,
                 {13.995081, 18.85173033, 24.65640644, 31.40910933, 39.10983900,
                  18.09956672, 23.78139856, 30.41125717, 22.92230869,
                  37.98914256, 29.42932302, 28.46330691}},
           Param{0,
                 3,
                 {19.14129688, 17.23551542, 15.47204418, 13.84481076,
                  12.34774274, 25.88071796, 23.44935747, 21.18400403,
                  34.55497623, 19.07858526, 31.50556268, 45.52494024}},
           Param{1,
                 2,
                 {26.76919312, 29.63715930, 31.87778528, 33.53547033,
                  34.65461372, 39.16604109, 42.67970080, 45.46339384,
                  55.38432127, 47.56151949, 59.66851239, 76.01255729}},
           Param{2,
                 1,
                 {37.43684168, 49.18592645, 62.50086717, 77.05703234,
                  92.52979049, 55.70564923, 71.69130399, 89.47671549,
                  80.02946615, 108.7372523, 100.9048268, 111.3680880}},
           Param{3,
                 0,
                 {52.35559802, 79.60868577, 117.5014780, 168.4075630,
                  234.7005288, 74.69857899, 110.9051230, 159.8173926,
                  104.5671243, 223.8089763, 151.5254329, 143.5265195}},
           Param{0,
                 4,
                 {51.20296914, 44.40568196, 38.36975443, 33.03021182,
                  28.32531395, 73.99608000, 64.54621044, 56.09539966,
                  105.9752792, 48.56605032, 93.00711593, 150.2774187}},
           Param{1,
                 3,
                 {71.60759163, 76.90289166, 79.94718042, 81.07404519,
                  80.59342316, 114.6375574, 121.0063498, 124.4769255,
                  177.7192334, 125.4469438, 185.4094293, 267.6940170}},
           Param{2,
                 2,
                 {100.1435515, 128.2487937, 158.1958207, 188.5800498,
                  218.1698182, 167.2948400, 210.0924969, 254.7803932,
                  269.6504087, 299.6918022, 332.2313797, 419.2123287}},
           Param{3,
                 1,
                 {140.0512247, 208.3052208, 299.5766222, 416.5714384,
                  560.7313480, 231.6534991, 338.3790884, 477.9461894,
                  370.8756496, 653.4689105, 530.7339235, 575.4569521}},
           Param{4,
                 0,
                 {195.8622922, 331.8006938, 547.3453647, 878.0146784,
                  1368.571343, 303.8706500, 504.4975951, 814.4488800,
                  464.3344450, 1277.289342, 754.4745166, 697.9539715}},
           Param{0,
                 5,
                 {136.9679425, 114.2395133, 94.90547960, 78.53132226,
                  64.72578486, 209.0222547, 175.1777879, 146.2112846,
                  317.1825356, 121.5320697, 267.1981240, 478.3089374}},
           Param{1,
                 4,
                 {191.5503076, 199.3580724, 200.0866458, 195.3991843,
                  186.7178900, 330.7262327, 337.3481026, 334.3125008,
                  554.3985043, 323.8463350, 557.8676468, 905.8466987}},
           Param{2,
                 3,
                 {267.8840003, 334.1750010, 399.7787543, 460.3073212,
                  512.5834986, 493.1640620, 602.6274596, 707.7593118,
                  879.2905159, 802.9609243, 1053.364347, 1515.297481}},
           Param{3,
                 2,
                 {374.6370261, 544.7830289, 762.8668555, 1028.245264,
                  1335.451526, 700.2796337, 1002.385047, 1381.012061,
                  1268.678381, 1832.699233, 1783.807535, 2221.370875}},
           Param{4,
                 1,
                 {523.9316316, 870.1786369, 1402.218717, 2187.804060,
                  3300.495997, 949.0275047, 1556.381009, 2473.690443,
                  1670.913625, 3804.346776, 2696.388636, 2862.422921}},
           Param{5,
                 0,
                 {732.7208351, 1368.402786, 2500.429825, 4460.987542,
                  7755.176807, 1221.715954, 2246.382890, 4034.130084,
                  2014.728556, 7060.654271, 3641.956374, 3282.262525}},
           Param{1,
                 5,
                 {512.3970729, 516.2774331, 499.6613552, 469.4219004,
                  430.9016687, 942.8567217, 927.3320590, 883.4940170,
                  1687.348090, 821.2184659, 1632.035652, 2951.931529}},
           Param{2,
                 4,
                 {716.5897006, 870.1359820, 1008.602248, 1120.486096,
                  1199.861152, 1432.622549, 1699.434992, 1927.779848,
                  2786.174044, 2103.719625, 3230.882422, 5259.191716}},
           Param{3,
                 3,
                 {1002.154045, 1424.035160, 1940.163289, 2532.402480,
                  3170.056587, 2076.695027, 2904.624879, 3889.763714,
                  4196.741440, 4989.930351, 5767.211798, 8235.614439}},
           Param{4,
                 2,
                 {1401.517115, 2281.214367, 3588.745256, 5441.726027,
                  7937.223048, 2885.885317, 4655.270783, 7246.084988,
                  5795.246771, 10856.20356, 9240.664885, 11312.20441}},
           Param{5,
                 1,
                 {1960.028234, 3595.428124, 6431.573658, 11187.09855,
                  18866.24069, 3841.409817, 7002.262571, 12433.89573,
                  7348.643175, 21445.73300, 13264.46177, 13743.51725}},
           Param{2,
                 5,
                 {1916.877450, 2263.996509, 2540.105662, 2719.599804,
                  2797.916656, 4112.344878, 4725.861520, 5165.724445,
                  8611.821137, 5410.396042, 9628.515427, 17562.45959}},
           Param{3,
                 4,
                 {2680.762070, 3720.325044, 4927.728028, 6222.184481,
                  7498.969752, 6066.810631, 8272.723962, 10738.52953,
                  13482.09936, 13274.60609, 18023.00125, 29305.57358}},
           Param{4,
                 3,
                 {3749.058283, 5977.824860, 9175.326886, 13509.68535,
                  19031.40011, 8603.053175, 13610.48039, 20671.70992,
                  19418.92815, 30036.05578, 30428.34436, 42949.22899}},
           Param{5,
                 2,
                 {5243.075526, 9443.763280, 16529.77450, 28012.28476,
                  45783.66856, 11745.70174, 21129.44170, 36886.25066,
                  25815.82101, 62235.64394, 46295.77138, 55504.69912}},
           Param{3,
                 5,
                 {7171.038539, 9713.877392, 12498.03557, 15249.93586,
                  17674.99890, 17510.26313, 23233.49299, 29163.08579,
                  42234.16627, 34647.12974, 54696.59997, 100280.1304}},
           Param{4,
                 4,
                 {10028.73090, 15657.84844, 23433.13716, 33472.59806,
                  45490.04305, 25254.17959, 39096.73700, 57778.96075,
                  63144.59383, 81140.37217, 96762.59084, 156448.4778}},
           Param{5,
                 3,
                 {14025.22703, 24796.59818, 42447.12223, 70031.17664,
                  110817.8933, 35181.41989, 62267.56858, 106460.6533,
                  87528.16425, 174868.3199, 155059.9825, 215432.7494}},
           Param{4,
                 5,
                 {26826.85518, 40994.41613, 59778.56881, 82760.05553,
                  108375.5575, 73223.71167, 110726.4758, 158850.1065,
                  200118.8933, 214984.0358, 298655.6703, 547719.0203}},
           Param{5,
                 4,
                 {37517.48231, 65085.77164, 108904.2136, 174787.6494,
                  267498.4230, 103716.9493, 180204.8895, 300896.4264,
                  287728.4698, 479434.3888, 501026.9530, 801847.2298}}));

class TransformationTest : public Test {
public:
  NavierStokesPoiseuille U;
  std::unique_ptr<Meshes> meshes;

  TransformationTest() {
    meshes = MeshesCreator("Triangle").WithLevel(2).WithPLevel(0).CreateUnique();
  }
};

TEST_F(TransformationTest, B3ToB8Test) {
  std::vector<double> coeff_b3(10, 1.0);
  std::vector<double> coeff_b8 = B3ToB8(coeff_b3);
  for (int i = 0; i < coeff_b8.size(); ++i)
    EXPECT_DOUBLE_EQ(1.0, coeff_b8[i]);
}

TEST_F(TransformationTest, B4ToB9Test) {
  std::vector<double> coeff_b4(15, 1.0);
  std::vector<double> coeff_b9 = B4ToB9(coeff_b4);
  for (int i = 0; i < coeff_b9.size(); ++i)
    EXPECT_DOUBLE_EQ(1.0, coeff_b9[i]);
}

TEST_F(TransformationTest, PoiseuilleFlowTest) {
  const Mesh &mesh = meshes->fine();
  for (cell c = mesh.cells(); c != mesh.cells_end(); ++c) {
    std::vector<IAInterval> b4_coeff_U = U.B4Coefficients_U(*c);
    std::vector<IAInterval> b9_coeff_U = U.B9Coefficients_U(*c);
    std::vector<IAInterval> transformed_b9_coeff_U = B4ToB9(b4_coeff_U);
    for (int i = 0; i < b9_coeff_U.size(); ++i)
      EXPECT_IAINTERVAL_NEAR(b9_coeff_U[i], transformed_b9_coeff_U[i], 1e-12);
  }
}

TEST_F(TransformationTest, PoiseuilleFlowDerivativeTest) {
  const Mesh &mesh = meshes->fine();
  for (cell c = mesh.cells(); c != mesh.cells_end(); ++c) {
    std::vector<IAInterval> b3_coeff_DU = U.B3Coefficients_DU(*c);
    std::vector<IAInterval> b8_coeff_DU = U.B8Coefficients_DU(*c);
    std::vector<IAInterval> transformed_b8_coeff_DU = B3ToB8(b3_coeff_DU);
    for (int i = 0; i < b8_coeff_DU.size(); ++i)
      EXPECT_IAINTERVAL_NEAR(b8_coeff_DU[i], transformed_b8_coeff_DU[i], 1e-12);
  }
}

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv).WithPPM()).RUN_ALL_MPP_TESTS();
}