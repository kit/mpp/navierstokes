#ifndef TESTNAVIERSTOKESHOMOTOPY_HPP
#define TESTNAVIERSTOKESHOMOTOPY_HPP

#include "TestEnvironment.hpp"

const ConfigMap defaultConfigMap = {
    {"ConfigVerbose", "1"},
    {"Distribution", "RCB2D"},
    {"LinearSolver", "GMRES"},
    {"LinearSteps", "200"},
    {"Preconditioner", "PS"},
    {"HomotopyStartRho", "0.5"},
    {"MeshStrip", "Strip_tri_length6_refined_sym"},
    {"StripLevel", "2"},
    {"StripCoarseLevel", "1"},
    {"plevelStrip", "0"},
    {"DivergenceHomotopyESolver", "LOBPCGSelectiveVeryFast"},
    {"DivergenceHomotopyESolverVerbose", "2"},
    {"DivergenceHomotopyCoarseESolver", "LOBPCGSelectiveVeryFast"},
    {"DivergenceHomotopyCoarseESolverVerbose", "2"},
    {"DivergenceHomotopyDegree", "2"},
    {"DivergenceHomotopyGoerischRTOrder", "1"},
    {"DivergenceHomotopyGoerischLagrangeDegree", "2"},
    {"DivergenceHomotopySeparationRho", "5e-4"},
    {"DivergenceHomotopyNearRho", "1e-3"},
    {"DivergenceHomotopySeparationCluster", "3e-3"},
    {"DivergenceHomotopyStepMax", "0.95"},
    {"DivergenceHomotopyStepSize", "0.01"},
    {"DivergenceHomotopyVerbose", "10"},
    {"LehmannGoerischVerbose", "10"}
};

#endif //TESTNAVIERSTOKESHOMOTOPY_HPP
