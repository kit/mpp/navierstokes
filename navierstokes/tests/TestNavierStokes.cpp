#include "TestNavierStokes.hpp"

#include "problem/NavierStokesProblem.hpp"
#include "NavierStokesApproximation.hpp"
#include "NavierStokesElement.hpp"

#include "ScalarElement.hpp"
#include "m++.hpp"

using namespace ::testing;
using namespace navierstokes;

class NavierStokesTest : public TestWithParam<ProblemParameter> {
protected:
  ConfigMap finalMap;

  std::unique_ptr<NavierStokesProblem> problem;
  double expectedVelocityL2Norm, expectedPressureL2Norm;

  NavierStokesTest() {
    finalMap = defaultConfigMap;
    finalMap["Mesh"] = GetParam().meshesName;
    finalMap["BarycentricRefinement"] = "0";
    finalMap["plevel"] = std::to_string(GetParam().pLevel);
    finalMap["VelocityLevel"] = std::to_string(GetParam().velocityLevel);
    finalMap["DefectLevel"] = std::to_string(GetParam().defectLevel);
    finalMap["PressureLevel"] = std::to_string(GetParam().pressureLevel);
    finalMap["ProblemType"] = GetParam().problemType;
    finalMap["Reynolds"] = std::to_string(GetParam().Re);
    finalMap["d0"] = std::to_string(GetParam().d0);
    finalMap["d1"] = std::to_string(GetParam().d1);
    finalMap["d2"] = std::to_string(GetParam().d2);
    if (GetParam().d3 >= 0.0)
      finalMap["d3"] = std::to_string(GetParam().d3);

    Config::Initialize(finalMap);
    Config::PrintInfo();

//    Logging::Precision(14);

    NavierStokesProblem::DisableSaveLoad();
    problem = CreateProblem();
    problem->PrintInfo();

    expectedVelocityL2Norm = GetParam().expectedVelocityL2Norm;
    expectedPressureL2Norm = GetParam().expectedPressureL2Norm;

    problem->Constants().UOmegaInftySqr<false>() = 1.0;
    problem->Constants().D_UOmegaInftySqr<false>() = 1.0;

    approximation::InitApproximationV(*problem);
    approximation::InitPressure(*problem);
  }


  void TearDown() override {
    Config::Close();
  }

  double ComputeVelocityL2Norm() {
    double sum = 0.0;
    for (cell c = problem->Velocity<false>().cells(); c != problem->Velocity<false>().cells_end(); ++c) {
      const Cell &C = *c;
      DivergenceFreeElement E(problem->Velocity<false>(), C);
      for (int q = 0; q < E.nQ(); ++q) {
        VectorField UOmega = E.VelocityField(q, problem->Velocity<false>())
                             + problem->VF_Gamma(E.QPoint(q), C);
        sum += E.QWeight(q) * (UOmega * UOmega);
      }
    }
    return sqrt(PPM->Sum(sum));
  }

  double ComputePressureL2Norm() {
    double sum = 0.0;
    for (cell c = problem->Pressure().cells(); c != problem->Pressure().cells_end(); ++c) {
      const Cell &C = *c;
      ScalarElement E(problem->Pressure(), C);
      for (int q = 0; q < E.nQ(); ++q) {
        double p = E.Value(q, problem->Pressure());
        sum += E.QWeight(q) * (p * p);
      }
    }
    return sqrt(PPM->Sum(sum));
  }

  bool CheckRangeVelocity() {
    bool check = true;
    for (cell c = problem->Velocity<false>().cells(); c != problem->Velocity<false>().cells_end(); ++c) {
      const Cell &C = *c;
      IAInterval u0;
      IAInterval u1;
      IAInterval Dx_u0;
      IAInterval Dy_u0;
      IAInterval Dx_u1;
      auto E = IANavierStokesElement::Create<false>(*problem, problem->Velocity<false>(), C);
      E->Range(u0, u1);
      E->RangeD(Dx_u0, Dy_u0, Dx_u1);
      for (int q = 0; q < E->nQ(); ++q) {
        IAVectorField u = E->VelocityField(q, problem->Velocity<false>());
        check = check && (u[0] <= u0);
        check = check && (u[1] <= u1);

        IATensor Du = E->VelocityFieldGradient(q, problem->Velocity<false>());
        check = check && (Du[0][0] <= Dx_u0);
        check = check && (Du[0][1] <= Dy_u0);
        check = check && (Du[1][0] <= Dx_u1);
        check = check && (Du[1][1] <= -Dx_u0);
      }
    }
    return PPM->And(check);
  }

  bool CheckRangesConstants() {
    bool check = true;
    const Mesh &mesh = problem->GetMeshes().fine();
    for (cell c = mesh.cells(); c != mesh.cells_end(); ++c) {
      const Cell &C = *c;
      auto E = IANavierStokesElement::template Create<false>(*problem, problem->Velocity<false>(), C);
      auto E_d = NavierStokesElement::template Create<false>(*problem, problem->Velocity<false>(), C);
      IAInterval W0, W1, UOmega0, UOmega1, Dx_UOmega0, Dy_UOmega0, Dx_UOmega1;
      E->Ranges(W0, W1, UOmega0, UOmega1, Dx_UOmega0, Dy_UOmega0, Dx_UOmega1);

      for (int i = 0; i < C.Corners(); ++i) {
        VectorField W = problem->VF_Gamma(C.Corner(i), C);
        check = check && (W[0] <= W0);
        check = check && (W[1] <= W1);

        VectorField UOmega = W + E_d->VelocityField(C.LocalCorner(i), problem->Velocity<false>());
        check = check && (UOmega[0] <= UOmega0);
        check = check && (UOmega[1] <= UOmega1);

        Tensor D_UOmega = problem->VFG_Gamma(C.Corner(i), C)
            + E_d->VelocityFieldGradient(C.LocalCorner(i), problem->Velocity<false>());
        check = check && (D_UOmega[0][0] <= Dx_UOmega0);
        check = check && (D_UOmega[0][1] <= Dy_UOmega0);
        check = check && (D_UOmega[1][0] <= Dx_UOmega1);
        check = check && (D_UOmega[1][1] <= -Dx_UOmega0);
      }

      for (int q = 0; q < E->nQ(); ++q) {
        VectorField W = problem->VF_Gamma(E_d->QPoint(q), C);
        check = check && (W[0] <= W0);
        check = check && (W[1] <= W1);

        VectorField UOmega = W + E_d->VelocityField(q, problem->Velocity<false>());
        check = check && (UOmega[0] <= UOmega0);
        check = check && (UOmega[1] <= UOmega1);

        Tensor D_UOmega = problem->VFG_Gamma(E_d->QPoint(q), C)
                          + E_d->VelocityFieldGradient(q, problem->Velocity<false>());
        check = check && (D_UOmega[0][0] <= Dx_UOmega0);
        check = check && (D_UOmega[0][1] <= Dy_UOmega0);
        check = check && (D_UOmega[1][0] <= Dx_UOmega1);
        check = check && (D_UOmega[1][1] <= -Dx_UOmega0);
      }
    }
    return PPM->And(check);
  }

  bool CheckRangesPlumSimple() {
    bool check = true;
    const Mesh &mesh = problem->GetMeshes().fine();
    for (cell c = mesh.cells(); c != mesh.cells_end(); ++c) {
      const Cell &C = *c;
      auto E = IANavierStokesElement::template Create<false>(*problem, problem->Velocity<false>(), C);
      auto E_d = NavierStokesElement::template Create<false>(*problem, problem->Velocity<false>(), C);
      IAInterval Dx_0, Dy_0_Dx_1;
      E->RangesPlumSimple(Dx_0, Dy_0_Dx_1);

      for (int q = 0; q < E->nQ(); ++q) {
        Tensor D_Omega = E_d->VelocityFieldGradient(q, problem->Velocity<false>())
                         - problem->VFG_V(E_d->QPoint(q), C);
        double value = 1 - 2 * E_d->QPoint(q)[1] + D_Omega[0][1] + D_Omega[1][0];

        check = check && (D_Omega[0][0] <= Dx_0);
        check = check && (value <= Dy_0_Dx_1);
      }

      // needed for corners!
      Dy_0_Dx_1 += IAInterval(-1.0, 1.0) * 1e-8;
      for (int i = 0; i < C.Corners(); ++i) {
        Tensor D_Omega = E_d->VelocityFieldGradient(C.LocalCorner(i), problem->Velocity<false>())
                         - problem->VFG_V(C.Corner(i), C);
        double value = 1 - 2 * c.Corner(i)[1] + D_Omega[0][1] + D_Omega[1][0];

        check = check && (D_Omega[0][0] <= Dx_0);
        check = check && (value <= Dy_0_Dx_1);
      }
    }
    return PPM->And(check);
  }

  bool CheckRangesPlumExtended() {
    bool check = true;
    const Mesh &mesh = problem->GetMeshes().fine();
    for (cell c = mesh.cells(); c != mesh.cells_end(); ++c) {
      const Cell &C = *c;
      auto E = IANavierStokesElement::template Create<false>(*problem, problem->Velocity<false>(), C);
      auto E_d = NavierStokesElement::template Create<false>(*problem, problem->Velocity<false>(), C);
      IAInterval Omega0, Omega1, Dx_Omega0, Dy_Omega0, Dx_Omega1, Dy_0_Dx_1;
      E->RangesPlumExtended(Omega0, Omega1, Dx_Omega0, Dy_Omega0, Dx_Omega1, Dy_0_Dx_1);

      for (int i = 0; i < C.Corners(); ++i) {
        VectorField Omega = E_d->VelocityField(C.LocalCorner(i), problem->Velocity<false>())
                            - problem->VF_V(C.Corner(i), C);
        check = check && (Omega[0] <= Omega0);
        check = check && (Omega[1] <= Omega1);

        Tensor D_Omega = E_d->VelocityFieldGradient(C.LocalCorner(i), problem->Velocity<false>())
                         - problem->VFG_V(C.Corner(i), C);
        check = check && (D_Omega[0][0] <= Dx_Omega0);
        check = check && (D_Omega[0][1] <= Dy_Omega0);
        check = check && (D_Omega[1][0] <= Dx_Omega1);
        check = check && (D_Omega[1][1] <= -Dx_Omega0);

        double value = D_Omega[0][1] + D_Omega[1][0];
        check = check && (value <= Dy_0_Dx_1);
      }

      for (int q = 0; q < E->nQ(); ++q) {
        VectorField Omega = E_d->VelocityField(q, problem->Velocity<false>())
                            - problem->VF_V(E_d->QPoint(q), C);
        check = check && (Omega[0] <= Omega0);
        check = check && (Omega[1] <= Omega1);

        Tensor D_Omega = E_d->VelocityFieldGradient(q, problem->Velocity<false>())
                         - problem->VFG_V(E_d->QPoint(q), C);
        check = check && (D_Omega[0][0] <= Dx_Omega0);
        check = check && (D_Omega[0][1] <= Dy_Omega0);
        check = check && (D_Omega[1][0] <= Dx_Omega1);
        check = check && (D_Omega[1][1] <= -Dx_Omega0);

        double value = D_Omega[0][1] + D_Omega[1][0];
        check = check && (value <= Dy_0_Dx_1);
      }
    }
    return PPM->And(check);
  }

  bool CheckRangesDelta() {
    bool check = true;
    const Mesh &mesh = problem->GetMeshes().fine();
    for (cell c = mesh.cells(); c != mesh.cells_end(); ++c) {
      const Cell &C = *c;
      auto E = IANavierStokesElement::template Create<false>(*problem, problem->ApprV(), C);
      auto E_d = NavierStokesElement::template Create<false>(*problem, problem->ApprV(), C);
      IAInterval Dx_UomegaTVT0, Dy_UomegaTVT0, Dx_UomegaTVT1;
      E->RangesDelta(Dx_UomegaTVT0, Dy_UomegaTVT0, Dx_UomegaTVT1);

      for (int q = 0; q < E->nQ(); ++q) {
        Tensor D_UOmegaTVT = problem->VFG_U(E_d->QPoint(q))
                             + E_d->VelocityFieldGradient(q, problem->Velocity<false>())
                             - E_d->VelocityFieldGradient(q, problem->ApprV());

        check = check && (D_UOmegaTVT[0][0] <= Dx_UomegaTVT0);
        check = check && (D_UOmegaTVT[0][1] <= Dy_UomegaTVT0);
        check = check && (D_UOmegaTVT[1][0] <= Dx_UomegaTVT1);
        check = check && (D_UOmegaTVT[1][1] <= -Dx_UomegaTVT0);
      }

      // needed for corners!
      Dy_UomegaTVT0 += IAInterval(-1.0, 1.0) * 1e-8;
      for (int i = 0; i < c.Corners(); ++i) {
        Tensor D_UOmegaTVT = problem->VFG_U(c.Corner(i))
                          + E_d->VelocityFieldGradient(c.LocalCorner(i), problem->Velocity<false>())
                          - E_d->VelocityFieldGradient(c.LocalCorner(i), problem->ApprV());

        check = check && (D_UOmegaTVT[0][0] <= Dx_UomegaTVT0);
        check = check && (D_UOmegaTVT[0][1] <= Dy_UomegaTVT0);
        check = check && (D_UOmegaTVT[1][0] <= Dx_UomegaTVT1);
        check = check && (D_UOmegaTVT[1][1] <= -Dx_UomegaTVT0);
      }
    }
    return PPM->And(check);
  }
};

void runTest(std::string name, double value, double expectedValue) {
  mout << name << " ..." << endl
       << "    computed value= " << value << endl
       << "    expected value= " << expectedValue << endl;
  EXPECT_NEAR(value, expectedValue, 1e-4);
}

void runTest(std::string name, bool value) {
  mout << name << " ..." << endl;
  if (value)
    mout << "    computed value= true" << endl;
  else
    mout << "    computed value= false" << endl;
  mout << "    expected value= true" << endl;
  EXPECT_TRUE(value);
}

TEST_P(NavierStokesTest, NavierStokesTest) {
  runTest("L2NormVelocityTest", ComputeVelocityL2Norm(), expectedVelocityL2Norm);
  runTest("L2NormPressureTest", ComputePressureL2Norm(), expectedPressureL2Norm);
  runTest("RangesVelocityTest", CheckRangeVelocity());
  runTest("RangesConstantsTest", CheckRangesConstants());
  runTest("RangesPlumSimpleTest", CheckRangesPlumSimple());
  runTest("RangesPlumExtendedTest", CheckRangesPlumExtended());
  runTest("RangesDeltaTest", CheckRangesDelta());
}

INSTANTIATE_TEST_SUITE_P(NavierStokesTest, NavierStokesTest,
                         ValuesIn(TestProblems));

int main(int argc, char **argv) {
  return MppTest(
      MppTestBuilder(argc, argv).
      WithoutDefaultConfig().
      WithScreenLogging().
      WithConfPath("../../../navierstokes/conf").
      WithPPM()
  ).RUN_ALL_MPP_TESTS();
}
