#include "TestNavierStokesHomotopy.hpp"

#include "problem/NavierStokesProblem.hpp"
#include "NavierStokesHomotopy.hpp"

#include "m++.hpp"

using namespace ::testing;
using namespace navierstokes;

struct Param {
  double re;
  std::string miniStrat;
  double expectedK;
};

class NavierStokesHomotopyTest : public TestWithParam<Param> {
protected:
  ConfigMap finalMap;

  std::unique_ptr<NavierStokesProblem> problem;

  NavierStokesHomotopyTest() {
    finalMap = defaultConfigMap;
    const Param &param = GetParam();
    finalMap["Reynolds"] = std::to_string(param.re);
    finalMap["MinimizationStrategy"] = param.miniStrat;

    Config::Initialize(finalMap);
    Config::PrintInfo();

    problem = CreateProblem();

    problem->Constants().Gamma1<false>() = 0.75;
    problem->Constants().Gamma2<false>() = 2.9;
    problem->SET_RAPPR(3.0);
    double rho_start = 0.5;

    navierstokes::NavierStokesHomotopy<true, false> homotopy(*problem);
    homotopy.start(rho_start);
  }

  void TearDown() override {
    Config::Close();
  }
};

TEST_P(NavierStokesHomotopyTest, NavierStokesHomotopyTest) {
  EXPECT_NEAR(problem->Constants().K<false>(), GetParam().expectedK, 1e-6);
}

INSTANTIATE_TEST_SUITE_P(
    NavierStokesHomotopyTest, NavierStokesHomotopyTest,
    Values(Param{1.0, "Lagrange", 1.417672150036609},
           //    Param{1.0, "Lagrange_all", 1.4172258003061557},
           Param{1.0, "RTLagrange", 1.4177007250839324},
           //    Param{1.0, "RTLagrange_all", 1.4172473344228291},
           Param{1.0, "RTLagrange_Liu", 1.4169068253511199}));

int main(int argc, char **argv) {
  return MppTest(
      MppTestBuilder(argc, argv).
          WithoutDefaultConfig().
          WithScreenLogging().
          WithConfPath("../../../navierstokes/conf").
          WithPPM()
  ).RUN_ALL_MPP_TESTS();
}