#ifndef TESTNAVIERSTOKES_HPP
#define TESTNAVIERSTOKES_HPP

#include "TestEnvironment.hpp"

const ConfigMap defaultConfigMap = {
    {"ConfigVerbose", "1"},
    {"ProblemVerbose", "2"},
    {"Distribution", "RCB2D"},
    {"LinearSolver", "GMRES"},
    {"LinearSteps", "200"},
    {"Preconditioner", "PS"},
    {"ApproximationVelocity", "1"},
    {"ApproximationPressure", "1"},
    {"ApproximationV", "1"},
    {"BarycentricRefinement", "0"}
};

struct ProblemParameter {
  std::string meshesName;
  std::string problemType;
  double d0;
  double d1;
  double d2;
  double d3;
  int pLevel;
  int velocityLevel;
  int defectLevel;
  int pressureLevel;
  double Re;
  double expectedVelocityL2Norm;
  double expectedPressureL2Norm;
};

static std::vector<ProblemParameter> TestProblems = {
    ProblemParameter{
        "Strip_tri_bottom_1step_obstacle12_sym",
        "Bottom",
        2.5, 0.5, 0.5, -1.0,
        0, 2, 2, 2,
        1.0,
        0.45271053279726, 1.0648983706727
    },
    ProblemParameter{
        "Strip_tri_bottom_1step_obstacle12_sym",
        "Bottom",
        2.5, 0.5, 0.5, -1.0,
        0, 2, 2, 2,
        10.0,
        0.45271298699709, 0.10708911785801
    },
    ProblemParameter{
        "Strip_tri_bottom_1step_obstacle03_sym",
        "Bottom",
        2.5, 0.5, 0.5, -1.0,
        0, 2, 2, 2,
        1.0,
        0.46050058722201, 3.2058167859217
    },
    ProblemParameter{
        "Strip_tri_bottom_2steps_obstacle01_sym",
        "Bottom",
        2.5, 0.5, 0.5, -1.0,
        0, 2, 2, 2,
        1.0,
        0.44896241782899, 0.25058077946654
    },
    ProblemParameter{
        "Strip_tri_bottom_square_obstacle01_sym",
        "Bottom",
        2.5, 0.5, 0.5, -1.0,
        0, 2, 2, 2,
        1.0,
        0.44913754767727, 0.23998092996186
    },
    ProblemParameter{
        "Strip_tri_center_square_obstacle01_sym",
        "Center",
        2.5, 0.5, 0.25, 0.75,
        0, 2, 2, 2,
        1.0,
        0.45311218842315, 12.066649678991
    },
    ProblemParameter{
        "Strip_tri_both_obstacle01_sym",
        "Both",
        2.5, 0.5, 0.25, 0.75,
        0, 2, 2, 2,
        1.0,
        0.46487836966999, 5.3329307134778
    }
};

#endif //TESTNAVIERSTOKES_HPP
