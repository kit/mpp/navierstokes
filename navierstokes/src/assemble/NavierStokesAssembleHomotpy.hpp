#ifndef NAVIERSTOKESASSEMBLEHOMOTPY_HPP
#define NAVIERSTOKESASSEMBLEHOMOTPY_HPP

#include "assemble/NavierStokesAssemble.hpp"
#include "LagrangeDiscretization.hpp"
#include "DivergenceFreeDiscretization.hpp"
#include "RTDiscretization.hpp"
#include "VectorFieldElement.hpp"
#include "NavierStokesDiscretization.hpp"

namespace navierstokes {

//========================================================================================
// Homotopy assembles
//========================================================================================
  template<bool VERIFIED>
  class HomotopyStokesAssemble : public IAEigenvalueAssemble<VERIFIED>,
                                 public StokesBoundaryConditions {
  protected:
    NavierStokesProblem &problem;
  public:
    HomotopyStokesAssemble(NavierStokesProblem &problem, double shift)
        : IAEigenvalueAssemble<VERIFIED>(shift), problem(problem) {}
  };

//----------------------------------------------------------------------------------------
// DivergenceHomotopyStokesAssemble
//----------------------------------------------------------------------------------------
  template<bool VERIFIED, bool adjoint>
  class DivergenceHomotopyStokesAssemble : public HomotopyStokesAssemble<VERIFIED> {
  protected:
    double midGamma_1;
    double midGamma_2;
    IAInterval &gamma1;
    IAInterval &gamma2;

    const IAEigenvalueType<VERIFIED> &Gamma1() const;

    const IAEigenvalueType<VERIFIED> &Gamma2() const;

    int evDegree = 2;

    std::string prefix = "";
  public:
    DivergenceHomotopyStokesAssemble(NavierStokesProblem &problem, double shift)
      : HomotopyStokesAssemble<VERIFIED>(problem, shift),
        midGamma_1(mid(problem.Constants().Gamma1<adjoint>())),
        midGamma_2(mid(problem.Constants().Gamma2<adjoint>())),
        gamma1(problem.Constants().Gamma1<adjoint>()),
        gamma2(problem.Constants().Gamma2<adjoint>()) {
      if constexpr (adjoint) prefix = "AdjointDivergenceHomotopy";
      else prefix = "DivergenceHomotopy";
      Config::Get(prefix + "EVDegree", evDegree);
    }

    void BoundaryConditionsEigenSolver(const cell &c, Eigenfct &u) const override {
      StokesBoundaryConditions::setDirichletBC(c, u);
    }

    void MatrixentriesEigenSolver(const cell &c, Matrix &A, Matrix &B,
                                  const double &shift, const double &t) override {
      const Cell &C = *c;
      int chi = 1;
      if (this->problem.CellOutsideApproximationDomain(*c)) chi = 0;
      double d = midGamma_1 + shift;
      double tt = t / (1.0 - t);

      ScalarElement E(A, C);
      RowEntries A_c(A, E);
      RowEntries B_c(B, E);
      for (int q = 0; q < E.nQ(); ++q)
        for (int i = 0; i < E.size(); ++i)
          for (int j = 0; j < E.size(); ++j) {
            VectorField D_i = E.Derivative(q, i);
            VectorField D_j = E.Derivative(q, j);
            double L_ij = E.Value(q, i) * E.Value(q, j);
            double N_ij = D_i * D_j + this->problem.Sigma() * L_ij;

            A_c(i, j, 0, 0) += E.QWeight(q) * (d * N_ij - midGamma_2 * chi * L_ij
                                               + tt * D_i[0] * D_j[0]);
            A_c(i, j, 0, 1) += E.QWeight(q) * tt * D_i[0] * D_j[1];
            A_c(i, j, 1, 0) += E.QWeight(q) * tt * D_i[1] * D_j[0];
            A_c(i, j, 1, 1) += E.QWeight(q) * (d * N_ij - midGamma_2 * chi * L_ij
                                               + tt * D_i[1] * D_j[1]);

            B_c(i, j, 0, 0) += E.QWeight(q) * N_ij;
            B_c(i, j, 1, 1) += E.QWeight(q) * N_ij;
          }
    }

    void MatrixentriesRayleighRitz(const cell &c,
                                   const Eigenfct &U_i, const Eigenfct &U_j,
                                   IAEigenvalueType<VERIFIED> &a, IAEigenvalueType<VERIFIED> &b,
                                   const double &shift, const double &t) override {
      using T = IAEigenvalueType<VERIFIED>;

      const Cell &C = *c;
      int chi = 1;
      if (this->problem.CellOutsideApproximationDomain(C)) chi = 0;
      T tt = t / (T(1.0) - t);

      VectorFieldElementT<T> E(U_i, C);
      for (int q = 0; q < E.nQ(); ++q) {
        TensorT<T> D_i = E.VectorGradient(q, U_i);
        TensorT<T> D_j = E.VectorGradient(q, U_j);
        T L_ij = E.VectorValue(q, U_i) * E.VectorValue(q, U_j);
        T N_ij = Frobenius(D_i, D_j) + this->problem.Sigma() * L_ij;

        a += E.QWeight(q) * ((Gamma1() + shift) * N_ij - Gamma2() * chi * L_ij
                             + tt * (D_i[0][0] + D_i[1][1]) * (D_j[0][0] + D_j[1][1]));
        b += E.QWeight(q) * N_ij;
      }
    }

    virtual std::unique_ptr<IDiscretization> CreateEVDisc(const Meshes &meshes) const = 0;

    virtual std::unique_ptr<IAIDiscretization> CreateIAEVDisc(const Meshes &meshes) const = 0;
  };

//----------------------------------------------------------------------------------------
// LagrangeDivergenceHomotopyStokesAssemble
//----------------------------------------------------------------------------------------
  template<bool VERIFIED, bool adjoint>
  class LagrangeDivergenceHomotopyStokesAssemble
      : public DivergenceHomotopyStokesAssemble<VERIFIED, adjoint> {
    int lagrange_degree = 2;
    int lagrange_size{};
  public:
    LagrangeDivergenceHomotopyStokesAssemble(NavierStokesProblem &problem, double shift, int lagrange_size)
        : DivergenceHomotopyStokesAssemble<VERIFIED, adjoint>(problem, shift), lagrange_size(lagrange_size) {
      Config::Get(this->prefix + "GoerischLagrangeDegree", lagrange_degree);
    }

    std::unique_ptr<IDiscretization>
    CreateEVDisc(const Meshes &meshes) const override {
      return std::make_unique<LagrangeDiscretization>(meshes, this->evDegree, 2);
    }

    std::unique_ptr<IAIDiscretization>
    CreateIAEVDisc(const Meshes &meshes) const override {
      return std::make_unique<IALagrangeDiscretization>(meshes, this->evDegree, 2);
    }

    std::unique_ptr<IDiscretization>
    CreateGoerischDisc(const IDiscretization &EVdisc) const override {
      return std::make_unique<LagrangeDiscretization>(
          dynamic_cast<const NonAdaptiveIDiscretization &>(EVdisc), lagrange_degree, lagrange_size);
    }

    std::unique_ptr<IAIDiscretization>
    CreateIAGoerischDisc(const IAIDiscretization &iaEVdisc) const override {
      return std::make_unique<IALagrangeDiscretization>(
          dynamic_cast<const IANonAdaptiveIDiscretization &>(iaEVdisc), lagrange_degree, lagrange_size);
    }

    void BoundaryConditionsGoerisch(const cell &c, Vector &w) const override {}

    virtual void PrintInfo() const override {
      mout.PrintInfo("Assemble", this->verbose,
                     PrintInfoEntry("Name", this->Name(), 0),
                     PrintInfoEntry("Shift", this->Shift(), 0),
                     PrintInfoEntry("Eigen pair degree", this->evDegree, 0),
                     PrintInfoEntry("Goerisch Lagrange degree", lagrange_degree, 0),
                     PrintInfoEntry("Goerisch Lagrange size", lagrange_size, 0)
      );
    }
  };

  template<bool VERIFIED, bool adjoint>
  class Lagrange7DivergenceHomotopyStokesAssemble
      : public LagrangeDivergenceHomotopyStokesAssemble<VERIFIED, adjoint> {
  public:
    Lagrange7DivergenceHomotopyStokesAssemble(NavierStokesProblem &problem, double shift)
      : LagrangeDivergenceHomotopyStokesAssemble<VERIFIED, adjoint>(problem, shift, 7) {}

    const char *Name() const override {
      return "Navier-Stokes divergence homotopy assemble 'Lagrange' (minimize all)";
    }

    void AssembleGoerischSystem(const cell &c, Matrix &systemMatrix, Goerischfcts &w,
                                double shift, const Eigenpairs &ep, double t) const override {
      const Cell &C = *c;
      double tt = t / (1.0 - t);
      double TT = tt / this->problem.Sigma();
      double d = this->midGamma_1 + shift;
      double S = d - 1.0;
      double s = this->problem.Sigma() * S;
      if (!this->problem.CellOutsideApproximationDomain(C)) {
        S -= this->midGamma_2 / this->problem.Sigma();
        s -= this->midGamma_2;
      }

      ScalarElement E(w, C);
      VectorFieldElement E_ep(ep(0), E);
      RowValuesVector rowValues(w, E);
      RowEntries rowEntries(systemMatrix, E);
      for (int q = 0; q < E.nQ(); ++q) {
        //assembling rhs
        for (int n = 0; n < w.size(); ++n) {
          Tensor W1 = E_ep.VectorGradient(q, ep(n)) / d;
          VectorField V = E_ep.VectorValue(q, ep(n));
          for (int i = 0; i < E.size(); ++i) {
            rowValues[n](i, 0) += E.QWeight(q) * s * V[0] * E.Value(q, i);
            rowValues[n](i, 1) += E.QWeight(q) * s * V[1] * E.Value(q, i);
            rowValues[n](i, 2) -= E.QWeight(q) * tt * (V * E.Derivative(q, i));
            rowValues[n](i, 3) -= E.QWeight(q) * (W1[0][0] * E.Value(q, i) + V[0] * E.Derivative(q, i)[0]);
            rowValues[n](i, 4) -= E.QWeight(q) * (W1[0][1] * E.Value(q, i) + V[0] * E.Derivative(q, i)[1]);
            rowValues[n](i, 5) -= E.QWeight(q) * (W1[1][0] * E.Value(q, i) + V[1] * E.Derivative(q, i)[0]);
            rowValues[n](i, 6) -= E.QWeight(q) * (W1[1][1] * E.Value(q, i) + V[1] * E.Derivative(q, i)[1]);
          }
        }
        //assembling system matrix
        for (int i = 0; i < E.size(); ++i) {
          for (int j = 0; j < E.size(); ++j) {
            double v_iv_j = E.Value(q, i) * E.Value(q, j);
            double v_iDv_j_0 = E.Value(q, i) * E.Derivative(q, j)[0];
            double v_iDv_j_1 = E.Value(q, i) * E.Derivative(q, j)[1];
            double Dv_i_0v_j = E.Derivative(q, i)[0] * E.Value(q, j);
            double Dv_i_1v_j = E.Derivative(q, i)[1] * E.Value(q, j);

            double Dv_iDv_j = E.Derivative(q, i) * E.Derivative(q, j);
            double Dv_i_0Dv_j_0 = E.Derivative(q, i)[0] * E.Derivative(q, j)[0];
            double Dv_i_0Dv_j_1 = E.Derivative(q, i)[0] * E.Derivative(q, j)[1];
            double Dv_i_1Dv_j_0 = E.Derivative(q, i)[1] * E.Derivative(q, j)[0];
            double Dv_i_1Dv_j_1 = E.Derivative(q, i)[1] * E.Derivative(q, j)[1];

            double WSsv_iv_j = E.QWeight(q) * (S + 1.0) * s * v_iv_j;
            double WStt = E.QWeight(q) * S * tt;
            double WS = E.QWeight(q) * S;
            double Wtt = E.QWeight(q) * tt;
            double WTT = E.QWeight(q) * TT;
            double Wsigma = E.QWeight(q) / this->problem.Sigma();
            double sigma = this->problem.Sigma();

            rowEntries(i, j, 0, 0) += WSsv_iv_j;
            rowEntries(i, j, 0, 2) -= WStt * v_iDv_j_0;
            rowEntries(i, j, 0, 3) -= WS * v_iDv_j_0;
            rowEntries(i, j, 0, 4) -= WS * v_iDv_j_1;

            rowEntries(i, j, 1, 1) += WSsv_iv_j;
            rowEntries(i, j, 1, 2) -= WStt * v_iDv_j_1;
            rowEntries(i, j, 1, 5) -= WS * v_iDv_j_0;
            rowEntries(i, j, 1, 6) -= WS * v_iDv_j_1;

            rowEntries(i, j, 2, 0) -= WStt * Dv_i_0v_j;
            rowEntries(i, j, 2, 1) -= WStt * Dv_i_1v_j;
            rowEntries(i, j, 2, 2) += Wtt * (v_iv_j + TT * Dv_iDv_j);
            rowEntries(i, j, 2, 3) += WTT * Dv_i_0Dv_j_0;
            rowEntries(i, j, 2, 4) += WTT * Dv_i_0Dv_j_1;
            rowEntries(i, j, 2, 5) += WTT * Dv_i_1Dv_j_0;
            rowEntries(i, j, 2, 6) += WTT * Dv_i_1Dv_j_1;

            rowEntries(i, j, 3, 0) -= WS * Dv_i_0v_j;
            rowEntries(i, j, 3, 2) += WTT * Dv_i_0Dv_j_0;
            rowEntries(i, j, 3, 3) += E.QWeight(q) * (v_iv_j / d + Dv_i_0Dv_j_0 / sigma);
            rowEntries(i, j, 3, 4) += Wsigma * Dv_i_0Dv_j_1;

            rowEntries(i, j, 4, 0) -= WS * Dv_i_1v_j;
            rowEntries(i, j, 4, 2) += WTT * Dv_i_1Dv_j_0;
            rowEntries(i, j, 4, 3) += Wsigma * Dv_i_1Dv_j_0;
            rowEntries(i, j, 4, 4) += E.QWeight(q) * (v_iv_j / d + Dv_i_1Dv_j_1 / sigma);

            rowEntries(i, j, 5, 1) -= WS * Dv_i_0v_j;
            rowEntries(i, j, 5, 2) += WTT * Dv_i_0Dv_j_1;
            rowEntries(i, j, 5, 5) += E.QWeight(q) * (v_iv_j / d + Dv_i_0Dv_j_0 / sigma);
            rowEntries(i, j, 5, 6) += Wsigma * Dv_i_0Dv_j_1;

            rowEntries(i, j, 6, 1) -= WS * Dv_i_1v_j;
            rowEntries(i, j, 6, 2) += WTT * Dv_i_1Dv_j_1;
            rowEntries(i, j, 6, 5) += Wsigma * Dv_i_1Dv_j_0;
            rowEntries(i, j, 6, 6) += E.QWeight(q) * (v_iv_j / d + Dv_i_1Dv_j_1 / sigma);
          }
        }
      }
    }

    void MatrixentriesGoerisch(const cell &c,
                               const Eigenpair &EP_i, const Eigenpair &EP_j,
                               const Goerischfct &W_i, const Goerischfct &W_j,
                               IAEigenvalueType<VERIFIED> &g,
                               const double &shift, const double &t) override {
      using T = IAEigenvalueType<VERIFIED>;

      const Cell &C = *c;
      T tt = t / (T(1.0) - t);
      T d = this->Gamma1() + shift;
      T s = this->problem.Sigma() * (d - T(1.0));
      if (!this->problem.CellOutsideApproximationDomain(C)) s -= this->Gamma2();

      ScalarElementT<T> E(W_i, C);
      VectorFieldElementT<T> E_ep(EP_i, E);

      for (int q = 0; q < E.nQ(); ++q) {
        TensorT<T> T_i(E.Value(q, W_i, 3), E.Value(q, W_i, 4),
                       E.Value(q, W_i, 5), E.Value(q, W_i, 6));
        T_i += E_ep.VectorGradient(q, EP_i.getEigenfct());
        TensorT<T> T_j(E.Value(q, W_j, 3), E.Value(q, W_j, 4),
                       E.Value(q, W_j, 5), E.Value(q, W_j, 6));
        T_j += E_ep.VectorGradient(q, EP_j.getEigenfct());
        VectorFieldT<T> W2_i(E.Value(q, W_i, 0), E.Value(q, W_i, 1));
        VectorFieldT<T> W2_j(E.Value(q, W_j, 0), E.Value(q, W_j, 1));
        T W3_i = E.Value(q, W_i, 2);
        T W3_j = E.Value(q, W_j, 2);
        VectorFieldT<T> W4_i(E.Derivative(q, W_i, 3)[0] + E.Derivative(q, W_i, 4)[1],
                             E.Derivative(q, W_i, 5)[0] + E.Derivative(q, W_i, 6)[1]);
        W4_i += this->problem.Sigma() * E_ep.VectorValue(q, EP_i.getEigenfct())
                - s * W2_i + tt * E.Derivative(q, W_i, 2);
        VectorFieldT<T> W4_j(E.Derivative(q, W_j, 3)[0] + E.Derivative(q, W_j, 4)[1],
                             E.Derivative(q, W_j, 5)[0] + E.Derivative(q, W_j, 6)[1]);
        W4_j += this->problem.Sigma() * E_ep.VectorValue(q, EP_j.getEigenfct())
                - s * W2_j + tt * E.Derivative(q, W_j, 2);

        g += E.QWeight(q) * (Frobenius(T_i, T_j) / d + s * (W2_i * W2_j) + tt * (W3_i * W3_j)
                             + (W4_i * W4_j) / this->problem.Sigma());
      }
    }
  };

  template<bool VERIFIED, bool adjoint>
  class Lagrange5DivergenceHomotopyStokesAssemble
      : public LagrangeDivergenceHomotopyStokesAssemble<VERIFIED, adjoint> {
  public:
    Lagrange5DivergenceHomotopyStokesAssemble(NavierStokesProblem &problem, double shift)
    : LagrangeDivergenceHomotopyStokesAssemble<VERIFIED, adjoint>(problem, shift, 5) {}

    const char *Name() const override {
      return "Navier-Stokes divergence homotopy assemble 'Lagrange'";
    }

    void AssembleGoerischSystem(const cell &c, Matrix &systemMatrix, Goerischfcts &w,
                                double shift, const Eigenpairs &ep, double t) const override {
      const Cell &C = *c;
      double tt = t / (1.0 - t);
      double TT = tt / this->problem.Sigma();
      double d = this->midGamma_1 + shift;
      double s = this->problem.Sigma() * (d - 1.0);
      if (!this->problem.CellOutsideApproximationDomain(*c)) {
        s -= this->midGamma_2;
      }

      ScalarElement E(w, C);
      VectorFieldElement E_ep(ep(0), E);
      RowValuesVector rowValues(w, E);
      RowEntries rowEntries(systemMatrix, E);
      for (int q = 0; q < E.nQ(); ++q) {
        //assembling rhs
        for (int n = 0; n < w.size(); ++n) {
          Tensor W1 = E_ep.VectorGradient(q, ep(n)) / d;
          VectorField V = E_ep.VectorValue(q, ep(n));
          VectorField W4 = ((1.0 - s / (ep[n] + shift) / this->problem.Sigma()) * V);

          for (int i = 0; i < E.size(); ++i) {
            rowValues[n](i, 0) -= E.QWeight(q) * tt * (W4 * E.Derivative(q, i));
            rowValues[n](i, 1) -= E.QWeight(q) * (W1[0][0] * E.Value(q, i) + W4[0] * E.Derivative(q, i)[0]);
            rowValues[n](i, 2) -= E.QWeight(q) * (W1[0][1] * E.Value(q, i) + W4[0] * E.Derivative(q, i)[1]);
            rowValues[n](i, 3) -= E.QWeight(q) * (W1[1][0] * E.Value(q, i) + W4[1] * E.Derivative(q, i)[0]);
            rowValues[n](i, 4) -= E.QWeight(q) * (W1[1][1] * E.Value(q, i) + W4[1] * E.Derivative(q, i)[1]);
          }
        }
        //assembling system matrix
        for (int i = 0; i < E.size(); ++i) {
          for (int j = 0; j < E.size(); ++j) {
            double v_iv_j = E.Value(q, i) * E.Value(q, j);

            double Dv_iDv_j = E.Derivative(q, i) * E.Derivative(q, j);
            double Dv_i_0Dv_j_0 = E.Derivative(q, i)[0] * E.Derivative(q, j)[0];
            double Dv_i_0Dv_j_1 = E.Derivative(q, i)[0] * E.Derivative(q, j)[1];
            double Dv_i_1Dv_j_0 = E.Derivative(q, i)[1] * E.Derivative(q, j)[0];
            double Dv_i_1Dv_j_1 = E.Derivative(q, i)[1] * E.Derivative(q, j)[1];

            double Wtt = E.QWeight(q) * tt;
            double WTT = E.QWeight(q) * TT;
            double Wsigma = E.QWeight(q) / this->problem.Sigma();
            double sigma = this->problem.Sigma();

            rowEntries(i, j, 0, 0) += Wtt * (v_iv_j + TT * Dv_iDv_j);
            rowEntries(i, j, 0, 1) += WTT * Dv_i_0Dv_j_0;
            rowEntries(i, j, 0, 2) += WTT * Dv_i_0Dv_j_1;
            rowEntries(i, j, 0, 3) += WTT * Dv_i_1Dv_j_0;
            rowEntries(i, j, 0, 4) += WTT * Dv_i_1Dv_j_1;

            rowEntries(i, j, 1, 0) += WTT * Dv_i_0Dv_j_0;
            rowEntries(i, j, 1, 1) += E.QWeight(q) * (v_iv_j / d + Dv_i_0Dv_j_0 / sigma);
            rowEntries(i, j, 1, 2) += Wsigma * Dv_i_0Dv_j_1;

            rowEntries(i, j, 2, 0) += WTT * Dv_i_1Dv_j_0;
            rowEntries(i, j, 2, 1) += Wsigma * Dv_i_1Dv_j_0;
            rowEntries(i, j, 2, 2) += E.QWeight(q) * (v_iv_j / d + Dv_i_1Dv_j_1 / sigma);

            rowEntries(i, j, 3, 0) += WTT * Dv_i_0Dv_j_1;
            rowEntries(i, j, 3, 3) += E.QWeight(q) * (v_iv_j / d + Dv_i_0Dv_j_0 / sigma);
            rowEntries(i, j, 3, 4) += Wsigma * Dv_i_0Dv_j_1;

            rowEntries(i, j, 4, 0) += WTT * Dv_i_1Dv_j_1;
            rowEntries(i, j, 4, 3) += Wsigma * Dv_i_1Dv_j_0;
            rowEntries(i, j, 4, 4) += E.QWeight(q) * (v_iv_j / d + Dv_i_1Dv_j_1 / sigma);
          }
        }
      }
    }

    void MatrixentriesGoerisch(const cell &c,
                               const Eigenpair &EP_i, const Eigenpair &EP_j,
                               const Goerischfct &W_i, const Goerischfct &W_j,
                               IAEigenvalueType<VERIFIED> &g,
                               const double &shift, const double &t) override {
      using T = IAEigenvalueType<VERIFIED>;

      const Cell &C = *c;
      T tt = t / (T(1.0) - t);
      T d = this->Gamma1() + shift;
      T s = this->problem.Sigma() * (d - T(1.0));
      if (!this->problem.CellOutsideApproximationDomain(C)) s -= this->Gamma2();

      ScalarElementT<T> E(W_i, C);
      VectorFieldElementT<T> E_ep(EP_i, E);

      for (int q = 0; q < E.nQ(); ++q) {
        TensorT<T> T_i(E.Value(q, W_i, 1), E.Value(q, W_i, 2),
                       E.Value(q, W_i, 3), E.Value(q, W_i, 4));
        T_i += E_ep.VectorGradient(q, EP_i.getEigenfct());
        TensorT<T> T_j(E.Value(q, W_j, 1), E.Value(q, W_j, 2),
                       E.Value(q, W_j, 3), E.Value(q, W_j, 4));
        T_j += E_ep.VectorGradient(q, EP_j.getEigenfct());
        VectorFieldT<T> V_i = E_ep.VectorValue(q, EP_i.getEigenfct());
        VectorFieldT<T> V_j = E_ep.VectorValue(q, EP_j.getEigenfct());
        VectorFieldT<T> W2_i = V_i / (EP_i.getEigenvalue() + shift);
        VectorFieldT<T> W2_j = V_j / (EP_j.getEigenvalue() + shift);
        T W3_i = E.Value(q, W_i, 0);
        T W3_j = E.Value(q, W_j, 0);
        VectorFieldT<T> W4_i(E.Derivative(q, W_i, 1)[0] + E.Derivative(q, W_i, 2)[1],
                             E.Derivative(q, W_i, 3)[0] + E.Derivative(q, W_i, 4)[1]);
        W4_i += this->problem.Sigma() * V_i - s * W2_i + tt * E.Derivative(q, W_i, 0);
        VectorFieldT<T> W4_j(E.Derivative(q, W_j, 1)[0] + E.Derivative(q, W_j, 2)[1],
                             E.Derivative(q, W_j, 3)[0] + E.Derivative(q, W_j, 4)[1]);
        W4_j += this->problem.Sigma() * V_j - s * W2_j + tt * E.Derivative(q, W_j, 0);

        g += E.QWeight(q) * (Frobenius(T_i, T_j) / d + s * (W2_i * W2_j) + tt * (W3_i * W3_j)
                             + (W4_i * W4_j) / this->problem.Sigma());
      }
    }
  };

//----------------------------------------------------------------------------------------
// RTLagrangeDivergenceHomotopyStokesAssemble
//----------------------------------------------------------------------------------------
  template<bool VERIFIED, bool adjoint>
  class RTLagrangeDivergenceHomotopyStokesAssemble
      : public DivergenceHomotopyStokesAssemble<VERIFIED, adjoint> {
    int rt_order = 1;
    int lagrange_degree = 2;
    int lagrange_size;
  public:
    RTLagrangeDivergenceHomotopyStokesAssemble(NavierStokesProblem &problem, double shift, int lagrange_size)
        : DivergenceHomotopyStokesAssemble<VERIFIED, adjoint>(problem, shift), lagrange_size(lagrange_size) {
      Config::Get(this->prefix + "GoerischRTOrder", rt_order);
      Config::Get(this->prefix + "GoerischLagrangeDegree", lagrange_degree);
    }

    std::unique_ptr<IDiscretization>
    CreateEVDisc(const Meshes &meshes) const override {
      return std::make_unique<LagrangeDiscretization>(meshes, this->evDegree, 2);
    }

    std::unique_ptr<IAIDiscretization>
    CreateIAEVDisc(const Meshes &meshes) const override {
      return std::make_unique<IALagrangeDiscretization>(meshes, this->evDegree, 2);
    }

    std::unique_ptr<IDiscretization>
    CreateGoerischDisc(const IDiscretization &EVdisc) const override {
      return std::make_unique<NavierStokesDiscDivergenceHomotopy>(
          dynamic_cast<const NonAdaptiveIDiscretization &>(EVdisc), rt_order, lagrange_degree, lagrange_size);
    }

    std::unique_ptr<IAIDiscretization>
    CreateIAGoerischDisc(const IAIDiscretization &iaEVdisc) const override {
      return std::make_unique<IANavierStokesDiscDivergenceHomotopy>(
          dynamic_cast<const IANonAdaptiveIDiscretization &>(iaEVdisc), rt_order, lagrange_degree, lagrange_size);
    }

    void BoundaryConditionsGoerisch(const cell &c, Vector &w) const override {}

    virtual void PrintInfo() const override {
      mout.PrintInfo("Assemble", this->verbose,
                     PrintInfoEntry("Name", this->Name(), 0),
                     PrintInfoEntry("Shift", this->Shift(), 0),
                     PrintInfoEntry("Eigen pair degree", this->evDegree, 0),
                     PrintInfoEntry("Goerisch RT order", rt_order, 0),
                     PrintInfoEntry("Goerisch Lagrange degree", lagrange_degree, 0),
                     PrintInfoEntry("Goerisch Lagrange size", lagrange_size, 0)
      );
    }
  };

  template<bool VERIFIED, bool adjoint>
  class RTLagrange3DivergenceHomotopyStokesAssemble
      : public RTLagrangeDivergenceHomotopyStokesAssemble<VERIFIED, adjoint> {
  public:
    RTLagrange3DivergenceHomotopyStokesAssemble(NavierStokesProblem &problem, double shift)
      : RTLagrangeDivergenceHomotopyStokesAssemble<VERIFIED, adjoint>(problem, shift, 3) {}

    const char *Name() const override {
      return "Navier-Stokes divergence homotopy assemble 'RTLagrange' (minimize all)";
    }

    void AssembleGoerischSystem(const cell &c, Matrix &systemMatrix, Goerischfcts &w,
                                double shift, const Eigenpairs &ep, double t) const override {
      const Cell &C = *c;
      double sigma = this->problem.Sigma();
      double tt = t / (1.0 - t);
      double TT = tt / sigma;
      double d = this->midGamma_1 + shift;
      double S = d - 1.0;
      double s = this->problem.Sigma() * S;
      if (!this->problem.CellOutsideApproximationDomain(C)) {
        S -= this->midGamma_2 / sigma;
        s -= this->midGamma_2;
      }

      NavierStokesElementDivergenceHomotopy E(w, C);
      VectorFieldElement E_ep(ep(0), E);
      MixedRowValuesVector rowValues(w, C, E);
      MixedRowEntries rowEntries(systemMatrix, C, E);
      for (int q = 0; q < E.nQ(); ++q) {
        //assembling rhs
        for (int n = 0; n < w.size(); ++n) {
          Tensor W1 = E_ep.VectorGradient(q, ep(n)) / d;
          VectorField V = E_ep.VectorValue(q, ep(n));
          VectorField F = (E.QWeight(q) * s) * V;

          for (int i = 0; i < E.RTSize(); ++i) {
            for (int k = 0; k < E.RTMaxk(i); ++k) {
              rowValues[n](E.RTIndex(), i, E.IndexingRT_k(i, k, 0)) -= E.QWeight(q) * (W1[0] * E.RTVectorValue(q, i, k) + V[0] * E.RTDivergence(q, i, k));
              rowValues[n](E.RTIndex(), i, E.IndexingRT_k(i, k, 1)) -= E.QWeight(q) * (W1[1] * E.RTVectorValue(q, i, k) + V[1] * E.RTDivergence(q, i, k));
            }
          }
          for (int i = 0; i < E.LagrangeSize(); ++i) {
            rowValues[n](E.LagrangeIndex(), i, 0) -= E.QWeight(q) * tt * (V * E.LagrangeDerivative(q, i));
            rowValues[n](E.LagrangeIndex(), i, 1) += F[0] * E.LagrangeValue(q, i);
            rowValues[n](E.LagrangeIndex(), i, 2) += F[1] * E.LagrangeValue(q, i);
          }
        }
        //assembling system matrix
        for (int i = 0; i < E.RTSize(); ++i)
          for (int j = 0; j < E.RTSize(); ++j)
            for (int k = 0; k < E.RTMaxk(i); ++k)
              for (int l = 0; l < E.RTMaxk(j); ++l) {
                double tmp = E.QWeight(q) * ((E.RTVectorValue(q, i, k) * E.RTVectorValue(q, j, l)) / d + E.RTDivergence(q, i, k) * E.RTDivergence(q, j, l) / sigma);
                rowEntries(E.RTIndex(), E.RTIndex(), i, j, E.IndexingRT_k(i, k, 0), E.IndexingRT_k(j, l, 0)) += tmp;
                rowEntries(E.RTIndex(), E.RTIndex(), i, j, E.IndexingRT_k(i, k, 1), E.IndexingRT_k(j, l, 1)) += tmp;
              }
        for (int i = 0; i < E.RTSize(); ++i)
          for (int k = 0; k < E.RTMaxk(i); ++k)
            for (int j = 0; j < E.LagrangeSize(); ++j) {
              VectorField tmp1 = (E.QWeight(q) * TT * E.RTDivergence(q, i, k)) * E.LagrangeDerivative(q, j);
              double tmp2 = E.QWeight(q) * S * E.LagrangeValue(q, j) * E.RTDivergence(q, i, k);
              rowEntries(E.RTIndex(), E.LagrangeIndex(), i, j, E.IndexingRT_k(i, k, 0), 0) += tmp1[0];
              rowEntries(E.RTIndex(), E.LagrangeIndex(), i, j, E.IndexingRT_k(i, k, 0), 1) -= tmp2;
              rowEntries(E.RTIndex(), E.LagrangeIndex(), i, j, E.IndexingRT_k(i, k, 1), 0) += tmp1[1];
              rowEntries(E.RTIndex(), E.LagrangeIndex(), i, j, E.IndexingRT_k(i, k, 1), 2) -= tmp2;

              rowEntries(E.LagrangeIndex(), E.RTIndex(), j, i, 0, E.IndexingRT_k(i, k, 0)) += tmp1[0];
              rowEntries(E.LagrangeIndex(), E.RTIndex(), j, i, 0, E.IndexingRT_k(i, k, 1)) += tmp1[1];
              rowEntries(E.LagrangeIndex(), E.RTIndex(), j, i, 1, E.IndexingRT_k(i, k, 0)) -= tmp2;
              rowEntries(E.LagrangeIndex(), E.RTIndex(), j, i, 2, E.IndexingRT_k(i, k, 1)) -= tmp2;
            }
        for (int i = 0; i < E.LagrangeSize(); ++i)
          for (int j = 0; j < E.LagrangeSize(); ++j) {
            VectorField tmp1 = (E.QWeight(q) * TT * s * E.LagrangeValue(q, j)) * E.LagrangeDerivative(q, i);
            double tmp2 = E.LagrangeValue(q, i) * E.LagrangeValue(q, j);
            rowEntries(E.LagrangeIndex(), E.LagrangeIndex(), i, j, 0, 0) += E.QWeight(q) * tt * (tmp2 + TT * (E.LagrangeDerivative(q, i) * E.LagrangeDerivative(q, j)));
            rowEntries(E.LagrangeIndex(), E.LagrangeIndex(), i, j, 0, 1) -= tmp1[0];
            rowEntries(E.LagrangeIndex(), E.LagrangeIndex(), i, j, 0, 2) -= tmp1[1];

            tmp2 *= E.QWeight(q) * s * (S + 1.0);

            rowEntries(E.LagrangeIndex(), E.LagrangeIndex(), j, i, 1, 0) -= tmp1[0];
            rowEntries(E.LagrangeIndex(), E.LagrangeIndex(), i, j, 1, 1) += tmp2;

            rowEntries(E.LagrangeIndex(), E.LagrangeIndex(), j, i, 2, 0) -= tmp1[1];
            rowEntries(E.LagrangeIndex(), E.LagrangeIndex(), i, j, 2, 2) += tmp2;
          }
      }
    }

    void MatrixentriesGoerisch(const cell &c,
                               const Eigenpair &EP_i, const Eigenpair &EP_j,
                               const Goerischfct &W_i, const Goerischfct &W_j,
                               IAEigenvalueType<VERIFIED> &g,
                               const double &shift, const double &t) override {
      using T = IAEigenvalueType<VERIFIED>;

      const Cell &C = *c;
      T tt = t / (T(1.0) - t);
      T d = this->Gamma1() + shift;
      T s = this->problem.Sigma() * (d - T(1.0));
      if (!this->problem.CellOutsideApproximationDomain(C)) s -= this->Gamma2();

      NavierStokesElementDivergenceHomotopyT<T> E(W_i, C);
      VectorFieldElementT<T> E_ep(EP_i, E);
      for (int q = 0; q < E.nQ(); ++q) {
        TensorT<T> T_i = E.W1(q, W_i);
        T_i += E_ep.VectorGradient(q, EP_i.getEigenfct());
        TensorT<T> T_j = E.W1(q, W_j);
        T_j += E_ep.VectorGradient(q, EP_j.getEigenfct());
        VectorFieldT<T> W2_i = E.W2(q, W_i);
        VectorFieldT<T> W2_j = E.W2(q, W_j);
        T W3_i = E.W3(q, W_i);
        T W3_j = E.W3(q, W_j);
        VectorFieldT<T> W4_i = E.DivW1(q, W_i);
        W4_i += this->problem.Sigma() * E_ep.VectorValue(q, EP_i.getEigenfct())
                - s * W2_i + tt * E.GradW3(q, W_i);
        VectorFieldT<T> W4_j = E.DivW1(q, W_j);
        W4_j += this->problem.Sigma() * E_ep.VectorValue(q, EP_j.getEigenfct())
                - s * W2_j + tt * E.GradW3(q, W_j);

        g += E.QWeight(q) * (Frobenius(T_i, T_j) / d + s * (W2_i * W2_j) + tt * (W3_i * W3_j)
                             + (W4_i * W4_j) / this->problem.Sigma());
      }
    }
  };

  template<bool VERIFIED, bool adjoint>
  class RTLagrange1DivergenceHomotopyStokesAssemble
      : public RTLagrangeDivergenceHomotopyStokesAssemble<VERIFIED, adjoint> {
  public:
    RTLagrange1DivergenceHomotopyStokesAssemble(NavierStokesProblem &problem, double shift)
        : RTLagrangeDivergenceHomotopyStokesAssemble<VERIFIED, adjoint>(problem, shift, 1) {}

    const char *Name() const override {
      return "Navier-Stokes divergence homotopy assemble 'RTLagrange'";
    }

    void AssembleGoerischSystem(const cell &c, Matrix &systemMatrix, Goerischfcts &w,
                                double shift, const Eigenpairs &ep, double t) const override {
      const Cell &C = *c;
      double sigma = this->problem.Sigma();
      double tt = t / (1.0 - t);
      double TT = tt / sigma;
      double d = this->midGamma_1 + shift;
      double S = d - 1.0;
      double s = this->problem.Sigma() * S;
      if (!this->problem.CellOutsideApproximationDomain(C)) {
        S -= this->midGamma_2 / sigma;
        s -= this->midGamma_2;
      }

      NavierStokesElementDivergenceHomotopy E(w, C);
      VectorFieldElement E_ep(ep(0), E);
      MixedRowValuesVector rowValues(w, C, E);
      MixedRowEntries rowEntries(systemMatrix, C, E);
      for (int q = 0; q < E.nQ(); ++q) {
        //assembling rhs
        for (int n = 0; n < w.size(); ++n) {
          Tensor W1 = E_ep.VectorGradient(q, ep(n)) / d;
          VectorField W4 = (1.0 - s / (ep[n] + shift) / this->problem.Sigma()) * E_ep.VectorValue(q, ep(n));

          for (int i = 0; i < E.RTSize(); ++i) {
            for (int k = 0; k < E.RTMaxk(i); ++k) {
              rowValues[n](E.RTIndex(), i, E.IndexingRT_k(i, k, 0)) -= E.QWeight(q) * (W1[0] * E.RTVectorValue(q, i, k) + W4[0] * E.RTDivergence(q, i, k));
              rowValues[n](E.RTIndex(), i, E.IndexingRT_k(i, k, 1)) -= E.QWeight(q) * (W1[1] * E.RTVectorValue(q, i, k) + W4[1] * E.RTDivergence(q, i, k));
            }
          }
          for (int i = 0; i < E.LagrangeSize(); ++i) {
            rowValues[n](E.LagrangeIndex(), i, 0) -= E.QWeight(q) * tt * (W4 * E.LagrangeDerivative(q, i));
          }
        }
        //assembling system matrix
        for (int i = 0; i < E.RTSize(); ++i)
          for (int j = 0; j < E.RTSize(); ++j)
            for (int k = 0; k < E.RTMaxk(i); ++k)
              for (int l = 0; l < E.RTMaxk(j); ++l) {
                double tmp = E.QWeight(q) * ((E.RTVectorValue(q, i, k) * E.RTVectorValue(q, j, l)) / d + E.RTDivergence(q, i, k) * E.RTDivergence(q, j, l) / sigma);
                rowEntries(E.RTIndex(), E.RTIndex(), i, j, E.IndexingRT_k(i, k, 0), E.IndexingRT_k(j, l, 0)) += tmp;
                rowEntries(E.RTIndex(), E.RTIndex(), i, j, E.IndexingRT_k(i, k, 1), E.IndexingRT_k(j, l, 1)) += tmp;
              }
        for (int i = 0; i < E.RTSize(); ++i)
          for (int k = 0; k < E.RTMaxk(i); ++k)
            for (int j = 0; j < E.LagrangeSize(); ++j) {
              VectorField tmp = (E.QWeight(q) * TT * E.RTDivergence(q, i, k)) * E.LagrangeDerivative(q, j);
              rowEntries(E.RTIndex(), E.LagrangeIndex(), i, j, E.IndexingRT_k(i, k, 0), 0) += tmp[0];
              rowEntries(E.RTIndex(), E.LagrangeIndex(), i, j, E.IndexingRT_k(i, k, 1), 0) += tmp[1];

              rowEntries(E.LagrangeIndex(), E.RTIndex(), j, i, 0, E.IndexingRT_k(i, k, 0)) += tmp[0];
              rowEntries(E.LagrangeIndex(), E.RTIndex(), j, i, 0, E.IndexingRT_k(i, k, 1)) += tmp[1];

            }
        for (int i = 0; i < E.LagrangeSize(); ++i)
          for (int j = 0; j < E.LagrangeSize(); ++j) {
            rowEntries(E.LagrangeIndex(), E.LagrangeIndex(), i, j, 0, 0)
                += E.QWeight(q) * tt * (E.LagrangeValue(q, i) * E.LagrangeValue(q, j)
                                        + TT * (E.LagrangeDerivative(q, i) * E.LagrangeDerivative(q, j)));
          }
      }
    }

    void MatrixentriesGoerisch(const cell &c,
                               const Eigenpair &EP_i, const Eigenpair &EP_j,
                               const Goerischfct &W_i, const Goerischfct &W_j,
                               IAEigenvalueType<VERIFIED> &g,
                               const double &shift, const double &t) override {
      using T = IAEigenvalueType<VERIFIED>;

      const Cell &C = *c;
      T tt = t / (T(1.0) - t);
      T d = this->Gamma1() + shift;
      T s = this->problem.Sigma() * (d - T(1.0));
      if (!this->problem.CellOutsideApproximationDomain(C)) s -= this->Gamma2();

      NavierStokesElementDivergenceHomotopyT<T> E(W_i, C);
      VectorFieldElementT<T> E_ep(EP_i, E);
      for (int q = 0; q < E.nQ(); ++q) {
        TensorT<T> T_i = E.W1(q, W_i);
        T_i += E_ep.VectorGradient(q, EP_i.getEigenfct());
        TensorT<T> T_j = E.W1(q, W_j);
        T_j += E_ep.VectorGradient(q, EP_j.getEigenfct());

        VectorFieldT<T> V_i = E_ep.VectorValue(q, EP_i.getEigenfct());
        VectorFieldT<T> W2_i = V_i / (EP_i.getEigenvalue() + shift);
        VectorFieldT<T> V_j = E_ep.VectorValue(q, EP_j.getEigenfct());
        VectorFieldT<T> W2_j = V_j / (EP_j.getEigenvalue() + shift);
        T W3_i = E.W3(q, W_i);
        T W3_j = E.W3(q, W_j);
        VectorFieldT<T> W4_i = E.DivW1(q, W_i);
        W4_i += (this->problem.Sigma() - s / (EP_i.getEigenvalue() + shift)) * V_i + tt * E.GradW3(q, W_i);
        VectorFieldT<T> W4_j = E.DivW1(q, W_j);
        W4_j += (this->problem.Sigma() - s / (EP_j.getEigenvalue() + shift)) * V_j + tt * E.GradW3(q, W_j);

        g += E.QWeight(q) * (Frobenius(T_i, T_j) / d + s * (W2_i * W2_j) + tt * (W3_i * W3_j)
                             + (W4_i * W4_j) / this->problem.Sigma());
      }
    }
  };

  template<bool VERIFIED, bool adjoint>
  class RTLagrangeLiuDivergenceHomotopyStokesAssemble
      : public RTLagrangeDivergenceHomotopyStokesAssemble<VERIFIED, adjoint> {
  public:
    RTLagrangeLiuDivergenceHomotopyStokesAssemble(NavierStokesProblem &problem, double shift)
        : RTLagrangeDivergenceHomotopyStokesAssemble<VERIFIED, adjoint>(problem, shift, 1) {}

    const char *Name() const override {
      return "Navier-Stokes divergence homotopy assemble 'RTLagrange' (Liu)";
    }

    void AssembleGoerischSystem(const cell &c, Matrix &systemMatrix, Goerischfcts &w,
                                double shift, const Eigenpairs &ep, double t) const override {
      const Cell &C = *c;
      double tt = t / (1.0 - t);
      double d = this->midGamma_1 + shift;
      double s = this->problem.Sigma() * d;
      double S = d;
      if (!this->problem.CellOutsideApproximationDomain(C)) {
        s -= this->midGamma_2;
        S -= this->midGamma_2 / this->problem.Sigma();
      }
      double TT = tt / s;

      NavierStokesElementDivergenceHomotopy E(w, C);
      VectorFieldElement E_ep(ep(0), E);
      MixedRowValuesVector rowValues(w, C, E);
      MixedRowEntries rowEntries(systemMatrix, C, E);
      for (int q = 0; q < E.nQ(); ++q) {
        //assembling rhs
        for (int n = 0; n < w.size(); ++n) {
          Tensor W1 = E_ep.VectorGradient(q, ep(n)) / d;
          VectorField W2 = E_ep.VectorValue(q, ep(n)) / S;

          for (int i = 0; i < E.RTSize(); ++i) {
            for (int k = 0; k < E.RTMaxk(i); ++k) {
              rowValues[n](E.RTIndex(), i, E.IndexingRT_k(i, k, 0)) -= E.QWeight(q) * (W1[0] * E.RTVectorValue(q, i, k) + W2[0] * E.RTDivergence(q, i, k));
              rowValues[n](E.RTIndex(), i, E.IndexingRT_k(i, k, 1)) -= E.QWeight(q) * (W1[1] * E.RTVectorValue(q, i, k) + W2[1] * E.RTDivergence(q, i, k));
            }
          }
          for (int i = 0; i < E.LagrangeSize(); ++i) {
            rowValues[n](E.LagrangeIndex(), i, 0) -= E.QWeight(q) * tt * (W2 * E.LagrangeDerivative(q, i));
          }
        }
        //assembling system matrix
        for (int i = 0; i < E.RTSize(); ++i)
          for (int j = 0; j < E.RTSize(); ++j)
            for (int k = 0; k < E.RTMaxk(i); ++k)
              for (int l = 0; l < E.RTMaxk(j); ++l) {
                double tmp = E.QWeight(q) * ((E.RTVectorValue(q, i, k) * E.RTVectorValue(q, j, l)) / d + E.RTDivergence(q, i, k) * E.RTDivergence(q, j, l) / s);
                rowEntries(E.RTIndex(), E.RTIndex(), i, j, E.IndexingRT_k(i, k, 0), E.IndexingRT_k(j, l, 0)) += tmp;
                rowEntries(E.RTIndex(), E.RTIndex(), i, j, E.IndexingRT_k(i, k, 1), E.IndexingRT_k(j, l, 1)) += tmp;
              }
        for (int i = 0; i < E.RTSize(); ++i)
          for (int k = 0; k < E.RTMaxk(i); ++k)
            for (int j = 0; j < E.LagrangeSize(); ++j) {
              VectorField tmp = (E.QWeight(q) * TT * E.RTDivergence(q, i, k)) * E.LagrangeDerivative(q, j);
              rowEntries(E.RTIndex(), E.LagrangeIndex(), i, j, E.IndexingRT_k(i, k, 0), 0) += tmp[0];
              rowEntries(E.RTIndex(), E.LagrangeIndex(), i, j, E.IndexingRT_k(i, k, 1), 0) += tmp[1];

              rowEntries(E.LagrangeIndex(), E.RTIndex(), j, i, 0, E.IndexingRT_k(i, k, 0)) += tmp[0];
              rowEntries(E.LagrangeIndex(), E.RTIndex(), j, i, 0, E.IndexingRT_k(i, k, 1)) += tmp[1];

            }
        for (int i = 0; i < E.LagrangeSize(); ++i)
          for (int j = 0; j < E.LagrangeSize(); ++j) {
            rowEntries(E.LagrangeIndex(), E.LagrangeIndex(), i, j, 0, 0)
                += E.QWeight(q) * tt * (E.LagrangeValue(q, i) * E.LagrangeValue(q, j)
                                        + TT * (E.LagrangeDerivative(q, i) * E.LagrangeDerivative(q, j)));
          }
      }
    }

    void MatrixentriesGoerisch(const cell &c,
                               const Eigenpair &EP_i, const Eigenpair &EP_j,
                               const Goerischfct &W_i, const Goerischfct &W_j,
                               IAEigenvalueType<VERIFIED> &g,
                               const double &shift, const double &t) override {
      using T = IAEigenvalueType<VERIFIED>;

      const Cell &C = *c;
      T tt = t / (T(1.0) - t);
      T d = this->Gamma1() + shift;
      T s = this->problem.Sigma() * d;
      if (!this->problem.CellOutsideApproximationDomain(C)) s -= this->Gamma2();

      NavierStokesElementDivergenceHomotopyT<T> E(W_i, C);
      VectorFieldElementT<T> E_ep(EP_i, E);
      for (int q = 0; q < E.nQ(); ++q) {
        TensorT<T> T_i = E.W1(q, W_i);
        T_i += E_ep.VectorGradient(q, EP_i.getEigenfct());
        TensorT<T> T_j = E.W1(q, W_j);
        T_j += E_ep.VectorGradient(q, EP_j.getEigenfct());
        T W3_i = E.W3(q, W_i);
        T W3_j = E.W3(q, W_j);

        VectorFieldT<T> V_i = E_ep.VectorValue(q, EP_i.getEigenfct());
        VectorFieldT<T> W2_i = E.DivW1(q, W_i) + tt * E.GradW3(q, W_i) + this->problem.Sigma() * V_i;
        VectorFieldT<T> V_j = E_ep.VectorValue(q, EP_j.getEigenfct());
        VectorFieldT<T> W2_j = E.DivW1(q, W_j) + tt * E.GradW3(q, W_j) + this->problem.Sigma() * V_j;

        g += E.QWeight(q) * (Frobenius(T_i, T_j) / d + tt * (W3_i * W3_j) + (W2_i * W2_j) / s);
      }
    }
  };

  template<bool VERIFIED, bool adjoint>
  std::unique_ptr<DivergenceHomotopyStokesAssemble<VERIFIED, adjoint>>
  createDivergenceHomotopyStokesAssemble(NavierStokesProblem &problem, double shift) {
    std::string minStrat = "RTLagrange_Liu";
    Config::Get("MinimizationStrategy", minStrat);
    if (minStrat == "RTLagrange")
      return std::make_unique<RTLagrange1DivergenceHomotopyStokesAssemble<VERIFIED, adjoint>>(problem, shift);
    if (minStrat == "RTLagrange_all")
      return std::make_unique<RTLagrange3DivergenceHomotopyStokesAssemble<VERIFIED, adjoint>>(problem, shift);
    if (minStrat == "Lagrange")
      return std::make_unique<Lagrange5DivergenceHomotopyStokesAssemble<VERIFIED, adjoint>>(problem, shift);
    if (minStrat == "Lagrange_all")
      return std::make_unique<Lagrange7DivergenceHomotopyStokesAssemble<VERIFIED, adjoint>>(problem, shift);
    if (minStrat == "RTLagrange_Liu")
      return std::make_unique<RTLagrangeLiuDivergenceHomotopyStokesAssemble<VERIFIED, adjoint>>(problem, shift);
    THROW("Minimization strategy " + minStrat + " for DivergenceHomotopyStokesAssemble not implemented!")
  }

  template<bool VERIFIED>
  class IndependentDivergenceHomotopyStokesAssemble : public IAEigenvalueAssemble<VERIFIED>,
                                                      public StokesBoundaryConditions {
  protected:
    double sigma;
    int evDegree = 3;
    int rt_order = 2;
    int lagrange_degree = 3;
    double compDomainR = infty;
  public:
    IndependentDivergenceHomotopyStokesAssemble(double _sigma, double compDomainR)
        : IAEigenvalueAssemble<VERIFIED>(0.0), sigma(_sigma), compDomainR(compDomainR) {
      Config::Get("IndependentDivergenceHomotopyEVDegree", evDegree);
      Config::Get("IndependentDivergenceHomotopyGoerischRTOrder", rt_order);
      Config::Get("IndependentDivergenceHomotopyGoerischLagrangeDegree", lagrange_degree);
    }

    bool CellOutsideApproximationDomain(const Cell &c) const {
      double x_max = c.Corner(0)[0];
      double x_min = c.Corner(0)[0];
      for (int i = 1; i < c.Corners(); ++i) {
        x_max = std::max(x_max, c.Corner(i)[0]);
        x_min = std::min(x_min, c.Corner(i)[0]);
      }
      if (x_min >= compDomainR || x_max <= -compDomainR)
        return true;
      return false;
    }

    const char *Name() const override {
      return "Independent Navier-Stokes divergence homotopy assemble 'RTLagrange' (Liu)";
    }

    void BoundaryConditionsEigenSolver(const cell &c, Eigenfct &u) const override {
      StokesBoundaryConditions::setDirichletBC(c, u);
    }

    void MatrixentriesEigenSolver(const cell &c, Matrix &A, Matrix &B,
                                  const double &shift, const double &t) override {
      const Cell &C = *c;
      int chi = 1;
      if (CellOutsideApproximationDomain(*c)) chi = 0;
      double tt = t / (1.0 - t);

      ScalarElement E(A, C);
      RowEntries A_c(A, E);
      RowEntries B_c(B, E);
      for (int q = 0; q < E.nQ(); ++q)
        for (int i = 0; i < E.size(); ++i)
          for (int j = 0; j < E.size(); ++j) {
            VectorField D_i = E.Derivative(q, i);
            VectorField D_j = E.Derivative(q, j);
            double L_ij = E.Value(q, i) * E.Value(q, j);
            double N_ij = D_i * D_j + sigma * L_ij;

            A_c(i, j, 0, 0) += E.QWeight(q) * (N_ij + tt * D_i[0] * D_j[0]);
            A_c(i, j, 0, 1) += E.QWeight(q) * tt * D_i[0] * D_j[1];
            A_c(i, j, 1, 0) += E.QWeight(q) * tt * D_i[1] * D_j[0];
            A_c(i, j, 1, 1) += E.QWeight(q) * (N_ij + tt * D_i[1] * D_j[1]);

            B_c(i, j, 0, 0) += (E.QWeight(q) * chi) * L_ij;
            B_c(i, j, 1, 1) += (E.QWeight(q) * chi) * L_ij;
          }
    }

    void MatrixentriesRayleighRitz(const cell &c,
                                   const Eigenfct &U_i, const Eigenfct &U_j,
                                   IAEigenvalueType<VERIFIED> &a, IAEigenvalueType<VERIFIED> &b,
                                   const double &shift, const double &t) override {
      using T = IAEigenvalueType<VERIFIED>;

      const Cell &C = *c;
      int chi = 1;
      if (CellOutsideApproximationDomain(C)) chi = 0;
      T tt = t / (T(1.0) - t);

      VectorFieldElementT<T> E(U_i, C);
      for (int q = 0; q < E.nQ(); ++q) {
        TensorT<T> D_i = E.VectorGradient(q, U_i);
        TensorT<T> D_j = E.VectorGradient(q, U_j);
        T L_ij = E.VectorValue(q, U_i) * E.VectorValue(q, U_j);
        T N_ij = Frobenius(D_i, D_j) + sigma * L_ij;

        a += E.QWeight(q) * (N_ij + tt * (D_i[0][0] + D_i[1][1]) * (D_j[0][0] + D_j[1][1]));
        b += (E.QWeight(q) * chi) * L_ij;
      }
    }

    std::unique_ptr<IDiscretization>
    CreateEVDisc(const Meshes &meshes) const {
      return std::make_unique<LagrangeDiscretization>(meshes, evDegree, 2);
    }

    std::unique_ptr<IAIDiscretization>
    CreateIAEVDisc(const Meshes &meshes) const {
      return std::make_unique<IALagrangeDiscretization>(meshes, evDegree, 2);
    }

    std::unique_ptr<IDiscretization>
    CreateGoerischDisc(const IDiscretization &EVdisc) const override {
      return std::make_unique<NavierStokesDiscDivergenceHomotopy>(
          dynamic_cast<const NonAdaptiveIDiscretization &>(EVdisc), rt_order, lagrange_degree);
    }

    std::unique_ptr<IAIDiscretization>
    CreateIAGoerischDisc(const IAIDiscretization &iaEVdisc) const override {
      return std::make_unique<IANavierStokesDiscDivergenceHomotopy>(
          dynamic_cast<const IANonAdaptiveIDiscretization &>(iaEVdisc), rt_order, lagrange_degree);
    }

    void BoundaryConditionsGoerisch(const cell &c, Vector &w) const override {}

    virtual void PrintInfo() const override {
      mout.PrintInfo("Assemble", this->verbose,
                     PrintInfoEntry("Name", this->Name(), 0),
                     PrintInfoEntry("Shift", this->Shift(), 0),
                     PrintInfoEntry("Eigen pair degree", this->evDegree, 0),
                     PrintInfoEntry("Goerisch RT order", rt_order, 0),
                     PrintInfoEntry("Goerisch Lagrange degree", lagrange_degree, 0)
      );
    }

    void AssembleGoerischSystem(const cell &c, Matrix &systemMatrix, Goerischfcts &w,
                                double shift, const Eigenpairs &ep, double t) const override {
      const Cell &C = *c;
      double tt = t / (1.0 - t);
      double TT = tt / sigma;
      double s = 1.0 / sigma;
      if (CellOutsideApproximationDomain(C))  s = 0.0;

      NavierStokesElementDivergenceHomotopy E(w, C);
      VectorFieldElement E_ep(ep(0), E);
      MixedRowValuesVector rowValues(w, C, E);
      MixedRowEntries rowEntries(systemMatrix, C, E);
      for (int q = 0; q < E.nQ(); ++q) {
        //assembling rhs
        for (int n = 0; n < w.size(); ++n) {
          VectorField W2 = E_ep.VectorValue(q, ep(n))  * s;

          for (int i = 0; i < E.RTSize(); ++i) {
            for (int k = 0; k < E.RTMaxk(i); ++k) {
              rowValues[n](E.RTIndex(), i, E.IndexingRT_k(i, k, 0)) -= E.QWeight(q) * (W2[0] * E.RTDivergence(q, i, k));
              rowValues[n](E.RTIndex(), i, E.IndexingRT_k(i, k, 1)) -= E.QWeight(q) * (W2[1] * E.RTDivergence(q, i, k));
            }
          }
          for (int i = 0; i < E.LagrangeSize(); ++i) {
            rowValues[n](E.LagrangeIndex(), i, 0) -= E.QWeight(q) * tt * (W2 * E.LagrangeDerivative(q, i));
          }
        }
        //assembling system matrix
        for (int i = 0; i < E.RTSize(); ++i)
          for (int j = 0; j < E.RTSize(); ++j)
            for (int k = 0; k < E.RTMaxk(i); ++k)
              for (int l = 0; l < E.RTMaxk(j); ++l) {
                double tmp = E.QWeight(q) * (E.RTVectorValue(q, i, k) * E.RTVectorValue(q, j, l) + E.RTDivergence(q, i, k) * E.RTDivergence(q, j, l) / sigma);
                rowEntries(E.RTIndex(), E.RTIndex(), i, j, E.IndexingRT_k(i, k, 0), E.IndexingRT_k(j, l, 0)) += tmp;
                rowEntries(E.RTIndex(), E.RTIndex(), i, j, E.IndexingRT_k(i, k, 1), E.IndexingRT_k(j, l, 1)) += tmp;
              }
        for (int i = 0; i < E.RTSize(); ++i)
          for (int k = 0; k < E.RTMaxk(i); ++k)
            for (int j = 0; j < E.LagrangeSize(); ++j) {
              VectorField tmp = (E.QWeight(q) * TT * E.RTDivergence(q, i, k)) * E.LagrangeDerivative(q, j);
              rowEntries(E.RTIndex(), E.LagrangeIndex(), i, j, E.IndexingRT_k(i, k, 0), 0) += tmp[0];
              rowEntries(E.RTIndex(), E.LagrangeIndex(), i, j, E.IndexingRT_k(i, k, 1), 0) += tmp[1];

              rowEntries(E.LagrangeIndex(), E.RTIndex(), j, i, 0, E.IndexingRT_k(i, k, 0)) += tmp[0];
              rowEntries(E.LagrangeIndex(), E.RTIndex(), j, i, 0, E.IndexingRT_k(i, k, 1)) += tmp[1];

            }
        for (int i = 0; i < E.LagrangeSize(); ++i)
          for (int j = 0; j < E.LagrangeSize(); ++j) {
            rowEntries(E.LagrangeIndex(), E.LagrangeIndex(), i, j, 0, 0)
                += E.QWeight(q) * tt * (E.LagrangeValue(q, i) * E.LagrangeValue(q, j)
                                        + TT * (E.LagrangeDerivative(q, i) * E.LagrangeDerivative(q, j)));
          }
      }
    }

    void MatrixentriesGoerisch(const cell &c,
                               const Eigenpair &EP_i, const Eigenpair &EP_j,
                               const Goerischfct &W_i, const Goerischfct &W_j,
                               IAEigenvalueType<VERIFIED> &g,
                               const double &shift, const double &t) override {
      using T = IAEigenvalueType<VERIFIED>;

      const Cell &C = *c;
      T tt = t / (T(1.0) - t);
      int chi = 1;
      if (CellOutsideApproximationDomain(C)) chi = 0;

      NavierStokesElementDivergenceHomotopyT<T> E(W_i, C);
      VectorFieldElementT<T> E_ep(EP_i, E);
      for (int q = 0; q < E.nQ(); ++q) {
        TensorT<T> T_i = E.W1(q, W_i);
        TensorT<T> T_j = E.W1(q, W_j);
        T W3_i = E.W3(q, W_i);
        T W3_j = E.W3(q, W_j);

        VectorFieldT<T> V_i = chi * E_ep.VectorValue(q, EP_i.getEigenfct());
        VectorFieldT<T> W2_i = E.DivW1(q, W_i) + tt * E.GradW3(q, W_i) + V_i;
        VectorFieldT<T> V_j = chi * E_ep.VectorValue(q, EP_j.getEigenfct());
        VectorFieldT<T> W2_j = E.DivW1(q, W_j) + tt * E.GradW3(q, W_j) + V_j;

        g += E.QWeight(q) * (Frobenius(T_i, T_j) + (W2_i * W2_j) / sigma + tt * (W3_i * W3_j));
      }
    }
  };


  template<bool VERIFIED>
  class IndependentDomainHomotopyStokesAssemble : public IAEigenvalueAssemble<VERIFIED>,
                                                  public StokesBoundaryConditions {
  protected:
    double sigma;
    int rt_order = 1;
    double compDomainR = infty;
  public:
    IndependentDomainHomotopyStokesAssemble(double _sigma, double compDomainR)
        : IAEigenvalueAssemble<VERIFIED>(0.0), sigma(_sigma), compDomainR(compDomainR) {
      Config::Get("IndependentDomainHomotopyGoerischRTOrder", rt_order);
    }

    bool CellOutsideApproximationDomain(const Cell &c) const {
      double x_max = c.Corner(0)[0];
      double x_min = c.Corner(0)[0];
      for (int i = 1; i < c.Corners(); ++i) {
        x_max = std::max(x_max, c.Corner(i)[0]);
        x_min = std::min(x_min, c.Corner(i)[0]);
      }
      if (x_min >= compDomainR || x_max <= -compDomainR)
        return true;
      return false;
    }

    const char *Name() const override {
      return "Independent Navier-Stokes domain homotopy assemble 'RTLagrange' (Liu)";
    }

    void BoundaryConditionsEigenSolver(const cell &c, Eigenfct &u) const override {
      StokesBoundaryConditions::setDirichletBC(c, u);
    }

    void MatrixentriesEigenSolver(const cell &c, Matrix &A, Matrix &B,
                                  const double &shift, const double &t) override {
      const Cell &C = *c;
      int chi = 1;
      if (CellOutsideApproximationDomain(*c)) chi = 0;

      DivergenceFreeElement E(A, C);
      RowEntries A_c(A, E);
      RowEntries B_c(B, E);
      for (int q = 0; q < E.nQ(); ++q)
        for (int i = 0; i < E.size(); ++i)
          for (int j = 0; j < E.size(); ++j)
            for (int k = 0; k < E.get_maxk(i); ++k)
              for (int l = 0; l < E.get_maxk(j); ++l) {
                double L_ij = E.VelocityField(q, i, k) * E.VelocityField(q, j, l);
                double N_ij = Frobenius(E.VelocityFieldGradient(q, i, k),
                                        E.VelocityFieldGradient(q, j, l)) + sigma * L_ij;
                A_c(i, j, k, l) += E.QWeight(q) * N_ij;
                B_c(i, j, k, l) += E.QWeight(q) * chi * L_ij;
              }
    }

    void MatrixentriesRayleighRitz(const cell &c,
                                   const Eigenfct &U_i, const Eigenfct &U_j,
                                   IAEigenvalueType<VERIFIED> &a, IAEigenvalueType<VERIFIED> &b,
                                   const double &shift, const double &t) override {
      using T = IAEigenvalueType<VERIFIED>;

      const Cell &C = *c;
      int chi = 1;
      if (CellOutsideApproximationDomain(C)) chi = 0;

      DivergenceFreeElementT<T> E(U_i, C);
      for (int q = 0; q < E.nQ(); ++q) {
        T L_ij = E.VelocityField(q, U_i) * E.VelocityField(q, U_j);
        T N_ij = Frobenius(E.VelocityFieldGradient(q, U_i),
                           E.VelocityFieldGradient(q, U_j)) + sigma * L_ij;

        a += E.QWeight(q) * N_ij;
        b += E.QWeight(q) * chi * L_ij;
      }
    }

    std::unique_ptr<IDiscretization>
    CreateEVDisc(const Meshes &meshes) const {
      return std::make_unique<DivergenceFreeDiscretization>(meshes);
    }

    std::unique_ptr<IAIDiscretization>
    CreateIAEVDisc(const Meshes &meshes) const {
      return std::make_unique<IADivergenceFreeDiscretization>(meshes);
    }

    std::unique_ptr<IDiscretization>
    CreateGoerischDisc(const IDiscretization &EVdisc) const override {
      return std::make_unique<RTDiscretization>(
          dynamic_cast<const NonAdaptiveIDiscretization &>(EVdisc), rt_order, 2);
    }

    std::unique_ptr<IAIDiscretization>
    CreateIAGoerischDisc(const IAIDiscretization &iaEVdisc) const override {
      return std::make_unique<IARTDiscretization>(
          dynamic_cast<const IANonAdaptiveIDiscretization &>(iaEVdisc), rt_order, 2);
    }

    void BoundaryConditionsGoerisch(const cell &c, Vector &w) const override {}

    virtual void PrintInfo() const override {
      mout.PrintInfo("Assemble", this->verbose,
                     PrintInfoEntry("Name", this->Name(), 0),
                     PrintInfoEntry("Shift", this->Shift(), 0),
                     PrintInfoEntry("Goerisch RT order", rt_order, 0)
      );
    }

    void AssembleGoerischSystem(const cell &c, Matrix &systemMatrix, Goerischfcts &w,
                                double shift, const Eigenpairs &ep, double t) const override {
//      const Cell &C = *c;
//      double tt = t / (1.0 - t);
//      double TT = tt / sigma;
//      double s = 1.0 / sigma;
//      if (CellOutsideApproximationDomain(C))  s = 0.0;
//
//      NavierStokesElementDivergenceHomotopy E(w, C);
//      VectorFieldElement E_ep(ep(0), E);
//      MixedRowValuesVector rowValues(w, C, E);
//      MixedRowEntries rowEntries(systemMatrix, C, E);
//      for (int q = 0; q < E.nQ(); ++q) {
//        //assembling rhs
//        for (int n = 0; n < w.size(); ++n) {
//          VectorField W2 = E_ep.VectorValue(q, ep(n))  * s;
//
//          for (int i = 0; i < E.RTSize(); ++i) {
//            for (int k = 0; k < E.RTMaxk(i); ++k) {
//              rowValues[n](E.RTIndex(), i, E.IndexingRT_k(i, k, 0)) -= E.QWeight(q) * (W2[0] * E.RTDivergence(q, i, k));
//              rowValues[n](E.RTIndex(), i, E.IndexingRT_k(i, k, 1)) -= E.QWeight(q) * (W2[1] * E.RTDivergence(q, i, k));
//            }
//          }
//          for (int i = 0; i < E.LagrangeSize(); ++i) {
//            rowValues[n](E.LagrangeIndex(), i, 0) -= E.QWeight(q) * tt * (W2 * E.LagrangeDerivative(q, i));
//          }
//        }
//        //assembling system matrix
//        for (int i = 0; i < E.RTSize(); ++i)
//          for (int j = 0; j < E.RTSize(); ++j)
//            for (int k = 0; k < E.RTMaxk(i); ++k)
//              for (int l = 0; l < E.RTMaxk(j); ++l) {
//                double tmp = E.QWeight(q) * (E.RTVectorValue(q, i, k) * E.RTVectorValue(q, j, l) + E.RTDivergence(q, i, k) * E.RTDivergence(q, j, l) / sigma);
//                rowEntries(E.RTIndex(), E.RTIndex(), i, j, E.IndexingRT_k(i, k, 0), E.IndexingRT_k(j, l, 0)) += tmp;
//                rowEntries(E.RTIndex(), E.RTIndex(), i, j, E.IndexingRT_k(i, k, 1), E.IndexingRT_k(j, l, 1)) += tmp;
//              }
//        for (int i = 0; i < E.RTSize(); ++i)
//          for (int k = 0; k < E.RTMaxk(i); ++k)
//            for (int j = 0; j < E.LagrangeSize(); ++j) {
//              VectorField tmp = (E.QWeight(q) * TT * E.RTDivergence(q, i, k)) * E.LagrangeDerivative(q, j);
//              rowEntries(E.RTIndex(), E.LagrangeIndex(), i, j, E.IndexingRT_k(i, k, 0), 0) += tmp[0];
//              rowEntries(E.RTIndex(), E.LagrangeIndex(), i, j, E.IndexingRT_k(i, k, 1), 0) += tmp[1];
//
//              rowEntries(E.LagrangeIndex(), E.RTIndex(), j, i, 0, E.IndexingRT_k(i, k, 0)) += tmp[0];
//              rowEntries(E.LagrangeIndex(), E.RTIndex(), j, i, 0, E.IndexingRT_k(i, k, 1)) += tmp[1];
//
//            }
//        for (int i = 0; i < E.LagrangeSize(); ++i)
//          for (int j = 0; j < E.LagrangeSize(); ++j) {
//            rowEntries(E.LagrangeIndex(), E.LagrangeIndex(), i, j, 0, 0)
//                += E.QWeight(q) * tt * (E.LagrangeValue(q, i) * E.LagrangeValue(q, j)
//                                        + TT * (E.LagrangeDerivative(q, i) * E.LagrangeDerivative(q, j)));
//          }
//      }
    }

    void MatrixentriesGoerisch(const cell &c,
                               const Eigenpair &EP_i, const Eigenpair &EP_j,
                               const Goerischfct &W_i, const Goerischfct &W_j,
                               IAEigenvalueType<VERIFIED> &g,
                               const double &shift, const double &t) override {
      using T = IAEigenvalueType<VERIFIED>;

//      const Cell &C = *c;
//      T tt = t / (T(1.0) - t);
//      int chi = 1;
//      if (CellOutsideApproximationDomain(C)) chi = 0;
//
//      NavierStokesElementDivergenceHomotopyT<T> E(W_i, C);
//      VectorFieldElementT<T> E_ep(EP_i, E);
//      for (int q = 0; q < E.nQ(); ++q) {
//        TensorT<T> T_i = E.W1(q, W_i);
//        TensorT<T> T_j = E.W1(q, W_j);
//        T W3_i = E.W3(q, W_i);
//        T W3_j = E.W3(q, W_j);
//
//        VectorFieldT<T> V_i = chi * E_ep.VectorValue(q, EP_i.getEigenfct());
//        VectorFieldT<T> W2_i = E.DivW1(q, W_i) + tt * E.GradW3(q, W_i) + V_i;
//        VectorFieldT<T> V_j = chi * E_ep.VectorValue(q, EP_j.getEigenfct());
//        VectorFieldT<T> W2_j = E.DivW1(q, W_j) + tt * E.GradW3(q, W_j) + V_j;
//
//        g += E.QWeight(q) * (Frobenius(T_i, T_j) + (W2_i * W2_j) / sigma + tt * (W3_i * W3_j));
//      }
    }
  };

} // namespace navierstokes

#endif //NAVIERSTOKESASSEMBLEHOMOTPY_HPP
