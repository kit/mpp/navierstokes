#ifndef NAVIERSTOKESASSEMBLEAPPROXIMATION_HPP
#define NAVIERSTOKESASSEMBLEAPPROXIMATION_HPP

#include "assemble/NavierStokesAssemble.hpp"

namespace navierstokes {

//----------------------------------------------------------------------------------------
// DivergenceFreeApproximationStokesAssemble
//----------------------------------------------------------------------------------------
  class DivergenceFreeApproximationStokesAssemble : public StokesAssemble {
  public:
    DivergenceFreeApproximationStokesAssemble(const NavierStokesProblem &problem)
        : StokesAssemble(problem) {}

    const char *Name() const override { return "Velocity assemble (divergence free)"; }

    void BoundaryConditions(const cell &c, Vector &u) const override {
      setDirichletBC(c, u);
    }

    void Residual(const cell &c, const Vector &velocity, Vector &defect) const override;

    void Jacobi(const cell &c, const Vector &velocity, Matrix &jacobi) const override;
  };

//----------------------------------------------------------------------------------------
// VApproximationStokesAssemble
//----------------------------------------------------------------------------------------
  class VApproximationStokesAssemble : public LinearStokesAssemble {
  public:
    VApproximationStokesAssemble(const NavierStokesProblem &problem)
        : LinearStokesAssemble(problem) {}

    const char *Name() const override { return "V assemble (divergence free)"; }

    void BoundaryConditions(const cell &c, Vector &u) const override {}

    void AssembleSystem(const cell &c, Matrix &systemMatrix, Vector &rhs) const override;
  };

//----------------------------------------------------------------------------------------
// MixedInitApproximationStokesAssemble
//----------------------------------------------------------------------------------------
  class MixedInitApproximationStokesAssemble : public LinearStokesAssemble {
  public:
    MixedInitApproximationStokesAssemble(const NavierStokesProblem &problem)
        : LinearStokesAssemble(problem) {}

    const char *Name() const override { return "Pressure init assemble (Taylor Hood)"; }

    void BoundaryConditions(const cell &c, Vector &u) const override {
      setDirichletBCVelocity(c, u);
    }

    void AssembleSystem(const cell &c, Matrix &systemMatrix, Vector &rhs) const override;
  };

//----------------------------------------------------------------------------------------
// MixedApproximationStokesAssemble
//----------------------------------------------------------------------------------------
  class MixedApproximationStokesAssemble : public StokesAssemble {
  public:
    MixedApproximationStokesAssemble(const NavierStokesProblem &problem)
        : StokesAssemble(problem) {}

    const char *Name() const override { return "Pressure assemble (Taylor Hood)"; }

    void BoundaryConditions(const cell &c, Vector &u) const override {
      setDirichletBCVelocity(c, u);
    }

    void Residual(const cell &c, const Vector &v, Vector &defect) const override;

    void Jacobi(const cell &c, const Vector &v, Matrix &jacobi) const override;
  };

} // namespace navierstokes


#endif //NAVIERSTOKESASSEMBLEAPPROXIMATION_HPP
