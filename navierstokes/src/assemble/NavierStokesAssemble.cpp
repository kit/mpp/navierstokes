// file:   NaviersStokesAssemble.C
// author: Jonathan Wunderlich

#include "NavierStokesAssemble.hpp"


namespace navierstokes {

//----------------------------------------------------------------------------------------
// StokesBoundaryConditions
//----------------------------------------------------------------------------------------
  void StokesBoundaryConditions::setDirichletBC(const cell &c, Vector &u) const {
    const Cell &C = *c;
    if (!u.OnBoundary(C))
      return;
    for (int face = 0; face < C.Faces(); ++face) {
      if (!u.OnBoundary(C, face))
        continue;
      rows R(u.GetMatrixGraph(), C, face);
      for (const row &r : R) {
        for (int k = 0; k < r.NumberOfDofs(); ++k) {
          u(r, k) = 0;
          u.D(r, k) = true;
        }
      }
    }
  }

  void StokesBoundaryConditions::setDirichletBCVelocity(const cell &c, Vector &u) const {
    const Cell &C = *c;
    if (!u.OnBoundary(C))
      return;
    for (int face = 0; face < C.Faces(); ++face) {
      if (!u.OnBoundary(C, face))
        continue;
      rows R(u.GetMatrixGraph(), C, face, 0);
      for (const row &r : R) {
        for (int k = 0; k < 2; ++k) {
          u(r, k) = 0;
          u.D(r, k) = true;
        }
      }
    }
  }

  void StokesBoundaryConditions::setDirichletBCexceptObstacle(const cell &c,
                                                              Vector &u) const {
    const Cell &C = *c;
    if (!u.OnBoundary(C))
      return;
    for (int face = 0; face < C.Faces(); ++face) {
      int bfp = u.GetMesh().BoundaryFacePart(C.Face(face));
      if (bfp == -1 || bfp == OBSTACLE_BC)
        continue;
      rows R(u.GetMatrixGraph(), C, face);
      for (const row &r : R) {
        for (int k = 0; k < r.NumberOfDofs(); ++k) {
          u(r, k) = 0;
          u.D(r, k) = true;
        }
      }
    }
  }

  void StokesAssemble::Initialize(Vector &u) const {
    TRY {
      u.ClearDirichletFlags();
      for (cell c = u.cells(); c != u.cells_end(); ++c) BoundaryConditions(c, u);
      u.DirichletConsistent();
    } CATCH ("Error in Initialize")
  }

  void LinearStokesAssemble::Initialize(Vector &u) const {
    TRY {
      u.ClearDirichletFlags();
      for (cell c = u.cells(); c != u.cells_end(); ++c) BoundaryConditions(c, u);
      u.DirichletConsistent();
    } CATCH ("Error in Initialize")
  }

  void LinearStokesAssemble::Initialize(Vectors &u) const {
    for (int n = 0; n < u.size(); ++n) {
      TRY {
        u[n].ClearDirichletFlags();
        for (cell c = u[n].cells(); c != u[n].cells_end(); ++c) BoundaryConditions(c, u[n]);
        u[n].DirichletConsistent();
      } CATCH ("Error in Initialize")
    }
  }

} // namespace navierstokes


