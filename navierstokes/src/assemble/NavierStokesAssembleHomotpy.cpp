#include "NavierStokesAssembleHomotpy.hpp"

namespace navierstokes {

//----------------------------------------------------------------------------------------
// DivergenceHomotopyStokesAssemble
//----------------------------------------------------------------------------------------
  template<>
  const IAInterval &DivergenceHomotopyStokesAssemble<true, false>::Gamma1() const {
    return gamma1;
  }

  template<>
  const IAInterval &DivergenceHomotopyStokesAssemble<true, true>::Gamma1() const {
    return gamma1;
  }

  template<>
  const double &DivergenceHomotopyStokesAssemble<false, false>::Gamma1() const {
    return midGamma_1;
  }

  template<>
  const double &DivergenceHomotopyStokesAssemble<false, true>::Gamma1() const {
    return midGamma_1;
  }

  template<>
  const IAInterval &DivergenceHomotopyStokesAssemble<true, false>::Gamma2() const {
    return gamma2;
  }

  template<>
  const IAInterval &DivergenceHomotopyStokesAssemble<true, true>::Gamma2() const {
    return gamma2;
  }

  template<>
  const double &DivergenceHomotopyStokesAssemble<false, false>::Gamma2() const {
    return midGamma_2;
  }

  template<>
  const double &DivergenceHomotopyStokesAssemble<false, true>::Gamma2() const {
    return midGamma_2;
  }

} // namespace navierstokes
