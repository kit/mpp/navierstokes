#include "NavierStokesAssembleApproximation.hpp"

#include "VectorFieldElement.hpp"
#include "RTElement.hpp"
#include "TaylorHoodElement.hpp"


namespace navierstokes {

//----------------------------------------------------------------------------------------
// DivergenceFreeApproximationStokesAssemble
//----------------------------------------------------------------------------------------
  void DivergenceFreeApproximationStokesAssemble::Residual(const cell &c,
                                                           const Vector &velocity,
                                                           Vector &defect) const {
    const Cell &C = *c;
    DivergenceFreeElement E(velocity, C);
    DivergenceFreeVectorAccess defect_c(E, defect);
    for (auto [q, weight, point]: E.QuadWithPoint()) {
      VectorField Uomega = E.VelocityField(q, velocity) + problem.VF_Gamma(point, C);
      Tensor Du = E.VelocityFieldGradient(q, velocity);
      Tensor DUomega = Du + problem.VFG_Gamma(point, C);
      Tensor Domega = Du - problem.VFG_V(point, C);
      for (auto i: defect_c.IterateOverShapeAndVector()) {
        i.VectorEntry() += weight * (Frobenius(Domega, E.VelocityFieldGradient(q, i))
                                     + problem.Re() * ((DUomega * Uomega) * E.VelocityField(q, i)));
      }
    }
  }

  void DivergenceFreeApproximationStokesAssemble::Jacobi(const cell &c,
                                                         const Vector &velocity,
                                                         Matrix &jacobi) const {
    const Cell &C = *c;
    DivergenceFreeElement E(velocity, C);
    DivergenceFreeMatrixAccess jacobi_c(E, jacobi);
    for (auto [q, weight, point]: E.QuadWithPoint()) {
      VectorField UOmega = E.VelocityField(q, velocity) + problem.VF_Gamma(point, C);
      for (auto i: jacobi_c.IterateOverShapeAndMatrix()) {
        for (auto j: i.IterateOverShapeAndMatrixRow()) {
          double s = E.VelocityField(q, i) * (E.VelocityFieldGradient(q, j) * UOmega)
                     - (E.VelocityFieldGradient(q, i) * E.VelocityField(q, j)) * UOmega;
          j.MatrixEntry() += weight * (Frobenius(E.VelocityFieldGradient(q, j),
                                                 E.VelocityFieldGradient(q, i))
                                       + problem.Re() * s);
        }
      }
    }
  }

//----------------------------------------------------------------------------------------
// VApproximationStokesAssemble
//----------------------------------------------------------------------------------------
  void VApproximationStokesAssemble::AssembleSystem(const cell &c,
                                                    Matrix &systemMatrix,
                                                    Vector &rhs) const {
    const Cell &C = *c;
    DivergenceFreeElement E(rhs, C);
    DivergenceFreeVectorAccess rhs_c(E, rhs);
    DivergenceFreeMatrixAccess systemMatrix_c(E, systemMatrix);
    for (auto [q, weight, point]: E.QuadWithPoint()) {
      Tensor DV = problem.VFG_V(point, C);
      VectorField V = problem.VF_V(point, C);
      for (auto i: rhs_c.IterateOverShapeAndVector()) {
        i.VectorEntry() += weight * (problem.Constants().UOmegaInftySqr<false>() *
                                     Frobenius(DV, E.VelocityFieldGradient(q, i))
                                     + problem.Constants().D_UOmegaInftySqr<false>() *
                                       (V * E.VelocityField(q, i)));
      }
      for (auto i: systemMatrix_c.IterateOverShapeAndMatrix()) {
        for (auto j: i.IterateOverShapeAndMatrixRow()) {
          j.MatrixEntry() += weight * (problem.Constants().UOmegaInftySqr<false>()
                                       * Frobenius(E.VelocityFieldGradient(q, i),
                                                   E.VelocityFieldGradient(q, j))
                                       + problem.Constants().D_UOmegaInftySqr<false>()
                                         * (E.VelocityField(q, i) * E.VelocityField(q, j)));
        }
      }
    }
  }

//----------------------------------------------------------------------------------------
// MixedInitApproximationStokesAssemble
//----------------------------------------------------------------------------------------
  void MixedInitApproximationStokesAssemble::AssembleSystem(const cell &c,
                                                            Matrix &systemMatrix,
                                                            Vector &rhs) const {
    const Cell &C = *c;
    TaylorHoodElement E(rhs, C);
    DivergenceFreeElement Esol(problem.Velocity<false>(), E);
    TaylorHoodVectorAccess rhs_c(E, rhs);
    TaylorHoodMatrixAccess systemMatrix_c(E, systemMatrix);
    for (auto [q, weight]: E.Quad()) {
      VectorField sol = Esol.VelocityField(q, problem.Velocity<false>());
      Tensor Dsol = Esol.VelocityFieldGradient(q, problem.Velocity<false>());
      for (auto i_n: rhs_c.IterateOverShapeAndVector(E.VelocityIndex())) {
        for (int k = 0; k < i_n.NumberOfComponents(); ++k) {
          i_n.VectorEntry(k) += weight * (Frobenius(Dsol, E.VelocityFieldRowGradient(q, i_n, k))
                                          + problem.Sigma() * sol *
                                            E.VelocityFieldComponent(q, i_n, k));
        }
      }
      for (auto i_n: systemMatrix_c.IterateOverShapeAndMatrix(E.VelocityIndex())) {
        for (auto j_n_m: i_n.IterateOverShapeAndMatrixRow(E.VelocityIndex())) {
          for (int k = 0; k < i_n.NumberOfComponents(); ++k) {
            for (int l = 0; l < j_n_m.NumberOfComponents(); ++l) {
              j_n_m.MatrixEntry(k, l) +=
                  weight * (Frobenius(E.VelocityFieldRowGradient(q, j_n_m, l),
                                      E.VelocityFieldRowGradient(q, i_n, k))
                            + problem.Sigma() * E.VelocityFieldComponent(q, i_n, k) *
                              E.VelocityFieldComponent(q, j_n_m, l));
            }
          }
        }
      }
      for (auto i_n: systemMatrix_c.IterateOverShapeAndMatrix(E.PressureIndex())) {
        for (auto j_n_m: i_n.IterateOverShapeAndMatrixRow(E.PressureIndex())) {
          j_n_m.MatrixEntry() += 1.0;
        }
      }
    }
  }

//----------------------------------------------------------------------------------------
// MixedApproximationStokesAssemble
//----------------------------------------------------------------------------------------
  void MixedApproximationStokesAssemble::Residual(const cell &c, const Vector &v,
                                                  Vector &defect) const {
    const Cell &C = *c;
    TaylorHoodElementT<> E(v, C);
    TaylorHoodVectorAccess defect_c(E, defect);
    for (auto [q, weight, point]: E.QuadWithPoint()) {
      VectorField Uomega = E.VelocityField(q, v) + problem.VF_Gamma(point, C);
      Tensor Du = E.VelocityFieldGradient(q, v);
      Tensor DUomega = Du + problem.VFG_Gamma(point, C);
      Tensor Domega = Du - problem.VFG_V(point, C);
      VectorField Dp = E.PressureGradient(q, v);
      double Divu = E.VelocityFieldDivergence(q, v);
      for (auto i_n: defect_c.IterateOverShapeAndVector(E.VelocityIndex())) {
        for (int k = 0; k < i_n.NumberOfComponents(); ++k) {
          i_n.VectorEntry(k) += weight * (Frobenius(Domega, E.VelocityFieldRowGradient(q, i_n, k))
                                          + problem.Re() * ((DUomega * Uomega + Dp) *
                                                            E.VelocityFieldComponent(q, i_n, k)));
        }
      }
      for (auto i_n: defect_c.IterateOverShapeAndVector(E.PressureIndex())) {
        i_n.VectorEntry() -= E.QWeight(q) * problem.Re() * Divu * E.PressureValue(q, i_n);
      }
    }
  }

  void MixedApproximationStokesAssemble::Jacobi(const cell &c, const Vector &v,
                                                Matrix &jacobi) const {
    const Cell &C = *c;
    TaylorHoodElementT<> E(v, C);
    TaylorHoodMatrixAccess jacobi_c(E, jacobi);
    for (auto [q, weight, point]: E.QuadWithPoint()) {
      VectorField UOmega = E.VelocityField(q, v) + problem.VF_Gamma(point, C);
      for (auto i_n: jacobi_c.IterateOverShapeAndMatrix(E.VelocityIndex())) {
        for (auto j_n_m: i_n.IterateOverShapeAndMatrixRow(E.VelocityIndex())) {
          for (int k = 0; k < i_n.NumberOfComponents(); ++k) {
            for (int l = 0; l < j_n_m.NumberOfComponents(); ++l) {
              double s = E.VelocityFieldComponent(q, i_n, k) * (E.VelocityFieldRowGradient(q, j_n_m, l) * UOmega)
                         - (E.VelocityFieldRowGradient(q, i_n, k) * E.VelocityFieldComponent(q, j_n_m, l)) * UOmega;
              j_n_m.MatrixEntry(k, l)
                  += weight * (Frobenius(E.VelocityFieldRowGradient(q, j_n_m, l),
                                         E.VelocityFieldRowGradient(q, i_n, k))
                               + problem.Re() * s);
            }
          }
        }
        for (auto j_n_m: i_n.IterateOverShapeAndMatrixRow(E.PressureIndex())) {
          for (int k = 0; k < i_n.NumberOfComponents(); ++k) {
            j_n_m.MatrixEntry(k, 0) -=
                weight * problem.Re() * E.VelocityFieldDivergence(q, i_n, k) *
                E.PressureValue(q, j_n_m);
          }
        }
      }
      for (auto i_n: jacobi_c.IterateOverShapeAndMatrix(E.PressureIndex())) {
        for (auto j_n_m: i_n.IterateOverShapeAndMatrixRow(E.VelocityIndex())) {
          for (int l = 0; l < j_n_m.NumberOfComponents(); ++l)
            j_n_m.MatrixEntry(0, l) -=
                weight * problem.Re() * E.VelocityFieldDivergence(q, j_n_m, l) *
                E.PressureValue(q, i_n);
        }
      }
    }
  }


} // namespace navierstokes