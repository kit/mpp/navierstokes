#ifndef NAVIERSTOKESASSEMBLE_H
#define NAVIERSTOKESASSEMBLE_H

#include "NavierStokesElement.hpp"
#include "problem/NavierStokesProblem.hpp"

#include "IAEigenvalueAssemble.hpp"


namespace navierstokes {

  const int OBSTACLE_BC = 10;

  class StokesBoundaryConditions {
  protected:
    void setDirichletBC(const cell &, Vector &) const;

    void setDirichletBCVelocity(const cell &, Vector &) const;

    void setDirichletBCexceptObstacle(const cell &, Vector &) const;
  };

//----------------------------------------------------------------------------------------
// StokesAssemble
//----------------------------------------------------------------------------------------
  class StokesAssemble : public IAssemble, public StokesBoundaryConditions {
  protected:
    const NavierStokesProblem &problem;
  public:
    StokesAssemble(const NavierStokesProblem &problem) : problem(problem) {}

    void Initialize(Vector &u) const override;

    virtual void BoundaryConditions(const cell &c, Vector &u) const = 0;
  };

  class LinearStokesAssemble : public ILinearAssemble, public StokesBoundaryConditions {
  protected:
    const NavierStokesProblem &problem;
  public:
    LinearStokesAssemble(const NavierStokesProblem &problem) : problem(problem) {}

    void Initialize(Vector &u) const override;

    void Initialize(Vectors &u) const override;

    virtual void BoundaryConditions(const cell &c, Vector &u) const = 0;
  };

} // namespace navierstokes


#endif // NAVIERSTOKESASSEMBLE_H
