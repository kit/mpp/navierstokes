#ifndef NAVIERSTOKESASSEMBLEDEFECT_HPP
#define NAVIERSTOKESASSEMBLEDEFECT_HPP

#include "assemble/NavierStokesAssemble.hpp"
#include "RTDiscretization.hpp"
#include "LagrangeDiscretization.hpp"
#include "RTElement.hpp"
#include "VectorFieldElement.hpp"

namespace navierstokes {

//========================================================================================
// Defect assembles
//========================================================================================
  class DefectStokesAssemble : public LinearStokesAssemble {
  public:
    DefectStokesAssemble(const NavierStokesProblem &problem) : LinearStokesAssemble(problem) {}

    virtual ~DefectStokesAssemble() = default;

    virtual std::shared_ptr<NonAdaptiveIDiscretization>
    CreateDefectDisc(const NonAdaptiveIDiscretization &disc) const = 0;

    virtual std::shared_ptr<IANonAdaptiveIDiscretization>
    CreateDefectDisc(const IANonAdaptiveIDiscretization &disc) const = 0;

    void BoundaryConditions(const cell &c, Vector &v) const override {}

    virtual void Defect(double &, double &, const Vectors &v) const = 0;
  };

//----------------------------------------------------------------------------------------
// RTDefectStokesAssemble
//----------------------------------------------------------------------------------------
  class RTDefectStokesAssemble : public DefectStokesAssemble {
    int order{};
  public:
    RTDefectStokesAssemble(const NavierStokesProblem &problem, int order)
        : DefectStokesAssemble(problem), order(order) {}

    std::shared_ptr<NonAdaptiveIDiscretization>
    CreateDefectDisc(const NonAdaptiveIDiscretization &disc) const override {
      return std::make_unique<RTDiscretization>(disc, order, 1);
    }

    std::shared_ptr<IANonAdaptiveIDiscretization>
    CreateDefectDisc(const IANonAdaptiveIDiscretization &disc) const override {
      return std::make_unique<IARTDiscretization>(disc, order, 1);
    }
  };

  template<bool VERIFIED, bool PRESSURE>
  class RTDefectStokesAssembleCoincide : public RTDefectStokesAssemble {
  public:
    RTDefectStokesAssembleCoincide(const NavierStokesProblem &problem, int order = 2)
      : RTDefectStokesAssemble(problem, order) {}

    const char *Name() const override {
      return "Defect assemble (Raviart Thomas; levels coincide)";
    }

    void AssembleSystem(const cell &c, Matrix &systemMatrix, Vectors &rhs) const override;

    void Defect(double &alpha1, double &alpha2, const Vectors &v) const override;
  };

  //Possible choice since gradients are zero in H(Omega)'
  template<bool VERIFIED>
  using RTVelocityDefectStokesAssembleCoincide = RTDefectStokesAssembleCoincide<VERIFIED, true>;

  template<bool VERIFIED>
  using RTPressureDefectStokesAssembleCoincide = RTDefectStokesAssembleCoincide<VERIFIED, true>;

  template<bool VERIFIED, bool PRESSURE>
  class RTDefectStokesAssembleDiffer : public RTDefectStokesAssemble {
  public:
    RTDefectStokesAssembleDiffer(const NavierStokesProblem &problem, int order = 2)
        : RTDefectStokesAssemble(problem, order) {}

    const char *Name() const override {
      return "Defect assemble (Raviart Thomas; levels differ)";
    }

    void AssembleSystem(Matrix &systemMatrix, Vectors &rhs) const override;

    void Defect(double &delta1, double &delta2, const Vectors &v) const override;
  };

  //Possible choice since gradients are zero in H(Omega)'
  template<bool VERIFIED>
  using RTVelocityDefectStokesAssembleDiffer = RTDefectStokesAssembleDiffer<VERIFIED, true>;

  template<bool VERIFIED>
  using RTPressureDefectStokesAssembleDiffer = RTDefectStokesAssembleDiffer<VERIFIED, true>;

//----------------------------------------------------------------------------------------
// LagrangeDefectStokesAssemble
//----------------------------------------------------------------------------------------
  class LagrangeDefectStokesAssemble : public DefectStokesAssemble {
    int degree{};
  public:
    LagrangeDefectStokesAssemble(const NavierStokesProblem &problem, int degree)
        : DefectStokesAssemble(problem), degree(degree) {}

    std::shared_ptr<NonAdaptiveIDiscretization>
    CreateDefectDisc(const NonAdaptiveIDiscretization &disc) const override {
      return std::make_unique<LagrangeDiscretization>(disc, degree, 2);
    }

    std::shared_ptr<IANonAdaptiveIDiscretization>
    CreateDefectDisc(const IANonAdaptiveIDiscretization &disc) const override {
      return std::make_unique<IALagrangeDiscretization>(disc, degree, 2);
    }
  };

template<bool VERIFIED, bool PRESSURE>
  class LagrangeDefectStokesAssembleCoincide : public LagrangeDefectStokesAssemble {
  public:
    LagrangeDefectStokesAssembleCoincide(const NavierStokesProblem &problem, int degree = 2)
      : LagrangeDefectStokesAssemble(problem, degree) {}

    const char *Name() const override {
      return "Defect assemble (Lagrange; levels coincide)";
    }

    void AssembleSystem(const cell &c, Matrix &systemMatrix, Vectors &rhs) const override;

    void Defect(double &delta1, double &delta2, const Vectors &v) const override;
  };

  //Possible choice since gradients are zero in H(Omega)'
  template<bool VERIFIED>
  using LagrangeVelocityDefectStokesAssembleCoincide = LagrangeDefectStokesAssembleCoincide<VERIFIED, true>;

  template<bool VERIFIED>
  using LagrangePressureDefectStokesAssembleCoincide = LagrangeDefectStokesAssembleCoincide<VERIFIED, true>;

  template<bool VERIFIED, bool PRESSURE>
  class LagrangeDefectStokesAssembleDiffer : public LagrangeDefectStokesAssemble {
  public:
    LagrangeDefectStokesAssembleDiffer(const NavierStokesProblem &problem, int degree = 2)
        : LagrangeDefectStokesAssemble(problem, degree) {}

    const char *Name() const override {
      return "Defect assemble (Lagrange; levels differ)";
    }

    void AssembleSystem(Matrix &systemMatrix, Vectors &rhs) const override;

    void Defect(double &delta1, double &delta2, const Vectors &v) const override;
  };

  //Possible choice since gradients are zero in H(Omega)'
  template<bool VERIFIED>
  using LagrangeVelocityDefectStokesAssembleDiffer = LagrangeDefectStokesAssembleDiffer<VERIFIED, true>;

  template<bool VERIFIED>
  using LagrangePressureDefectStokesAssembleDiffer = LagrangeDefectStokesAssembleDiffer<VERIFIED, true>;

} // namespace navierstokes

#endif //NAVIERSTOKESASSEMBLEDEFECT_HPP
