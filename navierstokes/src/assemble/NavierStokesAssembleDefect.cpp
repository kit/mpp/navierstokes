#include "NavierStokesAssembleDefect.hpp"

#include "VectorFieldElement.hpp"
#include "RTElement.hpp"

namespace navierstokes {
//----------------------------------------------------------------------------------------
// RTDefectStokesAssemble
//----------------------------------------------------------------------------------------
  template<bool VERIFIED, bool PRESSURE>
  void RTDefectStokesAssembleCoincide<VERIFIED, PRESSURE>::AssembleSystem(const cell &c,
                                                                          Matrix &systemMatrix,
                                                                          Vectors &rhs) const {
    const Cell &C = *c;
    DivergenceFreeElement Esol(problem.Velocity<false>(), C);
    std::unique_ptr<ScalarElement> Epressure = nullptr;
    if constexpr (PRESSURE) {
      Epressure = std::make_unique<ScalarElement>(problem.Pressure(), Esol);
    }
    RTElement E(rhs, Esol);
    RowValuesVector rowValues(rhs, E);
    RowEntries rowEntries(systemMatrix, E);
    for (int q = 0; q < E.nQ(); ++q) {
      //assembling rhs
      VectorField Uomega = problem.VF_U(E.QPoint(q))
                           + Esol.VelocityField(q, problem.Velocity<false>())
                           - Esol.VelocityField(q, problem.ApprV());
      Tensor Domega = Esol.VelocityFieldGradient(q, problem.Velocity<false>())
                      - Esol.VelocityFieldGradient(q, problem.ApprV());
      Tensor DUomega = problem.VFG_U(E.QPoint(q)) + Domega;
      VectorField s = (DUomega * Uomega);
      if constexpr (PRESSURE) {
        s += Epressure->Derivative(q, problem.Pressure());
      }
      s *= problem.Re() * problem.template C2sqr<double>();
      for (int n = 0; n < rhs.size(); ++n) {
        for (int i = 0; i < E.size(); ++i) {
          for (int k = 0; k < E.get_maxk(i); ++k) {
            rowValues[n](i, k) += E.QWeight(q) * (Domega[n] * E.VelocityField(q, i, k)
                                                  + s[n] * E.VelocityFieldDivergence(q, i, k));
          }
        }
      }
      //assembling system matrix
      for (int i = 0; i < E.size(); ++i) {
        for (int j = 0; j < E.size(); ++j) {
          for (int k = 0; k < E.get_maxk(i); ++k) {
            for (int l = 0; l < E.get_maxk(j); ++l) {
              rowEntries(i, j, k, l) +=
                  E.QWeight(q) * (E.VelocityField(q, i, k) * E.VelocityField(q, j, l)
                                  + problem.template C2sqr<double>()
                                    * E.VelocityFieldDivergence(q, i, k)
                                    * E.VelocityFieldDivergence(q, j, l));
            }
          }
        }
      }
    }
  }

  template<bool VERIFIED, bool PRESSURE>
  void RTDefectStokesAssembleCoincide<VERIFIED, PRESSURE>::Defect(double &delta1, double &delta2,
                                                                  const Vectors &v) const {
    using T = IAEigenvalueType<VERIFIED>;
    T d1{};
    T d2{};
    for (cell c = problem.Velocity<false>().cells(); c != problem.Velocity<false>().cells_end(); ++c) {
      const Cell &C = *c;
      DivergenceFreeElementT<T> Esol(problem.Velocity<false>(), C);
      std::unique_ptr<ScalarElementT<T>> Epressure = nullptr;
      if constexpr (PRESSURE) {
        Epressure = std::make_unique<ScalarElementT<T>>(problem.Pressure(), Esol);
      }
      RTElementT<T> E(v, Esol);
      for (auto [q, weight, point] : E.QuadWithPoint()) {
        VectorFieldT<T> Uomega = problem.VF_U(point)
                                 + Esol.VelocityField(q, problem.Velocity<false>())
                                 - Esol.VelocityField(q, problem.ApprV());
        TensorT<T> Domega = Esol.VelocityFieldGradient(q, problem.Velocity<false>())
                            - Esol.VelocityFieldGradient(q, problem.ApprV());
        TensorT<T> DUomega = problem.VFG_U(point) + Domega;
        TensorT<T> rhoDomega(E.VelocityField(q, v[0]),
                             E.VelocityField(q, v[1]));
        rhoDomega -= Domega;
        VectorFieldT<T> s = DUomega * Uomega;
        if constexpr (PRESSURE) {
          s += Epressure->Derivative(q, problem.Pressure());
        }
        s *= problem.Reynolds<T>();
        s -= VectorFieldT<T>(E.VelocityFieldDivergence(q, v[0]),
                             E.VelocityFieldDivergence(q, v[1]));
        d1 += weight * Frobenius(rhoDomega, rhoDomega);
        d2 += weight * (s * s);
      }
    }
    if constexpr (VERIFIED) {
      delta1 = sup(sqrt(PPM->Sum(abs(d1))));
      delta2 = sup(sqrt(PPM->Sum(abs(d2))));
    } else {
      delta1 = sqrt(PPM->Sum(abs(d1)));
      delta2 = sqrt(PPM->Sum(abs(d2)));
    }
  }

//  template
//  class RTDefectStokesAssembleCoincide<true, false>;
//
//  template
//  class RTDefectStokesAssembleCoincide<false, false>;

  template
  class RTDefectStokesAssembleCoincide<true, true>;

  template
  class RTDefectStokesAssembleCoincide<false, true>;

  template<bool PRESSURE>
  void rt_rhs(const NavierStokesProblem &problem, Vectors &rhs,
              DivergenceFreeElement &Esol, std::shared_ptr<ScalarElement> Epressure,
              const Cell &C, int level) {
    if (level < problem.Levels().velocityDefectLevel) {
      for (int i = 0; i < C.Children(); ++i) {
        auto childCell = problem.GetMesh(level + 1).find_cell(C.Child(i));
        if (childCell == problem.GetMesh(level + 1).cells_end()) THROW("Child not found");
        rt_rhs<PRESSURE>(problem, rhs, Esol, Epressure, *childCell, level + 1);
      }
    } else {
      RTElement E(rhs, C);
      RowValuesVector rowValues(rhs, E);
      for (int q = 0; q < E.nQ(); ++q) {
        Point qPointLocal = Esol.GlobalToLocal(E.QPoint(q));
        VectorField Uomega = problem.VF_U(E.QPoint(q))
                             + Esol.VelocityField(qPointLocal, problem.Velocity<false>())
                             - Esol.VelocityField(qPointLocal, problem.ApprV());
        Tensor Domega = Esol.VelocityFieldGradient(qPointLocal, problem.Velocity<false>())
                        - Esol.VelocityFieldGradient(qPointLocal, problem.ApprV());
        Tensor DUomega = problem.VFG_U(E.QPoint(q)) + Domega;
        VectorField s = (DUomega * Uomega);
        if constexpr (PRESSURE) {
          s += Epressure->Derivative(qPointLocal, problem.Pressure());
        }
        s *= problem.Re() * problem.template C2sqr<double>();
        for (int n = 0; n < rhs.size(); ++n) {
          for (int i = 0; i < E.size(); ++i) {
            for (int k = 0; k < E.get_maxk(i); ++k) {
              rowValues[n](i, k) += E.QWeight(q) * (Domega[n] * E.VelocityField(q, i, k)
                                                    + s[n] * E.VelocityFieldDivergence(q, i, k));
            }
          }
        }
      }
    }
  }

  template<bool VERIFIED, bool PRESSURE>
  void RTDefectStokesAssembleDiffer<VERIFIED, PRESSURE>::AssembleSystem(Matrix &systemMatrix,
                                                                        Vectors &rhs) const {
    rhs = 0;
    systemMatrix = 0;
    TRY {
      //assembling rhs
      for (cell c = problem.Velocity<false>().cells(); c != problem.Velocity<false>().cells_end(); ++c) {
        const Cell &C = *c;
        DivergenceFreeElement Esol(problem.Velocity<false>(), C);
        std::shared_ptr<ScalarElement> Epressure = nullptr;
        if constexpr (PRESSURE) {
          Epressure = std::make_shared<ScalarElement>(problem.Pressure(), Esol);
        }
        rt_rhs<PRESSURE>(problem, rhs, Esol, Epressure, C, problem.Levels().velocityLevel);
      }
      //assembling system matrix
      for (cell c = systemMatrix.cells(); c != systemMatrix.cells_end(); ++c) {
        RTElement E(systemMatrix, *c);
        RowEntries rowEntries(systemMatrix, E);
        for (int q = 0; q < E.nQ(); ++q) {
          for (int i = 0; i < E.size(); ++i) {
            for (int j = 0; j < E.size(); ++j) {
              for (int k = 0; k < E.get_maxk(i); ++k) {
                for (int l = 0; l < E.get_maxk(j); ++l) {
                  rowEntries(i, j, k, l) +=
                      E.QWeight(q) * (E.VelocityField(q, i, k) * E.VelocityField(q, j, l)
                                      + problem.template C2sqr<double>()
                                        * E.VelocityFieldDivergence(q, i, k)
                                        * E.VelocityFieldDivergence(q, j, l));
                }
              }
            }
          }
        }
      }
    } CATCH ("Error in Residual")
    rhs.ClearDirichletValues();
    rhs.Collect();
    systemMatrix.ClearDirichletValues();
  }

  template<bool PRESSURE, typename T>
  void rt_defect(const NavierStokesProblem &problem, T &d1, T &d2, const Vectors &v,
                 DivergenceFreeElementT<T> &Esol, std::shared_ptr<ScalarElementT<T>> Epressure,
                 const Cell &C, int level) {
    if (level < problem.Levels().velocityDefectLevel) {
      for (int i = 0; i < C.Children(); ++i) {
        auto childCell = problem.GetMesh(level + 1).find_cell(C.Child(i));
        if (childCell == problem.GetMesh(level + 1).cells_end()) THROW("Child not found");
        rt_defect<PRESSURE, T>(problem, d1, d2, v, Esol, Epressure, *childCell, level + 1);
      }
    } else {
      RTElementT<T> E(v, C);
      for (auto [q, weight, point] : E.QuadWithPoint()) {
        PointT<T> qPointLocal = Esol.GlobalToLocal(point);
        VectorFieldT<T> Uomega = problem.VF_U(point)
                                 + Esol.VelocityField(qPointLocal, problem.Velocity<false>())
                                 - Esol.VelocityField(qPointLocal, problem.ApprV());
        TensorT<T> Domega = Esol.VelocityFieldGradient(qPointLocal, problem.Velocity<false>())
                            - Esol.VelocityFieldGradient(qPointLocal, problem.ApprV());
        TensorT<T> DUomega = problem.VFG_U(point) + Domega;
        TensorT<T> rhoDomega(E.VelocityField(q, v[0]),
                             E.VelocityField(q, v[1]));
        rhoDomega -= Domega;
        VectorFieldT<T> s = DUomega * Uomega;
        if constexpr (PRESSURE) {
          s += Epressure->Derivative(q, problem.Pressure());
        }
        s *= problem.Reynolds<T>();
        s -= VectorFieldT<T>(E.VelocityFieldDivergence(q, v[0]),
                             E.VelocityFieldDivergence(q, v[1]));
        d1 += E.QWeight(q) * Frobenius(rhoDomega, rhoDomega);
        d2 += E.QWeight(q) * (s * s);
      }
    }
  }

  template<bool VERIFIED, bool PRESSURE>
  void RTDefectStokesAssembleDiffer<VERIFIED, PRESSURE>::Defect(double &delta1, double &delta2,
                                                                const Vectors &v) const {
    using T = IAEigenvalueType<VERIFIED>;
    T d1{};
    T d2{};
    for (cell c = problem.Velocity<false>().cells(); c != problem.Velocity<false>().cells_end(); ++c) {
      const Cell &C = *c;
      DivergenceFreeElementT<T> Esol(problem.Velocity<false>(), C);
      std::shared_ptr<ScalarElementT<T>> Epressure = nullptr;
      if constexpr (PRESSURE) {
        Epressure = std::make_shared<ScalarElementT<T>>(problem.Pressure(), Esol);
      }
      rt_defect<PRESSURE, T>(problem, d1, d2, v, Esol, Epressure, C, problem.Levels().velocityLevel);
    }
    if constexpr (VERIFIED) {
      delta1 = sup(sqrt(PPM->Sum(abs(d1))));
      delta2 = sup(sqrt(PPM->Sum(abs(d2))));
    } else {
      delta1 = sqrt(PPM->Sum(abs(d1)));
      delta2 = sqrt(PPM->Sum(abs(d2)));
    }
  }

//  template
//  class RTDefectStokesAssembleDiffer<true, false>;
//
//  template
//  class RTDefectStokesAssembleDiffer<false, false>;

  template
  class RTDefectStokesAssembleDiffer<true, true>;

  template
  class RTDefectStokesAssembleDiffer<false, true>;

//----------------------------------------------------------------------------------------
// LagrangeDefectStokesAssemble
//----------------------------------------------------------------------------------------
  template<bool VERIFIED, bool PRESSURE>
  void LagrangeDefectStokesAssembleCoincide<VERIFIED, PRESSURE>::AssembleSystem(const cell &c,
                                                                                Matrix &systemMatrix,
                                                                                Vectors &rhs) const {
    const Cell &C = *c;
    DivergenceFreeElement Esol(problem.Velocity<false>(), C);
    std::unique_ptr<ScalarElement> Epressure = nullptr;
    if constexpr (PRESSURE) {
      Epressure = std::make_unique<ScalarElement>(problem.Pressure(), Esol);
    }
    VectorFieldElement E(rhs, Esol);
    RowValuesVector rowValues(rhs, E);
    RowEntries rowEntries(systemMatrix, E);
    for (int q = 0; q < E.nQ(); ++q) {
      //assembling rhs
      VectorField Uomega = problem.VF_U(E.QPoint(q))
                           + Esol.VelocityField(q, problem.Velocity<false>())
                           - Esol.VelocityField(q, problem.ApprV());
      Tensor Domega = Esol.VelocityFieldGradient(q, problem.Velocity<false>())
                      - Esol.VelocityFieldGradient(q, problem.ApprV());
      Tensor DUomega = problem.VFG_U(E.QPoint(q)) + Domega;
      VectorField s = (DUomega * Uomega);
      if constexpr (PRESSURE) {
        s += Epressure->Derivative(q, problem.Pressure());
      }
      s *= problem.Re() * problem.template C2sqr<double>();
      for (int n = 0; n < rhs.size(); ++n) {
        for (int i = 0; i < E.size(); ++i) {
          for (int k = 0; k < 2; ++k) {
            rowValues[n](i, k) += E.QWeight(q) * (Domega[n] * E.VectorComponentValue(q, i, k)
                                                  + s[n] * E.Divergence(q, i, k));
          }
        }
      }
      //assembling system matrix
      for (int i = 0; i < E.size(); ++i) {
        for (int j = 0; j < E.size(); ++j) {
          for (int k = 0; k < 2; ++k) {
            for (int l = 0; l < 2; ++l) {
              rowEntries(i, j, k, l) +=
                  E.QWeight(q) * (E.VectorComponentValue(q, i, k) * E.VectorComponentValue(q, j, l)
                                  + problem.template C2sqr<double>()
                                    * E.Divergence(q, i, k) * E.Divergence(q, j, l));
            }
          }
        }
      }
    }
  }

  template<bool VERIFIED, bool PRESSURE>
  void LagrangeDefectStokesAssembleCoincide<VERIFIED, PRESSURE>::Defect(double &delta1, double &delta2,
                                                                        const Vectors &v) const {
    using T = IAEigenvalueType<VERIFIED>;
    T d1{};
    T d2{};
    for (cell c = problem.Velocity<false>().cells(); c != problem.Velocity<false>().cells_end(); ++c) {
      const Cell &C = *c;
      DivergenceFreeElementT<T> Esol(problem.Velocity<false>(), C);
      std::unique_ptr<ScalarElementT<T>> Epressure = nullptr;
      if constexpr (PRESSURE) {
        Epressure = std::make_unique<ScalarElementT<T>>(problem.Pressure(), Esol);
      }
      VectorFieldElementT<T> E(v, Esol);
      for (auto [q, weight, point] : E.QuadWithPoint()) {
        VectorFieldT<T> Uomega = problem.VF_U(point)
                                 + Esol.VelocityField(q, problem.Velocity<false>())
                                 - Esol.VelocityField(q, problem.ApprV());
        TensorT<T> Domega = Esol.VelocityFieldGradient(q, problem.Velocity<false>())
                            - Esol.VelocityFieldGradient(q, problem.ApprV());
        TensorT<T> DUomega = problem.VFG_U(point) + Domega;
        TensorT<T> rhoDomega(E.VectorValue(q, v[0]),
                             E.VectorValue(q, v[1]));
        rhoDomega -= Domega;
        VectorFieldT<T> s = DUomega * Uomega;
        if constexpr (PRESSURE) {
          s += Epressure->Derivative(q, problem.Pressure());
        }
        s *= problem.Reynolds<T>();
        s -= VectorFieldT<T>(E.Divergence(q, v[0]),
                             E.Divergence(q, v[1]));
        d1 += weight * Frobenius(rhoDomega, rhoDomega);
        d2 += weight * (s * s);
      }
    }
    if constexpr (VERIFIED) {
      delta1 = sup(sqrt(PPM->Sum(abs(d1))));
      delta2 = sup(sqrt(PPM->Sum(abs(d2))));
    } else {
      delta1 = sqrt(PPM->Sum(abs(d1)));
      delta2 = sqrt(PPM->Sum(abs(d2)));
    }
  }

//  template
//  class LagrangeDefectStokesAssembleCoincide<true, false>;
//
//  template
//  class LagrangeDefectStokesAssembleCoincide<false, false>;

  template
  class LagrangeDefectStokesAssembleCoincide<true, true>;

  template
  class LagrangeDefectStokesAssembleCoincide<false, true>;

  template<bool PRESSURE>
  void lagrange_rhs(const NavierStokesProblem &problem, Vectors &rhs,
                         DivergenceFreeElement &Esol, std::shared_ptr<ScalarElement> Epressure,
                         const Cell &C, int level) {
    if (level < problem.Levels().velocityDefectLevel) {
      for (int i = 0; i < C.Children(); ++i) {
        auto childCell = problem.GetMesh(level + 1).find_cell(C.Child(i));
        if (childCell == problem.GetMesh(level + 1).cells_end()) THROW("Child not found");
        lagrange_rhs<PRESSURE>(problem, rhs, Esol, Epressure, *childCell, level + 1);
      }
    } else {
      VectorFieldElement  E(rhs, C);
      RowValuesVector rowValues(rhs, E);
      for (int q = 0; q < E.nQ(); ++q) {
        Point qPointLocal = Esol.GlobalToLocal(E.QPoint(q));
        VectorField Uomega = problem.VF_U(E.QPoint(q))
                             + Esol.VelocityField(qPointLocal, problem.Velocity<false>())
                             - Esol.VelocityField(qPointLocal, problem.ApprV());
        Tensor Domega = Esol.VelocityFieldGradient(qPointLocal, problem.Velocity<false>())
                        - Esol.VelocityFieldGradient(qPointLocal, problem.ApprV());
        Tensor DUomega = problem.VFG_U(E.QPoint(q)) + Domega;
        VectorField s = (DUomega * Uomega);
        if constexpr (PRESSURE) {
          s += Epressure->Derivative(qPointLocal, problem.Pressure());
        }
        s *= problem.Re() * problem.template C2sqr<double>();
        for (int n = 0; n < rhs.size(); ++n) {
          for (int i = 0; i < E.size(); ++i) {
            for (int k = 0; k < 2; ++k) {
              rowValues[n](i, k) += E.QWeight(q) * (Domega[n] * E.VectorComponentValue(q, i, k)
                                                    + s[n] * E.Divergence(q, i, k));
            }
          }
        }
      }
    }
  }

  template<bool VERIFIED, bool PRESSURE>
  void LagrangeDefectStokesAssembleDiffer<VERIFIED, PRESSURE>::AssembleSystem(Matrix &systemMatrix,
                                                                              Vectors &rhs) const {
    rhs = 0;
    systemMatrix = 0;
    TRY {
      //assembling rhs
      for (cell c = problem.Velocity<false>().cells(); c != problem.Velocity<false>().cells_end(); ++c) {
        const Cell &C = *c;
        DivergenceFreeElement Esol(problem.Velocity<false>(), C);
        std::shared_ptr<ScalarElement> Epressure = nullptr;
        if constexpr (PRESSURE) {
          Epressure = std::make_shared<ScalarElement>(problem.Pressure(), Esol);
        }
        lagrange_rhs<PRESSURE>(problem, rhs, Esol, Epressure, C, problem.Levels().velocityLevel);
      }
      //assembling system matrix
      for (cell c = systemMatrix.cells(); c != systemMatrix.cells_end(); ++c) {
        VectorFieldElement E(systemMatrix, *c);
        RowEntries rowEntries(systemMatrix, E);
        for (int q = 0; q < E.nQ(); ++q) {
          for (int i = 0; i < E.size(); ++i) {
            for (int j = 0; j < E.size(); ++j) {
              for (int k = 0; k < 2; ++k) {
                for (int l = 0; l < 2; ++l) {
                  rowEntries(i, j, k, l) +=
                      E.QWeight(q) * (E.VectorComponentValue(q, i, k) * E.VectorComponentValue(q, j, l)
                                      + problem.template C2sqr<double>()
                                        * E.Divergence(q, i, k) * E.Divergence(q, j, l));
                }
              }
            }
          }
        }
      }
    } CATCH ("Error in Residual")
    rhs.ClearDirichletValues();
    rhs.Collect();
    systemMatrix.ClearDirichletValues();
  }

  template<bool PRESSURE, typename T>
  void lagrange_defect(const NavierStokesProblem &problem, T &d1, T &d2, const Vectors &v,
                         DivergenceFreeElementT<T> &Esol, std::shared_ptr<ScalarElementT<T>> Epressure,
                         const Cell &C, int level) {
    if (level < problem.Levels().velocityDefectLevel) {
      for (int i = 0; i < C.Children(); ++i) {
        auto childCell = problem.GetMesh(level + 1).find_cell(C.Child(i));
        if (childCell == problem.GetMesh(level + 1).cells_end()) THROW("Child not found");
        lagrange_defect<PRESSURE, T>(problem, d1, d2, v, Esol, Epressure, *childCell, level + 1);
      }
    } else {
      VectorFieldElementT<T> E(v, C);
      for (auto [q, weight, point] : E.QuadWithPoint()) {
        PointT<T> qPointLocal = Esol.GlobalToLocal(point);
        VectorFieldT<T> Uomega = problem.VF_U(point)
                                 + Esol.VelocityField(qPointLocal, problem.Velocity<false>())
                                 - Esol.VelocityField(qPointLocal, problem.ApprV());
        TensorT<T> Domega = Esol.VelocityFieldGradient(qPointLocal, problem.Velocity<false>())
                            - Esol.VelocityFieldGradient(qPointLocal, problem.ApprV());
        TensorT<T> DUomega = problem.VFG_U(point) + Domega;
        TensorT<T> rhoDomega(E.VectorValue(q, v[0]),
                             E.VectorValue(q, v[1]));
        rhoDomega -= Domega;
        VectorFieldT<T> s = DUomega * Uomega;
        if constexpr (PRESSURE) {
          s += Epressure->Derivative(q, problem.Pressure());
        }
        s *= problem.Reynolds<T>();
        s -= VectorFieldT<T>(E.Divergence(q, v[0]),
                             E.Divergence(q, v[1]));
        d1 += E.QWeight(q) * Frobenius(rhoDomega, rhoDomega);
        d2 += E.QWeight(q) * (s * s);
      }
    }
  }

  template<bool VERIFIED, bool PRESSURE>
  void LagrangeDefectStokesAssembleDiffer<VERIFIED, PRESSURE>::Defect(double &delta1, double &delta2,
                                                                      const Vectors &v) const {
    using T = IAEigenvalueType<VERIFIED>;
    T d1{};
    T d2{};
    for (cell c = problem.Velocity<false>().cells(); c != problem.Velocity<false>().cells_end(); ++c) {
      const Cell &C = *c;
      DivergenceFreeElementT<T> Esol(problem.Velocity<false>(), C);
      std::shared_ptr<ScalarElementT<T>> Epressure = nullptr;
      if constexpr (PRESSURE) {
        Epressure = std::make_shared<ScalarElementT<T>>(problem.Pressure(), Esol);
      }
      lagrange_defect<PRESSURE, T>(problem, d1, d2, v, Esol, Epressure, C, problem.Levels().velocityLevel);
    }
    if constexpr (VERIFIED) {
      delta1 = sup(sqrt(PPM->Sum(abs(d1))));
      delta2 = sup(sqrt(PPM->Sum(abs(d2))));
    } else {
      delta1 = sqrt(PPM->Sum(abs(d1)));
      delta2 = sqrt(PPM->Sum(abs(d2)));
    }
  }

//  template
//  class LagrangeDefectStokesAssembleDiffer<false, false>;
//
//  template
//  class LagrangeDefectStokesAssembleDiffer<true, false>;

  template
  class LagrangeDefectStokesAssembleDiffer<false, true>;

  template
  class LagrangeDefectStokesAssembleDiffer<true, true>;

} // namespace navierstokes
