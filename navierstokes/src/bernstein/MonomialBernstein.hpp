#ifndef MONOMIALBERNSTEIN_HPP
#define MONOMIALBERNSTEIN_HPP

#include <functional>

template<typename T>
struct COMP {
  T v, vp2, vp3, vp4, vp5;

  COMP(const T &value) : v(value) {
    vp2 = pow(value, 2);
    vp3 = pow(value, 3);
    vp4 = pow(value, 4);
    vp5 = pow(value, 5);
  }
};

template<typename T>
struct VALUES {
  COMP<T> X;
  COMP<T> Y;

  VALUES(const T &value_x, const T &value_y) : X(value_x), Y(value_y) {}
};

//----------------------------------------------------------------------------------------
// Bernstein coefficients (deg = 8) for monomials
//----------------------------------------------------------------------------------------
// deg = 0
template<typename T>
std::vector<T> B8_Xp0_Yp0(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C) {
  return std::vector<T>(45, T(1.0));
}

// deg = 1
template<typename T>
std::vector<T> B8_Xp0_Yp1(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp1_Yp0(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 2
template<typename T>
std::vector<T> B8_Xp0_Yp2(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp1_Yp1(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp2_Yp0(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 3
template<typename T>
std::vector<T> B8_Xp0_Yp3(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp1_Yp2(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp2_Yp1(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp3_Yp0(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 4
template<typename T>
std::vector<T> B8_Xp0_Yp4(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp1_Yp3(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp2_Yp2(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp3_Yp1(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp4_Yp0(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 5
template<typename T>
std::vector<T> B8_Xp0_Yp5(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp1_Yp4(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp2_Yp3(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp3_Yp2(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp4_Yp1(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp5_Yp0(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 6
template<typename T>
std::vector<T> B8_Xp1_Yp5(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp2_Yp4(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp3_Yp3(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp4_Yp2(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp5_Yp1(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 7
template<typename T>
std::vector<T> B8_Xp2_Yp5(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp3_Yp4(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp4_Yp3(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp5_Yp2(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 8
template<typename T>
std::vector<T> B8_Xp3_Yp5(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp4_Yp4(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B8_Xp5_Yp3(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

//----------------------------------------------------------------------------------------
// Bernstein coefficients (deg = 9) for monomials
//----------------------------------------------------------------------------------------
// deg = 0
template<typename T>
std::vector<T> B9_Xp0_Yp0(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C) {
  return std::vector<T>(55, T(1.0));
}

// deg = 1
template<typename T>
std::vector<T> B9_Xp0_Yp1(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp1_Yp0(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 2
template<typename T>
std::vector<T> B9_Xp0_Yp2(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp1_Yp1(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp2_Yp0(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 3
template<typename T>
std::vector<T> B9_Xp0_Yp3(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp1_Yp2(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp2_Yp1(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp3_Yp0(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 4
template<typename T>
std::vector<T> B9_Xp0_Yp4(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp1_Yp3(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp2_Yp2(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp3_Yp1(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp4_Yp0(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 5
template<typename T>
std::vector<T> B9_Xp0_Yp5(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp1_Yp4(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp2_Yp3(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp3_Yp2(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp4_Yp1(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp5_Yp0(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 6
template<typename T>
std::vector<T> B9_Xp1_Yp5(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp2_Yp4(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp3_Yp3(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp4_Yp2(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp5_Yp1(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 7
template<typename T>
std::vector<T> B9_Xp2_Yp5(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp3_Yp4(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp4_Yp3(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp5_Yp2(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 8
template<typename T>
std::vector<T> B9_Xp3_Yp5(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp4_Yp4(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp5_Yp3(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

// deg = 9
template<typename T>
std::vector<T> B9_Xp4_Yp5(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

template<typename T>
std::vector<T> B9_Xp5_Yp4(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C);

#endif //MONOMIALBERNSTEIN_HPP
