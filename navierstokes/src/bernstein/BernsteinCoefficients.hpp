#ifndef BERNSTEINCOEFFICIENTS_H
#define BERNSTEINCOEFFICIENTS_H


template<typename T>
std::vector<T> B4_TMP_x(const std::vector<T> &c, const T &sqrt2) {
  return std::vector<T>{
      c[1],
      c[1] + c[3] / 4,
      c[1] + c[4] / 4,
      5 * (c[6] - c[0]) - 2 * (c[1] + c[7]) - (c[3] - c[9]) / 4,
      c[1] + c[3] / 4 + ((-5 * (c[4] / 2 + c[8]) - 11 * c[2] + c[10]) / 2 - 8 * c[18]) / 3,
      ((-5 * (c[1] + c[13]) - c[4] + c[16]) / 2 - 8 * c[20]) / 3,
      c[7] - c[9] / 4,
      5 * (c[6] - c[0]) - 2 * (c[1] + c[7]) +
      ((5 * (c[2] - c[10] / 2) + c[4] + 11 * c[8]) / 2 + 8 * c[18]) / 3 - (c[3] - c[9]) / 4,
      -5 * c[0] - 2 * c[2] + ((9 * c[13] - c[5] - c[10]) / 2 + 5 * (c[6] + c[12])) / 2 +
      ((((c[9] + 5 * c[11] + 5 * c[15] + c[17]) / 2 +
         (-7 * (c[7] + c[14]) + 17 * c[8] - 5 * c[16])) / 2 + (5 * c[1] + c[4])) / 2 -
       4 * (c[19] * sqrt2 - 2 * c[20])) / 3,
      c[13] - c[16] / 4,
      c[7],
      c[7] - (c[9] - c[10]) / 4,
      ((c[16] - c[10]) / 2 + 5 * (c[6] - c[12])) / 2 +
      (((5 * (c[9] - c[15]) + c[11] - c[17]) / 2 + (-17 * (c[7] + c[13]) + 7 * (c[8] + c[14]))) /
       4 + 4 * c[19] * sqrt2) / 3,
      c[13] + (c[15] - c[16]) / 4,
      c[13]
  };
}

template<typename T>
std::vector<T> B4_TMP_y(const std::vector<T> &c, const T &sqrt2) {
  return std::vector<T>{
      c[2],
      c[2] + c[4] / 4,
      c[2] + c[5] / 4,
      ((-5 * (c[2] + c[8]) - c[4] + c[10]) / 2 - 8 * c[18]) / 3,
      ((-5 * (c[4] / 2 + c[13]) - 11 * c[1] + c[16]) / 2 - 8 * c[20]) / 3 + c[2] + c[5] / 4,
      5 * (c[12] - c[0]) - 2 * (c[2] + c[14]) - (c[5] - c[17]) / 4,
      c[8] - c[10] / 4,
      -5 * c[0] - 2 * c[1] + ((9 * c[8] - c[3] - c[16]) / 2 + 5 * (c[6] + c[12])) / 2 +
      ((((c[9] + 5 * (c[11] + c[15]) + c[17]) / 2 + (17 * c[13] - 7 * (c[7] + c[14]) - 5 * c[10])) /
        2 + (5 * c[2] + c[4])) / 2 + 4 * (2 * c[18] - c[19] * sqrt2)) / 3,
      5 * (c[12] - c[0]) - 2 * (c[2] + c[14]) + (c[17] - c[5]) / 4 +
      ((-5 * c[16] / 2 + (5 * c[1] + c[4] + 11 * c[13])) / 2 + 8 * c[20]) / 3,
      c[14] - c[17] / 4,
      c[8],
      c[8] - (c[10] - c[11]) / 4,
      ((c[10] - c[16]) / 2 + 5 * (c[12] - c[6])) / 2 +
      (((c[15] - c[9] + 5 * (c[17] - c[11])) / 2 + (7 * (c[7] + c[13]) - 17 * (c[8] + c[14]))) / 4 +
       4 * c[19] * sqrt2) / 3,
      c[14] + (c[16] - c[17]) / 4,
      c[14]
  };
}

template<typename T>
std::vector<T> B3_TMP_xx(const std::vector<T> &c, const T &sqrt2) {
  return std::vector<T>{
      c[3],
      2 * (10 * (c[6] - c[0]) - 6 * c[1] - c[3] - 4 * c[7]) + c[9],
      (-22 * c[2] - 8 * c[4] - 10 * c[8] + 2 * c[10] - 32 * c[18]) / 3 + c[3],
      2 * (10 * (c[0] - c[6]) + 4 * c[1] + 6 * c[7] - c[9]) + c[3],
      2 * (10 * (c[6] - c[0]) - 6 * c[1] - c[3] - 4 * c[7]) + c[9] +
      (32 * (c[2] + c[8] + 2 * c[18]) + 7 * (c[4] - c[10])) / 3,
      10 * (c[6] + c[12]) - 20 * c[0] - 8 * c[2] - c[5] - c[10] +
      ((c[9] + 5 * (c[11] + c[15]) + c[17]) / 2 +
       (20 * c[1] + 4 * c[4] + 17 * c[8] + 37 * c[13] - 7 * (c[7] + c[14] + c[16]) -
        16 * c[19] * sqrt2 + 64 * c[20])) / 3,
      c[9],
      2 * (10 * (c[0] - c[6]) + 4 * c[1] + 6 * c[7] - c[9]) + c[3] +
      2 * (4 * c[10] - 5 * c[2] - c[4] - 11 * c[8] - 16 * c[18]) / 3,
      2 * (10 * (c[0] - c[12]) + 4 * c[2]) + c[5] +
      (2 * (c[9] - c[4] - c[11] - 5 * (c[1] + c[7] + c[8]) - 22 * c[13] + 7 * c[14] + 4 * c[16]) -
       5 * c[15] - c[17] + 32 * (c[19] * sqrt2 - c[20])) / 3,
      c[15]
  };
}

template<typename T>
std::vector<T> B3_TMP_xy(const std::vector<T> &c, const T &sqrt2) {
  return std::vector<T>{
      c[4],
      (-22 * c[2] - 5 * c[4] - 10 * c[8] + 2 * c[10] - 32 * c[18]) / 3,
      (-22 * c[1] - 5 * c[4] - 10 * c[13] + 2 * c[16] - 32 * c[20]) / 3,
      (10 * c[2] + 2 * c[4] + 22 * c[8] - 5 * c[10] + 32 * c[18]) / 3,
      10 * (c[12] - 2 * c[0] + c[6]) + 9 * (c[8] + c[13]) - c[3] - c[5] +
      ((c[9] + 5 * (c[11] + c[15]) + c[17]) / 2 +
       (-2 * (c[1] + c[2]) + 7 * (c[4] - c[7] - c[14]) - 5 * (c[10] + c[16]) +
        16 * (2 * (c[18] + c[20]) - c[19] * sqrt2))) / 3,
      (10 * c[1] + 2 * c[4] + 22 * c[13] - 5 * c[16] + 32 * c[20]) / 3,
      c[10],
      10 * (2 * c[0] - c[6] - c[12]) + 8 * c[1] - 5 * c[8] + c[3] + c[16] +
      ((c[11] - c[9] - 5 * c[15] - c[17]) / 2 +
       (-10 * c[2] + 2 * (c[10] - c[4]) + 7 * (c[7] + c[14]) - 17 * c[13] +
        16 * (c[19] * sqrt2 - 2 * c[18]))) / 3,
      10 * (2 * c[0] - c[6] - c[12]) + 8 * c[2] - 5 * c[13] + c[5] + c[10] +
      ((c[15] - c[9] - 5 * c[11] - c[17]) / 2 +
       (-10 * c[1] + 2 * (c[16] - c[4]) + 7 * (c[7] + c[14]) - 17 * c[8] +
        16 * (c[19] * sqrt2 - 2 * c[20]))) / 3,
      c[16]
  };
}

template<typename T>
std::vector<T> B3_TMP_yy(const std::vector<T> &c, const T &sqrt2) {
  return std::vector<T>{
      c[5],
      c[5] + 2 * (-11 * c[1] - 4 * c[4] - 5 * c[13] + c[16] - 16 * c[20]) / 3,
      2 * (10 * (c[12] - c[0]) - 6 * c[2] - 4 * c[14] - c[5]) + c[17],
      2 * (-10 * c[0] - 4 * c[1] + 5 * (c[6] + c[12]))
      + (20 * c[2] + 4 * c[4] - 7 * (c[7] + c[10] + c[14]) + 37 * c[8] + 17 * c[13] + 64 * c[18]
         - 16 * c[19] * sqrt2 + (c[9] + 5 * (c[11] + c[15]) + c[17]) / 2) / 3 - c[3] - c[16],
      2 * (10 * (c[12] - c[0]) - 6 * c[2] - c[5] - 4 * c[14])
      + (7 * (c[4] - c[16]) + 32 * (c[1] + c[13] + 2 * c[20])) / 3 + c[17],
      2 * (10 * (c[0] - c[12]) + 4 * c[2] + 6 * c[14] - c[17]) + c[5],
      c[11],
      4 * (5 * (c[0] - c[6]) + 2 * c[1])
      + (-10 * (c[2] + c[13] + c[14]) + 14 * c[7] - 44 * c[8] - c[9] + 8 * c[10] - 5 * c[11]
         - 2 * (c[4] + c[15] - c[17]) - 32 * (c[18] - c[19] * sqrt2)) / 3 + c[3],
      2 * (10 * (c[0] - c[12]) + 4 * c[2] + 6 * c[14] - c[17]
           + (-5 * c[1] - c[4] - 11 * c[13] + 4 * c[16] - 16 * c[20]) / 3) + c[5],
      c[17]
  };
}

template<typename T>
std::vector<T> B3ToB8(const std::vector<T> &coeff) {
  return std::vector<T>{
      coeff[0],
      (5 * coeff[0] + 3 * coeff[1]) / 8,
      (5 * coeff[0] + 3 * coeff[2]) / 8,
      (5 * coeff[0] + 3 * (5 * coeff[1] + coeff[3]) / 2) / 14,
      (5 * coeff[0] + (15 * (coeff[1] + coeff[2]) / 2 + 3 * coeff[4]) / 2) / 14,
      (5 * coeff[0] + 3 * (5 * coeff[2] + coeff[5]) / 2) / 14,
      (5 * (coeff[0] + 3 * coeff[1]) + (15 * coeff[3] + coeff[6]) / 2) / 28,
      (5 * coeff[1] + (5 * (coeff[0] + coeff[2] + coeff[4]) + (5 * coeff[3] + coeff[7]) / 2) / 2) / 14,
      (5 * coeff[2] + (5 * (coeff[0] + coeff[1] + coeff[4]) + (5 * coeff[5] + coeff[8]) / 2) / 2) / 14,
      (5 * (coeff[0] + 3 * coeff[2]) + (15 * coeff[5] + coeff[9]) / 2) / 28,
      (3 * (coeff[1] + coeff[3]) + (coeff[0] + coeff[6]) / 2) / 7,
      (coeff[0] + 3 * (coeff[3] + coeff[4]) + (9 * coeff[1] + 3 * coeff[2] + (coeff[6] + 3 * coeff[7]) / 2) / 2) / 14,
      (2 * coeff[4] + (coeff[0] + 3 * (coeff[1] + coeff[2]) + coeff[3] + coeff[5] + (coeff[7] + coeff[8]) / 2) / 2) / 7,
      (coeff[0] + 3 * (coeff[4] + coeff[5]) + (3 * coeff[1] + 9 * coeff[2] + (3 * coeff[8] + coeff[9]) / 2) / 2) / 14,
      (3 * (coeff[2] + coeff[5]) + (coeff[0] + coeff[9]) / 2) / 7,
      ((coeff[0] + 15 * coeff[1]) / 2 + 5 * (3 * coeff[3] + coeff[6])) / 28,
      (3 * coeff[1] + 3 * coeff[4] + coeff[6] + (9 * coeff[3] + 3 * coeff[7] + (coeff[0] + 3 * coeff[2]) / 2) / 2) / 14,
      ((coeff[0] + 3 * (3 * (coeff[1] + coeff[3]) + coeff[5] + coeff[8]) + coeff[6]) / 2 + 3 * (coeff[2] + 3 * coeff[4] + coeff[7])) / 28,
      (3 * (coeff[1] + 3 * coeff[4] + coeff[8]) + (coeff[0] + 3 * (3 * (coeff[2] + coeff[5]) + coeff[3] + coeff[7]) + coeff[9]) / 2) / 28,
      (((coeff[0] + 3 * coeff[1]) / 2 + 9 * coeff[5] + 3 * coeff[8]) / 2 + 3 * (coeff[2] + coeff[4]) + coeff[9]) / 14,
      ((coeff[0] + 15 * coeff[2]) / 2 + 5 * (3 * coeff[5] + coeff[9])) / 28,
      (3 * (coeff[1] + 5 * coeff[3]) / 2 + 5 * coeff[6]) / 14,
      (((5 * coeff[1] + coeff[2]) / 2 + 5 * (coeff[4] + coeff[6] + coeff[7])) / 2 + 5 * coeff[3]) / 14,
      (((coeff[2] + coeff[5]) / 2 + coeff[1] + 3 * (coeff[3] + coeff[7]) + coeff[6] + coeff[8]) / 2 + 2 * coeff[4]) / 7,
      (3 * (coeff[3] + 3 * coeff[4] + coeff[5]) + (3 * (coeff[1] + coeff[2] + 3 * (coeff[7] + coeff[8])) + coeff[6] + coeff[9]) / 2) / 28,
      (2 * coeff[4] + (coeff[2] + coeff[7] + 3 * (coeff[5] + coeff[8]) + coeff[9] + (coeff[1] + coeff[3]) / 2) / 2) / 7,
      (((coeff[1] + 5 * coeff[2]) / 2 + 5 * (coeff[4] + coeff[8] + coeff[9])) / 2 + 5 * coeff[5]) / 14,
      (3 * (coeff[2] + 5 * coeff[5]) / 2 + 5 * coeff[9]) / 14,
      (3 * coeff[3] + 5 * coeff[6]) / 8,
      ((15 * (coeff[3] + coeff[7]) / 2 + 3 * coeff[4]) / 2 + 5 * coeff[6]) / 14,
      (5 * coeff[7] + (5 * (coeff[3] + coeff[4] + coeff[6]) + (coeff[5] + 5 * coeff[8]) / 2) / 2) / 14,
      (3 * (coeff[4] + coeff[8]) + coeff[6] + (3 * (coeff[3] + 3 * coeff[7]) + (3 * coeff[5] + coeff[9]) / 2) / 2) / 14,
      (((3 * coeff[3] + coeff[6]) / 2 + 3 * (coeff[5] + 3 * coeff[8])) / 2 + 3 * (coeff[4] + coeff[7]) + coeff[9]) / 14,
      (((coeff[3] + 5 * coeff[7]) / 2 + 5 * (coeff[4] + coeff[5] + coeff[9])) / 2 + 5 * coeff[8]) / 14,
      ((3 * coeff[4] + 15 * (coeff[5] + coeff[8]) / 2) / 2 + 5 * coeff[9]) / 14,
      (3 * coeff[5] + 5 * coeff[9]) / 8,
      coeff[6],
      (5 * coeff[6] + 3 * coeff[7]) / 8,
      (5 * coeff[6] + 3 * (5 * coeff[7] + coeff[8]) / 2) / 14,
      (5 * (coeff[6] + 3 * coeff[7]) + (15 * coeff[8] + coeff[9]) / 2) / 28,
      (3 * (coeff[7] + coeff[8]) + (coeff[6] + coeff[9]) / 2) / 7,
      ((coeff[6] + 15 * coeff[7]) / 2 + 5 * (3 * coeff[8] + coeff[9])) / 28,
      (3 * (coeff[7] + 5 * coeff[8]) / 2 + 5 * coeff[9]) / 14,
      (3 * coeff[8] + 5 * coeff[9]) / 8,
      coeff[9]
  };
}

template<typename T>
std::vector<T> B4ToB9(const std::vector<T> &coeff) {
  return std::vector<T>{
    coeff[0],
    (5 * coeff[0] + 4 * coeff[1]) / 9,
    (5 * coeff[0] + 4 * coeff[2]) / 9,
    (5 * (coeff[0] / 2 + coeff[1]) / 3 + coeff[3] / 2) / 3,
    (5 * (coeff[0] + coeff[1] + coeff[2]) / 3 + coeff[4]) / 6,
    (5 * (coeff[0] / 2 + coeff[2]) / 3 + coeff[5] / 2) / 3,
    (5 * ((coeff[0] / 2 + 2 * coeff[1]) / 3 + coeff[3] / 2) + coeff[6] / 3) / 7,
    (5 * (coeff[0] + coeff[3]) / 2 + 10 * (2 * coeff[1] + coeff[2]) / 3 + 5 * coeff[4] + coeff[7]) / 21,
    (5 * (coeff[0] + coeff[5]) / 2 + 10 * (coeff[1] + 2 * coeff[2]) / 3 + 5 * coeff[4] + coeff[8]) / 21,
    (5 * ((coeff[0] / 2 + 2 * coeff[2]) / 3 + coeff[5] / 2) + coeff[9] / 3) / 7,
    (((5 * coeff[0] + coeff[10]) / 2 + 10 * (2 * coeff[1] + coeff[6])) / 3 + 10 * coeff[3]) / 21,
    (((5 * (coeff[0] + coeff[6]) + coeff[11]) / 2 + 5 * coeff[2]) / 3 + 5 * coeff[7] / 2 + 5 * (coeff[3] + coeff[4] + coeff[1])) / 21,
    ((5 * coeff[0] + coeff[12]) / 2 + 5 * (2 * (2 * coeff[4] + coeff[1] + coeff[2]) + coeff[3] + coeff[5] + coeff[7] + coeff[8])) / 63,
    (((5 * (coeff[0] + coeff[9]) + coeff[13]) / 2 + 5 * coeff[1]) / 3 + 5 * coeff[8] / 2 + 5 * (coeff[2] + coeff[4] + coeff[5])) / 21,
    (((5 * coeff[0] + coeff[14]) / 2 + 10 * (2 * coeff[2] + coeff[9])) / 3 + 10 * coeff[5]) / 21,
    (((coeff[0] + 5 * coeff[10]) / 2 + 10 * (coeff[1] + 2 * coeff[6])) / 3 + 10 * coeff[3]) / 21,
    ((((coeff[0] + coeff[10]) / 2 + 2 * (coeff[2] + coeff[11] + 4 * (coeff[1] + coeff[6]))) / 3 + 4 * (coeff[4] + coeff[7])) / 3 + 2 * coeff[3]) / 7,
    (((coeff[0] / 2 + 4 * coeff[2] + 2 * coeff[6] + coeff[11]) / 3 + 2 * (coeff[1] + 2 * coeff[7] + coeff[8]) + coeff[5] + coeff[12] / 2) / 3 + coeff[3] + 2 * coeff[4]) / 7,
    (((coeff[0] / 2 + 4 * coeff[1] + 2 * coeff[9] + coeff[13]) / 3 + 2 * (coeff[2] + coeff[7] + 2 * coeff[8]) + coeff[3] + coeff[12] / 2) / 3 + 2 * coeff[4] + coeff[5]) / 7,
    ((((coeff[0] + coeff[14]) / 2 + 2 * (coeff[1] + coeff[13] + 4 * (coeff[2] + coeff[9]))) / 3 + 4 * (coeff[4] + coeff[8])) / 3 + 2 * coeff[5]) / 7,
    (((coeff[0] + 5 * coeff[14]) / 2 + 10 * (coeff[2] + 2 * coeff[9])) / 3 + 10 * coeff[5]) / 21,
    (coeff[1] / 3 + 5 * (coeff[3] / 2 + (2 * coeff[6] + coeff[10] / 2) / 3)) / 7,
    (((5 * (coeff[1] + coeff[10]) + coeff[2]) / 2 + 5 * coeff[11]) / 3 + 5 * coeff[4] / 2 + 5 * (coeff[3] + coeff[6] + coeff[7])) / 21,
    (((coeff[10] / 2 + 4 * coeff[11] + 2 * coeff[1] + coeff[2]) / 3 + 2 * (2 * coeff[4] + coeff[6] + coeff[8]) + coeff[12] + coeff[5] / 2) / 3 + coeff[3] + 2 * coeff[7]) / 7,
    ((coeff[1] + coeff[2] + coeff[6] + coeff[9] + coeff[11] + coeff[13]) / 3 + coeff[3] + coeff[5] + coeff[12] + 3 * (coeff[4] + coeff[7] + coeff[8])) / 14,
    (((coeff[14] / 2 + coeff[1] + 4 * coeff[13] + 2 * coeff[2]) / 3 + 2 * (2 * coeff[4] + coeff[7] + coeff[9]) + coeff[12] + coeff[3] / 2) / 3 + coeff[5] + 2 * coeff[8]) / 7,
    (((coeff[1] + 5 * (coeff[2] + coeff[14])) / 2 + 5 * coeff[13]) / 3 + 5 * coeff[4] / 2 + 5 * (coeff[5] + coeff[8] + coeff[9])) / 21,
    (coeff[2] / 3 + 5 * (coeff[5] / 2 + (2 * coeff[9] + coeff[14] / 2) / 3)) / 7,
    (coeff[3] / 2 + 5 * (coeff[6] + coeff[10] / 2) / 3) / 3,
    (5 * (coeff[3] + coeff[10]) / 2 + coeff[4] + 5 * coeff[7] + 10 * (2 * coeff[6] + coeff[11]) / 3) / 21,
    ((coeff[5] + 5 * coeff[10]) / 2 + 5 * (coeff[3] + coeff[4] + coeff[8] + coeff[12] + 2 * (coeff[11] + coeff[6] + 2 * coeff[7]))) / 63,
    (((coeff[10] / 2 + 4 * coeff[6] + 2 * coeff[13] + coeff[9]) / 3 + 2 * (coeff[4] + 2 * coeff[8] + coeff[11]) + coeff[3] + coeff[5] / 2) / 3 + 2 * coeff[7] + coeff[12]) / 7,
    (((coeff[14] / 2 + coeff[6] + 4 * coeff[9] + 2 * coeff[11]) / 3 + 2 * (2 * coeff[7] + coeff[13] + coeff[4]) + coeff[5] + coeff[3] / 2) / 3 + 2 * coeff[8] + coeff[12]) / 7,
    ((coeff[3] + 5 * coeff[14]) / 2 + 5 * (coeff[4] + coeff[5] + coeff[7] + coeff[12] + 2 * (2 * coeff[8] + coeff[9] + coeff[13]))) / 63,
    (coeff[4] + 5 * coeff[8] + 10 * (2 * coeff[9] + coeff[13]) / 3 + 5 * (coeff[14] + coeff[5]) / 2) / 21,
    (coeff[5] / 2 + 5 * (coeff[9] + coeff[14] / 2) / 3) / 3,
    (4 * coeff[6] + 5 * coeff[10]) / 9,
    (coeff[7] + 5 * (coeff[6] + coeff[10] + coeff[11]) / 3) / 6,
    (10 * (coeff[6] + 2 * coeff[11]) / 3 + 5 * coeff[7] + coeff[8] + 5 * (coeff[10] + coeff[12]) / 2) / 21,
    (((coeff[9] + 5 * (coeff[13] + coeff[10])) / 2 + 5 * coeff[6]) / 3 + 5 * coeff[8] / 2 + 5 * (coeff[7] + coeff[11] + coeff[12])) / 21,
    ((((coeff[10] + coeff[14]) / 2 + 2 * (coeff[6] + coeff[9] + 4 * (coeff[11] + coeff[13]))) / 3 + 4 * (coeff[7] + coeff[8])) / 3 + 2 * coeff[12]) / 7,
    (((coeff[6] + 5 * (coeff[11] + coeff[14])) / 2 + 5 * coeff[9]) / 3 + 5 * coeff[7] / 2 + 5 * (coeff[8] + coeff[12] + coeff[13])) / 21,
    (coeff[7] + 5 * coeff[8] + 10 * (coeff[9] + 2 * coeff[13]) / 3 + 5 * (coeff[12] + coeff[14]) / 2) / 21,
    (coeff[8] + 5 * (coeff[9] + coeff[13] + coeff[14]) / 3) / 6,
    (4 * coeff[9] + 5 * coeff[14]) / 9,
    coeff[10],
    (5 * coeff[10] + 4 * coeff[11]) / 9,
    (5 * (coeff[10] / 2 + coeff[11]) / 3 + coeff[12] / 2) / 3,
    (5 * ((coeff[10] / 2 + 2 * coeff[11]) / 3 + coeff[12] / 2) + coeff[13] / 3) / 7,
    (((5 * coeff[10] + coeff[14]) / 2 + 10 * (2 * coeff[11] + coeff[13])) / 3 + 10 * coeff[12]) / 21,
    (((coeff[10] + 5 * coeff[14]) / 2 + 10 * (coeff[11] + 2 * coeff[13])) / 3 + 10 * coeff[12]) / 21,
    (coeff[11] / 3 + 5 * (coeff[12] / 2 + (2 * coeff[13] + coeff[14] / 2) / 3)) / 7,
    (coeff[12] / 2 + 5 * (coeff[13] + coeff[14] / 2) / 3) / 3,
    (4 * coeff[13] + 5 * coeff[14]) / 9,
    coeff[14]
  };
}
#endif //BERNSTEINCOEFFICIENTS_H
