#include "MonomialBernstein.hpp"

#include "IAInterval.hpp"

template<typename T>
std::vector<T> EVALUATE(const COMP<T> &a0, const COMP<T> &a1,
                        const COMP<T> &b0, const COMP<T> &b1,
                        const COMP<T> &c0, const COMP<T> &c1,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1)> &F0,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1)> &F1,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1)> &F2,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1)> &F3,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1)> &F4,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1,
                                        const COMP<T> &c0, const COMP<T> &c1)> &F5,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1,
                                        const COMP<T> &c0, const COMP<T> &c1)> &F6,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1,
                                        const COMP<T> &c0, const COMP<T> &c1)> &F7,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1,
                                        const COMP<T> &c0, const COMP<T> &c1)> &F8,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1,
                                        const COMP<T> &c0, const COMP<T> &c1)> &F9,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1,
                                        const COMP<T> &c0, const COMP<T> &c1)> &F10,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1,
                                        const COMP<T> &c0, const COMP<T> &c1)> &F11) {
  return std::vector<T>{
    F0(a0, a1),
    F1(a0, a1, b0, b1),
    F1(a0, a1, c0, c1),
    F2(a0, a1, b0, b1),
    F5(a0, a1, b0, b1, c0, c1),
    F2(a0, a1, c0, c1),
    F3(a0, a1, b0, b1),
    F6(a0, a1, b0, b1, c0, c1),
    F6(a0, a1, c0, c1, b0, b1),
    F3(a0, a1, c0, c1),
    F4(a0, a1, b0, b1),
    F7(a0, a1, b0, b1, c0, c1),
    F8(a0, a1, b0, b1, c0, c1),
    F7(a0, a1, c0, c1, b0, b1),
    F4(a0, a1, c0, c1),
    F4(b0, b1, a0, a1),
    F9(a0, a1, b0, b1, c0, c1),
    F10(a0, a1, b0, b1, c0, c1),
    F10(a0, a1, c0, c1, b0, b1),
    F9(b0, b1, a0, a1, c0, c1),
    F4(c0, c1, a0, a1),
    F3(b0, b1, a0, a1),
    F7(b0, b1, a0, a1, c0, c1),
    F10(b0, b1, a0, a1, c0, c1),
    F11(a0, a1, b0, b1, c0, c1),
    F10(c0, c1, a0, a1, b0, b1),
    F7(c0, c1, a0, a1, b0, b1),
    F3(c0, c1, a0, a1),
    F2(b0, b1, a0, a1),
    F6(b0, b1, a0, a1, c0, c1),
    F8(b0, b1, a0, a1, c0, c1),
    F10(b0, b1, c0, c1, a0, a1),
    F10(c0, c1, b0, b1, a0, a1),
    F8(c0, c1, a0, a1, b0, b1),
    F6(c0, c1, a0, a1, b0, b1),
    F2(c0, c1, a0, a1),
    F1(b0, b1, a0, a1),
    F5(b0, b1, a0, a1, c0, c1),
    F6(b0, b1, c0, c1, a0, a1),
    F7(b0, b1, c0, c1, a0, a1),
    F9(c0, c1, a0, a1, b0, b1),
    F7(c0, c1, b0, b1, a0, a1),
    F6(c0, c1, b0, b1, a0, a1),
    F5(c0, c1, a0, a1, b0, b1),
    F1(c0, c1, a0, a1),
    F0(b0, b1),
    F1(b0, b1, c0, c1),
    F2(b0, b1, c0, c1),
    F3(b0, b1, c0, c1),
    F4(b0, b1, c0, c1),
    F4(c0, c1, b0, b1),
    F3(c0, c1, b0, b1),
    F2(c0, c1, b0, b1),
    F1(c0, c1, b0, b1),
    F0(c0, c1)
  };
}

template<typename T>
std::vector<T> b9_0p0_1p1(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a1.v;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (8*a1.v + b1.v) / 9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (7*a1.v + 2*b1.v) / 9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (2*a1.v + b1.v) / 3;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (5*a1.v + 4*b1.v) / 9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (7*a1.v + b1.v + c1.v) / 9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (6*a1.v + 2*b1.v + c1.v) / 9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (5*a1.v + 3*b1.v + c1.v) / 9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (5*a1.v + 2*b1.v + 2*c1.v) / 9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (4*a1.v + 4*b1.v + c1.v) / 9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (4*a1.v + 3*b1.v + 2*c1.v) / 9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (a1.v + b1.v + c1.v) / 3;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp0_Yp1(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p0_1p1<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp0_Yp1(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p0_1p1<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp1_Yp0(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p0_1p1<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp1_Yp0(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p0_1p1<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b9_0p0_1p2(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a1.vp2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.v*( 7*a1.v  +  2*b1.v ) / 9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return 7*a1.vp2/12 + 7*b1.v*a1.v/18 + b1.vp2/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return 5*a1.vp2/12 + b1.v*a1.v/2 + b1.vp2/12;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return 5*a1.vp2/18 + 5*b1.v*a1.v/9 + b1.vp2/6;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 7*a1.vp2/12 + ( 7*b1.v  +  7*c1.v )*a1.v/36 + b1.v*c1.v/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.vp2/12 + ( 12*b1.v  +  6*c1.v )*a1.v/36 + b1.v*(b1.v +  2*c1.v )/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.vp2/18 + ( 15*b1.v  +  5*c1.v )*a1.v/36 + b1.v*(b1.v + c1.v)/12;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.vp2/18 + ( 10*b1.v  +  10*c1.v )*a1.v/36 + b1.vp2/36 + b1.v*c1.v/9 + c1.vp2/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp2/6 + ( 8*b1.v  +  2*c1.v )*a1.v/18 + b1.vp2/6 + b1.v*c1.v/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp2/6 + ( 12*b1.v  +  8*c1.v )*a1.v/36 + b1.vp2/12 + b1.v*c1.v/6 + c1.vp2/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp2/12 + ( 3*b1.v  +  3*c1.v )*a1.v/12 + b1.vp2/12 + b1.v*c1.v/4 + c1.vp2/12;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp0_Yp2(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p0_1p2<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp0_Yp2(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p0_1p2<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp2_Yp0(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p0_1p2<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp2_Yp0(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p0_1p2<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b9_0p1_1p1(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.v*a1.v;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (( 7*a0.v  + b0.v)*a1.v + a0.v*b1.v)/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (( 21*a0.v  +  7*b0.v )*a1.v + b1.v*(7*a0.v + b0.v))/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ( 5*a0.v  +  3*b0.v )*a1.v/12 + b1.v*(a0.v + b0.v/3)/4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (( 5*a0.v  +  5*b0.v )*a1.v + b1.v*(5*a0.v + 3*b0.v))/18;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (7*( 6*a0.v  +  b0.v  +  c0.v )*a1.v + 7*(b1.v  + c1.v )*a0.v + b0.v*c1.v + c0.v*b1.v)/72;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 5*a0.v  +  2*b0.v  +  c0.v )*a1.v/12 + ( 6*a0.v  + b0.v + c0.v)*b1.v/36 + (a0.v + b0.v/3)*c1.v/12;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (( 20*a0.v  +  15*b0.v  +  5*c0.v )*a1.v + 3*( 5*a0.v  +  2*b0.v  +  c0.v )*b1.v + (5*a0.v + 3*b0.v)*c1.v)/72;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (( 10*a0.v  +  5*b0.v  +  5*c0.v )*a1.v + ( 5*a0.v  + b0.v +  2*c0.v )*b1.v + (5*a0.v + 2*b0.v + c0.v)*c1.v)/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (( 3*a0.v  +  4*b0.v  + c0.v)*a1.v + ( 4*a0.v  +  3*b0.v  + c0.v)*b1.v + c1.v*(a0.v + b0.v))/18;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ( 6*a0.v  +  6*b0.v  +  4*c0.v )*a1.v/36 + ( 6*a0.v  +  3*b0.v  +  3*c0.v )*b1.v/36 + (a0.v + (3*b0.v + c0.v)/4)*c1.v/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ( 2*a0.v  +  3*b0.v  +  3*c0.v )*a1.v/24 + ( 3*a0.v  +  2*b0.v  +  3*c0.v )*b1.v/24 + (a0.v + b0.v + 2*c0.v/3)*c1.v/8;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp1_Yp1(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p1_1p1<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp1_Yp1(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p1_1p1<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<typename T>
std::vector<T> b9_0p0_1p3(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a1.vp3;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp2*(b1.v +  2*a1.v )/3;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (a1.v + b1.v)*( 5*a1.v  + b1.v)*a1.v/12;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return 5*a1.vp3/21 + 15*a1.vp2*b1.v/28 + 3*a1.v*b1.vp2/14 + b1.vp3/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return 5*a1.vp3/42 + 10*a1.vp2*b1.v/21 + 5*a1.v*b1.vp2/14 + b1.vp3/21;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.vp3/12 + ( b1.v  +  c1.v )*a1.vp2/4 + a1.v*b1.v*c1.v/12;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.vp3/21 + ( 30*b1.v  +  15*c1.v )*a1.vp2/84 + b1.v*(b1.v +  2*c1.v )*a1.v/14 + b1.vp2*c1.v/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.vp3/42 + ( 30*b1.v  +  10*c1.v )*a1.vp2/84 + 5*b1.v*(b1.v + c1.v)*a1.v/28 + b1.vp2*(b1.v +  3*c1.v )/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.vp3/42 + ( 20*b1.v  +  20*c1.v )*a1.vp2/84 + ( 5*b1.vp2  +  20*b1.v *c1.v +  5*c1.vp2 )*a1.v/84 + b1.v*c1.v*(b1.v + c1.v)/42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp3/21 + ( 4*b1.v  +  c1.v )*a1.vp2/14 + ( 12*b1.vp2  +  8*b1.v *c1.v)*a1.v/42 + b1.vp3/21 + b1.vp2*c1.v/14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp3/21 + (( 18*b1.v  +  12*c1.v )*a1.vp2 + ( 12*b1.vp2  +  24*b1.v *c1.v +  4*c1.vp2 )*a1.v + b1.v*(b1.vp2 +  6*b1.v *c1.v +  3*c1.vp2 ))/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (a1.vp3 + 9*(b1.v  + c1.v )*a1.vp2 + 9*( b1.vp2  +  3*b1.v *c1.v + c1.vp2 )*a1.v + (b1.v + c1.v)*(b1.vp2 +  8*b1.v *c1.v + c1.vp2))/84;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp0_Yp3(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p0_1p3<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp0_Yp3(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p0_1p3<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp3_Yp0(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p0_1p3<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp3_Yp0(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p0_1p3<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b9_0p1_1p2(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.v*a1.vp2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (( 6*a0.v  + b0.v)*a1.vp2 + 2*a0.v*a1.v*b1.v)/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ( 15*a0.v  +  6*b0.v )*a1.vp2/36 + b1.v*(a0.v + b0.v/6)*a1.v/3 + a0.v*b1.vp2/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ( 20*a0.v  +  15*b0.v )*a1.vp2/84 + b1.v*(5*a0.v + 2*b0.v)*a1.v/14 + b1.vp2*(a0.v + b0.v/6)/14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ( 15*a0.v  +  20*b0.v )*a1.vp2/126 + b1.v*(20*a0.v + 15*b0.v)*a1.v/63 + b1.vp2*(5*a0.v + 2*b0.v)/42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 5*a0.v  +  b0.v  +  c0.v )*a1.vp2/12 + (6*( b1.v  + c1.v )*a0.v + b0.v*c1.v + c0.v*b1.v)*a1.v/36 + a0.v*b1.v*c1.v/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 60*a0.v  +  30*b0.v  +  15*c0.v )*a1.vp2/252 + (12*( 5*a0.v  +  b0.v  +  c0.v )*b1.v +  c1.v *(30*a0.v + 12*b0.v))*a1.v/252 + (b1.v*(a0.v + c0.v/6) +  (2*a0.v + b0.v/3) *c1.v)*b1.v/42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 30*a0.v  +  30*b0.v  +  10*c0.v )*a1.vp2/252 + (( 60*a0.v  +  30*b0.v  +  15*c0.v )*b1.v +  (20*a0.v + 15*b0.v)*c1.v)*a1.v/252 + ((5*a0.v + b0.v + c0.v)*b1.v + c1.v*(5*a0.v + 2*b0.v))*b1.v/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 30*a0.v  +  20*b0.v  +  20*c0.v )*a1.vp2/252 + (( 40*a0.v  +  10*b0.v  +  20*c0.v )*b1.v +  (40*a0.v + 20*b0.v + 10*c0.v) *c1.v)*a1.v/252 + ( 5*a0.v  +  2*c0.v )*b1.vp2/252 + (5*a0.v + b0.v + c0.v)*c1.v*b1.v/63 + (5*a0.v + 2*b0.v)*c1.vp2/252;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 6*a0.v  +  12*b0.v  +  3*c0.v )*a1.vp2/126 + (( 24*a0.v  +  24*b0.v  +  8*c0.v )*b1.v +  (6*a0.v + 8*b0.v) *c1.v)*a1.v/126 + 2*((a0.v + b0.v/2 + c0.v/4)*b1.v + 2*(a0.v/3 + b0.v/4)*c1.v)*b1.v/21;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ( 12*a0.v  +  18*b0.v  +  12*c0.v )*a1.vp2/252 + (( 36*a0.v  +  24*b0.v  +  24*c0.v )*b1.v +  (24*a0.v + 24*b0.v + 8*c0.v) *c1.v)*a1.v/252 + ( 12*a0.v  +  3*b0.v  +  6*c0.v )*b1.vp2/252 + (2*a0.v + b0.v + c0.v/2)*c1.v*b1.v/21 + (a0.v + 3*b0.v/4)*c1.vp2/63;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (a0.v +  3*b0.v  +  3*c0.v )*a1.vp2/84 + (( 6*a0.v  +  6*b0.v  +  9*c0.v )*b1.v +  (6*a0.v + 9*b0.v + 6*c0.v )*c1.v)*a1.v/84 + ( 3*a0.v  + b0.v +  3*c0.v )*b1.vp2/84 + (3*a0.v + 2*b0.v + 2*c0.v)*c1.v*b1.v/28 + (a0.v + b0.v + c0.v/3)*c1.vp2/28;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp1_Yp2(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p1_1p2<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp1_Yp2(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p1_1p2<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp2_Yp1(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p1_1p2<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp2_Yp1(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p1_1p2<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b9_0p0_1p4(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a1.vp4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp3*( 5*a1.v  +  4*b1.v )/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return 5*a1.vp4/18 + 5*a1.vp3*b1.v/9 + a1.vp2*b1.vp2/6;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.v*(5*a1.vp3 +  20*a1.vp2 *b1.v +  15*a1.v *b1.vp2 + 2*b1.vp3)/42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return 5*a1.vp4/126 + 20*a1.vp3*b1.v/63 + 10*a1.vp2*b1.vp2/21 + 10*a1.v*b1.vp3/63 + b1.vp4/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp2*(5*a1.vp2 + 5*(b1.v + c1.v)*a1.v + 3*b1.v*c1.v)/18;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v*(5*a1.vp3 + 20*(2*b1.v + c1.v)*a1.vp2/3 + 5*b1.v*(b1.v +  2*c1.v )*a1.v + 2*b1.vp2*c1.v)/42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.vp4/126 + ( 30*b1.v  +  10*c1.v )*a1.vp3/126 + 5*b1.v*(b1.v + c1.v)*a1.vp2/21 + 5*b1.vp2*(b1.v +  3*c1.v )*a1.v/126 + b1.vp3*c1.v/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.vp4/126 + ( 20*b1.v  +  20*c1.v )*a1.vp3/126 + ( 10*b1.vp2  +  40*b1.v *c1.v +  10*c1.vp2 )*a1.vp2/126 + 5*b1.v*c1.v*(b1.v + c1.v)*a1.v/63 + b1.vp2*c1.vp2/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a1.vp4 + ( 16*b1.v  +  4*c1.v )*a1.vp3 + ( 36*b1.vp2  +  24*b1.v *c1.v)*a1.vp2 + ( 16*b1.vp3  +  24*b1.vp2 *c1.v)*a1.v + b1.vp3*(b1.v +  4*c1.v ))/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp4/126 + ( 12*b1.v  +  8*c1.v )*a1.vp3/126 + ( 18*b1.vp2  +  36*b1.v *c1.v +  6*c1.vp2 )*a1.vp2/126 + ( 4*b1.vp3  +  24*b1.vp2 *c1.v +  12*b1.v *c1.vp2)*a1.v/126 + b1.vp3*c1.v/63 + b1.vp2*c1.vp2/42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ((b1.v + c1.v)*a1.vp3 + ( 3*b1.vp2  +  9*b1.v *c1.v +  3*c1.vp2 )*a1.vp2 + (b1.v + c1.v)*(b1.vp2 +  8*b1.v *c1.v + c1.vp2)*a1.v + b1.v*c1.v*(b1.vp2 +  3*b1.v *c1.v + c1.vp2))/42;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp0_Yp4(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p0_1p4<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp0_Yp4(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p0_1p4<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp4_Yp0(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p0_1p4<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp4_Yp0(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p0_1p4<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b9_0p1_1p3(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.v*a1.vp3;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ( 5*a0.v  + b0.v)*a1.vp3/9 + a0.v*a1.vp2*b1.v/3;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.v*(5*(b0.v/2 + a0.v)*a1.vp2 + 3*(5*a0.v + b0.v)*b1.v*a1.v/2 + 3*a0.v*b1.vp2/2)/18;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ( 10*a0.v  +  10*b0.v )*a1.vp3/84 + 5*b1.v*(b0.v/2 + a0.v)*a1.vp2/14 + (5*a0.v + b0.v)*b1.vp2*a1.v/28 + a0.v*b1.vp3/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ( 5*a0.v  +  10*b0.v )*a1.vp3/126 + 5*b1.v*(a0.v + b0.v)*a1.vp2/21 + 5*b1.vp2*(b0.v/2 + a0.v)*a1.v/21 + (5*a0.v + b0.v)*b1.vp3/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.v*((a0.v + (b0.v + c0.v)/4)*a1.vp2 + (3*(b1.v + c1.v)*a0.v/4 + 3*(b0.v*c1.v + c0.v*b1.v)/20)*a1.v + 3*a0.v*b1.v*c1.v/10)/18;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 30*a0.v  +  20*b0.v  +  10*c0.v )*a1.vp3/252 + (( 60*a0.v  +  15*b0.v  +  15*c0.v )*b1.v +  (15*b0.v + 30*a0.v) *c1.v)*a1.vp2/252 + (b1.v*(5*a0.v + c0.v) +  2*c1.v *(5*a0.v + b0.v))*b1.v*a1.v/84 + a0.v*b1.vp2*c1.v/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 20*a0.v  +  30*b0.v  +  10*c0.v )*a1.vp3/504 + (( 90*a0.v  +  60*b0.v  +  30*c0.v )*b1.v +  30*c1.v *(a0.v + b0.v))*a1.vp2/504 + 5*((a0.v + (b0.v + c0.v)/4)*b1.v + (b0.v/2 + a0.v)*c1.v)*b1.v*a1.v/42 + (b1.v*(5*a0.v + c0.v) +  3*c1.v *(5*a0.v + b0.v))*b1.vp2/504;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 10*a0.v  +  10*b0.v  +  10*c0.v )*a1.vp3/252 + (( 30*a0.v  +  10*b0.v  +  20*c0.v )*b1.v +  (30*a0.v + 20*b0.v + 10*c0.v )*c1.v)*a1.vp2/252 + (( 10*a0.v  +  5*c0.v )*b1.vp2 +  (40*a0.v + 10*b0.v + 10*c0.v) *b1.v*c1.v +  (5*b0.v + 10*a0.v) *c1.vp2)*a1.v/252 + (b1.v*(5*a0.v + c0.v) + c1.v*(5*a0.v + b0.v))*b1.v*c1.v/252;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a0.v +  4*b0.v  + c0.v)*a1.vp3/126 + (( 12*a0.v  +  18*b0.v  +  6*c0.v )*b1.v +  3*c1.v *(a0.v +  2*b0.v ))*a1.vp2/126 + ((a0.v + 2*b0.v/3 + c0.v/3)*b1.v + 2*c1.v*(a0.v + b0.v)/3)*b1.v*a1.v/7 + 2*b1.vp2*((a0.v + (b0.v + c0.v)/4)*b1.v + 3*(b0.v/2 + a0.v)*c1.v/2)/63;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ( 2*a0.v  +  6*b0.v  +  4*c0.v )*a1.vp3/252 + (( 18*a0.v  +  18*b0.v  +  18*c0.v )*b1.v +  (12*a0.v + 18*b0.v + 6*c0.v) *c1.v)*a1.vp2/252 + (( 18*a0.v  +  6*b0.v  +  12*c0.v )*b1.vp2 +  (36*a0.v + 24*b0.v + 12*c0.v) *c1.v*b1.v +  6*c1.vp2 *(a0.v + b0.v))*a1.v/252 + b1.v*(b1.vp2*(c0.v/2 + a0.v) +  (6*a0.v + 3*(b0.v + c0.v)/2) *b1.v*c1.v +  3*(b0.v/2 + a0.v) *c1.vp2)/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (b0.v + c0.v)*a1.vp3/168 + (( 3*a0.v  +  6*b0.v  +  9*c0.v )*b1.v +  3*(a0.v + 3*b0.v  + 2*c0.v ) *c1.v)*a1.vp2/168 + (( 6*a0.v  +  3*b0.v  +  9*c0.v )*b1.vp2 +  18*c1.v *(a0.v + b0.v + c0.v)*b1.v +  (6*a0.v + 9*b0.v + 3*c0.v) *c1.vp2)*a1.v/168 + (a0.v + c0.v)*b1.vp3/168 + (3*a0.v + b0.v + 2*c0.v)*c1.v*b1.vp2/56 + (3*a0.v + 2*b0.v + c0.v)*c1.vp2*b1.v/56 + c1.vp3*(a0.v + b0.v)/168;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp1_Yp3(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p1_1p3<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp1_Yp3(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p1_1p3<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp3_Yp1(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p1_1p3<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp3_Yp1(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p1_1p3<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b9_0p2_1p2(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp2*a1.vp2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((5*a0.v + 2*b0.v)*a1.v + 2*a0.v*b1.v)*a1.v*a0.v/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ( 10*a0.vp2  +  10*a0.v *b0.v + b0.vp2)*a1.vp2/36 + b1.v*a0.v*(5*a0.v + 2*b0.v)*a1.v/18 + a0.vp2*b1.vp2/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ( 10*a0.vp2  +  20*a0.v *b0.v +  5*b0.vp2 )*a1.vp2/84 + 5*b1.v*(a0.vp2 + a0.v*b0.v + b0.vp2/10)*a1.v/21 + b1.vp2*a0.v*(5*a0.v + 2*b0.v)/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ( 5*a0.vp2  +  20*a0.v *b0.v +  10*b0.vp2 )*a1.vp2/126 + 10*(a0.vp2 +  2*a0.v *b0.v + b0.vp2/2)*b1.v*a1.v/63 + 5*b1.vp2*(a0.vp2 + a0.v*b0.v + b0.vp2/10)/63;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 10*a0.vp2  + 5*( b0.v  +  c0.v )*a0.v + c0.v*b0.v)*a1.vp2/36 + (5*(b1.v + c1.v)*a0.v + 2*b0.v*c1.v + 2*c0.v*b1.v)*a0.v*a1.v/36 + a0.vp2*b1.v*c1.v/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 30*a0.vp2  + ( 40*b0.v  +  20*c0.v )*a0.v +  5*b0.v *(b0.v +  2*c0.v ))*a1.vp2/252 + (( 40*b1.v  +  20*c1.v )*a0.vp2 + (( 20*b0.v  +  20*c0.v )*b1.v +  20*b0.v *c1.v)*a0.v +  2*b0.vp2 *c1.v +  4*b0.v *b1.v*c0.v)*a1.v/252 + (5*(b1.v +  2*c1.v )*a0.v + 4*b0.v*c1.v + 2*c0.v*b1.v)*a0.v*b1.v/252;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 10*a0.vp2  + ( 30*b0.v  +  10*c0.v )*a0.v +  10*b0.v *(b0.v + c0.v))*a1.vp2/252 + (( 30*b1.v  +  10*c1.v )*a0.vp2 + ( (40*b0.v  +  20*c0.v) *b1.v +  20*b0.v *c1.v)*a0.v +  5*b0.v*(b1.v*(b0.v + 2*c0.v) + b0.v*c1.v))*a1.v/252 + 5*b1.v*((b1.v + c1.v)*a0.vp2 + ((b0.v + c0.v)*b1.v/2 + b0.v*c1.v)*a0.v + b0.v*(b0.v*c1.v + b1.v*c0.v)/10)/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 30*a0.vp2  + ( 60*b0.v  +  60*c0.v )*a0.v +  10*b0.vp2  +  40*c0.v *b0.v +  10*c0.vp2 )*a1.vp2/756 + (( 60*b1.v  +  60*c1.v )*a0.vp2 + (( 40*b0.v  +  80*c0.v )*b1.v +  (40*c0.v + 80*b0.v) *c1.v)*a0.v + ( 20*b0.v *c0.v +  10*c0.vp2 )*b1.v +  10*b0.v *c1.v*(b0.v +  2*c0.v ))*a1.v/756 + ( 10*b1.vp2  +  40*b1.v *c1.v +  10*c1.vp2 )*a0.vp2/756 + ( 10*b1.vp2 *c0.v +  20*c1.v *(b0.v + c0.v)*b1.v +  10*b0.v *c1.vp2)*a0.v/756 + b0.vp2*c1.vp2/756 + b0.v*b1.v*c0.v*c1.v/189 + b1.vp2*c0.vp2/756;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a0.vp2 + ( 8*b0.v  +  2*c0.v )*a0.v +  6*b0.vp2  +  4*c0.v *b0.v)*a1.vp2/126 + (( 8*a0.vp2  + ( 24*b0.v  +  8*c0.v )*a0.v +  8*b0.v *(b0.v + c0.v))*b1.v +  2*c1.v *(a0.vp2 +  4*a0.v *b0.v +  2*b0.vp2 ))*a1.v/126 + ((a0.vp2 + (4*b0.v + 2*c0.v)*a0.v/3 + b0.v*(b0.v +  2*c0.v )/6)*b1.v + (2*a0.vp2 +  4*a0.v *b0.v + b0.vp2)*c1.v/3)*b1.v/21;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ( 2*a0.vp2  + ( 12*b0.v  +  8*c0.v )*a0.v +  6*b0.vp2  +  12*c0.v *b0.v +  2*c0.vp2 )*a1.vp2/252 + (( 12*b1.v  +  8*c1.v )*a0.vp2 + (( 24*b0.v  +  24*c0.v )*b1.v +  c1.v *(24*b0.v + 8*c0.v))*a0.v + ( 4*b0.vp2  +  16*b0.v *c0.v +  4*c0.vp2 )*b1.v +  8*b0.v *c1.v*(b0.v + c0.v))*a1.v/252 + ( 6*b1.vp2  +  12*b1.v *c1.v +  2*c1.vp2 )*a0.vp2/252 + (( 4*b0.v  +  8*c0.v )*b1.vp2 +  (8*c0.v + 16*b0.v) *c1.v*b1.v +  4*b0.v *c1.vp2)*a0.v/252 + ( 2*b0.v *c0.v + c0.vp2)*b1.vp2/252 + b0.v*c1.v*(b0.v +  2*c0.v )*b1.v/126 + b0.vp2*c1.vp2/252;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ((b0.v + c0.v)*a0.v + b0.vp2 +  3*c0.v *b0.v + c0.vp2)*a1.vp2/84 + ((a0.vp2 + ( 4*b0.v  +  6*c0.v )*a0.v + b0.vp2 +  6*c0.v *b0.v +  3*c0.vp2 )*b1.v + (a0.vp2 + ( 6*b0.v  +  4*c0.v )*a0.v +  3*b0.vp2  +  6*c0.v *b0.v + c0.vp2)*c1.v)*a1.v/84 + (a0.vp2 + (b0.v +  3*c0.v )*a0.v + c0.v*(b0.v + c0.v))*b1.vp2/84 + (a0.vp2 + ( 2*b0.v  +  2*c0.v )*a0.v + (b0.vp2 + 4*c0.v*b0.v + c0.vp2)/3)*c1.v*b1.v/28 + (a0.vp2 + ( 3*b0.v  + c0.v)*a0.v + b0.v*(b0.v + c0.v))*c1.vp2/84;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp2_Yp2(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p2_1p2<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp2_Yp2(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p2_1p2<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

//TODO: 0_5
template<typename T>
std::vector<T> b9_0p0_1p5(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a1.vp5;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp4*(4*a1.v + 5*b1.v)/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp5/6 + 5*a1.vp4*b1.v/9 + 5*a1.vp3*b1.vp2/18;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp5/21 + 5*a1.vp4*b1.v/14 + 10*a1.vp3*b1.vp2/21 + 5*a1.vp2*b1.vp3/42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.v*(a1.vp4 + 20*a1.vp3*b1.v + 60*a1.vp2*b1.vp2 + 40*a1.v*b1.vp3 + 5*b1.vp4)/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp5/6 + 5*((b1.v + c1.v)*a1.vp4 + a1.vp3*b1.v*c1.v)/18;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a1.vp3 + 5*(b1.v + c1.v/2)*a1.vp2 + 10*b1.v*(b1.v + 2*c1.v)*a1.v/3 + 5*b1.vp2*c1.v/2)*a1.vp2/21;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a1.vp4 + (15*b1.v + 5*c1.v)*a1.vp3 + 30*b1.v*(b1.v + c1.v)*a1.vp2 + 10*b1.vp2*(b1.v + 3*c1.v)*a1.v + 5*b1.vp3*c1.v)*a1.v/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a1.vp4 + 10*(b1.v + c1.v)*a1.vp3 + 10*(b1.vp2 + 4*b1.v*c1.v + c1.vp2)*a1.vp2 + 20*b1.v*c1.v*(b1.v + c1.v)*a1.v + 5*b1.vp2*c1.vp2)*a1.v/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (4*b1.v + c1.v)*a1.vp4/126 + (24*b1.vp2 + 16*b1.v*c1.v)*a1.vp3/126 + (24*b1.vp3 + 36*b1.vp2*c1.v)*a1.vp2/126 + 2*b1.vp3*(b1.v + 4*c1.v)*a1.v/63 + b1.vp4*c1.v/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (3*b1.v + 2*c1.v)*a1.vp4/126 + (12*b1.vp2 + 24*b1.v*c1.v + 4*c1.vp2)*a1.vp3/126 + (6*b1.vp3 + 36*b1.vp2*c1.v + 18*b1.v*c1.vp2)*a1.vp2/126 + (4*b1.v + 6*c1.v)*c1.v*b1.vp2*a1.v/63 + b1.vp3*c1.vp2/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (b1.vp2 + 3*b1.v*c1.v + c1.vp2)*a1.vp3/42 + (b1.v + c1.v)*(b1.vp2 + 8*b1.v*c1.v + c1.vp2)*a1.vp2/42 + (3*b1.vp3*c1.v + 9*b1.vp2*c1.vp2 + 3*b1.v*c1.vp3)*a1.v/42 + b1.vp2*c1.vp2*(b1.v + c1.v)/42;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp0_Yp5(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p0_1p5<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp0_Yp5(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p0_1p5<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp5_Yp0(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p0_1p5<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp5_Yp0(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p0_1p5<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b9_0p1_1p4(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.v*a1.vp4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((4*a0.v + b0.v)*a1.v + 4*a0.v*b1.v)*a1.vp3/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a0.v + 2*b0.v/3)*a1.vp2 + b1.v*(8*a0.v + 2*b0.v)*a1.v/3 + a0.v*b1.vp2)*a1.vp2/6;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.v*((a0.v + 3*b0.v/2)*a1.vp3 +  6*b1.v *(a0.v + 2*b0.v/3)*a1.vp2 +  6*b1.vp2 *(a0.v + b0.v/4)*a1.v + a0.v*b1.vp3)/21;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (a0.v +  4*b0.v )*a1.vp4/126 + 8*b1.v*(a0.v + 3*b0.v/2)*a1.vp3/63 + 2*b1.vp2*(a0.v + 2*b0.v/3)*a1.vp2/7 + 8*b1.vp3*(a0.v + b0.v/4)*a1.v/63 + a0.v*b1.vp4/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp2*((a0.v + (b0.v + c0.v)/3)*a1.vp2 + (4*(b1.v + c1.v)*a0.v + b0.v*c1.v + c0.v*b1.v)*a1.v/3 + a0.v*b1.v*c1.v)/6;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v*((a0.v + b0.v + c0.v/2)*a1.vp3 + (( 4*a0.v  + 4*(b0.v + c0.v)/3)*b1.v +  2*(a0.v + 2*b0.v/3) *c1.v)*a1.vp2 +  2*(b1.v*(a0.v + c0.v/4) + 2* (a0.v + b0.v/4) *c1.v )*b1.v*a1.v + a0.v*b1.vp2*c1.v)/21;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a0.v +  3*b0.v  + c0.v)*a1.vp4/126 + (( 12*a0.v  +  12*b0.v  +  6*c0.v )*b1.v +  4*c1.v *(a0.v + 3*b0.v/2))*a1.vp3/126 + ((a0.v + (b0.v + c0.v)/3)*b1.v + (a0.v + 2*b0.v/3)*c1.v)*b1.v*a1.vp2/7 + 2*(b1.v*(a0.v + c0.v/4) +  3*(a0.v + b0.v/4) *c1.v)*b1.vp2*a1.v/63 + a0.v*b1.vp3*c1.v/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a0.v +  2*b0.v  +  2*c0.v )*a1.vp4/126 + (( 8*a0.v  +  4*b0.v  +  8*c0.v )*b1.v +  (8*a0.v + 8*b0.v + 4*c0.v) *c1.v)*a1.vp3/126 + (( 6*a0.v  +  4*c0.v )*b1.vp2 +  (24*a0.v + 8*b0.v + 8*c0.v) *c1.v*b1.v +  6*c1.vp2 *(a0.v + 2*b0.v/3))*a1.vp2/126 + 4*(b1.v*(a0.v + c0.v/4) + (a0.v + b0.v/4)*c1.v)*b1.v*c1.v*a1.v/63 + a0.v*b1.vp2*c1.vp2/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 4*b0.v  + c0.v)*a1.vp4/630 + (( 16*a0.v  +  48*b0.v  +  16*c0.v )*b1.v +  4*(a0.v + 4*b0.v ) *c1.v)*a1.vp3/630 + 4*((a0.v + b0.v + c0.v/2)*b1.v + c1.v*(2*a0.v/3 + b0.v))*b1.v*a1.vp2/35 + 8*((a0.v + (b0.v + c0.v)/3)*b1.v + (3*a0.v/2 + b0.v)*c1.v)*b1.vp2*a1.v/105 + 2*(b1.v*(a0.v + c0.v/4) +  (4*a0.v + b0.v) *c1.v)*b1.vp3/315;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ( 3*b0.v  +  2*c0.v )*a1.vp4/630 + (( 12*a0.v  +  24*b0.v  +  24*c0.v )*b1.v +  8*(a0.v + 3*b0.v  + c0.v) *c1.v)*a1.vp3/630 + (( 36*a0.v  +  18*b0.v  +  36*c0.v )*b1.vp2 +  (72*a0.v + 72*b0.v + 36*c0.v) *c1.v*b1.v +  (12*a0.v + 18*b0.v) *c1.vp2)*a1.vp2/630 + 2*((a0.v + 2*c0.v/3)*b1.vp2 +  (6*a0.v + 2*b0.v + 2*c0.v) *c1.v*b1.v +  c1.vp2 *(3*a0.v + 2*b0.v))*b1.v*a1.v/105 + (b1.v*(4*a0.v + c0.v) + 3*(4*a0.v + b0.v)*c1.v/2)*b1.vp2*c1.v/315;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (( 2*b0.v  +  3*c0.v )*b1.v +  (3*b0.v + 2*c0.v )*c1.v)*a1.vp3/210 + (( 3*a0.v  +  3*b0.v  +  9*c0.v )*b1.vp2 +  (9*a0.v + 18*b0.v  + 18*c0.v)  *c1.v*b1.v +  3*c1.vp2 *(a0.v +  3*b0.v  + c0.v))*a1.vp2/210 + (( 2*a0.v  +  3*c0.v )*b1.vp3 +  (18*a0.v + 9*b0.v + 18*c0.v) *c1.v*b1.vp2 +  (18*a0.v + 18*b0.v + 9*c0.v) *c1.vp2*b1.v +  (2*a0.v + 3*b0.v) *c1.vp3)*a1.v/210 + b1.v*((a0.v + 2*c0.v/3)*b1.vp2 +  (3*a0.v + b0.v + c0.v) *c1.v*b1.v + c1.vp2*(a0.v + 2*b0.v/3))*c1.v/70;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp1_Yp4(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p1_1p4<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp1_Yp4(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p1_1p4<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp4_Yp1(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p1_1p4<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp4_Yp1(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p1_1p4<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b9_0p2_1p3(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp2*a1.vp3;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp2*a0.v*((2*b0.v + 4*a0.v)*a1.v + 3*a0.v*b1.v)/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a0.vp2 + 4*a0.v*b0.v/3 + b0.vp2/6)*a1.vp2 +  b1.v *a0.v*(b0.v + 2*a0.v)*a1.v + a0.vp2*b1.vp2/2)*a1.v/6;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ( 4*a0.vp2  +  12*a0.v *b0.v +  4*b0.vp2 )*a1.vp3/84 + b1.v*(3*a0.vp2 + 4*a0.v*b0.v + b0.vp2/2)*a1.vp2/14 + b1.vp2*a0.v*(b0.v/2 + a0.v)*a1.v/7 + a0.vp2*b1.vp3/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (a0.vp2 +  8*a0.v *b0.v +  6*b0.vp2 )*a1.vp3/126 + 2*b1.v*(a0.vp2 +  3*a0.v *b0.v + b0.vp2)*a1.vp2/21 + b1.vp2*(a0.vp2 + 4*a0.v*b0.v/3 + b0.vp2/6)*a1.v/7 + b1.vp3*a0.v*(b0.v + 2*a0.v)/63;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v*((a0.vp2 + 2*(b0.v + c0.v)*a0.v/3 + c0.v*b0.v/6)*a1.vp2 + a0.v*((b1.v + c1.v)*a0.v + b0.v*c1.v/2 + c0.v*b1.v/2)*a1.v + a0.vp2*b1.v*c1.v/2)/6;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 12*a0.vp2  + ( 24*b0.v  +  12*c0.v )*a0.v +  4*b0.v *(b0.v +  2*c0.v ))*a1.vp3/252 + (( 36*b1.v  +  18*c1.v )*a0.vp2 + (( 24*b0.v  +  24*c0.v )*b1.v +  24*b0.v *c1.v)*a0.v +  3*b0.vp2 *c1.v +  6*b0.v *b1.v*c0.v)*a1.vp2/252 + ((b1.v +  2*c1.v )*a0.v + b0.v*c1.v + c0.v*b1.v/2)*a0.v*b1.v*a1.v/21 + a0.vp2*b1.vp2*c1.v/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (2*a0.vp2 + (12*b0.v + 4*c0.v)*a0.v + (6*b0.v)*(b0.v + c0.v))*a1.vp3/252 + ((18*b1.v + 6*c1.v)*a0.vp2 + ((36*b0.v + 18*c0.v)*b1.v + (18*b0.v)*c1.v)*a0.v + (6*b0.v)*(b1.v*(b0.v + 2*c0.v) + b0.v*c1.v))*a1.vp2/252 + ((b1.v + c1.v)*a0.vp2 + (((2*b0.v)/3 + (2*c0.v)/3)*b1.v + (4*b0.v)*c1.v/3)*a0.v + b0.v*(b0.v*c1.v + b1.v*c0.v)/6)*b1.v*a1.v/14 + b1.vp2*a0.v*((b1.v + 3*c1.v)*a0.v + (3*b0.v)*c1.v/2 + b1.v*c0.v/2)/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 2*a0.vp2  + ( 8*b0.v  +  8*c0.v )*a0.v +  2*b0.vp2  +  8*c0.v *b0.v +  2*c0.vp2 )*a1.vp3/252 + (( 12*b1.v  +  12*c1.v )*a0.vp2 + (( 12*b0.v  +  24*c0.v )*b1.v +  (12*c0.v + 24*b0.v) *c1.v)*a0.v + ( 8*b0.v *c0.v +  4*c0.vp2 )*b1.v +  4*b0.v *c1.v*(b0.v +  2*c0.v ))*a1.vp2/252 + (( 6*b1.vp2  +  24*b1.v *c1.v +  6*c1.vp2 )*a0.vp2 + ( 8*b1.vp2 *c0.v +  16*c1.v *(b0.v + c0.v)*b1.v +  8*b0.v *c1.vp2)*a0.v + b0.vp2*c1.vp2 +  4*b0.v *b1.v*c0.v*c1.v + b1.vp2*c0.vp2)*a1.v/252 + a0.v*b1.v*((b1.v + c1.v)*a0.v + (b0.v*c1.v + c0.v*b1.v)/2)*c1.v/63;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (( 8*b0.v  +  2*c0.v )*a0.v +  12*b0.vp2  +  8*c0.v *b0.v)*a1.vp3/630 + (( 12*a0.vp2  + ( 72*b0.v  +  24*c0.v )*a0.v +  36*b0.v *(b0.v + c0.v))*b1.v +  3*c1.v *(a0.vp2 +  8*a0.v *b0.v +  6*b0.vp2 ))*a1.vp2/630 + 2*((a0.vp2 + ( 2*b0.v  + c0.v)*a0.v + b0.v*(b0.v +  2*c0.v )/3)*b1.v + 2*c1.v*(a0.vp2 +  3*a0.v *b0.v + b0.vp2)/3)*b1.v*a1.v/35 + 2*((a0.vp2 + 2*(b0.v + c0.v)*a0.v/3 + c0.v*b0.v/6)*b1.v + (3*a0.vp2 + 4*a0.v*b0.v + b0.vp2/2)/2*c1.v)*b1.vp2/105;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
   return ((12*b0.v + 8*c0.v)*a0.v + 12*b0.vp2 + (24*c0.v)*b0.v + 4*c0.vp2)*a1.vp3/1260 + ((18*a0.vp2 + (72*b0.v + 72*c0.v)*a0.v + 18*b0.vp2 + (72*c0.v)*b0.v + 18*c0.vp2)*b1.v + (12*c1.v)*(a0.vp2 + (6*b0.v + 2*c0.v)*a0.v + (3*b0.v)*(b0.v + c0.v)))*a1.vp2/1260 + ((36*a0.vp2 + (36*b0.v + 72*c0.v)*a0.v + (24*c0.v)*b0.v + 12*c0.vp2)*b1.vp2 + 72*(a0.vp2 + (2*b0.v + c0.v)*a0.v + b0.v*(b0.v + 2*c0.v)/3)*c1.v*b1.v + (12*c1.vp2)*(a0.vp2 + (3*a0.v)*b0.v + b0.vp2))*a1.v/1260 + b1.v*(b1.vp2*(a0.vp2 + 4*c0.v*a0.v/3 + c0.vp2/6) + (6*b1.v)*(a0.vp2 + ((2*b0.v)/3 + (2*c0.v)/3)*a0.v + c0.v*b0.v/6)*c1.v + (3*c1.vp2)*(a0.vp2 + 4*a0.v*b0.v/3 + b0.vp2/6))/210;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (b0.vp2 + 3*c0.v*b0.v + c0.vp2)*a1.vp3/420 + (((6*b0.v + 9*c0.v)*a0.v + 3*b0.vp2 + 18*c0.v*b0.v + 9*c0.vp2)*b1.v + 9*c1.v*((b0.v + 2*c0.v/3)*a0.v + b0.vp2 + 2*c0.v*b0.v + c0.vp2/3))*a1.vp2/420 + ((3*a0.vp2 + (6*b0.v + 18*c0.v)*a0.v + 9*c0.v*(b0.v + c0.v))*b1.vp2 + 9*c1.v*(a0.vp2 + (4*b0.v + 4*c0.v)*a0.v + b0.vp2 + (4*c0.v)*b0.v + c0.vp2)*b1.v + (3*c1.vp2)*(a0.vp2 + (6*b0.v + 2*c0.v)*a0.v + (3*b0.v)*(b0.v + c0.v)))*a1.v/420 + (a0.vp2 + (3*c0.v)*a0.v + c0.vp2)*b1.vp3/420 + 3*c1.v*(a0.vp2 + (b0.v + 2*c0.v)*a0.v + (2*b0.v + c0.v)*c0.v/3)*b1.vp2/140 + 3*(a0.vp2 + (2*b0.v + c0.v)*a0.v + b0.v*(b0.v + 2*c0.v)/3)*c1.vp2*b1.v/140 + c1.vp3*(a0.vp2 + 3*a0.v*b0.v + b0.vp2)/420;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp2_Yp3(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p2_1p3<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp2_Yp3(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p2_1p3<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp3_Yp2(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p2_1p3<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp3_Yp2(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p2_1p3<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b9_0p1_1p5(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.v*a1.vp5;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (( 3*a0.v  + b0.v)*a1.vp5 + 5*a0.v*a1.vp4*b1.v) / 9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (a1.vp2*(a0.v + b0.v) +  5*b1.v *(a0.v + b0.v/3)*a1.v + 10*a0.v*b1.vp2/3)*a1.vp3/12;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (a1.vp3*(a0.v +  3*b0.v ) +  15*b1.v *(a0.v + b0.v)*a1.vp2 +  30*b1.vp2 *(a0.v + b0.v/3)*a1.v +  10*a0.v *b1.vp3)*a1.vp2/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.v*(a1.vp4*b0.v + 5*a1.vp3*(a0.v +  3*b0.v )*b1.v + 30*a1.vp2 *(a0.v + b0.v)*b1.vp2 +  a1.v *(30*a0.v + 10*b0.v)*b1.vp3 + 5*a0.v*b1.vp4)/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp3*((a0.v + (b0.v + c0.v)/2)*a1.vp2 + (5*(b1.v + c1.v)*a0.v/2 + 5*(b0.v*c1.v + c0.v*b1.v)/6)*a1.v + 10*a0.v*b1.v*c1.v/3)/12;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp2*((a0.v +  2*b0.v  + c0.v)*a1.vp3 + (( 10*a0.v  +  5*b0.v  +  5*c0.v )*b1.v +  5*c1.v *(a0.v + b0.v))*a1.vp2 +  10*b1.v *((a0.v + c0.v/3)*b1.v +  2*(a0.v + b0.v/3) *c1.v)*a1.v +  10*a0.v *b1.vp2*c1.v)/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v*((b0.v + c0.v/3)*a1.vp4 + 5*((a0.v +  2*b0.v  + c0.v)*b1.v + c1.v*(a0.v +  3*b0.v )/3)*a1.vp3 +  20*b1.v *(b1.v*(a0.v + (b0.v + c0.v)/2) + c1.v*(a0.v + b0.v))*a1.vp2 +  10*b1.vp2 *((a0.v + c0.v/3)*b1.v +  (3*a0.v + b0.v) *c1.v)*a1.v + 20*a0.v*b1.vp3*c1.v/3)/168;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v*((b0.v + c0.v)*a1.vp4 + 5*((a0.v + b0.v +  2*c0.v )*b1.v + c1.v*(a0.v +  2*b0.v  + c0.v))*a1.vp3 + 5*(( 2*a0.v  +  2*c0.v )*b1.vp2 +  (8*a0.v + 4*b0.v + 4*c0.v) *c1.v*b1.v +  2*c1.vp2 *(a0.v + b0.v))*a1.vp2 +  ((30*a0.v + 10*c0.v)*b1.v + (30*a0.v + 10*b0.v)*c1.v) *b1.v*c1.v*a1.v +  10*a0.v *b1.vp2*c1.vp2)/252;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (( 3*b0.v  + c0.v)*b1.v + b0.v*c1.v)*a1.vp4/126 + b1.v*((a0.v +  2*b0.v  + c0.v)*b1.v + 2*c1.v*(a0.v +  3*b0.v )/3)*a1.vp3/21 + b1.vp2*(b1.v*(2*a0.v + b0.v + c0.v) + 3*c1.v*(a0.v + b0.v))*a1.vp2/21 + ((a0.v + c0.v/3)*b1.v +  4*(a0.v + b0.v/3) *c1.v)*b1.vp3*a1.v/42 + a0.v*b1.vp4*c1.v/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (( 3*b0.v  +  3*c0.v )*b1.v +  c1.v *(3*b0.v + c0.v))*a1.vp4/252 + (( 6*a0.v  +  6*b0.v  +  12*c0.v )*b1.vp2 +  12*(a0.v + 2*b0.v  + c0.v) *c1.v*b1.v +  2*c1.vp2 *(a0.v +  3*b0.v ))*a1.vp3/252 + b1.v*(b1.vp2*(a0.v + c0.v) +  (6*a0.v + 3*b0.v + 3*c0.v) *c1.v*b1.v +  3*c1.vp2 *(a0.v + b0.v))*a1.vp2/42 + b1.vp2*((a0.v + c0.v/3)*b1.v + (3*a0.v + b0.v)/2*c1.v)*c1.v*a1.v/21 + a0.v*b1.vp3*c1.vp2/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ((b0.v +  3*c0.v )*b1.vp2 +  6*c1.v *(b0.v + c0.v)*b1.v +  (3*b0.v + c0.v) *c1.vp2)*a1.vp3/168 + ((a0.v +  3*c0.v )*b1.vp3 +  9*(a0.v + b0.v + 2*c0.v)  *c1.v*b1.vp2 +  9*c1.vp2 *(a0.v +  2*b0.v  + c0.v)*b1.v + c1.vp3*(a0.v +  3*b0.v ))*a1.vp2/168 + (b1.vp2*(a0.v + c0.v) +  3*(a0.v + (b0.v + c0.v)/2) *c1.v*b1.v + c1.vp2*(a0.v + b0.v))*b1.v*c1.v*a1.v/28 + ((a0.v + c0.v/3)*b1.v + (a0.v + b0.v/3)*c1.v)*b1.vp2*c1.vp2/56;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp1_Yp5(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p1_1p5<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp1_Yp5(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p1_1p5<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp5_Yp1(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p1_1p5<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp5_Yp1(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p1_1p5<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b9_0p2_1p4(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp2*a1.vp4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp3*a0.v*((3*a0.v + 2*b0.v)*a1.v + 4*a0.v*b1.v)/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp2*((a0.vp2 +  2*a0.v *b0.v + b0.vp2/3)*a1.vp2 +  4*b1.v *a0.v*(a0.v + 2*b0.v/3)*a1.v +  2*a0.vp2 *b1.vp2)/12;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.v*((a0.vp2 +  6*a0.v *b0.v +  3*b0.vp2 )*a1.vp3 +  12*b1.v *(a0.vp2 +  2*a0.v *b0.v + b0.vp2/3)*a1.vp2 +  18*b1.vp2 *a0.v*(a0.v + 2*b0.v/3)*a1.v +  4*a0.vp2 *b1.vp3)/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ( 2*a0.v *b0.v +  3*b0.vp2 )*a1.vp4/126 + 2*b1.v*(a0.vp2 +  6*a0.v *b0.v +  3*b0.vp2 )*a1.vp3/63 + b1.vp2*(a0.vp2 +  2*a0.v *b0.v + b0.vp2/3)*a1.vp2/7 + 2*b1.vp3*a0.v*(a0.v + 2*b0.v/3)*a1.v/21 + a0.vp2*b1.vp4/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp2*((a0.vp2 + (b0.v + c0.v)*a0.v + c0.v*b0.v/3)*a1.vp2 +  2*((b1.v + c1.v)*a0.v + 2*(b0.v*c1.v + c0.v*b1.v)/3) *a0.v*a1.v +  2*a0.vp2 *b1.v*c1.v)/12;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v*((a0.vp2 + ( 4*b0.v  +  2*c0.v )*a0.v + b0.v*(b0.v +  2*c0.v ))*a1.vp3 + (( 8*b1.v  +  4*c1.v )*a0.vp2 + (( 8*b0.v  +  8*c0.v )*b1.v +  8*b0.v *c1.v)*a0.v + 4*b0.v*(b0.v*c1.v +  2*b1.v *c0.v)/3)*a1.vp2 +  (6*(b1.v +  2*c1.v )*a0.v + 8*b0.v*c1.v + 4*c0.v*b1.v) *a0.v*b1.v*a1.v +  4*a0.vp2 *b1.vp2*c1.v)/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (( 3*b0.v  + c0.v)*a0.v +  3*b0.v *(b0.v + c0.v))*a1.vp4/252 + (( 6*a0.vp2  + ( 24*b0.v  +  12*c0.v )*a0.v +  6*b0.v *(b0.v +  2*c0.v ))*b1.v +  2*c1.v *(a0.vp2 +  6*a0.v *b0.v +  3*b0.vp2 ))*a1.vp3/252 + (b1.v*(a0.vp2 + (b0.v + c0.v)*a0.v + c0.v*b0.v/3) + (a0.vp2 +  2*a0.v *b0.v + b0.vp2/3)*c1.v)*b1.v*a1.vp2/14 + ((a0.v + 2*c0.v/3)*b1.v +  (3*a0.v + 2*b0.v) *c1.v)*a0.v*b1.vp2*a1.v/42 + a0.vp2*b1.vp3*c1.v/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (( 2*b0.v  +  2*c0.v )*a0.v + b0.vp2 +  4*c0.v *b0.v + c0.vp2)*a1.vp4/252 + (( 4*b1.v  +  4*c1.v )*a0.vp2 + (( 8*b0.v  +  16*c0.v )*b1.v +  (8*c0.v + 16*b0.v) *c1.v)*a0.v + ( 8*b0.v *c0.v +  4*c0.vp2 )*b1.v +  4*b0.v *c1.v*(b0.v +  2*c0.v ))*a1.vp3/252 + (( 6*b1.vp2  +  24*b1.v *c1.v +  6*c1.vp2 )*a0.vp2 + ( 12*b1.vp2 *c0.v +  24*c1.v *(b0.v + c0.v)*b1.v +  12*b0.v *c1.vp2)*a0.v +  2*b0.vp2 *c1.vp2 +  8*b0.v *b1.v*c0.v*c1.v +  2*b1.vp2 *c0.vp2)*a1.vp2/252 + ((b1.v + c1.v)*a0.v + 2*(b0.v*c1.v + c0.v*b1.v)/3)*a0.v*b1.v*c1.v*a1.v/21 + a0.vp2*b1.vp2*c1.vp2/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 3*b0.vp2  +  2*b0.v *c0.v)*a1.vp4/630 + ((( 24*b0.v  +  8*c0.v )*a0.v +  24*b0.v *(b0.v + c0.v))*b1.v +  (8*a0.v + 12*b0.v) *b0.v*c1.v)*a1.vp3/630 + ((a0.vp2 + ( 4*b0.v  +  2*c0.v )*a0.v + b0.v*(b0.v +  2*c0.v ))*b1.v + 2*c1.v*(a0.vp2 +  6*a0.v *b0.v +  3*b0.vp2 )/3)*b1.v*a1.vp2/35 + 4*(b1.v*(a0.vp2 + (b0.v + c0.v)*a0.v + c0.v*b0.v/3) + (3*a0.vp2 +  6*a0.v *b0.v + b0.vp2)/2*c1.v)*b1.vp2*a1.v/105 + a0.v*((a0.v + 2*c0.v/3)*b1.v +  4*(a0.v + 2*b0.v/3) *c1.v)*b1.vp3/210;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ( 3*b0.vp2  +  6*b0.v *c0.v + c0.vp2)*a1.vp4/1260 + ((( 24*b0.v  +  24*c0.v )*a0.v +  12*b0.vp2  +  48*c0.v *b0.v +  12*c0.vp2 )*b1.v +  ((24*b0.v + 8*c0.v)*a0.v + 24*b0.v*(b0.v + c0.v)) *c1.v)*a1.vp3/1260 + (( 18*a0.vp2  + ( 36*b0.v  +  72*c0.v )*a0.v +  36*c0.v *b0.v +  18*c0.vp2 )*b1.vp2 +  36*(a0.vp2 + ( 4*b0.v  +  2*c0.v )*a0.v + b0.v*(b0.v +  2*c0.v )) *c1.v*b1.v +  6*(a0.vp2 + 6*a0.v *b0.v + 3*b0.vp2)  *c1.vp2)*a1.vp2/1260 + ((a0.vp2 +  2*a0.v *c0.v + c0.vp2/3)*b1.vp2 +  (6*a0.vp2 + 6*(b0.v + c0.v)*a0.v + 2*c0.v*b0.v) *c1.v*b1.v +  c1.vp2 *(3*a0.vp2 +  6*a0.v *b0.v + b0.vp2))*b1.v*a1.v/105 + ((a0.v + 2*c0.v/3)*b1.v + (3*a0.v/2 + b0.v)*c1.v)*a0.v*b1.vp2*c1.v/105;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ((b0.vp2 +  6*b0.v *c0.v +  3*c0.vp2 )*b1.v +  (3*b0.vp2 + 6*c0.v *b0.v + c0.vp2) *c1.v)*a1.vp3/420 + ((( 3*b0.v  +  9*c0.v )*a0.v +  9*c0.v *(b0.v + c0.v))*b1.vp2 +  18*c1.v *((b0.v + c0.v)*a0.v + b0.vp2/2 +  2*c0.v *b0.v + c0.vp2/2)*b1.v +  ((9*b0.v + 3*c0.v)*a0.v + 9*b0.v*(b0.v + c0.v)) *c1.vp2)*a1.vp2/420 + ((a0.vp2 +  6*a0.v *c0.v +  3*c0.vp2 )*b1.vp3 +  9*(a0.vp2 + ( 2*b0.v  +  4*c0.v )*a0.v + 2*c0.v *b0.v + c0.vp2) *c1.v*b1.vp2 +  9*(a0.vp2 + ( 4*b0.v  +  2*c0.v )*a0.v + b0.v*(b0.v +  2*c0.v )) *c1.vp2*b1.v + c1.vp3*(a0.vp2 +  6*a0.v *b0.v +  3*b0.vp2 ))*a1.v/420 + ((a0.vp2 +  2*a0.v *c0.v + c0.vp2/3)*b1.vp2 +  (3*a0.vp2 + 3*(b0.v + c0.v)*a0.v + c0.v*b0.v) *c1.v*b1.v + c1.vp2*(a0.vp2 +  2*a0.v *b0.v + b0.vp2/3))*b1.v*c1.v/140;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp2_Yp4(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p2_1p4<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp2_Yp4(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p2_1p4<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp4_Yp2(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p2_1p4<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp4_Yp2(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p2_1p4<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b9_0p3_1p3(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp3*a1.vp3;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp2*a0.vp2*(a1.v*(a0.v + b0.v) + a0.v*b1.v)/3;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.v*a0.v*((a0.vp2 +  3*a0.v *b0.v + b0.vp2)*a1.vp2 +  3*a0.v *b1.v*(a0.v + b0.v)*a1.v + a0.vp2*b1.vp2)/12;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (a0.v + b0.v)*(a0.vp2 +  8*a0.v *b0.v + b0.vp2)*a1.vp3/84 + 3*a0.v*b1.v*(a0.vp2 +  3*a0.v *b0.v + b0.vp2)*a1.vp2/28 + 3*a0.vp2*b1.vp2*(a0.v + b0.v)*a1.v/28 + a0.vp3*b1.vp3/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return b0.v*(a0.vp2 +  3*a0.v *b0.v + b0.vp2)*a1.vp3/42 + b1.v*(a0.v + b0.v)*(a0.vp2 +  8*a0.v *b0.v + b0.vp2)*a1.vp2/42 + a0.v*b1.vp2*(a0.vp2 +  3*a0.v *b0.v + b0.vp2)*a1.v/14 + a0.vp2*b1.vp3*(a0.v + b0.v)/42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v*((a0.vp2 + 3*(b0.v + c0.v)*a0.v/2 + c0.v*b0.v)*a1.vp2 + 3*a0.v*((b1.v + c1.v)*a0.v + b0.v*c1.v + c0.v*b1.v)*a1.v/2 + a0.vp2*b1.v*c1.v)*a0.v/12;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a0.vp3 + ( 6*b0.v  +  3*c0.v )*a0.vp2 +  3*b0.v *(b0.v +  2*c0.v )*a0.v + b0.vp2*c0.v)*a1.vp3/84 + a0.v*(a0.vp2*(b1.v + c1.v/2) + 3*((b0.v + c0.v)*b1.v + b0.v*c1.v)*a0.v/2 + b0.vp2*c1.v/2 + b0.v*b1.v*c0.v)*a1.vp2/14 + a0.vp2*((b1.v +  2*c1.v )*a0.v +  2*b0.v *c1.v + c0.v*b1.v)*b1.v*a1.v/28 + a0.vp3*b1.vp2*c1.v/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((3*b0.v + c0.v)*a0.vp2 + (6*b0.v)*(b0.v + c0.v)*a0.v + b0.vp2*(b0.v + 3*c0.v))*a1.vp3/168 + ((3*b1.v + c1.v)*a0.vp3 + ((18*b0.v + 9*c0.v)*b1.v + (9*b0.v)*c1.v)*a0.vp2 + (9*b0.v)*((b0.v + 2*c0.v)*b1.v + b0.v*c1.v)*a0.v + b0.vp3*c1.v + (3*b1.v)*b0.vp2*c0.v)*a1.vp2/168 + b1.v*((c1.v + b1.v)*a0.vp2 + (((3*b0.v)/2 + (3*c0.v)/2)*b1.v + (3*b0.v)*c1.v)*a0.v + c1.v*b0.vp2 + b0.v*b1.v*c0.v)*a0.v*a1.v/28 + b1.vp2*((b1.v + 3*c1.v)*a0.v + (3*b0.v)*c1.v + b1.v*c0.v)*a0.vp2/168;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((b0.v + c0.v)*a0.vp2 + (b0.vp2 + (4*c0.v)*b0.v + c0.vp2)*a0.v + b0.v*c0.v*(b0.v + c0.v))*a1.vp3/84 + ((c1.v + b1.v)*a0.vp3 + ((3*b0.v + 6*c0.v)*b1.v + (6*c1.v)*(b0.v + c0.v/2))*a0.vp2 + (((6*c0.v)*b0.v + 3*c0.vp2)*b1.v + (3*b0.v)*c1.v*(b0.v + 2*c0.v))*a0.v + b0.v*c0.v*(b0.v*c1.v + b1.v*c0.v))*a1.vp2/84 + ((b1.vp2 + (4*b1.v)*c1.v + c1.vp2)*a0.vp2 + ((3*c0.v)*b1.vp2 + (6*c1.v)*(b0.v + c0.v)*b1.v + (3*b0.v)*c1.vp2)*a0.v + b0.vp2*c1.vp2 + (4*b1.v)*c1.v*c0.v*b0.v + b1.vp2*c0.vp2)*a0.v*a1.v/84 + c1.v*((c1.v + b1.v)*a0.v + b0.v*c1.v + b1.v*c0.v)*b1.v*a0.vp2/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((3*b0.vp2 + (2*c0.v)*b0.v)*a0.v + 2*b0.vp3 + (3*b0.vp2)*c0.v)*a1.vp3/210 + (((9*b0.v + 3*c0.v)*a0.vp2 + (18*b0.v)*(b0.v + c0.v)*a0.v + (3*b0.vp2)*(b0.v + 3*c0.v))*b1.v + (3*b0.v)*c1.v*(a0.vp2 + (3*a0.v)*b0.v + b0.vp2))*a1.vp2/210 + ((a0.vp3 + (6*b0.v + 3*c0.v)*a0.vp2 + (3*b0.v)*(b0.v + 2*c0.v)*a0.v + b0.vp2*c0.v)*b1.v + (2*c1.v)*(a0.v + b0.v)*(a0.vp2 + (8*a0.v)*b0.v + b0.vp2)/3)*b1.v*a1.v/70 + ((a0.vp2 + ((3*b0.v)/2 + (3*c0.v)/2)*a0.v + c0.v*b0.v)*b1.v + (3*c1.v)*(a0.vp2 + (3*a0.v)*b0.v + b0.vp2)/2)*b1.vp2*a0.v/105;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ((3*b0.vp2 + (6*c0.v)*b0.v + c0.vp2)*a0.v + b0.v*(b0.vp2 + (6*c0.v)*b0.v + 3*c0.vp2))*a1.vp3/420 + (((9*b0.v + 9*c0.v)*b1.v + (9*c1.v)*(b0.v + c0.v/3))*a0.vp2 + ((9*b0.vp2 + (36*c0.v)*b0.v + 9*c0.vp2)*b1.v + (18*b0.v)*(b0.v + c0.v)*c1.v)*a0.v + 3*((3*c0.v)*b1.v*(b0.v + c0.v) + b0.v*c1.v*(b0.v + 3*c0.v))*b0.v)*a1.vp2/420 + ((3*b1.vp2 + (6*b1.v)*c1.v + c1.vp2)*a0.vp3 + ((9*b0.v + 18*c0.v)*b1.vp2 + (36*c1.v)*(b0.v + c0.v/2)*b1.v + (9*b0.v)*c1.vp2)*a0.vp2 + (((18*c0.v)*b0.v + 9*c0.vp2)*b1.vp2 + (18*b0.v)*c1.v*(b0.v + 2*c0.v)*b1.v + (9*b0.vp2)*c1.vp2)*a0.v + b0.v*(b0.vp2*c1.vp2 + (6*b1.v)*c1.v*c0.v*b0.v + (3*b1.vp2)*c0.vp2))*a1.v/420 + b1.v*((b1.vp2 + (6*b1.v)*c1.v + 3*c1.vp2)*a0.vp2 + ((3*c0.v)*b1.vp2 + (9*c1.v)*(b0.v + c0.v)*b1.v + (9*b0.v)*c1.vp2)*a0.v + (3*b0.vp2)*c1.vp2 + (6*b1.v)*c1.v*c0.v*b0.v + b1.vp2*c0.vp2)*a0.v/420;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (b0.v + c0.v)*(b0.vp2 + (8*c0.v)*b0.v + c0.vp2)*a1.vp3/1680 + (((9*b0.vp2 + (54*c0.v)*b0.v + 27*c0.vp2)*a0.v + (27*b0.vp2)*c0.v + (54*b0.v)*c0.vp2 + 9*c0.vp3)*b1.v + (27*c1.v)*((b0.vp2 + (2*c0.v)*b0.v + c0.vp2/3)*a0.v + b0.vp3/3 + (2*b0.vp2)*c0.v + b0.v*c0.vp2))*a1.vp2/1680 + (((9*b0.v + 27*c0.v)*a0.vp2 + (54*c0.v)*(b0.v + c0.v)*a0.v + (27*b0.v)*c0.vp2 + 9*c0.vp3)*b1.vp2 + (54*c1.v)*((b0.v + c0.v)*a0.vp2 + (b0.vp2 + (4*c0.v)*b0.v + c0.vp2)*a0.v + b0.v*c0.v*(b0.v + c0.v))*b1.v + (27*c1.vp2)*((b0.v + c0.v/3)*a0.vp2 + (2*b0.v)*(b0.v + c0.v)*a0.v + b0.vp2*(b0.v + 3*c0.v)/3))*a1.v/1680 + (a0.v + c0.v)*(a0.vp2 + (8*c0.v)*a0.v + c0.vp2)*b1.vp3/1680 + (3*c1.v)*(a0.vp3 + (3*b0.v + 6*c0.v)*a0.vp2 + ((6*c0.v)*b0.v + 3*c0.vp2)*a0.v + b0.v*c0.vp2)*b1.vp2/560 + (3*c1.vp2)*(a0.vp3 + (6*b0.v + 3*c0.v)*a0.vp2 + (3*b0.v)*(b0.v + 2*c0.v)*a0.v + b0.vp2*c0.v)*b1.v/560 + c1.vp3*(a0.v + b0.v)*(a0.vp2 + (8*a0.v)*b0.v + b0.vp2)/1680;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp3_Yp3(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p3_1p3<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp3_Yp3(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p3_1p3<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<typename T>
std::vector<T> b9_0p2_1p5(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp2*a1.vp5;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp4*a0.v*(2*a1.v*(a0.v + b0.v) + 5*a0.v*b1.v)/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (a1.vp2*(a0.vp2 +  4*a0.v *b0.v + b0.vp2) +  10*a0.v *b1.v*(a0.v + b0.v)*a1.v +  10*a0.vp2 *b1.vp2)*a1.vp3/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp2*(2*b0.v*a1.vp3*(a0.v + b0.v) + 5*b1.v*(a0.vp2 +  4*a0.v *b0.v + b0.vp2)*a1.vp2 +  20*a0.v *b1.vp2*(a0.v + b0.v)*a1.v +  10*a0.vp2 *b1.vp3)/84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return 5*a1.v*(a1.vp4*b0.vp2/10 + b0.v*a1.vp3*(a0.v + b0.v)*b1.v + a1.vp2*(a0.vp2 +  4*a0.v *b0.v + b0.vp2)*b1.vp2 +  2*a0.v *a1.v*(a0.v + b0.v)*b1.vp3 + a0.vp2*b1.vp4/2)/63;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((a0.vp2 + ( 2*b0.v  +  2*c0.v )*a0.v + c0.v*b0.v)*a1.vp2 +  5*a0.v *((b1.v + c1.v)*a0.v + b0.v*c1.v + c0.v*b1.v)*a1.v +  10*a0.vp2 *b1.v*c1.v)*a1.vp3/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (((2*b0.v + c0.v)*a0.v + b0.v*(b0.v +  2*c0.v ))*a1.vp3 + 5*(a0.vp2*(b1.v + c1.v/2) + (( 2*b0.v  +  2*c0.v )*b1.v +  2*b0.v *c1.v)*a0.v + b0.vp2*c1.v/2 + b0.v*b1.v*c0.v)*a1.vp2 +  10*a0.v *((b1.v +  2*c1.v )*a0.v +  2*b0.v *c1.v + c0.v*b1.v)*b1.v*a1.v +  15*a0.vp2 *b1.vp2*c1.v)*a1.vp2/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.v*(b0.v*(b0.v + c0.v)*a1.vp4/10 + (((c0.v/2 + b0.v)*a0.v + b0.v*(b0.v +  2*c0.v )/2)*b1.v + b0.v*c1.v*(a0.v + b0.v)/2)*a1.vp3 + ((a0.vp2 + ( 2*b0.v  +  2*c0.v )*a0.v + c0.v*b0.v)*b1.v + c1.v*(a0.vp2 +  4*a0.v *b0.v + b0.vp2))*b1.v*a1.vp2 + (b1.v*(a0.v + c0.v) +  3*c1.v *(a0.v + b0.v))*a0.v*b1.vp2*a1.v + a0.vp2*b1.vp3*c1.v)/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.v*(((b0.vp2 + c0.vp2)/10 + 2*c0.v*b0.v/5)*a1.vp4 + (((b0.v +  2*c0.v )*a0.v +  2*c0.v *b0.v + c0.vp2)*b1.v +  ((c0.v + 2*b0.v)*a0.v + b0.v*(b0.v +  2*c0.v )) *c1.v)*a1.vp3 + (b1.vp2*(a0.vp2 +  4*a0.v *c0.v + c0.vp2) +  4*(a0.vp2 + ( 2*b0.v  +  2*c0.v )*a0.v + c0.v*b0.v) *c1.v*b1.v + (a0.vp2 +  4*a0.v *b0.v + b0.vp2)*c1.vp2)*a1.vp2 +  6*a0.v *(b1.v*(a0.v + c0.v) + c1.v*(a0.v + b0.v))*b1.v*c1.v*a1.v +  3*a0.vp2 *b1.vp2*c1.vp2)/378;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 2*b0.v *(b0.v + c0.v)*b1.v + b0.vp2*c1.v)*a1.vp4/126 + 4*(((c0.v/2 + b0.v)*a0.v + b0.v*(b0.v/2 + c0.v ))*b1.v + b0.v*c1.v*(a0.v + b0.v))*b1.v*a1.vp3/63 + (2*(a0.vp2 + ( 2*b0.v  +  2*c0.v )*a0.v + c0.v*b0.v)*b1.v + 3*c1.v*(a0.vp2 +  4*a0.v *b0.v + b0.vp2))*b1.vp2*a1.vp2/63 + a0.v*(b1.v*(a0.v + c0.v) +  4*c1.v *(a0.v + b0.v))*b1.vp3*a1.v/63 + a0.vp2*b1.vp4*c1.v/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ((b0.vp2 +  4*b0.v *c0.v + c0.vp2)*b1.v +  2*b0.v *c1.v*(b0.v + c0.v))*a1.vp4/252 + ((( 4*b0.v  +  8*c0.v )*a0.v +  8*c0.v *b0.v +  4*c0.vp2 )*b1.vp2 +  8*((c0.v + 2*b0.v)*a0.v + b0.v*(b0.v +  2*c0.v )) *c1.v*b1.v +  4*b0.v *c1.vp2*(a0.v + b0.v))*a1.vp3/252 + b1.v*(b1.vp2*(a0.vp2 +  4*a0.v *c0.v + c0.vp2) +  6*(a0.vp2 + ( 2*b0.v  +  2*c0.v )*a0.v + c0.v*b0.v) *c1.v*b1.v +  3*(a0.vp2 + 4*a0.v *b0.v + b0.vp2) *c1.vp2)*a1.vp2/126 + (2*b1.v*(a0.v + c0.v) + 3*c1.v*(a0.v + b0.v))*a0.v*b1.vp2*c1.v*a1.v/63 + a0.vp2*b1.vp3*c1.vp2/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (c0.v*(b0.v + c0.v)*b1.vp2 + c1.v*(b0.vp2 +  4*b0.v *c0.v + c0.vp2)*b1.v + b0.v*c1.vp2*(b0.v + c0.v))*a1.vp3/84 + (c0.v*(a0.v + c0.v)*b1.vp3 +  3*((b0.v +  2*c0.v )*a0.v + 2*c0.v *b0.v + c0.vp2) *c1.v*b1.vp2 +  (6*(c0.v/2 + b0.v)*a0.v + 3*b0.v*(b0.v +  2*c0.v )) *c1.vp2*b1.v + b0.v*c1.vp3*(a0.v + b0.v))*a1.vp2/84 + b1.v*(b1.vp2*(a0.vp2 +  4*a0.v *c0.v + c0.vp2) +  3*(a0.vp2 + ( 2*b0.v  +  2*c0.v )*a0.v + c0.v*b0.v )*c1.v*b1.v + (a0.vp2 +  4*a0.v *b0.v + b0.vp2)*c1.vp2)*c1.v*a1.v/84 + a0.v*(b1.v*(a0.v + c0.v) + c1.v*(a0.v + b0.v))*b1.vp2*c1.vp2/84;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp2_Yp5(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p2_1p5<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp2_Yp5(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p2_1p5<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp5_Yp2(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p2_1p5<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp5_Yp2(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p2_1p5<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b9_0p3_1p4(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp3*a1.vp4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp3*a0.vp2*((2*a0.v + 3*b0.v)*a1.v +  4*a0.v *b1.v)/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp2*a0.v*((a0.vp2 +  6*a0.v *b0.v +  3*b0.vp2 )*a1.vp2 +  (8*a0.v + 12*b0.v) *b1.v*a0.v*a1.v +  6*a0.vp2 *b1.vp2)/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((3*a0.vp2 +  6*a0.v *b0.v + b0.vp2)*b0.v*a1.vp3/4 + a0.v*b1.v*(a0.vp2 +  6*a0.v *b0.v +  3*b0.vp2 )*a1.vp2 +  3*(a0.v + 3*b0.v/2) *b1.vp2*a0.vp2*a1.v + a0.vp3*b1.vp3)*a1.v/21;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ( 3*a0.v *b0.vp2 +  2*b0.vp3 )*a1.vp4/126 + 2*b1.v*(a0.vp2 +  2*a0.v *b0.v + b0.vp2/3)*b0.v*a1.vp3/21 + a0.v*b1.vp2*(a0.vp2 +  6*a0.v *b0.v +  3*b0.vp2 )*a1.vp2/21 + (4*a0.v + 6*b0.v)*b1.vp3*a0.vp2*a1.v/63 + a0.vp3*b1.vp4/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp2*a0.v*((a0.vp2 + 3*( b0.v  + c0.v )*a0.v +  3*c0.v *b0.v)*a1.vp2 +  a0.v *(4*(b1.v + c1.v)*a0.v + 6*b0.v*c1.v + 6*c0.v*b1.v)*a1.v +  6*a0.vp2 *b1.v*c1.v)/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 2*a1.v*(3*((b0.v/4 + c0.v/8)*a0.vp2 + b0.v*(b0.v +  2*c0.v )*a0.v/4 + b0.vp2*c0.v/8)*a1.vp3 + a0.v*(a0.vp2*(b1.v + c1.v/2) + (3*(b0.v  + c0.v )*b1.v +  3*b0.v *c1.v)*a0.v + 3*b0.vp2*c1.v/2 +  3*b0.v *b1.v*c0.v)*a1.vp2 + 3*a0.vp2*b1.v*((b1.v +  2*c1.v )*a0.v +  3*b0.v *c1.v + 3*c0.v*b1.v/2)*a1.v/2 + 3*a0.vp3*b1.vp2*c1.v/2)/63;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((b0.v + c0.v)*a0.v + b0.v*(b0.v/3 +  c0.v ))*b0.v*a1.vp4/84 + ((( 12*b0.v  +  6*c0.v )*b1.v +  6*b0.v *c1.v)*a0.vp2 +  12*(b1.v*(b0.v +  2*c0.v ) + b0.v*c1.v) *b0.v*a0.v +  2*b0.vp3 *c1.v +  6*b0.vp2 *b1.v*c0.v)*a1.vp3/252 + a0.v*b1.v*((b1.v + c1.v)*a0.vp2 + (( 3*b0.v  +  3*c0.v )*b1.v +  6*b0.v *c1.v)*a0.v +  3*b0.vp2 *c1.v +  3*b0.v *b1.v*c0.v)*a1.vp2/42 + a0.vp2*((b1.v +  3*c1.v )*a0.v + 9*b0.v*c1.v/2 + 3*c0.v*b1.v/2)*b1.vp2*a1.v/63 + a0.vp3*b1.vp3*c1.v/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((b0.vp2 +  4*b0.v *c0.v + c0.vp2)*a0.v +  2*b0.v *c0.v*(b0.v + c0.v))*a1.vp4/252 + ((( 4*b0.v  +  8*c0.v )*b1.v +  (4*c0.v + 8*b0.v) *c1.v)*a0.vp2 + (( 16*b0.v *c0.v +  8*c0.vp2 )*b1.v +  8*b0.v *c1.v*(b0.v +  2*c0.v ))*a0.v +  4*b0.vp2 *c0.v*c1.v +  4*c0.vp2 *b1.v*b0.v)*a1.vp3/252 + a0.v*((b1.vp2 +  4*b1.v *c1.v + c1.vp2)*a0.vp2 + ( 6*b1.vp2 *c0.v +  12*c1.v *(b0.v + c0.v)*b1.v +  6*b0.v *c1.vp2)*a0.v +  3*b0.vp2 *c1.vp2 +  12*b0.v *b1.v*c0.v*c1.v +  3*b1.vp2 *c0.vp2)*a1.vp2/126 + a0.vp2*b1.v*(2*(b1.v + c1.v)*a0.v + 3*(b0.v*c1.v + c0.v*b1.v))*c1.v*a1.v/63 + a0.vp3*b1.vp2*c1.vp2/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ( 2*b0.vp3  +  3*b0.vp2 *c0.v)*a1.vp4/630 + 4*(((b0.v + c0.v)*a0.v + b0.v*(b0.v/3 +  c0.v ))*b1.v + (a0.v/2 + b0.v/3)*b0.v*c1.v)*b0.v*a1.vp3/105 + 2*b1.v*(((c0.v/2 + b0.v)*a0.vp2 + b0.v*(b0.v +  2*c0.v )*a0.v + b0.vp2*c0.v/2)*b1.v + b0.v*c1.v*(a0.vp2 +  2*a0.v *b0.v + b0.vp2/3))*a1.vp2/35 + 4*a0.v*b1.vp2*((a0.vp2 + ( 3*b0.v  +  3*c0.v )*a0.v +  3*c0.v *b0.v)*b1.v + 3*c1.v*(a0.vp2 +  6*a0.v *b0.v +  3*b0.vp2 )/2)*a1.v/315 + a0.vp2*b1.vp3*(b1.v*(a0.v + 3*c0.v/2) +  c1.v *(4*a0.v + 6*b0.v))/315;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return b0.v*(b0.vp2 + (6*c0.v)*b0.v + 3*c0.vp2)*a1.vp4/1260 + (((12*b0.vp2 + (48*c0.v)*b0.v + 12*c0.vp2)*a0.v + (24*b0.v)*c0.v*(b0.v + c0.v))*b1.v + 24*((b0.v + c0.v)*a0.v + b0.v*(b0.v + 3*c0.v)/3)*c1.v*b0.v)*a1.vp3/1260 + (((18*b0.v + 36*c0.v)*a0.vp2 + ((72*c0.v)*b0.v + 36*c0.vp2)*a0.v + (18*b0.v)*c0.vp2)*b1.vp2 + (72*c1.v)*((b0.v + c0.v/2)*a0.vp2 + b0.v*(b0.v + 2*c0.v)*a0.v + b0.vp2*c0.v/2)*b1.v + (18*c1.vp2)*(a0.vp2 + (2*a0.v)*b0.v + b0.vp2/3)*b0.v)*a1.vp2/1260 + b1.v*((a0.vp2 + (6*c0.v)*a0.v + 3*c0.vp2)*b1.vp2 + (6*c1.v)*(a0.vp2 + (3*b0.v + 3*c0.v)*a0.v + (3*c0.v)*b0.v)*b1.v + 3*(a0.vp2 + (6*a0.v)*b0.v + 3*b0.vp2)*c1.vp2)*a0.v*a1.v/315 + (2*c1.v)*b1.vp2*((a0.v + (3*c0.v)/2)*b1.v + 3*(a0.v + (3*b0.v)/2)*c1.v/2)*a0.vp2/315;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (((3*b0.vp2)*c0.v + (6*b0.v)*c0.vp2 + c0.vp3)*b1.v + b0.v*c1.v*(b0.vp2 + (6*c0.v)*b0.v + 3*c0.vp2))*a1.vp3/420 + ((9*c0.v)*((b0.v + c0.v)*a0.v + c0.v*(b0.v + c0.v/3))*b1.vp2 + (9*c1.v)*((b0.vp2 + (4*c0.v)*b0.v + c0.vp2)*a0.v + (2*b0.v)*c0.v*(b0.v + c0.v))*b1.v + 9*((b0.v + c0.v)*a0.v + b0.v*(b0.v + 3*c0.v)/3)*c1.vp2*b0.v)*a1.vp2/420 + (((3*c0.v)*a0.vp2 + (6*c0.vp2)*a0.v + c0.vp3)*b1.vp3 + (9*c1.v)*((b0.v + 2*c0.v)*a0.vp2 + ((4*c0.v)*b0.v + 2*c0.vp2)*a0.v + b0.v*c0.vp2)*b1.vp2 + (18*c1.vp2)*((b0.v + c0.v/2)*a0.vp2 + b0.v*(b0.v + 2*c0.v)*a0.v + b0.vp2*c0.v/2)*b1.v + (3*c1.vp3)*(a0.vp2 + (2*a0.v)*b0.v + b0.vp2/3)*b0.v)*a1.v/420 + c1.v*b1.v*((a0.vp2 + (6*c0.v)*a0.v + 3*c0.vp2)*b1.vp2 + (3*c1.v)*(a0.vp2 + (3*b0.v + 3*c0.v)*a0.v + (3*c0.v)*b0.v)*b1.v + (a0.vp2 + (6*a0.v)*b0.v + 3*b0.vp2)*c1.vp2)*a0.v/420;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp3_Yp4(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p3_1p4<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp3_Yp4(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p3_1p4<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp4_Yp3(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p3_1p4<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp4_Yp3(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p3_1p4<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b9_0p3_1p5(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp3*a1.vp5;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (a1.v*(a0.v + 3*b0.v) + 5*a0.v*b1.v)*a1.vp4*a0.vp2/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp3*(3*b0.v*(a0.v + b0.v)*a1.vp2 + 5*a0.v*b1.v*(a0.v + 3*b0.v)*a1.v + 10*a0.vp2*b1.vp2)*a0.v/36;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return 5*((3*a0.v + b0.v)*a1.vp3*b0.vp2/10 + 3*a0.v*b0.v*b1.v*(a0.v + b0.v)*a1.vp2/2 + a0.vp2*b1.vp2*(a0.v + 3*b0.v)*a1.v + a0.vp3*b1.vp3)*a1.vp2/42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return 5*(a1.vp4*b0.vp3/10 + (3*a0.v + b0.v)*a1.vp3*b0.vp2*b1.v/2 + 3*a0.v*b0.v*a1.vp2*(a0.v + b0.v)*b1.vp2 + a0.vp2*a1.v*(a0.v + 3*b0.v)*b1.vp3 + a0.vp3*b1.vp4/2)*a1.v/63;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp3*((3*(b0.v + c0.v)*a0.v + 6*b0.v*c0.v)*a1.vp2 + 5*(a0.v*((b1.v + c1.v)*a0.v + 3*b0.v*c1.v + 3*b1.v*c0.v)*a1.v + 4*a0.vp2*b1.v*c1.v))*a0.v/72;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.vp2*(3*((b0.v + 2*c0.v)*a0.v + b0.v*c0.v)*b0.v*a1.vp3/10 + 3*(((b0.v + c0.v)*b1.v + b0.v*c1.v)*a0.v + b0.vp2*c1.v + 2*b1.v*c0.v*b0.v)*a0.v*a1.vp2/2 + b1.v*((b1.v + 2*c1.v)*a0.v + 6*b0.v*c1.v + 3*b1.v*c0.v)*a0.vp2*a1.v + 3*a0.vp3*b1.vp2*c1.v)/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.v*(b0.vp2*(b0.v + 3*c0.v)*a1.vp4/10 + 3*(((b0.v + 2*c0.v)*a0.v + b0.v*c0.v)*b1.v + (a0.v + b0.v/3)*c1.v*b0.v)*b0.v*a1.vp3/2 + 3*b1.v*(b1.v*((b0.v + c0.v)*a0.v + 2*b0.v*c0.v) + 2*b0.v*c1.v*(a0.v + b0.v))*a0.v*a1.vp2 + b1.vp2*((a0.v + 3*c0.v)*b1.v + 3*c1.v*(a0.v + 3*b0.v))*a0.vp2*a1.v + 2*a0.vp3*b1.vp3*c1.v)/252;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5*a1.v*(b0.v*c0.v*(b0.v + c0.v)*a1.vp4/10 + (((b0.v + c0.v/2)*c0.v*b1.v + b0.v*c1.v*(b0.v + 2*c0.v)/2)*a0.v + b0.v*c0.v*(b0.v*c1.v + b1.v*c0.v)/2)*a1.vp3 + ((c0.v*b1.vp2 + 2*b1.v*(b0.v + c0.v)*c1.v + b0.v*c1.vp2)*a0.v + b0.vp2*c1.vp2 + 4*b0.v*c0.v*b1.v*c1.v + b1.vp2*c0.vp2)*a0.v*a1.vp2 + b1.v*c1.v*a0.vp2*((b1.v + c1.v)*a0.v + 3*b0.v*c1.v + 3*b1.v*c0.v)*a1.v + a0.vp3*b1.vp2*c1.vp2)/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (b1.v*(b0.v + 3*c0.v) + b0.v*c1.v)*b0.vp2*a1.vp4/126 + b1.v*(((b0.v + 2*c0.v)*a0.v + b0.v*c0.v)*b1.v + 2*(a0.v + b0.v/3)*c1.v*b0.v)*b0.v*a1.vp3/21 + b1.vp2*(b1.v*((b0.v + c0.v)*a0.v + 2*b0.v*c0.v) + 3*b0.v*c1.v*(a0.v + b0.v))*a0.v*a1.vp2/21 + b1.vp3*a0.vp2*((a0.v + 3*c0.v)*b1.v + 4*c1.v*(a0.v + 3*b0.v))*a1.v/126 + a0.vp3*b1.vp4*c1.v/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (3*c0.v*b1.v*(b0.v + c0.v) + b0.v*c1.v*(b0.v + 3*c0.v))*b0.v*a1.vp4/252 + (((12*b0.v*c0.v + 6*c0.vp2)*a0.v + 6*b0.v*c0.vp2)*b1.vp2 + 12*((b0.v + 2*c0.v)*a0.v + b0.v*c0.v)*c1.v*b0.v*b1.v + (6*a0.v + 2*b0.v)*c1.vp2*b0.vp2)*a1.vp3/252 + b1.v*(c0.v*(a0.v + c0.v)*b1.vp2/3 + ((b0.v + c0.v)*a0.v + 2*b0.v*c0.v)*c1.v*b1.v + b0.v*c1.vp2*(a0.v + b0.v))*a0.v*a1.vp2/14 + b1.vp2*((a0.v + 3*c0.v)*b1.v + 3*c1.v*(a0.v + 3*b0.v)/2)*c1.v*a0.vp2*a1.v/63 + a0.vp3*b1.vp3*c1.vp2/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ((3*b0.v*c0.vp2 + c0.vp3)*b1.vp2 + 6*b0.v*c0.v*c1.v*(b0.v + c0.v)*b1.v + b0.vp2*c1.vp2*(b0.v + 3*c0.v))*a1.vp3/168 + ((3*c0.vp2*a0.v + c0.vp3)*b1.vp3 + 9*c1.v*c0.v*((2*b0.v + c0.v)*a0.v + b0.v*c0.v)*b1.vp2 + 9*((b0.v + 2*c0.v)*a0.v + b0.v*c0.v)*c1.vp2*b0.v*b1.v + 3*(a0.v + b0.v/3)*c1.vp3*b0.vp2)*a1.vp2/168 + b1.v*(2*c0.v*(a0.v + c0.v)*b1.vp2 + 3*((b0.v + c0.v)*a0.v + 2*b0.v*c0.v)*c1.v*b1.v + 2*b0.v*c1.vp2*(a0.v + b0.v))*c1.v*a0.v*a1.v/56 + b1.vp2*c1.vp2*a0.vp2*((a0.v + 3*c0.v)*b1.v + c1.v*(a0.v + 3*b0.v))/168;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp3_Yp5(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p3_1p5<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp3_Yp5(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p3_1p5<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp5_Yp3(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p3_1p5<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp5_Yp3(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p3_1p5<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b9_0p4_1p4(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp4*a1.vp4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a0.v + 4*b0.v)*a1.v + 4*a0.v*b1.v)*a1.vp3*a0.vp3/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (b0.v*(a0.v + 3*b0.v/2)*a1.vp2 + a0.v*b1.v*(a0.v + 4*b0.v)*a1.v + 3*a0.vp2*b1.vp2/2)*a1.vp2*a0.vp2/9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (b0.vp2*(a0.v + 2*b0.v/3)*a1.vp3 + 8*b1.v*a0.v*(a0.v + 3*b0.v/2)*b0.v*a1.vp2/3 + a0.vp2*b1.vp2*(a0.v + 4*b0.v)*a1.v + 2*a0.vp3*b1.vp3/3)*a1.v*a0.v/14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (4*a0.v*b0.vp3 + b0.vp4)*a1.vp4/126 + 4*b1.v*(a0.v + 2*b0.v/3)*a0.v*b0.vp2*a1.vp3/21 + 4*b1.vp2*a0.vp2*(a0.v + 3*b0.v/2)*b0.v*a1.vp2/21 + 2*a0.vp3*b1.vp3*(a0.v + 4*b0.v)*a1.v/63 + a0.vp4*b1.vp4/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp2*(((b0.v + c0.v)*a0.v + 3*b0.v*c0.v)*a1.vp2 + ((b1.v + c1.v)*a0.v + 4*b0.v*c1.v + 4*b1.v*c0.v)*a0.v*a1.v + 3*a0.vp2*b1.v*c1.v)*a0.vp2/18;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v*(((b0.v + 2*c0.v)*a0.v + 2*b0.v*c0.v)*b0.v*a1.vp3 + 8*(((b0.v + c0.v)*b1.v + b0.v*c1.v)*a0.v/3 + b0.vp2*c1.v/2 + b1.v*c0.v*b0.v)*a0.v*a1.vp2 + b1.v*((b1.v + 2*c1.v)*a0.v + 8*b0.v*c1.v + 4*b1.v*c0.v)*a0.vp2*a1.v + 2*a0.vp3*b1.vp2*c1.v)*a0.v/42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((b0.v + 3*c0.v)*a0.v + b0.v*c0.v)*b0.vp2*a1.vp4/126 + ((b1.v*(b0.v + 2*c0.v) + b0.v*c1.v)*a0.v + 2*b0.vp2*c1.v/3 + 2*b1.v*c0.v*b0.v)*a0.v*b0.v*a1.vp3/21 + b1.v*(((b0.v + c0.v)*b1.v + 2*b0.v*c1.v)*a0.v + 3*b0.vp2*c1.v + 3*b1.v*c0.v*b0.v)*a0.vp2*a1.vp2/21 + b1.vp2*((b1.v + 3*c1.v)*a0.v + 12*b0.v*c1.v + 4*b1.v*c0.v)*a0.vp3*a1.v/126 + a0.vp4*b1.vp3*c1.v/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return c0.v*((b0.v + c0.v)*a0.v + b0.v*c0.v/2)*b0.v*a1.vp4/63 + 2*(((2*b0.v*c0.v + c0.vp2)*b1.v + b0.v*c1.v*(b0.v + 2*c0.v))*a0.v + 2*c1.v*c0.v*b0.vp2 + 2*b0.v*c0.vp2*b1.v)*a0.v*a1.vp3/63 + (2*(c0.v*b1.vp2 + 2*b1.v*(b0.v + c0.v)*c1.v + b0.v*c1.vp2)*a0.v + 3*b0.vp2*c1.vp2 + 12*b0.v*c0.v*b1.v*c1.v + 3*b1.vp2*c0.vp2)*a0.vp2*a1.vp2/63 + b1.v*((b1.v + c1.v)*a0.v + 4*b0.v*c1.v + 4*b1.v*c0.v)*c1.v*a0.vp3*a1.v/63 + a0.vp4*b1.vp2*c1.vp2/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return b0.vp3*(b0.v + 4*c0.v)*a1.vp4/630 + 8*(((b0.v + 3*c0.v)*a0.v + b0.v*c0.v)*b1.v + b0.v*c1.v*(a0.v + b0.v/4))*b0.vp2*a1.vp3/315 + 2*b1.v*(((b0.v + 2*c0.v)*a0.v + 2*b0.v*c0.v)*b1.v + 2*(a0.v + 2*b0.v/3)*c1.v*b0.v)*a0.v*b0.v*a1.vp2/35 + 8*b1.vp2*(((b0.v + c0.v)*a0.v + 3*b0.v*c0.v)*b1.v + 3*c1.v*(a0.v + 3*b0.v/2)*b0.v)*a0.vp2*a1.v/315 + b1.vp3*(b1.v*(a0.v + 4*c0.v) + 4*c1.v*(a0.v + 4*b0.v))*a0.vp3/630;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (2*b0.vp3*c0.v + 3*b0.vp2*c0.vp2)*a1.vp4/630 + 4*((3*c0.v*b1.v*(b0.v + c0.v) + b0.v*c1.v*(b0.v + 3*c0.v))*a0.v + b0.v*c0.v*(b0.v*c1.v + 3*b1.v*c0.v/2))*b0.v*a1.vp3/315 + 2*((c0.v*(b0.v + c0.v/2)*b1.vp2 + b0.v*c1.v*(b0.v + 2*c0.v)*b1.v + b0.vp2*c1.vp2/2)*a0.v + b0.vp3*c1.vp2/3 + 2*b0.vp2*c0.v*b1.v*c1.v + b0.v*c0.vp2*b1.vp2)*a0.v*a1.vp2/35 + 4*b1.v*a0.vp2*((c0.v*b1.vp2/3 + b1.v*(b0.v + c0.v)*c1.v + b0.v*c1.vp2)*a0.v + 3*b0.vp2*c1.vp2/2 + 3*b0.v*c0.v*b1.v*c1.v + b1.vp2*c0.vp2/2)*a1.v/105 + b1.vp2*((b1.v + 3*c1.v/2)*a0.v + 6*b0.v*c1.v + 4*b1.v*c0.v)*c1.v*a0.vp3/315;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return ((3*b0.vp2*c0.vp2 + 2*b0.v*c0.vp3)*b1.v + (2*b0.v + 3*c0.v)*c1.v*c0.v*b0.vp2)*a1.vp3/210 + (((9*b0.v*c0.vp2 + 3*c0.vp3)*a0.v + 3*b0.v*c0.vp3)*b1.vp2 + 18*c1.v*c0.v*((b0.v + c0.v)*a0.v + b0.v*c0.v/2)*b0.v*b1.v + 3*((b0.v + 3*c0.v)*a0.v + b0.v*c0.v)*c1.vp2*b0.vp2)*a1.vp2/210 + 3*a0.v*((a0.v + 2*c0.v/3)*c0.vp2*b1.vp3/3 + ((2*b0.v + c0.v)*a0.v + 2*b0.v*c0.v)*c1.v*c0.v*b1.vp2 + ((b0.v + 2*c0.v)*a0.v + 2*b0.v*c0.v)*c1.vp2*b0.v*b1.v + (a0.v + 2*b0.v/3)*c1.vp3*b0.vp2/3)*a1.v/70 + b1.v*((2*c0.v*a0.v/3 + c0.vp2)*b1.vp2 + ((b0.v + c0.v)*a0.v + 3*b0.v*c0.v)*c1.v*b1.v + 2*c1.vp2*(a0.v + 3*b0.v/2)*b0.v/3)*c1.v*a0.vp2/70;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp4_Yp4(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p4_1p4<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp4_Yp4(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p4_1p4<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<typename T>
std::vector<T> b9_0p4_1p5(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp4*a1.vp5;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (5*a0.vp4*a1.vp4*b1.v + 4*a0.vp3*a1.vp5*b0.v) / 9;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (5*a0.vp2*b1.vp2 + 10*a0.v*b0.v*a1.v*b1.v + 3*b0.vp2*a1.vp2)*a1.vp3*a0.vp2/18;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp2*(5*a0.vp3*b1.vp3 + 20*a0.vp2*a1.v*b0.v*b1.vp2 + 15*a0.v*a1.vp2*b0.vp2*b1.v + 2*a1.vp3*b0.vp3)*a0.v/42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.v*(5*a0.vp4*b1.vp4 + 40*a0.vp3*a1.v*b0.v*b1.vp3 + 60*a0.vp2*a1.vp2*b0.vp2*b1.vp2 + 20*a0.v*a1.vp3*b0.vp3*b1.v + a1.vp4*b0.vp4)/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp3*(3*a1.vp2*b0.v*c0.v + 5*(a0.v*(b0.v*c1.v + b1.v*c0.v)*a1.v + a0.vp2*b1.v*c1.v))*a0.vp2/18;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp2*(2*a1.vp3*b0.vp2*c0.v + 5*(a0.v*b0.v*(b0.v*c1.v + 2*b1.v*c0.v)*a1.vp2 + b1.v*(8*b0.v*c1.v + 4*b1.v*c0.v)*a0.vp2*a1.v/3 + a0.vp3*b1.vp2*c1.v))*a0.v/42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v*(a1.vp4*b0.vp3*c0.v + 5*(a0.v*b0.vp2*(b0.v*c1.v + 3*b1.v*c0.v)*a1.vp3 + 6*a0.vp2*b0.v*b1.v*(b0.v*c1.v + b1.v*c0.v)*a1.vp2 + (6*b0.v*b1.vp2*c1.v + 2*b1.vp3*c0.v)*a0.vp3*a1.v + a0.vp4*b1.vp3*c1.v))/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v*(a1.vp4*b0.vp2*c0.vp2 + 5*(2*a0.v*b0.v*c0.v*(b0.v*c1.v + b1.v*c0.v)*a1.vp3 + 2*a0.vp2*(b0.vp2*c1.vp2 + 4*b0.v*c0.v*b1.v*c1.v + b1.vp2*c0.vp2)*a1.vp2 + 4*a0.vp3*b1.v*c1.v*(b0.v*c1.v + b1.v*c0.v)*a1.v + a0.vp4*b1.vp2*c1.vp2))/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (b0.vp4*c1.v + 4*b0.vp3*b1.v*c0.v)*a1.vp4/126 + 8*b1.v*(b0.v*c1.v + 3*b1.v*c0.v/2)*a0.v*b0.vp2*a1.vp3/63 + 2*b1.vp2*(b0.v*c1.v + 2*b1.v*c0.v/3)*a0.vp2*b0.v*a1.vp2/7 + (16*a0.vp3*b0.v*b1.vp3*c1.v + 4*a0.vp3*b1.vp4*c0.v)*a1.v/126 + a0.vp4*b1.vp4*c1.v/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F10 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return (2*b0.vp3*c0.v*c1.v + 3*b0.vp2*b1.v*c0.vp2)*a1.vp4/126 + 2*a0.v*b0.v*(b0.vp2*c1.vp2 + 6*b0.v*c0.v*b1.v*c1.v + 3*b1.vp2*c0.vp2)*a1.vp3/63 + b1.v*(b0.vp2*c1.vp2 + 2*b0.v*c0.v*b1.v*c1.v + b1.vp2*c0.vp2/3)*a0.vp2*a1.vp2/7 + (12*a0.vp3*b0.v*b1.vp2*c1.vp2 + 8*a0.vp3*b1.vp3*c0.v*c1.v)*a1.v/126 + a0.vp4*b1.vp3*c1.vp2/126;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F11 = [](const COMP<T> &a0, const COMP<T> &a1,
               const COMP<T> &b0, const COMP<T> &b1,
               const COMP<T> &c0, const COMP<T> &c1) {
    return b0.v*c0.v*(b0.vp2*c1.vp2 + 3*b0.v*c0.v*b1.v*c1.v + b1.vp2*c0.vp2)*a1.vp3/42 + a0.v*(b0.v*c1.v + b1.v*c0.v)*(b0.vp2*c1.vp2 + 8*b0.v*c0.v*b1.v*c1.v + b1.vp2*c0.vp2)*a1.vp2/42 + a0.vp2*b1.v*c1.v*(b0.vp2*c1.vp2 + 3*b0.v*c0.v*b1.v*c1.v + b1.vp2*c0.vp2)*a1.v/14 + a0.vp3*b1.vp2*c1.vp2*(b0.v*c1.v + b1.v*c0.v)/42;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11);
}

template<>
std::vector<double> B9_Xp4_Yp5(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p4_1p5<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B9_Xp4_Yp5(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p4_1p5<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B9_Xp5_Yp4(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b9_0p4_1p5<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B9_Xp5_Yp4(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b9_0p4_1p5<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}