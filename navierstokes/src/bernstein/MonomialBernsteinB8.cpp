#include "MonomialBernstein.hpp"

#include "IAInterval.hpp"

template<typename T>
std::vector<T> EVALUATE(const COMP<T> &a0, const COMP<T> &a1,
                        const COMP<T> &b0, const COMP<T> &b1,
                        const COMP<T> &c0, const COMP<T> &c1,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1)> &F0,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1)> &F1,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1)> &F2,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1)> &F3,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1)> &F4,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1,
                                        const COMP<T> &c0, const COMP<T> &c1)> &F5,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1,
                                        const COMP<T> &c0, const COMP<T> &c1)> &F6,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1,
                                        const COMP<T> &c0, const COMP<T> &c1)> &F7,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1,
                                        const COMP<T> &c0, const COMP<T> &c1)> &F8,
                        std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                                        const COMP<T> &b0, const COMP<T> &b1,
                                        const COMP<T> &c0, const COMP<T> &c1)> &F9) {
  return std::vector<T>{
    F0(a0, a1),
    F1(a0, a1, b0, b1),
    F1(a0, a1, c0, c1),
    F2(a0, a1, b0, b1),
    F5(a0, a1, b0, b1, c0, c1),
    F2(a0, a1, c0, c1),
    F3(a0, a1, b0, b1),
    F6(a0, a1, b0, b1, c0, c1),
    F6(a0, a1, c0, c1, b0, b1),
    F3(a0, a1, c0, c1),
    F4(a0, a1, b0, b1),
    F7(a0, a1, b0, b1, c0, c1),
    F8(a0, a1, b0, b1, c0, c1),
    F7(a0, a1, c0, c1, b0, b1),
    F4(a0, a1, c0, c1),
    F3(b0, b1, a0, a1),
    F7(b0, b1, a0, a1, c0, c1),
    F9(a0, a1, b0, b1, c0, c1),
    F9(a0, a1, c0, c1, b0, b1),
    F7(c0, c1, a0, a1, b0, b1),
    F3(c0, c1, a0, a1),
    F2(b0, b1, a0, a1),
    F6(b0, b1, a0, a1, c0, c1),
    F8(b0, b1, a0, a1, c0, c1),
    F9(c0, c1, b0, b1, a0, a1),
    F8(c0, c1, a0, a1, b0, b1),
    F6(c0, c1, a0, a1, b0, b1),
    F2(c0, c1, a0, a1),
    F1(b0, b1, a0, a1),
    F5(b0, b1, a0, a1, c0, c1),
    F6(b0, b1, c0, c1, a0, a1),
    F7(b0, b1, c0, c1, a0, a1),
    F7(c0, c1, b0, b1, a0, a1),
    F6(c0, c1, b0, b1, a0, a1),
    F5(c0, c1, a0, a1, b0, b1),
    F1(c0, c1, a0, a1),
    F0(b0, b1),
    F1(b0, b1, c0, c1),
    F2(b0, b1, c0, c1),
    F3(b0, b1, c0, c1),
    F4(c0, c1, b0, b1),
    F3(c0, c1, b0, b1),
    F2(c0, c1, b0, b1),
    F1(c0, c1, b0, b1),
    F0(c0, c1)
  };
}

template<typename T>
std::vector<T> b8_0p0_1p1(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a1.v;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (7 * a1.v + b1.v) / 8;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (3 * a1.v + b1.v) / 4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (5 * a1.v + 3 * b1.v) / 8;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (a1.v + b1.v) / 2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (3 * a1.v + (b1.v + c1.v) / 2) / 4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (b1.v + (5 * a1.v + c1.v) / 2) / 4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a1.v + (3 * b1.v + c1.v) / 4) / 2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a1.v + (b1.v + c1.v) / 2) / 2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (3 * (a1.v + b1.v) / 2 + c1.v) / 4;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template<>
std::vector<double> B8_Xp0_Yp1(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p0_1p1<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B8_Xp0_Yp1(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p0_1p1<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B8_Xp1_Yp0(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p0_1p1<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B8_Xp1_Yp0(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p0_1p1<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b8_0p0_1p2(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a1.vp2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.v * (3 * a1.v + b1.v) / 4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((15 * a1.vp2 + b1.vp2) / 4 + 3 * b1.v * a1.v) / 7;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (5 * a1.vp2 + 3 * (5 * b1.v * a1.v + b1.vp2) / 2) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (3 * (a1.vp2 + b1.vp2) / 2 + 4 * b1.v * a1.v) / 7;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (15 * a1.vp2 + 6 * (b1.v + c1.v) * a1.v + c1.v * b1.v) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (5 * a1.vp2 + (5 * (2 * b1.v + c1.v) * a1.v + b1.v * (b1.v + 2 * c1.v)) / 2) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (3 * a1.vp2 + (4 * (3 * b1.v + c1.v) * a1.v + 3 * b1.v * (b1.v + c1.v)) / 2) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((3 * a1.vp2 + (8 * (b1.v + c1.v) * a1.v + b1.vp2 + c1.vp2) / 2) / 2 + c1.v * b1.v) / 7;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((3 * a1.vp2 + 3 * (3 * b1.v + 2 * c1.v) * a1.v + 3 * b1.vp2 + c1.vp2) / 2
            + 3 * c1.v * b1.v) / 14;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template<>
std::vector<double> B8_Xp0_Yp2(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p0_1p2<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B8_Xp0_Yp2(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p0_1p2<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B8_Xp2_Yp0(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p0_1p2<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B8_Xp2_Yp0(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p0_1p2<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> B8_Xp1_Yp1(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.v * a1.v;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((6 * a0.v + b0.v) * a1.v + a0.v * b1.v) / 8;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((15 * a0.v + 6 * b0.v) * a1.v / 2 + b1.v * (3 * a0.v + b0.v / 2)) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((20 * a0.v + 15 * b0.v) * a1.v + b1.v * (15 * a0.v + 6 * b0.v)) / 56;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((3 * a0.v + 4 * b0.v) * a1.v + (4 * a0.v + 3 * b0.v) * b1.v) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (6 * ((5 * a0.v + b0.v + c0.v) * a1.v + (b1.v + c1.v) * a0.v)
            + b0.v * c1.v + b1.v * c0.v) / 56;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (5 * (4 * a0.v + 2 * b0.v + c0.v) * a1.v + 2 * (5 * a0.v + b0.v + c0.v) * b1.v
            + c1.v * (5 * a0.v + 2 * b0.v)) / 56;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((3 * (a0.v + b0.v) + c0.v) * a1.v + 3 * (4 * a0.v + 2 * b0.v + c0.v) * b1.v / 4
            + (a0.v + 3 * b0.v / 4) * c1.v) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (((3 * a0.v + 2 * (b0.v + c0.v)) * a1.v + (4 * a0.v + b0.v + 2 * c0.v) * b1.v / 2) / 2
            + (a0.v + b0.v / 2 + c0.v / 4) * c1.v) / 7;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (3 * ((2 * (a0.v + c0.v) + 3 * b0.v) * a1.v + (3 * a0.v + 2 * (b0.v + c0.v)) * b1.v) / 2
            + c1.v * (3 * (a0.v + b0.v) + c0.v)) / 28;
  };

  return EVALUATE<T>(A.X, A.Y, B.X, B.Y, C.X, C.Y, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template std::vector<double> B8_Xp1_Yp1(const VALUES<double> &A,
                                        const VALUES<double> &B,
                                        const VALUES<double> &C);

template std::vector<IAInterval> B8_Xp1_Yp1(const VALUES<IAInterval> &A,
                                            const VALUES<IAInterval> &B,
                                            const VALUES<IAInterval> &C);

template<typename T>
std::vector<T> b8_0p0_1p3(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a1.vp3;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp2 * (5 * a1.v + 3 * b1.v) / 8;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (5 * a1.vp3 + 3 * (5 * a1.vp2 * b1.v + a1.v * b1.vp2) / 2) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (5 * (a1.vp3 + 3 * a1.vp2 * b1.v) + (15 * a1.v * b1.vp2 + b1.vp3) / 2) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (a1.v + b1.v) * (a1.vp2 + 5 * a1.v * b1.v + b1.vp2) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (5 * a1.vp3 + 3 * (5 * (b1.v + c1.v) * a1.vp2 / 2 + a1.v * b1.v * c1.v) / 2) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (5 * (a1.vp3 + (2 * b1.v + c1.v) * a1.vp2)
            + (5 * b1.v * (b1.v + 2 * c1.v) * a1.v + b1.vp2 * c1.v) / 2) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a1.vp3 + 3 * b1.v * (b1.v + c1.v) * a1.v + (3 * (3 * b1.v + c1.v) * a1.vp2
                                                        + b1.vp2 * (3 * c1.v + b1.v) / 2) / 2) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a1.vp3 + 3 * (b1.v + c1.v) * a1.vp2 + (b1.vp2 + 4 * b1.v * c1.v + c1.vp2) * a1.v
            + b1.v * c1.v * (b1.v + c1.v) / 2) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a1.vp3 + 3 * (3 * b1.v + 2 * c1.v) * a1.vp2
            + 3 * (3 * b1.vp2 + 6 * b1.v * c1.v + c1.vp2) * a1.v
            + b1.v * (b1.vp2 + 6 * b1.v * c1.v + 3 * c1.vp2)) / 56;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template<>
std::vector<double> B8_Xp0_Yp3(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p0_1p3<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B8_Xp0_Yp3(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p0_1p3<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B8_Xp3_Yp0(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p0_1p3<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B8_Xp3_Yp0(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p0_1p3<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b8_0p1_1p2(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.v * a1.vp2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((5 * a0.v + b0.v) * a1.vp2 / 2 + a0.v * a1.v * b1.v) / 4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((5 * (2 * a0.v + b0.v) * a1.vp2 + a0.v * b1.vp2) / 2
            + b1.v * (5 * a0.v + b0.v) * a1.v) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((10 * (a0.v + b0.v) * a1.vp2 + b1.vp2 * (5 * a0.v + b0.v)) / 4
            + 5 * b1.v * (a0.v + b0.v / 2) * a1.v) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a0.v + 2 * b0.v) * a1.vp2 / 2 + 2 * (a0.v + b0.v) * b1.v * a1.v
            + b1.vp2 * (a0.v + b0.v / 2)) / 7;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (5 * (4 * a0.v + b0.v + c0.v) * a1.vp2 / 2
            + (5 * (b1.v + c1.v) * a0.v + b0.v * c1.v + b1.v * c0.v) * a1.v
            + a0.v * b1.v * c1.v) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (10 * ((3 * a0.v + 2 * b0.v + c0.v) * a1.vp2
                  + ((4 * a0.v + b0.v + c0.v) * b1.v + c1.v * (2 * a0.v + b0.v)) * a1.v)
            + (b1.v * (5 * a0.v + c0.v) + 2 * c1.v * (5 * a0.v + b0.v)) * b1.v) / 168;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((2 * a0.v + 3 * b0.v + c0.v) * a1.vp2 / 2
            + ((3 * a0.v + 2 * b0.v + c0.v) * b1.v + c1.v * (a0.v + b0.v)) * a1.v
            + ((a0.v + (b0.v + c0.v) / 4) * b1.v + c1.v * (a0.v + b0.v / 2)) * b1.v) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (a0.v + b0.v + c0.v) * a1.vp2 / 14
           + (((3 * a0.v + b0.v + 2 * c0.v) * b1.v + (3 * a0.v + 2 * b0.v + c0.v) * c1.v) * a1.v +
              (2 * a0.v + (b0.v + c0.v) / 2) * c1.v * b1.v
              + ((2 * a0.v + c0.v) * b1.vp2 / 2 + (a0.v + b0.v / 2) * c1.vp2) / 2) / 21;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (((a0.v + 3 * b0.v + 2 * c0.v) * a1.vp2 + (3 * a0.v + b0.v + 2 * c0.v) * b1.vp2
             + c1.vp2 * (a0.v + b0.v)) / 2 + (3 * a0.v + 2 * b0.v + c0.v) * c1.v * b1.v
            + (3 * (a0.v + b0.v + c0.v) * b1.v + (2 * a0.v + 3 * b0.v + c0.v) * c1.v) * a1.v) / 28;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template<>
std::vector<double> B8_Xp1_Yp2(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p1_1p2<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B8_Xp1_Yp2(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p1_1p2<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B8_Xp2_Yp1(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p1_1p2<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B8_Xp2_Yp1(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p1_1p2<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b8_0p0_1p4(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a1.vp4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp3 * (a1.v + b1.v) / 2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (3 * (a1.vp4 + a1.vp2 * b1.vp2) / 2 + 4 * a1.vp3 * b1.v) / 7;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.v * (a1.v + b1.v) * (a1.vp2 + 5 * a1.v * b1.v + b1.vp2) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a1.vp4 + b1.vp4) / 2
            + 2 * (4 * (a1.vp3 * b1.v + a1.v * b1.vp3) + 9 * a1.vp2 * b1.vp2)) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp2 * (3 * (a1.vp2 + c1.v * b1.v) + 4 * (b1.v + c1.v) * a1.v) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v * (a1.vp3 + 2 * ((2 * b1.v + c1.v) * a1.vp2 + b1.v * (b1.v + 2 * c1.v) * a1.v)
                   + b1.vp2 * c1.v) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((a1.vp4 + b1.vp3 * c1.v) / 2 + 9 * b1.v * (b1.v + c1.v) * a1.vp2
            + 2 * ((3 * b1.v + c1.v) * a1.vp3 + b1.vp2 * (b1.v + 3 * c1.v) * a1.v)) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((a1.vp4 + b1.vp2 * c1.vp2) / 2
            + 4 * ((b1.v + c1.v) * a1.vp3 + b1.v * c1.v * (b1.v + c1.v) * a1.v)
            + 3 * (b1.vp2 + 4 * b1.v * c1.v + c1.vp2) * a1.vp2) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (((3 * b1.v + 2 * c1.v) * a1.vp3
             + 3 * ((3 * (b1.vp2 + 2 * b1.v * c1.v) + c1.vp2) * a1.vp2
                    + (b1.vp3 + 3 * (2 * b1.vp2 * c1.v + b1.v * c1.vp2)) * a1.v +
                    b1.vp2 * c1.vp2)) / 2 + b1.vp3 * c1.v) / 35;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template<>
std::vector<double> B8_Xp0_Yp4(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p0_1p4<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B8_Xp0_Yp4(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p0_1p4<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B8_Xp4_Yp0(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p0_1p4<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B8_Xp4_Yp0(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p0_1p4<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b8_0p1_1p3(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.v * a1.vp3;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((4 * a0.v + b0.v) * a1.vp3 + 3 * a0.v * a1.vp2 * b1.v) / 8;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((3 * a0.v + 2 * b0.v) * a1.vp2
            + 3 * ((2 * a0.v + b0.v / 2) * b1.v * a1.v + a0.v * b1.vp2 / 2)) * a1.v / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a0.v * b1.vp3 / 2 + (2 * a0.v + 3 * b0.v) * a1.vp3
             + 3 * b1.v * (3 * a0.v + 2 * b0.v) * a1.vp2) / 2
            + 3 * (a0.v + b0.v / 4) * b1.vp2 * a1.v) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a0.v + 4 * b0.v) * a1.vp3 / 2 + 3 * (2 * a0.v + 3 * b0.v) * b1.v * a1.vp2
            + 3 * b1.vp2 * (3 * a0.v + 2 * b0.v) * a1.v + 2 * (a0.v + b0.v / 4) * b1.vp3) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v * ((3 * a0.v + b0.v + c0.v) * a1.vp2
                   + 3 * ((b1.v + c1.v) * a0.v + (b0.v * c1.v + b1.v * c0.v) / 4) * a1.v
                   + 3 * a0.v * b1.v * c1.v / 2) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (((2 * (a0.v + b0.v) + c0.v) * a1.vp3
             + (2 * (3 * a0.v + b0.v + c0.v) * b1.v + (3 * a0.v + 2 * b0.v) * c1.v) * a1.vp2 +
             a0.v * b1.vp2 * c1.v / 2) / 2
            + (b1.v * (a0.v + c0.v / 4) + 2 * c1.v * (a0.v + b0.v / 4)) * b1.v * a1.v) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((a0.v + 3 * b0.v + c0.v) * a1.vp3
            +
            3 * (3 * (2 * (a0.v + b0.v) + c0.v) * b1.v + c1.v * (2 * a0.v + 3 * b0.v)) * a1.vp2 / 2
            + 3 * ((3 * a0.v + b0.v + c0.v) * b1.v + (3 * a0.v + 2 * b0.v) * c1.v) * b1.v * a1.v
            + (b1.v * (a0.v + c0.v / 4) + 3 * c1.v * (a0.v + b0.v / 4)) * b1.vp2) / 70;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (((a0.v + 2 * (b0.v + c0.v)) * a1.vp3
             + 3 * ((2 * (a0.v + c0.v) + b0.v) * b1.v + (2 * (a0.v + b0.v) + c0.v) * c1.v) *
               a1.vp2 + ((3 * a0.v + 2 * c0.v) * b1.vp2
                         + 4 * (3 * a0.v + b0.v + c0.v) * b1.v * c1.v +
                         (3 * a0.v + 2 * b0.v) * c1.vp2) * a1.v) / 2
            + c1.v * (b1.v * (a0.v + c0.v / 4) + c1.v * (a0.v + b0.v / 4)) * b1.v) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((3 * b0.v + 2 * c0.v) * a1.vp3 +
            (9 * (a0.v + 2 * (b0.v + c0.v)) * b1.v + 6 * (a0.v + 3 * b0.v + c0.v) * c1.v) * a1.vp2 +
            (9 * (2 * (a0.v + c0.v) + b0.v) * b1.vp2 + 18 * (2 * (a0.v + b0.v) + c0.v) * c1.v * b1.v
             + 6 * c1.vp2 * (a0.v + 3 * b0.v / 2)) * a1.v
            + (b1.vp2 * (3 * a0.v + 2 * c0.v) + 6 * (3 * a0.v + b0.v + c0.v) * b1.v * c1.v
               + 3 * (3 * a0.v + 2 * b0.v) * c1.vp2) * b1.v) / 280;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template<>
std::vector<double> B8_Xp1_Yp3(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p1_1p3<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B8_Xp1_Yp3(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p1_1p3<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B8_Xp3_Yp1(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p1_1p3<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B8_Xp3_Yp1(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p1_1p3<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> B8_Xp2_Yp2(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp2 * a1.vp2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a0.v * ((a1.v + b1.v / 2) * a0.v + b0.v * a1.v / 2) * a1.v / 2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (((6 * a0.vp2 + 8 * a0.v * b0.v + b0.vp2) * a1.vp2 + a0.vp2 * b1.vp2) / 4
            + a0.v * b1.v * (2 * a0.v + b0.v) * a1.v) / 7;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a0.vp2 + 3 * a0.v * b0.v + b0.vp2) * a1.vp2
            + b1.v * (3 * a0.vp2 + 4 * a0.v * b0.v + b0.vp2 / 2) * a1.v +
            a0.v * b1.vp2 * (a0.v + b0.v / 2)) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a0.vp2 / 2 + 4 * a0.v * b0.v + 3 * b0.vp2) * a1.vp2
            + 4 * b1.v * (a0.vp2 + 3 * a0.v * b0.v + b0.vp2) * a1.v
            + b1.vp2 * (3 * a0.vp2 + 4 * a0.v * b0.v + b0.vp2 / 2)) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return
        (((6 * a0.vp2 + 4 * (b0.v + c0.v) * a0.v + c0.v * b0.v) * a1.vp2 + a0.vp2 * c1.v * b1.v) /
         4 +
         a0.v * ((b1.v + c1.v) * a0.v + (b0.v * c1.v + b1.v * c0.v) / 2) * a1.v) / 7;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((3 * a0.vp2 + 3 * (2 * b0.v + c0.v) * a0.v + b0.v * (b0.v + 2 * c0.v)) * a1.vp2
            + (6 * (2 * b1.v + c1.v) * a0.vp2 + 8 * ((b0.v + c0.v) * b1.v + b0.v * c1.v) * a0.v +
               b0.vp2 * c1.v + 2 * b0.v * b1.v * c0.v) * a1.v / 2
            + a0.v * b1.v * (a0.v * (b1.v + 2 * c1.v) + b0.v * c1.v + b1.v * c0.v / 2)) / 42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (((3 * b1.v + c1.v) * a0.vp2 + 3 * ((2 * b0.v + c0.v) * b1.v + b0.v * c1.v) * a0.v
             + ((b0.v + 2 * c0.v) * b1.v + b0.v * c1.v) * b0.v) * a1.v
            + ((a0.vp2 + 2 * (3 * b0.v + c0.v) * a0.v + 3 * b0.v * (b0.v + c0.v)) * a1.vp2
               + (3 * (b1.v + c1.v) * a0.vp2 + 2 * ((b0.v + c0.v) * b1.v + 2 * b0.v * c1.v) * a0.v
                  + b0.v * (b0.v * c1.v + b1.v * c0.v) / 2) * b1.v) / 2) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((4 * ((b0.v + c0.v) * a0.v + c0.v * b0.v) + a0.vp2 + b0.vp2 + c0.vp2) * a1.vp2
            + (b1.vp2 + 4 * c1.v * b1.v + c1.vp2) * a0.vp2 +
            (b0.vp2 * c1.vp2 + b1.vp2 * c0.vp2) / 6) / 70
           + (2 * (3 * (b1.v + c1.v) * a0.vp2 +
                   3 * ((b0.v + 2 * c0.v) * b1.v + (2 * b0.v + c0.v) * c1.v) * a0.v
                   + (2 * c0.v * b0.v + c0.vp2) * b1.v + b0.v * c1.v * (b0.v + 2 * c0.v)) * a1.v
              + 2 * (b1.vp2 * c0.v + 2 * c1.v * (b0.v + c0.v) * b1.v + b0.v * c1.vp2) * a0.v
              + b0.v * b1.v * c0.v * c1.v) / 105;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((((3 * b0.v + 2 * c0.v) * a0.v + 3 * b0.vp2 + 6 * c0.v * b0.v + c0.vp2) * a1.vp2
             + (3 * (4 * ((b0.v + c0.v) * a0.v + c0.v * b0.v) + a0.vp2 + b0.vp2 + c0.vp2) * b1.v
                + 2 * (a0.vp2 + 2 * (3 * b0.v + c0.v) * a0.v + 3 * b0.v * (b0.v + c0.v)) * c1.v) *
               a1.v
             + (3 * a0.vp2 + 3 * (b0.v + 2 * c0.v) * a0.v + 2 * c0.v * b0.v + c0.vp2) * b1.vp2
             + c1.vp2 * (a0.vp2 + 3 * a0.v * b0.v + b0.vp2)) / 2
            + (3 * (a0.vp2 + (2 * b0.v + c0.v) * a0.v) + b0.v * (b0.v + 2 * c0.v)) * c1.v * b1.v) /
           70;
  };

  return EVALUATE<T>(A.X, A.Y, B.X, B.Y, C.X, C.Y, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template std::vector<double> B8_Xp2_Yp2(const VALUES<double> &A,
                                        const VALUES<double> &B,
                                        const VALUES<double> &C);

template std::vector<IAInterval> B8_Xp2_Yp2(const VALUES<IAInterval> &A,
                                            const VALUES<IAInterval> &B,
                                            const VALUES<IAInterval> &C);

template<typename T>
std::vector<T> b8_0p0_1p5(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a1.vp5;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp4 * (3 * a1.v + 5 * b1.v) / 8;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (3 * (a1.vp5 + 5 * a1.vp4 * b1.v) / 2 + 5 * a1.vp3 * b1.vp2) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp2 * (a1.vp3 + 5 * (3 * (a1.vp2 * b1.v + 2 * a1.v * b1.vp2) + 2 * b1.vp3)) / 56;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return b1.v * a1.v * (a1.v + b1.v) * (a1.vp2 + 5 * a1.v * b1.v + b1.vp2) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (3 * (a1.vp5 + 5 * (b1.v + c1.v) * a1.vp4 / 2) / 2 + 5 * a1.vp3 * b1.v * c1.v) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp2 * (a1.vp3 + 5 * ((2 * b1.v + c1.v) * a1.vp2
                                   + 2 * (b1.v * (b1.v + 2 * c1.v) * a1.v + b1.vp2 * c1.v))) / 56;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v * ((3 * b1.v + c1.v) * a1.vp3 +
                   2 * (3 * (2 * b1.v * (b1.v + c1.v) * a1.vp2 + b1.vp2 * (b1.v + 3 * c1.v) * a1.v)
                        + 2 * b1.vp3 * c1.v)) / 56;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v * ((b1.v + c1.v) * a1.vp3
                   + 2 * ((b1.vp2 + 4 * b1.v * c1.v + c1.vp2) * a1.vp2
                          + 3 * b1.v * c1.v * (b1.v + c1.v) * a1.v + b1.vp2 * c1.vp2)) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((3 * (b1.vp2 + 2 * b1.v * c1.v) + c1.vp2) * a1.vp3
            + 3 * ((b1.vp3 + 3 * (2 * b1.vp2 * c1.v + b1.v * c1.vp2)) * a1.vp2
                   + (2 * b1.vp3 * c1.v + 3 * b1.vp2 * c1.vp2) * a1.v) + b1.vp3 * c1.vp2) / 56;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template<>
std::vector<double> B8_Xp0_Yp5(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p0_1p5<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B8_Xp0_Yp5(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p0_1p5<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B8_Xp5_Yp0(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p0_1p5<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B8_Xp5_Yp0(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p0_1p5<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b8_0p1_1p4(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a1.vp4 * a0.v;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((3 * a0.v + b0.v) * a1.vp4 / 4 + a1.vp3 * a0.v * b1.v) / 2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return 3 * a1.vp2 * ((a0.v + b0.v) * a1.vp2 + 4 * b1.v * (a0.v + b0.v / 3) * a1.v
                         + (2 * a0.v) * b1.vp2) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.v * ((a0.v + 3 * b0.v) * a1.vp3 + 12 * b1.v * (a0.v + b0.v) * a1.vp2
                   + 6 * b1.vp2 * (3 * a0.v + b0.v) * a1.v + 4 * a0.v * b1.vp3) / 56;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a1.vp4 * b0.v + a0.v * b1.vp4) / 2 + 2 * b1.v * (a0.v + 3 * b0.v) * a1.vp3
            + 9 * b1.vp2 * (a0.v + b0.v) * a1.vp2 + 2 * b1.vp3 * (3 * a0.v + b0.v) * a1.v) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 3 * a1.vp2 * ((a0.v + (b0.v + c0.v) / 2) * a1.vp2
                         + 2 * ((b1.v + c1.v) * a0.v + (b1.v * c0.v + c1.v * b0.v) / 3) * a1.v +
                         2 * a0.v * b1.v * c1.v) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((a0.v + 2 * b0.v + c0.v) * a1.vp3
            + 4 * ((2 * a0.v + b0.v + c0.v) * b1.v + c1.v * (a0.v + b0.v)) * a1.vp2 +
            2 * b1.v * (b1.v * (3 * a0.v + c0.v)
                        + 2 * (3 * a0.v + b0.v) * c1.v) * a1.v + 4 * a0.v * b1.vp2 * c1.v) * a1.v /
           56;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((3 * b0.v + c0.v) * a1.vp4 / 4
            + (3 * (a0.v + 2 * b0.v + c0.v) * b1.v + c1.v * (a0.v + 3 * b0.v)) * a1.vp3
            + 9 * b1.v * (b1.v * (a0.v + (b0.v + c0.v) / 2) + c1.v * (a0.v + b0.v)) * a1.vp2
            + b1.vp2 * (b1.v * (3 * a0.v + c0.v) + 3 * (3 * a0.v + b0.v) * c1.v) * a1.v +
            a0.v * b1.vp3 * c1.v) / 70;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (((b0.v + c0.v) * a1.vp4 / 2
             + 3 * ((a0.v + c0.v) * b1.vp2 + 2 * b1.v * (2 * a0.v + b0.v + c0.v) * c1.v +
                    c1.vp2 * (a0.v + b0.v)) * a1.vp2 + a0.v * b1.vp2 * c1.vp2) / 2
            + ((a0.v + b0.v + 2 * c0.v) * b1.v + (a0.v + 2 * b0.v + c0.v) * c1.v) * a1.vp3
            + b1.v * (b1.v * (3 * a0.v + c0.v) + (3 * a0.v + b0.v) * c1.v) * c1.v * a1.v) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (3 * (3 * (a0.v + b0.v + 2 * c0.v) * b1.vp2 + 6 * (a0.v + 2 * b0.v + c0.v) * c1.v * b1.v
                 + c1.vp2 * (a0.v + 3 * b0.v)) * a1.vp2 / 2
            + (3 * (b0.v + c0.v) * b1.v + (3 * b0.v + c0.v) * c1.v) * a1.vp3
            + 3 * b1.v * (b1.vp2 * (a0.v + c0.v) + 3 * b1.v * (2 * a0.v + b0.v + c0.v) * c1.v
                          + 3 * c1.vp2 * (a0.v + b0.v)) * a1.v
            + 3 * (b1.v * (a0.v + c0.v / 3) + (3 * a0.v + b0.v) * c1.v / 2) * b1.vp2 * c1.v) / 140;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template<>
std::vector<double> B8_Xp1_Yp4(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p1_1p4<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B8_Xp1_Yp4(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p1_1p4<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B8_Xp4_Yp1(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p1_1p4<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B8_Xp4_Yp1(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p1_1p4<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b8_0p2_1p3(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp2 * a1.vp3;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a0.v * ((3 * a0.v + 2 * b0.v) * a1.v + 3 * a0.v * b1.v) * a1.vp2 / 8;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((3 * a0.vp2 + 6 * a0.v * b0.v + b0.vp2) * a1.vp2 +
            3 * (a0.v * b1.v * (3 * a0.v + 2 * b0.v) * a1.v
                 + a0.vp2 * b1.vp2)) * a1.v / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a0.vp2 + 6 * a0.v * b0.v + 3 * b0.vp2) * a1.vp3
            + 3 * (3 * a0.vp2 + 6 * a0.v * b0.v + b0.vp2) * b1.v * a1.vp2
            + 3 * a0.v * b1.vp2 * (3 * a0.v + 2 * b0.v) * a1.v + a0.vp2 * b1.vp3) / 56;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((2 * a0.v * b0.v + 3 * b0.vp2) * a1.vp3 +
            3 * b1.v * (a0.vp2 + 6 * a0.v * b0.v + 3 * b0.vp2) * a1.vp2
            + 3 * (3 * a0.vp2 + 6 * a0.v * b0.v + b0.vp2) * b1.vp2 * a1.v +
            a0.v * b1.vp3 * (3 * a0.v + 2 * b0.v)) / 70;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 3 * ((a0.vp2 + (b0.v + c0.v) * a0.v + c0.v * b0.v / 3) * a1.vp2
                + (3 * (b1.v + c1.v) * a0.v + 2 * b0.v * c1.v + 2 * b1.v * c0.v) * a0.v * a1.v / 2
                + a0.vp2 * b1.v * c1.v) * a1.v / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((a0.vp2 + 2 * (2 * b0.v + c0.v) * a0.v + b0.v * (b0.v + 2 * c0.v)) * a1.vp3 +
            (3 * (2 * b1.v + c1.v) * a0.vp2 + (6 * (b0.v + c0.v) * b1.v + 6 * b0.v * c1.v) * a0.v +
             b0.vp2 * c1.v + 2 * b0.v * c0.v * b1.v) * a1.vp2
            + (3 * (b1.v + 2 * c1.v) * a0.v + 4 * b0.v * c1.v + 2 * b1.v * c0.v) * a0.v * b1.v *
              a1.v + a0.vp2 * b1.vp2 * c1.v) / 56;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (((3 * b0.v + c0.v) * a0.v + 3 * b0.v * (b0.v + c0.v)) * a1.vp3
            + 3 * (3 * (a0.vp2 + 2 * (2 * b0.v + c0.v) * a0.v + b0.v * (b0.v + 2 * c0.v)) * b1.v
                   + c1.v * (a0.vp2 + 6 * a0.v * b0.v + 3 * b0.vp2)) * a1.vp2 / 2
            + 9 * b1.v * (b1.v * (a0.vp2 + (b0.v + c0.v) * a0.v + c0.v * b0.v / 3)
                          + (a0.vp2 + 2 * a0.v * b0.v + b0.vp2 / 3) * c1.v) * a1.v +
            (b1.v * (3 * a0.v + 2 * c0.v)
             + 3 * (3 * a0.v + 2 * b0.v) * c1.v) * a0.v * b1.vp2 / 2) / 140;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((2 * (b0.v + c0.v) * a0.v + b0.vp2 + 4 * c0.v * b0.v + c0.vp2) * a1.vp3
            + (3 * (b1.v + c1.v) * a0.vp2 + 6 * ((b0.v + 2 * c0.v) * b1.v
                                                 + (2 * b0.v + c0.v) * c1.v) * a0.v +
               3 * (2 * c0.v * b0.v + c0.vp2) * b1.v
               + 3 * b0.v * c1.v * (b0.v + 2 * c0.v)) * a1.vp2 +
            (3 * (b1.vp2 + 4 * c1.v * b1.v + c1.vp2) * a0.vp2
             + 6 * (b1.vp2 * c0.v + 2 * c1.v * (b0.v + c0.v) * b1.v + b0.v * c1.vp2) * a0.v +
             b0.vp2 * c1.vp2
             + 4 * b0.v * b1.v * c0.v * c1.v + b1.vp2 * c0.vp2) * a1.v
            + (3 * (b1.v + c1.v) * a0.v + 2 * b0.v * c1.v + 2 * b1.v * c0.v) * a0.v * c1.v * b1.v) /
           140;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((3 * b0.vp2 + 6 * c0.v * b0.v + c0.vp2) * a1.vp3
            + (9 * (2 * (b0.v + c0.v) * a0.v + b0.vp2 + 4 * c0.v * b0.v + c0.vp2) * b1.v
               + 6 * ((3 * b0.v + c0.v) * a0.v + 3 * b0.v * (b0.v + c0.v)) * c1.v) * a1.vp2
            + (9 * (a0.vp2 + 2 * (b0.v + 2 * c0.v) * a0.v + 2 * c0.v * b0.v + c0.vp2) * b1.vp2
               +
               18 * (a0.vp2 + (4 * b0.v + 2 * c0.v) * a0.v + b0.v * (b0.v + 2 * c0.v)) * c1.v * b1.v
               + 3 * (a0.vp2 + 6 * a0.v * b0.v + 3 * b0.vp2) * c1.vp2) * a1.v
            + 3 * (b1.vp2 * (a0.vp2 + 2 * a0.v * c0.v + c0.vp2 / 3)
                   + 2 * b1.v * (3 * (a0.vp2 + (b0.v + c0.v) * a0.v) + c0.v * b0.v) * c1.v
                   + (3 * a0.vp2 + 6 * a0.v * b0.v + b0.vp2) * c1.vp2) * b1.v) / 560;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template<>
std::vector<double> B8_Xp2_Yp3(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p2_1p3<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B8_Xp2_Yp3(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p2_1p3<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B8_Xp3_Yp2(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p2_1p3<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B8_Xp3_Yp2(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p2_1p3<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}


template<typename T>
std::vector<T> b8_0p1_1p5(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a1.vp5 * a0.v;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((2 * a0.v + b0.v) * a1.vp5 + 5 * a1.vp4 * a0.v * b1.v) / 8;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp3 * (a1.vp2 * (a0.v + 2 * b0.v)
                     + 5 * (b1.v * (2 * a0.v + b0.v) * a1.v + 2 * b1.vp2 * a0.v)) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp2 * (a1.vp3 * b0.v + 5 * b1.v * (a0.v + 2 * b0.v) * a1.vp2 +
                     10 * b1.vp2 * (2 * a0.v + b0.v) * a1.v + 10 * a0.v * b1.vp3) / 56;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return b1.v * a1.v * (a1.vp3 * b0.v + 2 * b1.v * (a0.v + 2 * b0.v) * a1.vp2
                          + 2 * b1.vp2 * (2 * a0.v + b0.v) * a1.v + a0.v * b1.vp3) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp3 * ((a0.v + b0.v + c0.v) * a1.vp2
                     + 5 * ((b1.v + c1.v) * a0.v + (b1.v * c0.v + c1.v * b0.v) / 2) * a1.v +
                     10 * b1.v * c1.v * a0.v) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp2 * ((b0.v + c0.v / 2) * a1.vp3 + 5 * ((a0.v + b0.v + c0.v) * b1.v
                                                       + c1.v * (a0.v + 2 * b0.v) / 2) * a1.vp2 +
                     10 * b1.v * (b1.v * (a0.v + c0.v / 2)
                                  + c1.v * (2 * a0.v + b0.v)) * a1.v + 15 * a0.v * b1.vp2 * c1.v) /
           84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v * (((b0.v + c0.v / 2) * b1.v + c1.v * b0.v / 2) * a1.vp3
                   + 2 * b1.v * (b1.v * (a0.v + b0.v + c0.v) + c1.v * (a0.v + 2 * b0.v)) * a1.vp2
                   + b1.vp2 * (b1.v * (2 * a0.v + c0.v) + 3 * c1.v * (2 * a0.v + b0.v)) * a1.v +
                   2 * a0.v * b1.vp3 * c1.v) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v * ((b1.v * (b0.v + 2 * c0.v) + (2 * b0.v + c0.v) * c1.v) * a1.vp3
                   + ((2 * a0.v + 4 * c0.v) * b1.vp2 + 8 * b1.v * (a0.v + b0.v + c0.v) * c1.v
                      + 2 * c1.vp2 * (a0.v + 2 * b0.v)) * a1.vp2
                   + 12 * b1.v * ((a0.v + c0.v / 2) * b1.v + (a0.v + b0.v / 2) * c1.v) * c1.v * a1.v
                   + 6 * a0.v * b1.vp2 * c1.vp2) / 84;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((((b0.v + 2 * c0.v) * b1.vp2 + 4 * c1.v * (b0.v + c0.v / 2) * b1.v
              + b0.v * c1.vp2) * a1.vp3
             + b1.v * (b1.vp2 * (a0.v + 2 * c0.v) + 6 * b1.v * (a0.v + b0.v + c0.v) * c1.v
                       + 3 * c1.vp2 * (a0.v + 2 * b0.v)) * a1.vp2 + a0.v * b1.vp3 * c1.vp2) / 4 +
            b1.vp2 * (b1.v * (a0.v + c0.v / 2) + 3 * c1.v * (a0.v + b0.v / 2) / 2) * c1.v * a1.v) /
           14;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template<>
std::vector<double> B8_Xp1_Yp5(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p1_1p5<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B8_Xp1_Yp5(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p1_1p5<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B8_Xp5_Yp1(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p1_1p5<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B8_Xp5_Yp1(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p1_1p5<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b8_0p2_1p4(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a1.vp4 * a0.vp2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a1.v + 2 * b1.v) * a0.v + a1.v * b0.v) * a1.vp3 * a0.v / 4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp2 * ((a0.vp2 + 4 * b0.v * a0.v + b0.vp2) * a1.vp2
                     + 8 * b1.v * a0.v * (a0.v + b0.v) * a1.v + 6 * b1.vp2 * a0.vp2) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.v * (b0.v * (a0.v + b0.v) * a1.vp3
                   + 2 * (b1.v * (a0.vp2 + 4 * b0.v * a0.v + b0.vp2) * a1.vp2
                          + 3 * a0.v * b1.vp2 * (a0.v + b0.v) * a1.v + a0.vp2 * b1.vp3)) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a1.vp4 * b0.vp2 + a0.vp2 * b1.vp4) / 2 + 4 * b1.v * b0.v * (a0.v + b0.v) * a1.vp3
            + 3 * b1.vp2 * (a0.vp2 + 4 * b0.v * a0.v + b0.vp2) * a1.vp2
            + 4 * a0.v * (a0.v + b0.v) * b1.vp3 * a1.v) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((a0.vp2 + 2 * (b0.v + c0.v) * a0.v + c0.v * b0.v) * a1.vp2
            + 4 * a0.v * ((b1.v + c1.v) * a0.v + b1.v * c0.v + c1.v * b0.v) * a1.v
            + 6 * a0.vp2 * b1.v * c1.v) * a1.vp2 / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (((b0.v + c0.v / 2) * a0.v + b0.v * (b0.v / 2 + c0.v)) * a1.vp3
            + ((2 * b1.v + c1.v) * a0.vp2 + 4 * ((b0.v + c0.v) * b1.v + c1.v * b0.v) * a0.v
               + 2 * b1.v * b0.v * c0.v + b0.vp2 * c1.v) * a1.vp2
            + 3 * b1.v * a0.v * ((b1.v + 2 * c1.v) * a0.v + b1.v * c0.v + 2 * c1.v * b0.v) * a1.v
            + 3 * a0.vp2 * b1.vp2 * c1.v) * a1.v / 42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((b0.v * (b0.v + c0.v) * a1.vp4 / 2
             + 3 * b1.v * (b1.v * (a0.vp2 + 2 * (b0.v + c0.v) * a0.v + c0.v * b0.v)
                           + c1.v * (a0.vp2 + 4 * b0.v * a0.v + b0.vp2)) * a1.vp2 +
             a0.vp2 * b1.vp3 * c1.v) / 2
            + (((2 * b0.v + c0.v) * a0.v + b0.v * (b0.v + 2 * c0.v)) * b1.v
               + c1.v * b0.v * (a0.v + b0.v)) * a1.vp3
            + b1.vp2 * a0.v * (b1.v * (a0.v + c0.v) + 3 * c1.v * (a0.v + b0.v)) * a1.v) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (((b0.vp2 + 4 * c0.v * b0.v + c0.vp2) * a1.vp4 / 4
             + 2 * (((b0.v + 2 * c0.v) * b1.v + c1.v * (2 * b0.v + c0.v)) * a0.v
                    + (2 * c0.v * b0.v + c0.vp2) * b1.v
                    + c1.v * b0.v * (b0.v + 2 * c0.v)) * a1.vp3) / 3
            + ((b1.vp2 + 4 * c1.v * b1.v + c1.vp2) * a0.vp2
               + 4 * (b1.vp2 * c0.v + 2 * c1.v * (b0.v + c0.v) * b1.v + b0.v * c1.vp2) * a0.v
               + b1.vp2 * c0.vp2
               + 4 * b1.v * b0.v * c1.v * c0.v + b0.vp2 * c1.vp2) * a1.vp2 / 2
            + 2 * b1.v * a0.v * ((b1.v + c1.v) * a0.v + b1.v * c0.v + c1.v * b0.v) * c1.v * a1.v
            + a0.vp2 * b1.vp2 * c1.vp2 / 2) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((((b0.vp2 + 4 * c0.v * b0.v + c0.vp2) * b1.v + 2 * c1.v * b0.v * (b0.v + c0.v)) * a1.vp3
             + 3 * (((b0.v + 2 * c0.v) * a0.v + 2 * c0.v * b0.v + c0.vp2) * b1.vp2
                    + 2 * ((2 * b0.v + c0.v) * a0.v + b0.v * (b0.v + 2 * c0.v)) * c1.v * b1.v +
                    b0.v * c1.vp2 * (a0.v + b0.v)) * a1.vp2
             + b1.v * (b1.vp2 * (a0.vp2 + 4 * c0.v * a0.v + c0.vp2) +
                       6 * b1.v * (a0.vp2 + 2 * (b0.v + c0.v) * a0.v
                                   + c0.v * b0.v) * c1.v +
                       3 * c1.vp2 * (a0.vp2 + 4 * b0.v * a0.v + b0.vp2)) * a1.v) / 2
            + b1.vp2 * a0.v * c1.v * (b1.v * (a0.v + c0.v) + 3 * c1.v * (a0.v + b0.v) / 2)) / 70;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template<>
std::vector<double> B8_Xp2_Yp4(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p2_1p4<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B8_Xp2_Yp4(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p2_1p4<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B8_Xp4_Yp2(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p2_1p4<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B8_Xp4_Yp2(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p2_1p4<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> B8_Xp3_Yp3(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp3 * a1.vp3;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a0.vp2 * a1.vp2 * ((a0.v + 3 * b0.v / 2) * a1.v + 3 * a0.v * b1.v / 2) / 4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a0.v * ((a0.vp2 + 6 * a0.v * b0.v + 3 * b0.vp2) * a1.vp2
                   + 3 * (a0.v * (2 * a0.v + 3 * b0.v) * b1.v * a1.v
                          + a0.vp2 * b1.vp2)) * a1.v / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (((3 * a0.vp2 * b0.v + 6 * a0.v * b0.vp2 + b0.vp3) * a1.vp3
             + 3 * a0.v * b1.v * (a0.vp2 + 6 * a0.v * b0.v + 3 * b0.vp2) * a1.vp2 +
             a0.vp3 * b1.vp3) / 2
            + 3 * a0.vp2 * (a0.v + 3 * b0.v / 2) * b1.vp2 * a1.v) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (((3 * a0.v * b0.vp2 + 2 * b0.vp3) * a1.vp3
             + 3 * b0.v * b1.v * (3 * a0.vp2 + 6 * a0.v * b0.v + b0.vp2) * a1.vp2
             + 3 * a0.v * b1.vp2 * (a0.vp2 + 6 * a0.v * b0.v + 3 * b0.vp2) * a1.v) / 2
            + a0.vp2 * (a0.v + 3 * b0.v / 2) * b1.vp3) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a0.v * ((a0.vp2 + 3 * (b0.v + c0.v) * a0.v
                    + 3 * c0.v * b0.v) * a1.vp2
                   + 3 * ((b1.v + c1.v) * a0.v + 3 * (b0.v * c1.v + b1.v * c0.v) / 2) *
                     a0.v * a1.v + 3 * a0.vp2 * b1.v * c1.v) * a1.v / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((((2 * b0.v + c0.v) * a0.vp2 + 2 * b0.v * (b0.v + 2 * c0.v) * a0.v
              + b0.vp2 * c0.v) * a1.vp3 + a0.vp3 * b1.vp2 * c1.v) / 2 +
            a0.v * ((b1.v + c1.v / 2) * a0.vp2
                    + 3 * (((b0.v + c0.v) * b1.v + b0.v * c1.v) * a0.v + b0.vp2 * c1.v / 2
                           + b0.v * c0.v * b1.v)) * a1.vp2
            + a0.vp2 * ((b1.v + 2 * c1.v) * a0.v + 3 * b0.v * c1.v
                        + 3 * b1.v * c0.v / 2) * b1.v * a1.v) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((3 * (b0.v + c0.v) * a0.v + b0.v * (b0.v + 3 * c0.v)) * b0.v * a1.vp3
            + (9 * ((2 * b0.v + c0.v) * b1.v + b0.v * c1.v) * a0.vp2
               + 18 * ((b0.v + 2 * c0.v) * b1.v + b0.v * c1.v) * b0.v * a0.v + 3 * b0.vp3 * c1.v
               + 9 * b0.vp2 * c0.v * b1.v) * a1.vp2 / 2
            + 3 * a0.v * ((b1.v + c1.v) * a0.vp2
                          + (3 * (b0.v + c0.v) * b1.v + 6 * b0.v * c1.v) * a0.v
                          + 3 * b0.vp2 * c1.v + 3 * b0.v * c0.v * b1.v) * b1.v * a1.v
            + a0.vp2 * ((b1.v + 3 * c1.v) * a0.v
                        + 3 * (3 * b0.v * c1.v + b1.v * c0.v) / 2) * b1.vp2) / 140;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((((b0.vp2 + 4 * c0.v * b0.v + c0.vp2) * a0.v + 2 * b0.v * c0.v * (b0.v + c0.v)) * a1.vp3
             + (3 * ((b0.v + 2 * c0.v) * b1.v + (2 * b0.v + c0.v) * c1.v) * a0.vp2
                + 6 * ((2 * c0.v * b0.v + c0.vp2) * b1.v + b0.v * c1.v * (b0.v + 2 * c0.v)) * a0.v
                + 3 * b0.vp2 * c1.v * c0.v + 3 * b1.v * c0.vp2 * b0.v) * a1.vp2
             + a0.v * ((b1.vp2 + 4 * c1.v * b1.v + c1.vp2) * a0.vp2 +
                       6 * (b1.vp2 * c0.v + 2 * c1.v * (b0.v + c0.v) * b1.v + b0.v * c1.vp2) * a0.v
                       + 3 * b0.vp2 * c1.vp2 + 12 * b0.v * b1.v * c0.v * c1.v +
                       3 * b1.vp2 * c0.vp2) * a1.v) / 2
            + ((b1.v + c1.v) * a0.v + 3 * b0.v * c1.v / 2 + 3 * b1.v * c0.v / 2) * a0.vp2 * c1.v *
              b1.v) / 70;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (b0.v * (b0.vp2 + 6 * c0.v * b0.v + 3 * c0.vp2) * a1.vp3
            + ((9 * (b0.vp2 + 4 * c0.v * b0.v + c0.vp2) * a0.v
                + 18 * b0.v * c0.v * (b0.v + c0.v)) * b1.v
               + 6 * (3 * (b0.v + c0.v) * a0.v + b0.v * (b0.v + 3 * c0.v)) * c1.v * b0.v) * a1.vp2
            + (9 * ((b0.v + 2 * c0.v) * a0.vp2 + 2 * (2 * c0.v * b0.v + c0.vp2) * a0.v
                    + b0.v * c0.vp2) * b1.vp2
               + 18 * c1.v * ((2 * b0.v + c0.v) * a0.vp2 + 2 * b0.v * (b0.v + 2 * c0.v) * a0.v
                              + b0.vp2 * c0.v) * b1.v
               + 3 * c1.vp2 * b0.v * (3 * (a0.vp2 + 2 * a0.v * b0.v) + b0.vp2)) * a1.v
            + a0.v * (b1.vp2 * (a0.vp2 + 6 * a0.v * c0.v + 3 * c0.vp2) +
                      6 * (a0.vp2 + 3 * (b0.v + c0.v) * a0.v + 3 * c0.v * b0.v) * b1.v * c1.v +
                      3 * (a0.vp2 + 6 * a0.v * b0.v + 3 * b0.vp2) * c1.vp2) * b1.v) / 560;
  };

  return EVALUATE<T>(A.X, A.Y, B.X, B.Y, C.X, C.Y, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template std::vector<double> B8_Xp3_Yp3(const VALUES<double> &A,
                                        const VALUES<double> &B,
                                        const VALUES<double> &C);

template std::vector<IAInterval> B8_Xp3_Yp3(const VALUES<IAInterval> &A,
                                            const VALUES<IAInterval> &B,
                                            const VALUES<IAInterval> &C);

template<typename T>
std::vector<T> b8_0p2_1p5(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a1.vp5 * a0.vp2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a1.v + 5 * b1.v) * a0.v + 2 * a1.v * b0.v) * a1.vp4 * a0.v / 8;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a1.vp3 * (b0.v * a1.vp2 * (a0.v + b0.v / 2) +
                     5 * (b1.v * a0.v * (a0.v + 2 * b0.v) * a1.v / 2 + b1.vp2 * a0.vp2)) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (a1.vp3 * b0.vp2 + 10 * (b0.v * a1.vp2 * (a0.v + b0.v / 2) * b1.v +
                                    a1.v * a0.v * (a0.v + 2 * b0.v) * b1.vp2 + a0.vp2 * b1.vp3)) *
           a1.vp2 / 56;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return b1.v * a1.v * (a1.vp3 * b0.vp2 + 4 * b0.v * a1.vp2 * (a0.v + b0.v / 2) * b1.v +
                          2 * a1.v * a0.v * (a0.v + 2 * b0.v) * b1.vp2 + a0.vp2 * b1.vp3) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.vp3 * (((b0.v + c0.v) * a0.v + c0.v * b0.v) * a1.vp2 +
                     5 * a0.v * ((b1.v + c1.v) * a0.v / 2 + (b1.v * c0.v + c1.v * b0.v)) * a1.v +
                     (10 * a0.vp2) * b1.v * c1.v) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (b0.v * (b0.v + 2 * c0.v) * a1.vp3
            + 5 * ((2 * (((b0.v + c0.v) * b1.v + c1.v * b0.v) * a0.v + b1.v * b0.v * c0.v)
                    + b0.vp2 * c1.v) * a1.vp2
                   + 2 * b1.v * a0.v * ((b1.v + 2 * c1.v) * a0.v + 2 * b1.v * c0.v +
                                        4 * c1.v * b0.v) * a1.v
                   + 6 * a0.vp2 * b1.vp2 * c1.v)) * a1.vp2 / 168;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((b1.v * (b0.v + 2 * c0.v) + c1.v * b0.v) * b0.v * a1.vp3
            + 4 * b1.v * (b1.v * ((b0.v + c0.v) * a0.v + c0.v * b0.v)
                          + (2 * a0.v + b0.v) * b0.v * c1.v) * a1.vp2 +
            2 * b1.vp2 * a0.v * (b1.v * (a0.v + 2 * c0.v) + 3 * c1.v * (a0.v + 2 * b0.v)) * a1.v +
            4 * a0.vp2 * b1.vp3 * c1.v) * a1.v / 56;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((b1.v * c0.v * (b0.v + c0.v / 2) + c1.v * b0.v * (b0.v / 2 + c0.v)) * a1.vp3 +
            ((2 * c0.v * a0.v + c0.vp2) * b1.vp2 +
             4 * b1.v * ((b0.v + c0.v) * a0.v + c0.v * b0.v) * c1.v +
             (2 * a0.v + b0.v) * b0.v * c1.vp2) * a1.vp2 +
            3 * b1.v * a0.v * (b1.v * (a0.v + 2 * c0.v) + c1.v * (a0.v + 2 * b0.v)) * c1.v * a1.v +
            3 * a0.vp2 * b1.vp2 * c1.vp2) * a1.v / 42;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((((2 * c0.v * b0.v + c0.vp2) * b1.vp2 + 2 * c1.v * b0.v * (b0.v + 2 * c0.v) * b1.v +
              b0.vp2 * c1.vp2) * a1.vp3 + a0.vp2 * b1.vp3 * c1.vp2) / 2
            + b1.v * (b1.vp2 * c0.v * (a0.v + c0.v / 2)
                      + 3 * b1.v * ((b0.v + c0.v) * a0.v + c0.v * b0.v) * c1.v
                      + 3 * (a0.v + b0.v / 2) * b0.v * c1.vp2) * a1.vp2
            + b1.vp2 * a0.v * (b1.v * (a0.v + 2 * c0.v)
                               + 3 * c1.v * (a0.v / 2 + b0.v)) * c1.v * a1.v) / 28;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template<>
std::vector<double> B8_Xp2_Yp5(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p2_1p5<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B8_Xp2_Yp5(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p2_1p5<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B8_Xp5_Yp2(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p2_1p5<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B8_Xp5_Yp2(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p2_1p5<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b8_0p3_1p4(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp3 * a1.vp4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a0.v + 3 * b0.v) * a1.v + 4 * a0.v * b1.v) * a1.vp3 * a0.vp2 / 8;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((3 * b0.v * (a0.v + b0.v) * a1.vp2 / 2 + 3 * a0.vp2 * b1.vp2) / 2
            + a0.v * b1.v * (a0.v + 3 * b0.v) * a1.v) * a1.vp2 * a0.v / 7;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return 3 * a1.v * (b0.vp2 * (a0.v + b0.v / 3) * a1.vp3 / 2 +
                       2 * a0.v * b0.v * b1.v * (a0.v + b0.v) * a1.vp2
                       + a0.vp2 * b1.vp2 * (a0.v + 3 * b0.v) * a1.v + 2 * a0.vp3 * b1.vp3 / 3) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a1.vp4 * b0.vp3 + a0.vp3 * b1.vp4) / 2
            + 2 * b0.vp2 * (3 * a0.v + b0.v) * b1.v * a1.vp3 +
            9 * a0.v * b0.v * b1.vp2 * (a0.v + b0.v) * a1.vp2
            + 2 * a0.vp2 * (a0.v + 3 * b0.v) * b1.vp3 * a1.v) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (3 * ((((b0.v + c0.v) * a0.v / 2 + c0.v * b0.v) / 2) * a1.vp2 + a0.vp2 * b1.v * c1.v)
            + a0.v * (a0.v * (b1.v + c1.v) + 3 * (b0.v * c1.v + c0.v * b1.v)) * a1.v) * a0.v *
           a1.vp2 / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v * (((b0.v + 2 * c0.v) * a0.v + c0.v * b0.v) * b0.v * a1.vp3 / 2
                   + 2 * a0.v * ((b1.v * (b0.v + c0.v) + b0.v * c1.v) * a0.v + b0.vp2 * c1.v
                                 + 2 * b0.v * b1.v * c0.v) * a1.vp2 +
                   b1.v * a0.vp2 * (a0.v * (b1.v + 2 * c1.v)
                                    + 6 * b0.v * c1.v + 3 * c0.v * b1.v) * a1.v +
                   2 * a0.vp3 * b1.vp2 * c1.v) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((b0.vp2 * (b0.v + 3 * c0.v) * a1.vp4 / 2
             + 9 * b1.v * a0.v * (((b0.v + c0.v) * a0.v + 2 * c0.v * b0.v) * b1.v
                                  + 2 * b0.v * c1.v * (a0.v + b0.v)) * a1.vp2) / 2
            + b0.v * (3 * ((b0.v + 2 * c0.v) * a0.v + c0.v * b0.v) * b1.v
                      + b0.v * c1.v * (3 * a0.v + b0.v)) * a1.vp3 +
            b1.vp2 * ((a0.v + 3 * c0.v) * b1.v
                      + 3 * (a0.v + 3 * b0.v) * c1.v) * a0.vp2 * a1.v + a0.vp3 * b1.vp3 * c1.v) /
           70;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((b0.v * c0.v * (b0.v + c0.v) * a1.vp4 / 2 + a0.vp3 * b1.vp2 * c1.vp2) / 2
            + (((2 * c0.v * b0.v + c0.vp2) * b1.v + b0.v * c1.v * (b0.v + 2 * c0.v)) * a0.v
               + b0.vp2 * c1.v * c0.v + c0.vp2 * b1.v * b0.v) * a1.vp3
            + 3 * a0.v * (((b1.vp2 * c0.v + b0.v * c1.vp2) / 2
                           + c1.v * (b0.v + c0.v) * b1.v) * a0.v + b0.vp2 * c1.vp2 / 2
                          + 2 * b0.v * b1.v * c0.v * c1.v + b1.vp2 * c0.vp2 / 2) * a1.vp2
            + c1.v * b1.v * a0.vp2 * (a0.v * (b1.v + c1.v) + 3 * b0.v * c1.v
                                      + 3 * c0.v * b1.v) * a1.v) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (b0.v * (3 * c0.v * b1.v * (b0.v + c0.v) + b0.v * c1.v * (b0.v + 3 * c0.v)) * a1.vp3
            + 9 * (c0.v * b1.vp2 * (a0.v + c0.v) / 3
                   + ((b0.v + c0.v) * a0.v + 2 * c0.v * b0.v) * b1.v * c1.v +
                   b0.v * c1.vp2 * (a0.v + b0.v)) * b1.v * a0.v * a1.v
            + c1.v * ((a0.v + 3 * c0.v) * b1.v + 3 * (a0.v + 3 * b0.v) * c1.v / 2) * b1.vp2 * a0.vp2
            + (9 * ((2 * c0.v * b0.v + c0.vp2) * a0.v + b0.v * c0.vp2) * b1.vp2
               + 18 * ((b0.v + 2 * c0.v) * a0.v + c0.v * b0.v) * c1.v * b0.v * b1.v
               + 3 * c1.vp2 * b0.vp2 * (3 * a0.v + b0.v)) * a1.vp2 / 2) / 140;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template<>
std::vector<double> B8_Xp3_Yp4(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p3_1p4<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B8_Xp3_Yp4(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p3_1p4<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B8_Xp4_Yp3(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p3_1p4<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B8_Xp4_Yp3(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p3_1p4<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> b8_0p3_1p5(const COMP<T> &a0, const COMP<T> &a1,
                          const COMP<T> &b0, const COMP<T> &b1,
                          const COMP<T> &c0, const COMP<T> &c1) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp3 * a1.vp5;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (5 * a0.vp3 * a1.vp4 * b1.v + 3 * a0.vp2 * a1.vp5 * b0.v) / 8;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (5 * a0.vp3 * a1.vp3 * b1.vp2
            + 3 * (5 * a0.vp2 * a1.vp4 * b0.v * b1.v + a0.v * a1.vp5 * b0.vp2) / 2) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a1.vp5 * b0.vp3 + 15 * a0.v * a1.vp4 * b0.vp2 * b1.v) / 2
            + 5 * (a0.vp3 * a1.vp2 * b1.vp3 + 3 * a0.vp2 * a1.vp3 * b0.v * b1.vp2)) / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return b1.v * a1.v * (a0.vp3 * b1.vp3 + a1.vp3 * b0.vp3
                          + 6 * (a0.vp2 * a1.v * b0.v * b1.vp2 + a0.v * a1.vp2 * b0.vp2 * b1.v)) /
           14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5 * a0.v *
           (3 * (a1.vp2 * c0.v * b0.v / 10 + a0.v * (b0.v * c1.v + c0.v * b1.v) * a1.v / 4)
            + a0.vp2 * b1.v * c1.v) * a1.vp3 / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return 5 * (a1.vp3 * b0.vp2 * c0.v / 10 + a0.v * b0.v * (b0.v * c1.v / 2 + c0.v * b1.v) * a1.vp2
                + (2 * b0.v * b1.v * c1.v + b1.vp2 * c0.v) * a0.vp2 * a1.v +
                a0.vp3 * b1.vp2 * c1.v) * a1.vp2 / 28;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a1.v * (4 * a0.vp3 * b1.vp3 * c1.v + 3 * (a1.vp3 * b0.vp2 * b1.v * c0.v
                                                     + 2 * (3 * a0.vp2 * a1.v * b0.v * b1.vp2 * c1.v
                                                            + a0.vp2 * a1.v * b1.vp3 * c0.v
                                                            + 2 *
                                                              (a0.v * a1.vp2 * b0.vp2 * b1.v * c1.v
                                                               + a0.v * a1.vp2 * b0.v * b1.vp2 *
                                                                 c0.v)))
                   + a1.vp3 * b0.vp3 * c1.v) / 56;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (b0.v * c0.v * (b0.v * c1.v + c0.v * b1.v) * a1.vp3 / 2
            + a0.v * (b0.vp2 * c1.vp2 + 4 * b0.v * b1.v * c0.v * c1.v + b1.vp2 * c0.vp2) * a1.vp2
            + 3 * a0.vp2 * b1.v * c1.v * (b0.v * c1.v + c0.v * b1.v) * a1.v
            + a0.vp3 * b1.vp2 * c1.vp2) * a1.v / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (b0.v * (b0.vp2 * c1.vp2 + 6 * b0.v * b1.v * c0.v * c1.v + 3 * b1.vp2 * c0.vp2) * a1.vp3
            + 9 * b1.v * a0.v *
              (b0.vp2 * c1.vp2 + 2 * b0.v * b1.v * c0.v * c1.v + b1.vp2 * c0.vp2 / 3) * a1.vp2
            + (9 * a0.vp2 * b0.v * b1.vp2 * c1.vp2 + 6 * a0.vp2 * b1.vp3 * c0.v * c1.v) * a1.v
            + a0.vp3 * b1.vp3 * c1.vp2) / 56;
  };

  return EVALUATE<T>(a0, a1, b0, b1, c0, c1, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template<>
std::vector<double> B8_Xp3_Yp5(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p3_1p5<double>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<IAInterval> B8_Xp3_Yp5(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p3_1p5<IAInterval>(A.X, A.Y, B.X, B.Y, C.X, C.Y);
}

template<>
std::vector<double> B8_Xp5_Yp3(const VALUES<double> &A,
                               const VALUES<double> &B,
                               const VALUES<double> &C) {
  return b8_0p3_1p5<double>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<>
std::vector<IAInterval> B8_Xp5_Yp3(const VALUES<IAInterval> &A,
                                   const VALUES<IAInterval> &B,
                                   const VALUES<IAInterval> &C) {
  return b8_0p3_1p5<IAInterval>(A.Y, A.X, B.Y, B.X, C.Y, C.X);
}

template<typename T>
std::vector<T> B8_Xp4_Yp4(const VALUES<T> &A, const VALUES<T> &B, const VALUES<T> &C) {
  std::function<T(const COMP<T> &a0, const COMP<T> &a1)>
      F0 = [](const COMP<T> &a0, const COMP<T> &a1) {
    return a0.vp4 * a1.vp4;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F1 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (a0.vp4 * a1.vp3 * b1.v + a0.vp3 * a1.vp4 * b0.v) / 2;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F2 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return (3 * (a0.vp4 * a1.vp2 * b1.vp2 + a0.vp2 * a1.vp4 * b0.vp2) / 2 +
            4 * a0.vp3 * a1.vp3 * b0.v * b1.v) / 7;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F3 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return a0.v * a1.v * (a0.vp3 * b1.vp3 + a1.vp3 * b0.vp3
                          + 6 * (a0.vp2 * a1.v * b0.v * b1.vp2 + a0.v * a1.vp2 * b0.vp2 * b1.v)) /
           14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1)>
      F4 = [](const COMP<T> &a0, const COMP<T> &a1, const COMP<T> &b0, const COMP<T> &b1) {
    return ((a0.vp4 * b1.vp4 + a1.vp4 * b0.vp4) / 2 + 18 * a0.vp2 * a1.vp2 * b0.vp2 * b1.vp2
            + 8 * (a0.vp3 * a1.v * b0.v * b1.vp3 + a0.v * a1.vp3 * b0.vp3 * b1.v)) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F5 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (3 * (a1.vp2 * c0.v * b0.v + a0.vp2 * b1.v * c1.v)
            + 4 * a0.v * (b0.v * c1.v + c0.v * b1.v) * a1.v) * a0.vp2 * a1.vp2 / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F6 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return a0.v * a1.v * (a1.vp3 * b0.vp2 * c0.v + a0.vp3 * b1.vp2 * c1.v
                          + 2 * (a0.v * b0.v * (b0.v * c1.v + 2 * c0.v * b1.v) * a1.vp2
                                 + (2 * b0.v * b1.v * c1.v + b1.vp2 * c0.v) * a0.vp2 * a1.v)) / 14;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F7 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((a1.vp4 * b0.vp3 * c0.v + a0.vp4 * b1.vp3 * c1.v) / 2
            + 2 * a0.v * b0.vp2 * (b0.v * c1.v + 3 * c0.v * b1.v) * a1.vp3
            + 9 * a0.vp2 * b0.v * b1.v * (b0.v * c1.v + c0.v * b1.v) * a1.vp2
            + 2 * (3 * b0.v * b1.vp2 * c1.v + b1.vp3 * c0.v) * a0.vp3 * a1.v) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F8 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return ((a1.vp4 * b0.vp2 * c0.vp2 + a0.vp4 * b1.vp2 * c1.vp2) / 2
            + 4 * a0.v * b0.v * c0.v * (b0.v * c1.v + c0.v * b1.v) * a1.vp3
            + 3 * a0.vp2 * (b0.vp2 * c1.vp2 + 4 * b0.v * b1.v * c0.v * c1.v + b1.vp2 * c0.vp2) *
              a1.vp2
            + 4 * b1.v * c1.v * (b0.v * c1.v + c0.v * b1.v) * a0.vp3 * a1.v) / 35;
  };

  std::function<T(const COMP<T> &a0, const COMP<T> &a1,
                  const COMP<T> &b0, const COMP<T> &b1,
                  const COMP<T> &c0, const COMP<T> &c1)>
      F9 = [](const COMP<T> &a0, const COMP<T> &a1,
              const COMP<T> &b0, const COMP<T> &b1,
              const COMP<T> &c0, const COMP<T> &c1) {
    return (((2 * b0.vp3 * c0.v * c1.v + 3 * b0.vp2 * b1.v * c0.vp2) * a1.vp3
             + 3 * a0.v * b0.v *
               (b0.vp2 * c1.vp2 + 6 * b0.v * b1.v * c0.v * c1.v + 3 * b1.vp2 * c0.vp2) * a1.vp2
             + 9 * b1.v * a0.vp2 *
               (b0.vp2 * c1.vp2 + 2 * b0.v * b1.v * c0.v * c1.v + b1.vp2 * c0.vp2 / 3) * a1.v
             + 3 * a0.vp3 * b0.v * b1.vp2 * c1.vp2) / 2 + a0.vp3 * b1.vp3 * c0.v * c1.v) / 35;
  };

  return EVALUATE<T>(A.X, A.Y, B.X, B.Y, C.X, C.Y, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9);
}

template std::vector<double> B8_Xp4_Yp4(const VALUES<double> &A,
                                        const VALUES<double> &B,
                                        const VALUES<double> &C);

template std::vector<IAInterval> B8_Xp4_Yp4(const VALUES<IAInterval> &A,
                                            const VALUES<IAInterval> &B,
                                            const VALUES<IAInterval> &C);