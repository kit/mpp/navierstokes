#include "NavierStokes.hpp"
#include "IAIO.hpp"
#include "m++.hpp"

int main(int argc, char **argv) {
  Config::SetConfPath("./../navierstokes/conf/");
  Config::SetConfigFileName("m++.conf");
  Mpp::initialize(&argc, argv);

  Config::PrintInfo();

  SetIAOutputPrecision(18, 13);

  navierstokes::run();

  return 0;
}
