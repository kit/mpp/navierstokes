#include "m++.hpp"
#include "Parallel.hpp"
#include "Plotting.hpp"

#include "Gelfand.hpp"
#include "Newton.hpp"

int main(int argc, char **argv) {
  Config::SetConfPath("./../navierstokes/conf/");
  Config::SetConfigFileName("gelfand.conf");
  Mpp::initialize(&argc, argv);  
  Config::PrintInfo();
  bool clear = false;
  Config::Get("ClearData", clear);
  if (clear && PPM->master()) 
    int cleared = system("exec rm -rf data/vtu/*");
  
  GelfandProblem problem;
  GelfandAssemble assemble(problem,2);
  Newton newton; 
  Vector u(0.0, assemble.GetSharedDisc());
  u.PrintInfo();
  assemble.Initialize(u);
  mpp::plot("u0") << u << mpp::endp;
  newton(assemble, u);
  mpp::plot("u") << u << mpp::endp;
  if (u.size() < 100)
    mout << "u " << endl << u << endl;
  mout << "max(u) = " << u.Max() << endl;
  return 0;
}
