#ifndef NAVIERSTOKES_NAVIERSTOKESIPROBLEM_HPP
#define NAVIERSTOKES_NAVIERSTOKESIPROBLEM_HPP

#include <memory>
#include "Config.hpp"
#include "Meshes.hpp"
#include "MeshesCreator.hpp"

class NavierStokesIProblem {
protected:
  int verbose = 1;

  std::string meshesName = "";

  std::shared_ptr<Meshes> meshes = nullptr;

  std::shared_ptr<CoarseGeometry> cGeo = nullptr;
public:
  explicit NavierStokesIProblem(std::string meshName = "") : meshesName(meshName) {
    if (meshesName.empty())
      Config::Get("Mesh", meshesName);

    Config::Get("ProblemVerbose", verbose);
  }

  explicit NavierStokesIProblem(std::shared_ptr<Meshes> meshes) : meshes(std::move(meshes)) {
    Config::Get("ProblemVerbose", verbose);
  }

  virtual ~NavierStokesIProblem() = default;

  virtual bool HasExactSolution() const { return false; }

  void CreateMeshes(MeshesCreator meshesCreator) {
    std::string injectedMeshName = meshesCreator.GetMeshName();
    cGeo = CreateCoarseGeometryShared(injectedMeshName.empty() ? meshesName : injectedMeshName);
    meshes = meshesCreator.WithCoarseGeometry(cGeo).CreateShared();
  }

  const CoarseGeometry &Domain() const { return *cGeo; };

  const Meshes &GetMeshes() const {
    if (meshes == nullptr) Exit("Meshes not initialized") return *meshes;
  }

  bool IsMeshInitialized() const { return meshes != nullptr; }

  const Mesh &GetMesh(int level) const { return (*meshes)[level]; }

  const Mesh &GetMesh(MeshIndex level) const { return (*meshes)[level]; }

  [[nodiscard]] std::shared_ptr<Meshes> IntoMeshes() const {
    if (meshes == nullptr) Exit("Meshes not initialized")
          return this->meshes;
  }

  virtual std::string Name() const = 0;
};


#endif // NAVIERSTOKES_NAVIERSTOKESIPROBLEM_HPP
