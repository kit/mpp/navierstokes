#ifndef NAVIERSTOKESCONSTANTS_HPP
#define NAVIERSTOKESCONSTANTS_HPP

#include "SaveLoad.hpp"
#include "IAInterval.hpp"

namespace navierstokes {

  class NavierStokesConstants {
  protected:
    double gammaInftySqr{};

    double uomegaInftySqr{};
    double duomegaInftySqr{};
    double uomegaCoarseInftySqr{};
    double duomegaCoarseInftySqr{};

    double omegaInftySqr{};
    double domegaInftySqr{};

    double normApprV{};
    double normDApprV{};
    double normDApprVInfty{};

    IAInterval velocityL4Diff{};

    double delta{};
    double delta3{};
    double alpha{};
    double alpha_unique{};
    double alphaPressure{};
    double k{};
    double k_ast{};
    IAInterval constThm{};

    double tau{};
    double kappa{};
    double gamma0{};
    IAInterval gamma1{};
    IAInterval gamma2{};
    IAInterval gamma1_ast{};
    IAInterval gamma2_ast{};

    double rhoAfterDomainHomotopy{};
    double  kAfterDomainHomotopy{};
    int numEVAfterDomainHomotopy{};

  public:
    Loader &load(Loader &loader);

    Saver &save(Saver &saver) const;

    double &GammaInftySqr() { return gammaInftySqr; }

    IAInterval GammaInfty() { return sqrt(IAInterval(gammaInftySqr)); }

    template<bool coarse>
    double &UOmegaInftySqr();

    template<bool coarse>
    const double &UOmegaInftySqr() const;

    template<bool coarse>
    IAInterval UOmegaInfty();

    template<bool coarse>
    double &D_UOmegaInftySqr();

    template<bool coarse>
    const double &D_UOmegaInftySqr() const;

    template<bool coarse>
    IAInterval D_UOmegaInfty();

    double &OmegaInftySqr() { return omegaInftySqr; }


    IAInterval OmegaInfty() { return sqrt(IAInterval(omegaInftySqr)); }

    double &D_OmegaInftySqr() { return domegaInftySqr; }

    IAInterval D_OmegaInfty() { return sqrt(IAInterval(domegaInftySqr)); }

    double &NormApprV() { return normApprV; }

    double &NormDApprV() { return normDApprV; }

    double &NormDApprVInfty() { return normDApprVInfty; }

    IAInterval &VelocityL4Diff() { return velocityL4Diff; }

    double &Delta() { return delta; }

    double &Delta3() { return delta3; }

    double &Alpha() { return alpha; }

    double &AlphaUnique() { return alpha_unique; }

    double &AlphaPressure() { return alphaPressure; }

    template<bool adjoint>
    double &K();

    IAInterval &ConstantThm() { return constThm; }

    double &Tau() { return tau; }

    double &Kappa() { return kappa; }

    double &Gamma0() { return gamma0; }

    template<bool adjoint>
    IAInterval &Gamma1();

    template<bool adjoint>
    IAInterval &Gamma2();

    double &RhoAfterDomainHomotopy() { return rhoAfterDomainHomotopy; }

    double &KAfterDomainHomotopy() { return kAfterDomainHomotopy; }

    int &NumEVAfterDomainHomotopy() { return numEVAfterDomainHomotopy; }
  };

  inline Saver &operator<<(Saver &saver, const NavierStokesConstants &constants) {
    return constants.save(saver);
  }

  inline Loader &operator>>(Loader &loader, NavierStokesConstants &constants) {
    return constants.load(loader);
  }

} //namespace navierstokes

#endif //NAVIERSTOKESCONSTANTS_HPP
