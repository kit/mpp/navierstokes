#ifndef NAVIERSTOKESPROBLEMS_HPP
#define NAVIERSTOKESPROBLEMS_HPP

#include "NavierStokesProblem.hpp"

namespace navierstokes {

//----------------------------------------------------------------------------------------
// NavierStokesProblemBottom
//----------------------------------------------------------------------------------------
  class NavierStokesProblemBottom : public NavierStokesProblem {
  public:
    NavierStokesProblemBottom(double _Re) : NavierStokesProblem(_Re) {}

    std::string Name() const override {
      return "Navier-Stokes Problem: obstacle at bottom";
    }

    bool CellNearObstacle(const Cell &c) const override;

    bool CellInObstacleRow(const Cell &c) const override;

    IACoefficients B4Coefficients_D_Theta_6(const Cell &c) const override;

    IACoefficients B3Coefficients_DD_Theta_6(const Cell &c) const override;

    void MinusB9Coefficients_V(IACoefficients &V0, IACoefficients &V1,
                               const Cell &c) const override;

    void MinusB8Coefficients_DV(IACoefficients &Dx_V0, IACoefficients &Dy_V0,
                                IACoefficients &Dx_V1, const Cell &c) const override;

  protected:
    double theta(const double &y, const Cell &c) const override;

    IAInterval theta(const IAInterval &y, const Cell &c) const override;

    double d_theta(const double &y, const Cell &c) const override;

    IAInterval d_theta(const IAInterval &y, const Cell &c) const override;

    double dd_theta(const double &y, const Cell &c) const override;

    IAInterval dd_theta(const IAInterval &y, const Cell &c) const override;

    double ValueTheta(const Cell &c) const override;
  };

//----------------------------------------------------------------------------------------
// NavierStokesProblemCenter
//----------------------------------------------------------------------------------------
  class NavierStokesProblemCenter : public NavierStokesProblem {
  public:
    NavierStokesProblemCenter(double _Re) : NavierStokesProblem(_Re) {}

    std::string Name() const override {
      return "Navier-Stokes Problem: obstacle in middle";
    }

    bool CellNearObstacle(const Cell &c) const override;

    bool CellInObstacleRow(const Cell &c) const override;

    IACoefficients B4Coefficients_D_Theta_6(const Cell &c) const override;

    IACoefficients B3Coefficients_DD_Theta_6(const Cell &c) const override;

    void MinusB9Coefficients_V(IACoefficients &V0, IACoefficients &V1,
                               const Cell &c) const override;

    void MinusB8Coefficients_DV(IACoefficients &Dx_V0, IACoefficients &Dy_V0,
                                IACoefficients &Dx_V1, const Cell &c) const override;

  protected:
    double theta(const double &y, const Cell &c) const override;

    IAInterval theta(const IAInterval &y, const Cell &c) const override;

    double d_theta(const double &y, const Cell &c) const override;

    IAInterval d_theta(const IAInterval &y, const Cell &c) const override;

    double dd_theta(const double &y, const Cell &c) const override;

    IAInterval dd_theta(const IAInterval &y, const Cell &c) const override;

    double ValueTheta(const Cell &c) const override;
  };

//----------------------------------------------------------------------------------------
// NavierStokesProblemBoth
//----------------------------------------------------------------------------------------
  class NavierStokesProblemBoth : public NavierStokesProblem {
  public:
    NavierStokesProblemBoth(double _Re) : NavierStokesProblem(_Re) {}

    std::string Name() const override {
      return "Navier-Stokes Problem: obstacle at bottom and top";
    }

    bool CellNearObstacle(const Cell &c) const override;

    bool CellInObstacleRow(const Cell &c) const override;

    IACoefficients B4Coefficients_D_Theta_6(const Cell &c) const override;

    IACoefficients B3Coefficients_DD_Theta_6(const Cell &c) const override;

    void MinusB9Coefficients_V(IACoefficients &V0, IACoefficients &V1,
                               const Cell &c) const override;

    void MinusB8Coefficients_DV(IACoefficients &Dx_V0, IACoefficients &Dy_V0,
                                IACoefficients &Dx_V1, const Cell &c) const override;

  protected:
    double theta(const double &y, const Cell &c) const override;

    IAInterval theta(const IAInterval &y, const Cell &c) const override;

    double d_theta(const double &y, const Cell &c) const override;

    IAInterval d_theta(const IAInterval &y, const Cell &c) const override;

    double dd_theta(const double &y, const Cell &c) const override;

    IAInterval dd_theta(const IAInterval &y, const Cell &c) const override;

    double ValueTheta(const Cell &c) const override;
  };

} //namespace navierstokes


#endif //NAVIERSTOKESPROBLEMS_HPP
