#ifndef NAVIERSTOKESPROBLEM_H
#define NAVIERSTOKESPROBLEM_H

#include "Config.hpp"
#include "Algebra.hpp"
#include "NavierStokesIProblem.hpp"

#include "problem/NavierStokesPoiseuille.hpp"
#include "problem/NavierStokesSobolev.hpp"
#include "problem/NavierStokesConstants.hpp"

class Meshes;

namespace navierstokes {

// Subdomains
// =================================================================================================
//            |           |       -17       |     -7     |       -27       |           |
//            |         d3|-----------------|------------|-----------------|           |
//    -999    |    -99    |       -14       |     -4     |       -24       |    -99    |   -999
//            |         d2|-----------------|------------|-----------------|           |
//            |           |       -11       |     -1     |       -21       |           |
// =================================================================================================

  enum NBStrategy {
    NB_Wieners, NB_PlumSimple, NB_PlumExtended
  };

  struct NavierStokesLevels {
    int pLevel = 0;
    int velocityLevel = 2;
    int velocityCoarseLevel = 2;
    int pressureLevel = 2;
    int velocityDefectLevel = 2;
    int pressureDefectLevel = 2;
    bool velocityLevelsDiffer = true;
    int coarseLevel = 2;
    int fineLevel = 2;

    NavierStokesLevels(NBStrategy nbStrat);

    NavierStokesLevels &operator=(const NavierStokesLevels &levels);

    Loader &load(Loader &loader);

    Saver &save(Saver &saver) const;

    friend bool operator==(const NavierStokesLevels &l1, const NavierStokesLevels &l2);

    friend bool operator!=(const NavierStokesLevels &l1, const NavierStokesLevels &l2);
  };

  inline Saver &operator<<(Saver &saver, const NavierStokesLevels &levels) {
    return levels.save(saver);
  }

  inline Loader &operator>>(Loader &loader, NavierStokesLevels &levels) {
    return levels.load(loader);
  }

  enum NavierStokesPart {
    PartFirst,
    PartVelocityNorms = PartFirst,
    PartApprVNorms,
    PartDefectVelocity,
    PartNormbound,
    PartSurjectivity,
    PartProofVelocity,
    PartDefectPressure,
    PartLast,
    PartComputeVelocity,
    PartComputeApprV,
    PartComputePressure,
    PartPlotVelocity,
    PartPlotPressure,
  };

  class PartsToRun {
    NavierStokesPart begin;
    NavierStokesPart end;
    std::list<NavierStokesPart> partsToRun{};
  public:
    PartsToRun();

    //returns if part is about to run or not
    bool operator()(NavierStokesPart part);

    NavierStokesPart Begin() const { return begin; }

    NavierStokesPart End() const { return end; }

    void Reset(NavierStokesPart begin);

    void ResetExceptApproximation(NavierStokesPart begin);
  };

  struct VectorData {
    std::shared_ptr<NonAdaptiveIDiscretization> discVelocity = nullptr;
    std::shared_ptr<IANonAdaptiveIDiscretization> iaDiscVelocity = nullptr;
    std::unique_ptr<Vector> vectorVelocity = nullptr;
    std::unique_ptr<Vector> vectorVelocityCoarse = nullptr;
    std::unique_ptr<Vector> vectorApprV = nullptr;

    std::shared_ptr<NonAdaptiveIDiscretization> discPressure = nullptr;
    std::shared_ptr<IANonAdaptiveIDiscretization> iaDiscPressure = nullptr;
    std::unique_ptr<Vector> vectorPressure = nullptr;

    void ClearVelocity();

    void ClearPressure();
  };

  class NavierStokesObstacle {
    friend class NavierStokesSubdomainSetter;
  protected:
    double d0 = 2.5;
    double d1 = 0.5;
    double d2 = 0.5;
    double d3 = 1.0;

    NavierStokesObstacle();
  };

  class NavierStokesProblemConstants {
  protected:
    double re;
    IAInterval reIA;
    double sigma = 1.0;

    NavierStokesProblemConstants(double _Re);

    void adaptSigma(NBStrategy nbStrat);
  };

//========================================================================================
// NavierStokesProblem base class
//========================================================================================
  class NavierStokesProblem : public NavierStokesIProblem,
                              public NavierStokesProblemConstants,
                              public NavierStokesObstacle,
                              public NavierStokesPoiseuille,
                              public NavierStokesSobolev {
    static bool disableSaveLoad;
  public:
    static void DisableSaveLoad();

    static bool SaveLoadDisabled() { return disableSaveLoad; }

  protected:
    bool verified = true;
    std::string coarseGeometryName = "None";
    std::string fileName = "";
    std::string pathPrefix = "data/";
    NBStrategy nbStrategy = NB_PlumExtended;
    std::unique_ptr<NavierStokesLevels> levels = nullptr;
    PartsToRun partsToRun;
    NavierStokesConstants constants;
    VectorData vectorData;

    double Rappr = -1.0;
    double Reig = -1.0;

  public:
    NavierStokesProblem(double _Re);

    void PrintInfo();

    NBStrategy NormboundStrategy() const { return nbStrategy; }

    std::string NormboundStrategyName() const;

    NavierStokesConstants &Constants() { return constants; }

    const NavierStokesConstants &Constants() const { return constants; }

    bool Verified() const { return verified; }

    bool IsHomotopy() const;

    double Re() const { return re; }

    template<typename T>
    const T &Reynolds() const;

    bool ContinuousReynolds() const { return inf(reIA) != sup(reIA); };

    double Sigma() const { return sigma; }

    double getApproximationDomainX() const { return Rappr; }

    constexpr double getApproximationDomainY() const { return 1.0; }

    double getEigenvalueDomainX() const { return Reig; }

    constexpr double getEigenvalueDomainY() const { return 1.0; }

    void SaveData(NavierStokesPart part);

    void LoadData();

    bool RunPart(NavierStokesPart part);

    void ResetPartsToRun(NavierStokesPart begin);

// Meshes
//----------------------------------------------------------------------------------------
    std::string MeshesName() const { return meshes->Name(); }

    void InitMeshes();

    void ClearMeshes() { meshes = nullptr; }

    const NavierStokesLevels &Levels() const { return *levels; }

// Velocity functions
//----------------------------------------------------------------------------------------
    template<bool coarse>
    void SetUpVelocity();

    template<bool coarse>
    bool VelocityInitialized() const;

    template<typename T>
    const NonAdaptiveIDiscretizationT<T, SpaceDimension, TimeDimension> &DiscVelocity() const;

    template<bool coarse>
    Vector &Velocity();

    template<bool coarse>
    const Vector &Velocity() const;

// ApprV functions
//----------------------------------------------------------------------------------------
    void SetUpApprV();

    bool ApprVInitialized() const { return vectorData.vectorApprV != nullptr; }

    Vector &ApprV() { return *vectorData.vectorApprV; }

    const Vector &ApprV() const { return *vectorData.vectorApprV; }

    void ClearVelocity() { vectorData.ClearVelocity(); }

// Pressure functions
//----------------------------------------------------------------------------------------
    void SetUpPressure();

    bool PressureInitialized() const { return vectorData.vectorPressure != nullptr; }

    template<typename T>
    const NonAdaptiveIDiscretizationT<T, SpaceDimension, TimeDimension> &DiscPressure() const;

    Vector &Pressure() { return *vectorData.vectorPressure; }

    const Vector &Pressure() const { return *vectorData.vectorPressure; }

    void ClearPressure() { vectorData.ClearPressure(); }

// Paths for SaveLoad and plot
//----------------------------------------------------------------------------------------
    template<bool coarse>
    std::string PathVelocity() const;

    std::string PathApprV() const;

    std::string PathPressure() const;

    std::string PathData() const;

    std::string PlotName() const;

// Subdomains, cell location
//----------------------------------------------------------------------------------------
  public:
    /// Returns if the cell is contained in the rectangular with the obstacle
    virtual bool CellNearObstacle(const Cell &c) const = 0;

    virtual bool CellInObstacleRow(const Cell &c) const = 0;

    bool CellInObstacleColumn(const Cell &c) const;

    bool CellOutside(const Cell &c) const;

    bool CellOutsideApproximationDomain(const Cell &c) const;

    std::function<short (const Cell &)> GetSdSetter() const;

    std::function<short (const Cell &)> GetSdSetterStrip() const;

    void ComputeRadius();

    void ComputeRadiusStrip(Meshes &meshes);

// Functions V, Gamma, P, phi, psi, theta
//----------------------------------------------------------------------------------------
  public:
    template<typename T>
    VelocityGradientT<T> G_U(const PointT<T> &z) const {
      return VelocityGradientT<T>(-2 * T(sigma), re * (1 - 2 * z[1]),
                                  re * (1 - 2 * z[1]), -2 * T(sigma));
    }

    template<typename T>
    VelocityT<T> VF_V(const PointT<T> &z, const Cell &c) const {
      if (CellNearObstacle(c)) return VF_U(z);
      if (CellOutside(c)) return VelocityT<T>();
      return VelocityT<T>(-phi(z[0], c) * d_psi(z[1], c),
                          d_phi(z[0], c) * psi(z[1], c));
    }

    template<typename T>
    VelocityGradientT<T> VFG_V(const PointT<T> &z, const Cell &c) const {
      if (CellNearObstacle(c)) return VFG_U(z);
      if (CellOutside(c)) return VelocityGradientT<T>();
      return VelocityGradientT<T>{
          -d_phi(z[0], c) * d_psi(z[1], c), -phi(z[0], c) * dd_psi(z[1], c),
          dd_phi(z[0], c) * psi(z[1], c),   d_phi(z[0], c) * d_psi(z[1], c)
      };
    }

    template<typename T>
    VelocityT<T> VF_Gamma(const PointT<T> &z, const Cell &c) const {
      if (CellNearObstacle(c)) return VelocityT<T>();
      if (CellOutside(c)) return VF_U(z);
      return VF_U(z) - VF_V(z, c);
    }

    template<typename T>
    VelocityGradientT<T> VFG_Gamma(const PointT<T> &z, const Cell &c) const {
      if (CellNearObstacle(c)) return VelocityGradientT<T>();
      if (CellOutside(c)) return VFG_U(z);
      return VFG_U(z) - VFG_V(z, c);
    }

    template<typename T>
    T PressureP(const PointT<T> &z) const { return T(-2.0) / re * z[0]; }

    virtual IACoefficients B4Coefficients_D_Theta_6(const Cell &c) const = 0;

    virtual IACoefficients B3Coefficients_DD_Theta_6(const Cell &c) const = 0;

    IACoefficients B4Coefficients_D_Psi(const Cell &c) const;

    IACoefficients B3Coefficients_DD_Psi(const Cell &c) const;

    virtual void MinusB9Coefficients_V(IACoefficients &V0, IACoefficients &V1,
                                       const Cell &c) const = 0;

    virtual void MinusB8Coefficients_DV(IACoefficients &Dx_V0, IACoefficients &Dy_V0,
                                        IACoefficients &Dx_V1, const Cell &c) const = 0;

    void B9Coefficients_Gamma(IACoefficients &G0, IACoefficients &G1, const Cell &c) const;

    void PlusB8Coefficients_DGamma(IACoefficients &Dx_G0, IACoefficients &Dy_G0,
                                   IACoefficients &Dx_G1, const Cell &c) const;
  protected:
    template<typename T>
    T phi(const T &x, const Cell &c) const;

    template<typename T>
    T d_phi(const T &x, const Cell &c) const;

    template<typename T>
    T dd_phi(const T &x, const Cell &c) const;

    template<typename T>
    T psi(const T &y, const Cell &c) const { return theta(y, c) / 6 - intU1(y); }

    template<typename T>
    T d_psi(const T &y, const Cell &c) const { return d_theta(y, c) / 6 - U1(y); }

    template<typename T>
    T dd_psi(const T &y, const Cell &c) const { return dd_theta(y, c) / 6 - d_U1(y); }

    virtual double theta(const double &y, const Cell &c) const = 0;

    virtual IAInterval theta(const IAInterval &y, const Cell &c) const = 0;

    virtual double d_theta(const double &y, const Cell &c) const = 0;

    virtual IAInterval d_theta(const IAInterval &y, const Cell &c) const = 0;

    virtual double dd_theta(const double &y, const Cell &c) const = 0;

    virtual IAInterval dd_theta(const IAInterval &y, const Cell &c) const = 0;

    virtual double ValueTheta(const Cell &c) const = 0;

//----------------------------------------------------------------------------------------
  public:
    void SET_RAPPR(double r) { Rappr = r; }
  };

  std::unique_ptr<NavierStokesProblem> CreateProblem(double Re = -1.0);

} // namespace navierstokes

#endif // NAVIERSTOKESPROBLEM_H