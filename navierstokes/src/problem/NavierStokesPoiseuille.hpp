#ifndef NAVIERSTOKESPOISEUILLE_HPP
#define NAVIERSTOKESPOISEUILLE_HPP

#include "Tensor.hpp"

class Cell;

using IACoefficients = std::vector<IAInterval>;

class NavierStokesPoiseuille {
public:
  template<typename T>
  VelocityT<T> VF_U(const PointT<T> &z) const {
    return VelocityT<T>(z[1] * (1 - z[1]), T{});
  }

  template<typename T>
  VelocityGradientT<T> VFG_U(const PointT<T> &z) const {
    return VelocityGradientT<T>(T{}, 1 - 2 * z[1],
                                T{}, T{});
  }

  template<typename T>
  VectorFieldT<T> Laplace_U(const PointT<T> &z) const {
    return VectorFieldT<T>(T(-2.0), T{});
  }

  IACoefficients B4Coefficients_U(const Cell &c) const;

  IACoefficients B9Coefficients_U(const Cell &c) const;

  IACoefficients B3Coefficients_DU(const Cell &c) const;

  IACoefficients B8Coefficients_DU(const Cell &c) const;

protected:
  double intU1(const double &y) const { return y * y * (0.5 - y / 3); }

  IAInterval intU1(const IAInterval &y) const { return sqr(y) * (0.5 - y / 3); }

  template<typename T>
  T U1(const T &y) const { return y * (1.0 - y); }

  template<typename T>
  T d_U1(const T &y) const { return 1.0 - 2.0 * y; }
};

#endif //NAVIERSTOKESPOISEUILLE_HPP
