#include "NavierStokesSobolev.hpp"

namespace navierstokes {

  NavierStokesSobolev::NavierStokesSobolev()
    : c2(infty), c4(infty), c2sqr(infty), c4sqr(infty),
      midC2(infty), midC4(infty), midC2sqr(infty), midC4sqr(infty) {}

  void NavierStokesSobolev::initConstants(double sigma) {
    IAInterval Pi = IAInterval::Pi();
    IAInterval PiSqr = IAInterval::PiSqr();
    IAInterval sqrt2 = IAInterval::Sqrt2();
    if (sigma == 0.0) {
      c2sqr = 1 / PiSqr;
      c4sqr = 1 / Pi / sqrt2;
      c2 = 1 / Pi;
      c4 = sqrt(c4sqr);
    } else if (sigma < inf(PiSqr)) {
      c2sqr = 1 / (PiSqr + sigma);
      c4sqr = 1 / sqrt2 / (Pi + sigma / Pi);
      c2 = sqrt(c2sqr);
      c4 = sqrt(c4sqr);
    } else if (sigma >= sup(PiSqr)) {
      c2sqr = 1 / (PiSqr + sigma);
      c4sqr = 1 / sqrt(IAInterval(8) * sigma);
      c2 = sqrt(c2sqr);
      c4 = sqrt(c4sqr);
    } else {
      ERROR("Bad choice of sigma")
    }

    midC2 = mid(c2);
    midC2sqr = mid(c2sqr);
    midC4 = mid(c4);
    midC4sqr = mid(c4sqr);
  }

  template<>
  const double &NavierStokesSobolev::C2() const {
    return midC2;
  }

  template<>
  const IAInterval &NavierStokesSobolev::C2() const {
    return c2;
  }

  template<>
  const double &NavierStokesSobolev::C2sqr() const {
    return midC2sqr;
  }

  template<>
  const IAInterval &NavierStokesSobolev::C2sqr() const {
    return c2sqr;
  }

  template<>
  const double &NavierStokesSobolev::C4() const {
    return midC4;
  }

  template<>
  const IAInterval &NavierStokesSobolev::C4() const {
    return c4;
  }

  template<>
  const double &NavierStokesSobolev::C4sqr() const {
    return midC4sqr;
  }

  template<>
  const IAInterval &NavierStokesSobolev::C4sqr() const {
    return c4sqr;
  }

} //namespace navierstokes