#include "geometry/NavierStokesCoarseGeometry.hpp"
#include "NavierStokesProblem.hpp"
#include "MeshesCreator.hpp"
#include "DivergenceFreeDiscretization.hpp"
#include "LagrangeDiscretization.hpp"
#include "Plotting.hpp"

#include "bernstein/MonomialBernstein.hpp"

using namespace std::placeholders;

namespace navierstokes {

  std::map<NavierStokesPart, std::vector<std::string>> PartToString = {
      {PartVelocityNorms,   {"VelocityNorms",  "NormsVelocity", "First"}},
      {PartApprVNorms,      {"ApprVNorms",     "NormsApprV"}},
      {PartDefectVelocity,  {"DefectVelocity", "VelocityDefect"}},
      {PartNormbound,       {"Normbound"}},
      {PartSurjectivity,    {"Surjectivity"}},
      {PartProofVelocity,   {"ProofVelocity"}},
      {PartDefectPressure,  {"DefectPressure", "PressureDefect"}},
      {PartLast,            {"Results",        "Result",        "Last"}},
      {PartComputeVelocity, {"ComputeVelocity"}},
      {PartComputeApprV,    {"ComputeApprV"}},
      {PartComputePressure, {"ComputeApprV"}},
      {PartPlotVelocity,    {"PlotVelocity"}},
      {PartPlotPressure,    {"PlotPressure"}}
  };

//----------------------------------------------------------------------------------------
// Levels
//----------------------------------------------------------------------------------------
  NavierStokesLevels::NavierStokesLevels(NBStrategy nbStrat) {
    Config::Get("VelocityLevel", velocityLevel);
    velocityCoarseLevel = velocityLevel;
    Config::Get("VelocityCoarseLevel", velocityCoarseLevel);
    if (nbStrat == NB_Wieners && velocityCoarseLevel != velocityLevel) {
      velocityCoarseLevel = velocityLevel;
      Warning("Velocity coarse level set to velocity level")
    }
    Config::Get("PressureLevel", pressureLevel);
    Config::Get("VelocityDefectLevel", velocityDefectLevel);
    Config::Get("PressureDefectLevel", pressureDefectLevel);
    if (velocityLevel != pressureLevel) {
      pressureLevel = velocityLevel;
      Warning("Pressure level set to velocity level")
    }
    velocityLevelsDiffer = (velocityLevel != velocityCoarseLevel);

    Config::Get("plevel", pLevel);
    if (pLevel > coarseLevel) {
      WarningOnMaster("pLevel= " + std::to_string(pLevel)
                      + " too large; pLevel set to coarseLevel= " + std::to_string(coarseLevel));
      pLevel = coarseLevel;
    }

    int m1 = std::min(velocityLevel, velocityDefectLevel);
    int m2 = std::min(pressureLevel, pressureDefectLevel);
    coarseLevel = std::min(std::min(m1, velocityCoarseLevel), m2);

    m1 = std::max(velocityLevel, velocityDefectLevel);
    m2 = std::max(pressureLevel, pressureDefectLevel);
    fineLevel = std::max(std::max(m1, velocityCoarseLevel), m2);
  }

  NavierStokesLevels &NavierStokesLevels::operator=(const NavierStokesLevels &levels) {
    velocityLevel = levels.velocityLevel;
    velocityCoarseLevel = levels.velocityCoarseLevel;
    pressureLevel = levels.pressureLevel;
    velocityDefectLevel = levels.velocityDefectLevel;
    pressureDefectLevel = levels.pressureDefectLevel;
    return *this;
  }

  Loader &NavierStokesLevels::load(Loader &loader) {
    return loader >> velocityLevel >> velocityCoarseLevel >> pressureLevel
                  >> velocityDefectLevel >> pressureDefectLevel;
  }

  Saver &NavierStokesLevels::save(Saver &saver) const {
    return saver << velocityLevel << velocityCoarseLevel << pressureLevel
                 << velocityDefectLevel << pressureDefectLevel;
  }

  bool operator==(const NavierStokesLevels &l1, const NavierStokesLevels &l2) {
    return l1.velocityLevel == l2.velocityLevel &&
           l1.velocityCoarseLevel == l2.velocityCoarseLevel &&
           l1.pressureLevel == l2.pressureLevel &&
           l1.velocityDefectLevel == l2.velocityDefectLevel &&
           l1.pressureDefectLevel == l2.pressureDefectLevel;
  }

  bool operator!=(const NavierStokesLevels &l1, const NavierStokesLevels &l2) {
    return !(l1 == l2);
  }

//----------------------------------------------------------------------------------------
// Parts
//----------------------------------------------------------------------------------------
  NavierStokesPart Part(const std::string &part) {
    for (auto &[key, value]: PartToString) {
      for (auto &name: value) {
        if (name == part)
          return key;
      }
    }
    return PartFirst;
  }

  const std::string &Part(NavierStokesPart part) {
    auto iter = PartToString.find(part);
    if (iter != PartToString.end())
      return iter->second[0];
    THROW("Part not implemented")
  }

  PartsToRun::PartsToRun() {
    std::string beginPart = "First";
    Config::Get("RestartAt", beginPart);
    std::string endPart = "Last";
    Config::Get("ExitAt", endPart);
    begin = Part(beginPart);
    end = Part(endPart);
    if (begin > PartLast) {
      begin = PartFirst;
      WarningOnMaster("RestartAt set to " + Part(begin) + "!")
    }
    if (end < begin || end > PartLast) {
      end = PartLast;
      WarningOnMaster("ExitAt set to " + Part(end) + "!")
    }
    for (NavierStokesPart part = begin; part <= end; part = NavierStokesPart(part + 1)) {
      partsToRun.push_back(part);
    }

    bool compVelocity = false;
    Config::Get("ComputeVelocity", compVelocity);
    if (compVelocity)
      partsToRun.push_back(PartComputeVelocity);
    bool compApprV = false;
    Config::Get("ComputeApprV", compApprV);
    if (compApprV)
      partsToRun.push_back(PartComputeApprV);
    bool compPressure = false;
    Config::Get("ComputePressure", compPressure);
    if (compPressure)
      partsToRun.push_back(PartComputePressure);

    bool plotVelocity = true;
    Config::Get("PlotVelocity", plotVelocity);
    if (plotVelocity)
      partsToRun.push_back(PartPlotVelocity);
    bool plotPressure = true;
    Config::Get("PlotPressure", plotPressure);
    if (plotPressure)
      partsToRun.push_back(PartPlotPressure);
  }

  bool PartsToRun::operator()(NavierStokesPart part) {
    return std::find(partsToRun.begin(), partsToRun.end(), part) != partsToRun.end();
  }

  void PartsToRun::Reset(NavierStokesPart begin) {
    this->begin = begin;
    for (NavierStokesPart part = begin; part <= end; part = NavierStokesPart(part + 1)) {
      partsToRun.push_back(part);
    }
    partsToRun.push_back(PartComputeVelocity);
    partsToRun.push_back(PartComputeApprV);
    partsToRun.push_back(PartComputePressure);
  }

  void PartsToRun::ResetExceptApproximation(NavierStokesPart begin) {
    this->begin = begin;
    for (NavierStokesPart part = begin; part <= end; part = NavierStokesPart(part + 1)) {
      partsToRun.push_back(part);
    }
  }

//----------------------------------------------------------------------------------------
// VectorData
//----------------------------------------------------------------------------------------
  void VectorData::ClearVelocity() {
    vectorApprV = nullptr;
    vectorVelocity = nullptr;
    vectorVelocityCoarse = nullptr;
    discVelocity = nullptr;
    iaDiscVelocity = nullptr;
  }

  void VectorData::ClearPressure() {
    vectorPressure = nullptr;
    discPressure = nullptr;
    iaDiscPressure = nullptr;
  }

  NavierStokesObstacle::NavierStokesObstacle() {
    Config::Get("d1", d1);
    Config::Get("d0", d0);
    Config::Get("d2", d2);
    Config::Get("d3", d3);
    if (d1 < 0) ERROR("'d1' negative!");
    if (d1 >= d0) ERROR("'d0' too small!");
    if (d2 < 0) ERROR("'d2' negative!");
    if (d2 > 1) ERROR("'d2' larger than 1!");
    if (d3 < 0) ERROR("'d3' negative!");
    if (d3 > 1) ERROR("'d3' larger than 1!");
    if (d2 >= d3) ERROR("'d2' larger than 'd3'!")
  }

  NavierStokesProblemConstants::NavierStokesProblemConstants(double _Re) : re(_Re) {
    Config::Get("Sigma", sigma);
    if (re <= 0.0)
      Config::Get("Reynolds", re);

    double reLength = 0.0;
    Config::Get("ReynoldsLength", reLength);
    reIA = IAInterval(re, re + reLength);
  }

  void NavierStokesProblemConstants::adaptSigma(NBStrategy nbStrat) {
    if (sigma != 0.0) {
      if (nbStrat == NB_Wieners) {
        sigma = 0.0;
        WarningOnMaster("Sigma set to 0.0")
      }
    } else {
      if (nbStrat == NB_PlumSimple || nbStrat == NB_PlumExtended) {
        sigma = 1.0;
        WarningOnMaster("Sigma set to 1.0")
      }
    }
  }

//========================================================================================
// NavierStokesProblem base class
//========================================================================================
  bool NavierStokesProblem::disableSaveLoad = false;

  void NavierStokesProblem::DisableSaveLoad() {
    disableSaveLoad = true;
  }

  NavierStokesProblem::NavierStokesProblem(double _Re) : NavierStokesProblemConstants(_Re) {
    Config::Get("VerifiedComputation", verified);
    Config::Get("UseCoarseGeometry", coarseGeometryName);
    if (coarseGeometryName != "None") {
      fileName = coarseGeometryName;
    } else {
      Config::Get("Mesh", fileName);
    }

    std::string strat = "PlumExtended";
    Config::Get("NormboundStrategy", strat);
    if (strat == "Wieners")
      nbStrategy = NB_Wieners;
    else if (strat == "PlumSimple")
      nbStrategy = NB_PlumSimple;
    else if (strat == "PlumExtended")
      nbStrategy = NB_PlumExtended;

    levels = std::make_unique<NavierStokesLevels>(nbStrategy);
    adaptSigma(nbStrategy);
    initConstants(sigma);
    LoadData();
  }

  void NavierStokesProblem::PrintInfo() {
    bool barycentricRefinement = true;
    Config::Get("BarycentricRefinement", barycentricRefinement);
    mout.PrintInfo("Problem", verbose,
                   PrintInfoEntry("Name", Name(), 0),
                   PrintInfoEntry("Reynoldsnumber", re, 0),
                   PrintInfoEntry("Reynoldsnumber range", reIA, 0),
                   PrintInfoEntry("sigma", sigma, 0),
                   PrintInfoEntry("Normbound model", NormboundStrategyName()),
                   PrintInfoEntry("Verified computation", verified ? "yes" : "no"),
                   PrintInfoEntry("BarycentricRefinement", barycentricRefinement ? "yes" : "no", 0),
                   PrintInfoEntry("Velocity level", levels->velocityLevel),
                   PrintInfoEntry("Velocity level coarse", levels->velocityCoarseLevel),
                   PrintInfoEntry("Pressure level", levels->pressureLevel),
                   PrintInfoEntry("Velocity defect level", levels->velocityDefectLevel),
                   PrintInfoEntry("Pressure defect level", levels->pressureDefectLevel),
                   PrintInfoEntry("RestartAt", Part(partsToRun.Begin())),
                   PrintInfoEntry("ExitAt", Part(partsToRun.End())),
                   PrintInfoEntry("C2", c2),
                   PrintInfoEntry("C4", c4),
                   PrintInfoEntry("C2^2", c2sqr, 4),
                   PrintInfoEntry("C4^2", c4sqr, 4),
                   PrintInfoEntry("d0", d0, 2),
                   PrintInfoEntry("d1", d1, 2),
                   PrintInfoEntry("d2", d2, 2),
                   PrintInfoEntry("d3", d3, 2));
  }

  std::string NavierStokesProblem::NormboundStrategyName() const {
    switch (nbStrategy) {
      case NB_Wieners:
        return "Wieners";
      case NB_PlumSimple:
        return "Plum simple";
      case NB_PlumExtended:
        return "Plum extended";
    }
    ERROR("Normbound strategy not implemented")
  }

  bool NavierStokesProblem::IsHomotopy() const {
    return nbStrategy == NB_PlumSimple || nbStrategy == NB_PlumExtended;
  }

  template<>
  const double &NavierStokesProblem::Reynolds() const {
    return re;
  }

  template<>
  const IAInterval &NavierStokesProblem::Reynolds() const {
    return reIA;
  }

  void NavierStokesProblem::SaveData(NavierStokesPart part) {
    PSaver saver(PathData().c_str());
    saver << *levels << verified << (int) nbStrategy << (int) part << constants;
    saver.close();
  }

  void NavierStokesProblem::LoadData() {
    if (!SaveLoad::PFileExists(PathData().c_str()) || disableSaveLoad) {
      partsToRun.Reset(PartFirst);
      return;
    }
    PLoader loader(PathData().c_str());

    NavierStokesLevels levels1(nbStrategy);
    loader >> levels1;
    if (*levels != levels1) {
      partsToRun.Reset(PartFirst);
      loader.close();
      return;
    }

    bool verifiedSaved;
    loader >> verifiedSaved;
    if (verifiedSaved != verified) {
      partsToRun.ResetExceptApproximation(PartFirst);
    }

    int nbStrat;
    loader >> nbStrat;
    NBStrategy nbstratSaved = (NBStrategy) nbStrat;
    if (nbStrategy != nbstratSaved) {
      partsToRun.ResetExceptApproximation(PartVelocityNorms);
    }

    int part;
    loader >> part;
    NavierStokesPart partSaved = (NavierStokesPart) part;

    if (partSaved < partsToRun.Begin()) {
      partsToRun.ResetExceptApproximation(NavierStokesPart(partSaved + 1));
    }

    loader >> constants;
    loader.close();
  }

  bool NavierStokesProblem::RunPart(NavierStokesPart part) {
    return partsToRun(part);
  }

  void NavierStokesProblem::ResetPartsToRun(NavierStokesPart begin) {
    partsToRun.Reset(begin);
  }


// Meshes
//----------------------------------------------------------------------------------------
  void NavierStokesProblem::InitMeshes() {
    if (meshes != nullptr) return;

    bool barycentricRefinement = true;
    Config::Get("BarycentricRefinement", barycentricRefinement);
    std::shared_ptr<CoarseGeometry> coarseGeometry = nullptr;
    if (coarseGeometryName == "Bottom45") {
      double R = 3.0;
      Config::Get("StripRadius", R);
      double w = 0.125;
      Config::Get("StripWidth", w);
      double r = 0.25;
      Config::Get("ObstacleRadius", r);
      coarseGeometry = std::make_shared<NavierStokesCoarseGeometryBottom45>(R, 1.0, w, r);
    } else if (coarseGeometryName == "CenterSquare") {
      double R = 3.0;
      Config::Get("StripRadius", R);
      double w = 0.125;
      Config::Get("StripWidth", w);
      coarseGeometry = std::make_shared<NavierStokesCoarseGeometryCenterSquare>(R, 1.0, w);
    }

    MeshesCreator creator = MeshesCreator().
        WithPLevel(levels->pLevel).
        WithCLevel(levels->coarseLevel).
        WithLevel(levels->fineLevel).
        WithCoarseGeometry(coarseGeometry).
        WithSubdomainSetter(GetSdSetter());
    if (barycentricRefinement) {
      creator.WithFinalBarycentricRefinement();
      creator.WithShiftedCenter();
    }
    meshes = creator.CreateShared();
    ComputeRadius();
    meshes->PrintInfo();
  }

// Velocity functions
//----------------------------------------------------------------------------------------
  template<>
  void NavierStokesProblem::SetUpVelocity<false>() {
    if (!vectorData.discVelocity.operator bool()) {
      vectorData.discVelocity = std::make_shared<DivergenceFreeDiscretization>(*meshes, 18, 1);
      vectorData.iaDiscVelocity = std::make_shared<IADivergenceFreeDiscretization>(*meshes, 18, 1);
    }
    vectorData.vectorVelocity = std::make_unique<Vector>(vectorData.discVelocity,
                                                         levels->velocityLevel);
    vectorData.vectorVelocity->SetIADisc(vectorData.iaDiscVelocity);
  }

  template<>
  void NavierStokesProblem::SetUpVelocity<true>() {
    if (!vectorData.discVelocity.operator bool()) {
      vectorData.discVelocity = std::make_unique<DivergenceFreeDiscretization>(*meshes, 18, 1);
      vectorData.iaDiscVelocity = std::make_unique<IADivergenceFreeDiscretization>(*meshes, 18, 1);
    }
    vectorData.vectorVelocityCoarse = std::make_unique<Vector>(vectorData.discVelocity,
                                                               levels->velocityCoarseLevel);
    vectorData.vectorVelocityCoarse->SetIADisc(vectorData.iaDiscVelocity);
  }

  template<>
  bool NavierStokesProblem::VelocityInitialized<true>() const {
    return vectorData.vectorVelocityCoarse != nullptr;
  }

  template<>
  bool NavierStokesProblem::VelocityInitialized<false>() const {
    return vectorData.vectorVelocity != nullptr;
  }

  template<>
  const NonAdaptiveIDiscretization &NavierStokesProblem::DiscVelocity<double>() const {
    return *vectorData.discVelocity;
  }

  template<>
  const IANonAdaptiveIDiscretization &NavierStokesProblem::DiscVelocity<IAInterval>() const {
    return *vectorData.iaDiscVelocity;
  }

  template<>
  Vector &NavierStokesProblem::Velocity<true>() {
    return *vectorData.vectorVelocityCoarse;
  }

  template<>
  Vector &NavierStokesProblem::Velocity<false>() {
    return *vectorData.vectorVelocity;
  }

  template<>
  const Vector &NavierStokesProblem::Velocity<true>() const {
    return *vectorData.vectorVelocityCoarse;
  }

  template<>
  const Vector &NavierStokesProblem::Velocity<false>() const {
    return *vectorData.vectorVelocity;
  }

// ApprV functions
//----------------------------------------------------------------------------------------
  void NavierStokesProblem::SetUpApprV() {
    if (!vectorData.discVelocity.operator bool()) {
      vectorData.discVelocity = std::make_unique<DivergenceFreeDiscretization>(*meshes, 18, 1);
      vectorData.iaDiscVelocity = std::make_unique<IADivergenceFreeDiscretization>(*meshes, 18, 1);
    }
    vectorData.vectorApprV = std::make_unique<Vector>(vectorData.discVelocity,
                                                      levels->velocityLevel);
    vectorData.vectorApprV->SetIADisc(vectorData.iaDiscVelocity);
  }

// Pressure functions
//----------------------------------------------------------------------------------------
  void NavierStokesProblem::SetUpPressure() {
    vectorData.discPressure = std::make_shared<LagrangeDiscretization>(*meshes, 1, 18, 1);
    vectorData.iaDiscPressure = std::make_unique<IALagrangeDiscretization>(*meshes, 1, 18, 1);
    vectorData.vectorPressure = std::make_unique<Vector>(vectorData.discPressure,
                                                         levels->pressureLevel);
    vectorData.vectorPressure->SetIADisc(vectorData.iaDiscPressure);
  }

  template<>
  const NonAdaptiveIDiscretization &NavierStokesProblem::DiscPressure<double>() const {
    return *vectorData.discPressure;
  }

  template<>
  const IANonAdaptiveIDiscretization &NavierStokesProblem::DiscPressure<IAInterval>() const {
    return *vectorData.iaDiscPressure;
  }

// Paths for SaveLoad and plot
//----------------------------------------------------------------------------------------
  template<>
  std::string NavierStokesProblem::PathVelocity<true>() const {
    return pathPrefix + "approximations/Velocity_l" + std::to_string(levels->velocityCoarseLevel)
           + "_" + fileName + "_" + std::to_string(re);
  }

  template<>
  std::string NavierStokesProblem::PathVelocity<false>() const {
    return pathPrefix + "approximations/Velocity_l" + std::to_string(levels->velocityLevel)
           + "_" + fileName + "_" + std::to_string(re);
  }

  std::string NavierStokesProblem::PathApprV() const {
    return pathPrefix + "approximations/ApprV_" + fileName + "_" + std::to_string(re);
  }

  std::string NavierStokesProblem::PathPressure() const {
    return pathPrefix + "approximations/Pressure_" + fileName + "_" + std::to_string(re);
  }

  std::string NavierStokesProblem::PathData() const {
    return pathPrefix + "data/Data_" + fileName + "_" + std::to_string(re);
  }

  std::string NavierStokesProblem::PlotName() const {
    return "Approximations_" + fileName + "_" + std::to_string(re);
  }

// Subdomains, cell location
//----------------------------------------------------------------------------------------
  bool NavierStokesProblem::CellInObstacleColumn(const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -4:
      case -7:
        return true;
      default:
        return false;
    }
  }

  bool NavierStokesProblem::CellOutside(const Cell &c) const {
    return c.Subdomain() <= -90;
  }

  bool NavierStokesProblem::CellOutsideApproximationDomain(const Cell &c) const {
    return c.Subdomain() <= -900;
  }

  short subdomain(double x_min, double x_max, int row_id, double d0, double d1) {
    if (x_max <= -d0 || d0 <= x_min)
      return -99;
    if (x_max <= -d1)
      return row_id - 10;
    if (d1 <= x_min)
      return row_id - 20;
    return row_id;
  }

  short GetSubdomain(const Cell &c, double d0, double d1, double d2, double d3) {
    double x_min = infty;
    double x_max = -infty;
    double y_min = infty;
    double y_max = -infty;
    for (int i = 0; i < c.Corners(); ++i) {
      x_min = std::min(x_min, c.Corner(i)[0]);
      x_max = std::max(x_max, c.Corner(i)[0]);
      y_min = std::min(y_min, c.Corner(i)[1]);
      y_max = std::max(y_max, c.Corner(i)[1]);
    }
    if (y_max <= d2)
      return subdomain(x_min, x_max, -1, d0, d1);
    if (y_min >= d3)
      return subdomain(x_min, x_max, -7, d0, d1);
    return subdomain(x_min, x_max, -4, d0, d1);
  }

  short GetSubdomainStrip(const Cell &c, double Rappr) {
    double x_min = infty;
    double x_max = -infty;
    for (int i = 0; i < c.Corners(); ++i) {
      x_min = std::min(x_min, c.Corner(i)[0]);
      x_max = std::max(x_max, c.Corner(i)[0]);
    }
    if (x_max <= -Rappr || Rappr <= x_min)
      return -999;
    return c.Subdomain();
  }

  std::function<short (const Cell &)> NavierStokesProblem::GetSdSetter() const {
    return std::bind(GetSubdomain, _1, d0, d1, d2, d3);
  }

  std::function<short (const Cell &)> NavierStokesProblem::GetSdSetterStrip() const {
    return std::bind(GetSubdomainStrip, _1, Rappr);
  }

  void NavierStokesProblem::ComputeRadius() {
    Rappr = -1.0;
    Reig = -1.0;
    const Mesh &cMesh = meshes->coarse();
    for (cell c = cMesh.cells(); c != cMesh.cells_end(); ++c)
      for (int i = 0; i < c.Corners(); ++i)
        Rappr = std::max(Rappr, abs(c.Corner(i)[0]));
    Rappr = PPM->Max(Rappr);
  }

  void NavierStokesProblem::ComputeRadiusStrip(Meshes &meshes) {
    Reig = -1.0;
    const Mesh &cMesh = meshes.coarse();
    for (cell c = cMesh.cells(); c != cMesh.cells_end(); ++c)
      for (int i = 0; i < c.Corners(); ++i)
        Reig = std::max(Reig, abs(c.Corner(i)[0]));
    Reig = PPM->Max(Reig);
  }

// Functions V, Gamma, P, phi, psi, theta
//----------------------------------------------------------------------------------------
  IACoefficients NavierStokesProblem::B4Coefficients_D_Psi(const Cell &c) const {
    IACoefficients coeff_d_psi = B4Coefficients_D_Theta_6(c);
    IACoefficients coeff_U = B4Coefficients_U(c);
    for (int j = 0; j < coeff_U.size(); ++j)
      coeff_d_psi[j] -= coeff_U[j];
    return coeff_d_psi;
  }

  IACoefficients NavierStokesProblem::B3Coefficients_DD_Psi(const Cell &c) const {
    IACoefficients coeff_dd_psi = B3Coefficients_DD_Theta_6(c);
    IACoefficients coeff_DU = B3Coefficients_DU(c);
    for (int j = 0; j < coeff_DU.size(); ++j)
      coeff_dd_psi[j] -= coeff_DU[j];
    return coeff_dd_psi;
  }

  void NavierStokesProblem::B9Coefficients_Gamma(IACoefficients &G0, IACoefficients &G1,
                                                 const Cell &c) const {
    G0 = B9Coefficients_U(c);
    G1 = IACoefficients(55);
    MinusB9Coefficients_V(G0, G1, c);
  }

  void NavierStokesProblem::PlusB8Coefficients_DGamma(IACoefficients &Dx_G0, IACoefficients &Dy_G0,
                                                      IACoefficients &Dx_G1, const Cell &c) const {
    MinusB8Coefficients_DV(Dx_G0, Dy_G0, Dx_G1, c);
    IACoefficients coeff_Dy_U0 = B8Coefficients_DU(c);
    for (int j = 0; j < 45; ++j) Dy_G0[j] += coeff_Dy_U0[j];
  }

} // namespace navierstokes
