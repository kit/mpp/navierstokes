#include "NavierStokesConstants.hpp"

namespace navierstokes {

  Loader &NavierStokesConstants::load(Loader &loader) {
    loader >> gammaInftySqr
           >> uomegaInftySqr >> duomegaInftySqr >> omegaInftySqr >> domegaInftySqr
           >> normApprV >> normDApprV >> normDApprVInfty
           >> delta >> delta3 >> k >> k_ast
           >> tau >> kappa >> gamma0 >> gamma1 >> gamma2 >> gamma1_ast >> gamma2_ast
           >> rhoAfterDomainHomotopy >> kAfterDomainHomotopy >> numEVAfterDomainHomotopy
           >> alpha >> alpha_unique >> alphaPressure;
    return loader;
  }

  Saver &NavierStokesConstants::save(Saver &saver) const {
    saver << gammaInftySqr
          << uomegaInftySqr << duomegaInftySqr << omegaInftySqr << domegaInftySqr
          << normApprV << normDApprV << normDApprVInfty
          << delta << delta3 << k << k_ast
          << tau << kappa << gamma0 << gamma1 << gamma2 << gamma1_ast << gamma2_ast
          << rhoAfterDomainHomotopy << kAfterDomainHomotopy << numEVAfterDomainHomotopy
          << alpha << alpha_unique << alphaPressure;
    return saver;
  }

  template<>
  double &NavierStokesConstants::UOmegaInftySqr<true>() {
    return uomegaCoarseInftySqr;
  }

  template<>
  double &NavierStokesConstants::UOmegaInftySqr<false>() {
    return uomegaInftySqr;
  }

  template<>
  const double &NavierStokesConstants::UOmegaInftySqr<true>() const {
    return uomegaCoarseInftySqr;
  }

  template<>
  const double &NavierStokesConstants::UOmegaInftySqr<false>() const {
    return uomegaInftySqr;
  }

  template<>
  IAInterval NavierStokesConstants::UOmegaInfty<true>() {
    return sqrt(IAInterval(uomegaCoarseInftySqr));
  }

  template<>
  IAInterval NavierStokesConstants::UOmegaInfty<false>() {
    return sqrt(IAInterval(uomegaInftySqr));
  }

  template<>
  double &NavierStokesConstants::D_UOmegaInftySqr<true>() {
    return duomegaCoarseInftySqr;
  }

  template<>
  double &NavierStokesConstants::D_UOmegaInftySqr<false>() {
    return duomegaInftySqr;
  }

  template<>
  const double &NavierStokesConstants::D_UOmegaInftySqr<true>() const {
    return duomegaCoarseInftySqr;
  }

  template<>
  const double &NavierStokesConstants::D_UOmegaInftySqr<false>() const {
    return duomegaInftySqr;
  }

  template<>
  IAInterval NavierStokesConstants::D_UOmegaInfty<true>() {
    return sqrt(IAInterval(duomegaCoarseInftySqr));
  }

  template<>
  IAInterval NavierStokesConstants::D_UOmegaInfty<false>() {
    return sqrt(IAInterval(duomegaInftySqr));
  }

  template<>
  double &NavierStokesConstants::K<false>() {
    return k;
  }

  template<>
  double &NavierStokesConstants::K<true>() {
    return k_ast;
  }

  template<>
  IAInterval &NavierStokesConstants::Gamma1<false>() {
    return gamma1;
  }

  template<>
  IAInterval &NavierStokesConstants::Gamma1<true>() {
    return gamma1_ast;
  }

  template<>
  IAInterval &NavierStokesConstants::Gamma2<false>() {
    return gamma2;
  }

  template<>
  IAInterval &NavierStokesConstants::Gamma2<true>() {
    return gamma2_ast;
  }

} //namespace navierstokes