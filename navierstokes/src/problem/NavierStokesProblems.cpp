#include "problem/NavierStokesProblems.hpp"

#include "Meshes.hpp"

#include "bernstein/MonomialBernstein.hpp"
#include "problem/NavierStokesProblemUtil.hpp"

namespace navierstokes {

  template<typename T>
  T NavierStokesProblem::phi(const T &x, const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -4:
      case -7:
        return T(1.0);
      case -11:
      case -14:
      case -17: {
        T d = T(d0) - d1;
        return eta((d0 + x) / d);
      }
      case -21:
      case -24:
      case -27: {
        T d = T(d0) - d1;
        return eta((d0 - x) / d);
      }
      default: Exit(std::to_string(c.Subdomain()) + " is not a suitable subdomain")
    }
  }

  template double NavierStokesProblem::phi(const double &x, const Cell &c) const;

  template IAInterval NavierStokesProblem::phi(const IAInterval &x, const Cell &c) const;

  template<typename T>
  T NavierStokesProblem::d_phi(const T &x, const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -4:
      case -7:
        return T(0.0);
      case -11:
      case -14:
      case -17: {
        T d = T(d0) - d1;
        return d_eta((d0 + x) / d) / d;
      }
      case -21:
      case -24:
      case -27: {
        T d = T(d0) - d1;
        return -d_eta((d0 - x) / d) / d;
      }
      default: Exit(std::to_string(c.Subdomain()) + " is not a suitable subdomain")
    }
  }

  template double NavierStokesProblem::d_phi(const double &x, const Cell &c) const;

  template IAInterval NavierStokesProblem::d_phi(const IAInterval &x, const Cell &c) const;

  template<typename T>
  T NavierStokesProblem::dd_phi(const T &x, const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -4:
      case -7:
        return T(0.0);
      case -11:
      case -14:
      case -17: {
        T d = T(d0) - d1;
        return dd_eta((d0 + x) / d) / sqr(d);
      }
      case -21:
      case -24:
      case -27: {
        T d = T(d0) - d1;
        return dd_eta((d0 - x) / d) / sqr(d);
      }
      default: Exit(std::to_string(c.Subdomain()) + " is not a suitable subdomain")
    }
  }

  template double NavierStokesProblem::dd_phi(const double &x, const Cell &c) const;

  template IAInterval NavierStokesProblem::dd_phi(const IAInterval &x, const Cell &c) const;

//----------------------------------------------------------------------------------------
// NavierStokesProblemBottom
//----------------------------------------------------------------------------------------
  bool NavierStokesProblemBottom::CellNearObstacle(const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
        return true;
      default:
        return false;
    }
  }

  bool NavierStokesProblemBottom::CellInObstacleRow(const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -11:
      case -21:
        return true;
      default:
        return false;
    }
  }

  double NavierStokesProblemBottom::theta(const double &y, const Cell &c) const {
    if (CellInObstacleRow(c))
      return 0.0;
    return eta((y - d2) / (1.0 - d2));
  }

  IAInterval NavierStokesProblemBottom::theta(const IAInterval &y, const Cell &c) const {
    if (CellInObstacleRow(c))
      return IAInterval(0.0);
    return eta((y - d2) / (IAInterval(1) - d2));
  }

  double NavierStokesProblemBottom::d_theta(const double &y, const Cell &c) const {
    if (CellInObstacleRow(c))
      return 0.0;
    return d_eta((y - d2) / (1 - d2)) / (1 - d2);
  }

  IAInterval NavierStokesProblemBottom::d_theta(const IAInterval &y, const Cell &c) const {
    if (CellInObstacleRow(c))
      return IAInterval(0.0);
    IAInterval d = IAInterval(1) - d2;
    return d_eta((y - d2) / d) / d;
  }

  double NavierStokesProblemBottom::dd_theta(const double &y, const Cell &c) const {
    if (CellInObstacleRow(c))
      return 0.0;
    return dd_eta((y - d2) / (1 - d2)) / sqr(1 - d2);
  }

  IAInterval NavierStokesProblemBottom::dd_theta(const IAInterval &y, const Cell &c) const {
    if (CellInObstacleRow(c))
      return IAInterval(0.0);
    IAInterval d = IAInterval(1) - d2;
    return dd_eta((y - d2) / d) / sqr(d);
  }

  double NavierStokesProblemBottom::ValueTheta(const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -11:
      case -21:
        return 0.0;
      default: Exit("Theta not constant in cell with subdomain " + std::to_string(c.Subdomain()))
    }
  }

  IACoefficients NavierStokesProblemBottom::B4Coefficients_D_Theta_6(const Cell &c) const {
    return b4Coefficients_D_Theta_6(c, d2);
  }

  IACoefficients NavierStokesProblemBottom::B3Coefficients_DD_Theta_6(const Cell &c) const {
    return b3Coefficients_DD_Theta_6(c, d2);
  }

  void NavierStokesProblemBottom::MinusB9Coefficients_V(IACoefficients &V0, IACoefficients &V1,
                                                        const Cell &c) const {
    if (CellInObstacleRow(c))
      minusB9Coefficients_V(d0, d1, ValueTheta(c), V0, V1, c);
    else
      minusB9Coefficients_V(d0, d1, d2, 1.0, V0, V1, c);
  }

  void NavierStokesProblemBottom::MinusB8Coefficients_DV(IACoefficients &Dx_V0,
                                                         IACoefficients &Dy_V0,
                                                         IACoefficients &Dx_V1,
                                                         const Cell &c) const {
    if (CellInObstacleRow(c))
      minusB8Coefficients_DV(d0, d1, ValueTheta(c), Dx_V0, Dy_V0, Dx_V1, c);
    else
      minusB8Coefficients_DV(d0, d1, d2, 1.0, Dx_V0, Dy_V0, Dx_V1, c);
  }

//----------------------------------------------------------------------------------------
// NavierStokesProblemCenter
//----------------------------------------------------------------------------------------
  bool NavierStokesProblemCenter::CellNearObstacle(const Cell &c) const {
    switch (c.Subdomain()) {
      case -4:
        return true;
      default:
        return false;
    }
  }

  bool NavierStokesProblemCenter::CellInObstacleRow(const Cell &c) const {
    switch (c.Subdomain()) {
      case -4:
      case -14:
      case -24:
        return true;
      default:
        return false;
    }
  }

  double NavierStokesProblemCenter::theta(const double &y, const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -11:
      case -21:
        return 0.5 * eta(y / d2);
      case -7:
      case -17:
      case -27:
        return 0.5 + 0.5 * eta((y - d3) / (1 - d3));
      case -4:
      case -14:
      case -24:
        return 0.5;
      default: Exit(std::to_string(c.Subdomain()) + " is not a suitable subdomain")
    }
  }

  IAInterval NavierStokesProblemCenter::theta(const IAInterval &y, const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -11:
      case -21:
        return eta(y / d2) / 2;
      case -7:
      case -17:
      case -27: {
        IAInterval d = IAInterval(1) - d3;
        return 0.5 + eta((y - d3) / d) / 2;
      }
      case -4:
      case -14:
      case -24:
        return IAInterval::c2r();
      default: Exit(std::to_string(c.Subdomain()) + " is not a suitable subdomain")
    }
  }

  double NavierStokesProblemCenter::d_theta(const double &y, const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -11:
      case -21:
        return 0.5 / d2 * d_eta(y / d2);
      case -7:
      case -17:
      case -27:
        return 0.5 / (1 - d3) * d_eta((y - d3) / (1 - d3));
      case -4:
      case -14:
      case -24:
        return 0.0;
      default: Exit(std::to_string(c.Subdomain()) + " is not a suitable subdomain")
    }
  }

  IAInterval NavierStokesProblemCenter::d_theta(const IAInterval &y, const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -11:
      case -21:
        return IAInterval::c2r() / d2 * d_eta(y / d2);
      case -7:
      case -17:
      case -27: {
        IAInterval d = IAInterval(1) - d3;
        return d_eta((y - d3) / d) / d / 2;
      }
      case -4:
      case -14:
      case -24:
        return IAInterval();
      default: Exit(std::to_string(c.Subdomain()) + " is not a suitable subdomain")
    }
  }

  double NavierStokesProblemCenter::dd_theta(const double &y, const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -11:
      case -21:
        return 0.5 / d2 / d2 * dd_eta(y / d2);
      case -7:
      case -17:
      case -27:
        return 0.5 / pow(1 - d3, 2) * dd_eta((y - d3) / (1 - d3));
      case -4:
      case -14:
      case -24:
        return 0.0;
      default: Exit(std::to_string(c.Subdomain()) + " is not a suitable subdomain")
    }
  }

  IAInterval NavierStokesProblemCenter::dd_theta(const IAInterval &y, const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -11:
      case -21:
        return IAInterval::c2r() / sqr(IAInterval(d2)) * dd_eta(y / d2);
      case -7:
      case -17:
      case -27: {
        IAInterval d = IAInterval(1) - d3;
        return dd_eta((y - d3) / d) / sqr(d) / 2;
      }
      case -4:
      case -14:
      case -24:
        return IAInterval();
      default: Exit(std::to_string(c.Subdomain()) + " is not a suitable subdomain")
    }
  }

  double NavierStokesProblemCenter::ValueTheta(const Cell &c) const {
    switch (c.Subdomain()) {
      case -4:
      case -14:
      case -24:
        return 0.5;
      default: Exit("Theta not constant in cell with subdomain " + std::to_string(c.Subdomain()))
    }
  }

  IACoefficients NavierStokesProblemCenter::B4Coefficients_D_Theta_6(const Cell &c) const {
    if (c.Subdomain() == -1) /// bottom
      return b4Coefficients_D_Theta_6(c, 0.0, d2, 0.5);
    if (c.Subdomain() == -7) /// top
      return b4Coefficients_D_Theta_6(c,  d3, 1.0, 0.5);
    Exit(std::to_string(c.Subdomain()) + " is not a suitable sub domain")
  }

  IACoefficients NavierStokesProblemCenter::B3Coefficients_DD_Theta_6(const Cell &c) const {
    if (c.Subdomain() == -1) /// bottom
      return b3Coefficients_DD_Theta_6(c, 0.0, d2, 0.5);
    if (c.Subdomain() == -7) /// top
      return b3Coefficients_DD_Theta_6(c, d3, 1.0, 0.5);
    Exit(std::to_string(c.Subdomain()) + " is not a suitable sub domain")
  }

  void NavierStokesProblemCenter::MinusB9Coefficients_V(IACoefficients &V0, IACoefficients &V1,
                                                        const Cell &c) const {
    if (CellInObstacleRow(c))
      minusB9Coefficients_V(d0, d1, ValueTheta(c), V0, V1, c);
    else {
      switch(c.Subdomain()) {
        case -11: case -21: /// bottom
          minusB9Coefficients_V(d0, d1, 0.0, d2, V0, V1, c, 0.5);
          break;
        case -17: case -27: /// top
          minusB9Coefficients_V(d0, d1, d3, 1.0, V0, V1, c, 0.5, 0.5);
          break;
        default:
        Exit("Not implemented")
      }
    }
  }

  void NavierStokesProblemCenter::MinusB8Coefficients_DV(IACoefficients &Dx_V0,
                                                         IACoefficients &Dy_V0,
                                                         IACoefficients &Dx_V1,
                                                         const Cell &c) const {
    if (CellInObstacleRow(c))
      minusB8Coefficients_DV(d0, d1, ValueTheta(c), Dx_V0, Dy_V0, Dx_V1, c);
    else {
      switch(c.Subdomain()) {
        case -11: case -21: /// bottom
          minusB8Coefficients_DV(d0, d1, 0.0, d2, Dx_V0, Dy_V0, Dx_V1, c, 0.5);
          break;
        case -17: case -27: /// top
          minusB8Coefficients_DV(d0, d1, d3, 1.0, Dx_V0, Dy_V0, Dx_V1, c, 0.5, 0.5);
          break;
        default:
        Exit("Not implemented")
      }
    }
  }

//----------------------------------------------------------------------------------------
// NavierStokesProblemBoth
//----------------------------------------------------------------------------------------
  bool NavierStokesProblemBoth::CellNearObstacle(const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -7:
        return true;
      default:
        return false;
    }
  }

  bool NavierStokesProblemBoth::CellInObstacleRow(const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -11:
      case -21:
      case -7:
      case -17:
      case -27:
        return true;
      default:
        return false;
    }
  }

  double NavierStokesProblemBoth::theta(const double &y, const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -11:
      case -21:
        return 0.0;
      case -4:
      case -14:
      case -24:
        return eta((y - d2) / (d3 - d2));
      case -7:
      case -17:
      case -27:
        return 1.0;
      default: Exit(std::to_string(c.Subdomain()) + " is not a suitable subdomain")
    }
  }

  IAInterval NavierStokesProblemBoth::theta(const IAInterval &y, const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -11:
      case -21:
        return IAInterval(0.0);
      case -4:
      case -14:
      case -24:
        return eta((y - d2) / (IAInterval(d3) - d2));
      case -7:
      case -17:
      case -27:
        return IAInterval(1.0);
      default: Exit(std::to_string(c.Subdomain()) + " is not a suitable subdomain")
    }
  }

  double NavierStokesProblemBoth::d_theta(const double &y, const Cell &c) const {
    if (CellInObstacleRow(c))
      return 0.0;
    return d_eta((y - d2) / (d3 - d2)) / (d3 - d2);
  }

  IAInterval NavierStokesProblemBoth::d_theta(const IAInterval &y, const Cell &c) const {
    if (CellInObstacleRow(c))
      return IAInterval(0.0);
    IAInterval d = IAInterval(d3) - d2;
    return d_eta((y - d2) / d) / d;
  }

  double NavierStokesProblemBoth::dd_theta(const double &y, const Cell &c) const {
    if (CellInObstacleRow(c))
      return 0.0;
    return dd_eta((y - d2) / (d3 - d2)) / sqr(d3 - d2);
  }

  IAInterval NavierStokesProblemBoth::dd_theta(const IAInterval &y, const Cell &c) const {
    if (CellInObstacleRow(c))
      return IAInterval(0.0);
    IAInterval d = IAInterval(d3) - d2;
    return dd_eta((y - d2) / d) / sqr(d);
  }

  double NavierStokesProblemBoth::ValueTheta(const Cell &c) const {
    switch (c.Subdomain()) {
      case -1:
      case -11:
      case -21:
        return 0.0;
      case -7:
      case -17:
      case -27:
        return 1.0;
      default: Exit("Theta not constant in cell with subdomain " + std::to_string(c.Subdomain()))
    }
  }

  IACoefficients NavierStokesProblemBoth::B4Coefficients_D_Theta_6(const Cell &c) const {
    return b4Coefficients_D_Theta_6(c, d2, d3);
  }

  IACoefficients NavierStokesProblemBoth::B3Coefficients_DD_Theta_6(const Cell &c) const {
    return b3Coefficients_DD_Theta_6(c, d2, d3);
  }

  void NavierStokesProblemBoth::MinusB9Coefficients_V(IACoefficients &V0, IACoefficients &V1,
                                                      const Cell &c) const {
    if (CellInObstacleRow(c))
      minusB9Coefficients_V(d0, d1, ValueTheta(c), V0, V1, c);
    else
      minusB9Coefficients_V(d0, d1, d2, d3, V0, V1, c);
  }

  void NavierStokesProblemBoth::MinusB8Coefficients_DV(IACoefficients &Dx_V0,
                                                       IACoefficients &Dy_V0,
                                                       IACoefficients &Dx_V1,
                                                       const Cell &c) const {
    if (CellInObstacleRow(c))
      minusB8Coefficients_DV(d0, d1, ValueTheta(c), Dx_V0, Dy_V0, Dx_V1, c);
    else
      minusB8Coefficients_DV(d0, d1, d2, d3, Dx_V0, Dy_V0, Dx_V1, c);
  }


  std::unique_ptr<NavierStokesProblem> CreateProblem(double Re) {
    std::string problemType = "Bottom";
    Config::Get("ProblemType", problemType);
    if (problemType == "Bottom")
      return std::make_unique<NavierStokesProblemBottom>(Re);
    if (problemType == "Center")
      return std::make_unique<NavierStokesProblemCenter>(Re);
    if (problemType == "Both")
      return std::make_unique<NavierStokesProblemBoth>(Re);
    ERROR("Problem type not implemented")
  }

} //namespace navierstokes
