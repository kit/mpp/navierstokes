#ifndef NAVIERSTOKESSOBOLEV_HPP
#define NAVIERSTOKESSOBOLEV_HPP

#include "IAInterval.hpp"

namespace navierstokes {

  class NavierStokesSobolev {
  protected:
    IAInterval c2, c4, c2sqr, c4sqr;
    double midC2, midC4, midC2sqr, midC4sqr;

    void initConstants(double sigma);

    NavierStokesSobolev();

  public:
    template<typename T>
    const T &C2() const;

    template<typename T>
    const T &C2sqr() const;

    template<typename T>
    const T &C4() const;

    template<typename T>
    const T &C4sqr() const;
  };

} //namespace navierstokes

#endif //NAVIERSTOKESSOBOLEV_HPP
