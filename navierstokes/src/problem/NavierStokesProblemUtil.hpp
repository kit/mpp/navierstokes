#ifndef NAVIERSTOKESPROBLEMETA_HPP
#define NAVIERSTOKESPROBLEMETA_HPP

namespace navierstokes {

  inline double sqr(double d) { return d * d; }

  template<typename T>
  T eta(const T &z) {
    return pow(z, 3) * (6.0 * sqr(z) - 15.0 * z + 10.0);
  }

  template<typename T>
  T d_eta(const T &z) {
    return 30.0 * sqr(z * (1.0 - z));
  }

  template<typename T>
  T dd_eta(const T &z) {
    return 60.0 * z * (2.0 * sqr(z) - 3.0 * z + 1.0);
  }

  /**
   * Subtracts Bernstein coefficients (deg = 9) of functions if cell is contained in an
   * obstacle row, i.e., theta=const:
   * -phi(x) * psi'(y) = -phi(x) * (-y*(1-y))
   * phi'(x) * psi(y)  = -phi'(x) * (-y^2*(1/2-y/3) + theta / 6)
   */
  void minusB9Coefficients_V(IAInterval d0, double d1, double theta,
                             IACoefficients &V0, IACoefficients &V1, const Cell &c) {
    VALUES<IAInterval> AA(c[0][0], c[0][1]);
    VALUES<IAInterval> BB(c[1][0], c[1][1]);
    VALUES<IAInterval> CC(c[2][0], c[2][1]);

    IACoefficients Xp0_Yp0 = B9_Xp0_Yp0(AA, BB, CC);
    IACoefficients Xp0_Yp1 = B9_Xp0_Yp1(AA, BB, CC);
    IACoefficients Xp0_Yp2 = B9_Xp0_Yp2(AA, BB, CC);
    IACoefficients Xp0_Yp3 = B9_Xp0_Yp3(AA, BB, CC);

    IACoefficients Xp1_Yp0 = B9_Xp1_Yp0(AA, BB, CC);
    IACoefficients Xp1_Yp1 = B9_Xp1_Yp1(AA, BB, CC);
    IACoefficients Xp1_Yp2 = B9_Xp1_Yp2(AA, BB, CC);
    IACoefficients Xp1_Yp3 = B9_Xp1_Yp3(AA, BB, CC);

    IACoefficients Xp2_Yp0 = B9_Xp2_Yp0(AA, BB, CC);
    IACoefficients Xp2_Yp1 = B9_Xp2_Yp1(AA, BB, CC);
    IACoefficients Xp2_Yp2 = B9_Xp2_Yp2(AA, BB, CC);
    IACoefficients Xp2_Yp3 = B9_Xp2_Yp3(AA, BB, CC);

    IACoefficients Xp3_Yp0 = B9_Xp3_Yp0(AA, BB, CC);
    IACoefficients Xp3_Yp1 = B9_Xp3_Yp1(AA, BB, CC);
    IACoefficients Xp3_Yp2 = B9_Xp3_Yp2(AA, BB, CC);
    IACoefficients Xp3_Yp3 = B9_Xp3_Yp3(AA, BB, CC);

    IACoefficients Xp4_Yp0 = B9_Xp4_Yp0(AA, BB, CC);
    IACoefficients Xp4_Yp1 = B9_Xp4_Yp1(AA, BB, CC);
    IACoefficients Xp4_Yp2 = B9_Xp4_Yp2(AA, BB, CC);
    IACoefficients Xp4_Yp3 = B9_Xp4_Yp3(AA, BB, CC);

    IACoefficients Xp5_Yp1 = B9_Xp5_Yp1(AA, BB, CC);
    IACoefficients Xp5_Yp2 = B9_Xp5_Yp2(AA, BB, CC);

    IAInterval e = d0 - d1;
    IAInterval d0_e = d0 / e;

    IAInterval tmp0 = d0_e * (1 - d0_e);
    int tmp1 = 1;
    IAInterval tmp2 = 1 - 2 * d0_e;
    IAInterval tmp3 = 1 - 6 * tmp0;
    IAInterval tmp4 = tmp0 * tmp2;
    IAInterval tmp5 = pow(tmp0, 2);
    IAInterval tmp6 = pow(d0_e, 3) * (10 - 3 * d0_e * (5 - 2 * d0_e));

    if (c.Subdomain() / 10 == -1) { ///left: change some signs
      tmp1 *= -1;
      tmp3 *= -1.0;
      tmp5 *= -1.0;
    }

    for (int j = 0; j < 55; ++j) {
      V0[j] -= ((((-6 * tmp1 * (Xp5_Yp1[j] - Xp5_Yp2[j]) / e
                   - 15 * tmp2 * (Xp4_Yp1[j] - Xp4_Yp2[j])) / e
                  - 10 * tmp3 * (Xp3_Yp1[j] - Xp3_Yp2[j])) / e
                 + 30 * tmp4 * (Xp2_Yp1[j] - Xp2_Yp2[j])) / e
                - 30 * tmp5 * (Xp1_Yp1[j] - Xp1_Yp2[j])) / e
               + tmp6 * (Xp0_Yp1[j] - Xp0_Yp2[j]);
      V1[j] -= 5 * ((((-tmp1 * (theta * Xp4_Yp0[j] - 3 * Xp4_Yp2[j] + 2 * Xp4_Yp3[j]) / e
                       - 2 * tmp2 * (theta * Xp3_Yp0[j] - 3 * Xp3_Yp2[j] + 2 * Xp3_Yp3[j])) / e
                      - tmp3 * (theta * Xp2_Yp0[j] - 3 * Xp2_Yp2[j] + 2 * Xp2_Yp3[j])) / e
                     + 2 * tmp4 * (theta * Xp1_Yp0[j] - 3 * Xp1_Yp2[j] + 2 * Xp1_Yp3[j])) / e
                    - tmp5 * (theta * Xp0_Yp0[j] - 3 * Xp0_Yp2[j] + 2 * Xp0_Yp3[j])) / e;
    }
  }

  /**
   * Subtracts Bernstein coefficients (deg = 9) of functions if cell is not contained in an
   * obstacle row, i.e., theta is not const:
   * -phi(x) * psi'(y) = -phi(x) * (-y*(1-y) + theta * eta'((y-a)/(b-a)) / (b-a) / 6)
   * phi'(x) * psi'(y) = -phi'(x) * (-y^2*(1/2-y/3) + value / 6 + theta * eta((y-a)/(b-a)) / 6)
   */
  void minusB9Coefficients_V(IAInterval d0, double d1, IAInterval a, double b,
                             IACoefficients &V0, IACoefficients &V1, const Cell &c,
                             double theta = 1.0, double value = 0.0) {
    VALUES<IAInterval> AA(c[0][0], c[0][1]);
    VALUES<IAInterval> BB(c[1][0], c[1][1]);
    VALUES<IAInterval> CC(c[2][0], c[2][1]);

    IACoefficients Xp0_Yp0 = B9_Xp0_Yp0(AA, BB, CC);
    IACoefficients Xp0_Yp1 = B9_Xp0_Yp1(AA, BB, CC);
    IACoefficients Xp0_Yp2 = B9_Xp0_Yp2(AA, BB, CC);
    IACoefficients Xp0_Yp3 = B9_Xp0_Yp3(AA, BB, CC);
    IACoefficients Xp0_Yp4 = B9_Xp0_Yp4(AA, BB, CC);
    IACoefficients Xp0_Yp5 = B9_Xp0_Yp5(AA, BB, CC);

    IACoefficients Xp1_Yp0 = B9_Xp1_Yp0(AA, BB, CC);
    IACoefficients Xp1_Yp1 = B9_Xp1_Yp1(AA, BB, CC);
    IACoefficients Xp1_Yp2 = B9_Xp1_Yp2(AA, BB, CC);
    IACoefficients Xp1_Yp3 = B9_Xp1_Yp3(AA, BB, CC);
    IACoefficients Xp1_Yp4 = B9_Xp1_Yp4(AA, BB, CC);
    IACoefficients Xp1_Yp5 = B9_Xp1_Yp5(AA, BB, CC);

    IACoefficients Xp2_Yp0 = B9_Xp2_Yp0(AA, BB, CC);
    IACoefficients Xp2_Yp1 = B9_Xp2_Yp1(AA, BB, CC);
    IACoefficients Xp2_Yp2 = B9_Xp2_Yp2(AA, BB, CC);
    IACoefficients Xp2_Yp3 = B9_Xp2_Yp3(AA, BB, CC);
    IACoefficients Xp2_Yp4 = B9_Xp2_Yp4(AA, BB, CC);
    IACoefficients Xp2_Yp5 = B9_Xp2_Yp5(AA, BB, CC);

    IACoefficients Xp3_Yp0 = B9_Xp3_Yp0(AA, BB, CC);
    IACoefficients Xp3_Yp1 = B9_Xp3_Yp1(AA, BB, CC);
    IACoefficients Xp3_Yp2 = B9_Xp3_Yp2(AA, BB, CC);
    IACoefficients Xp3_Yp3 = B9_Xp3_Yp3(AA, BB, CC);
    IACoefficients Xp3_Yp4 = B9_Xp3_Yp4(AA, BB, CC);
    IACoefficients Xp3_Yp5 = B9_Xp3_Yp5(AA, BB, CC);

    IACoefficients Xp4_Yp0 = B9_Xp4_Yp0(AA, BB, CC);
    IACoefficients Xp4_Yp1 = B9_Xp4_Yp1(AA, BB, CC);
    IACoefficients Xp4_Yp2 = B9_Xp4_Yp2(AA, BB, CC);
    IACoefficients Xp4_Yp3 = B9_Xp4_Yp3(AA, BB, CC);
    IACoefficients Xp4_Yp4 = B9_Xp4_Yp4(AA, BB, CC);
    IACoefficients Xp4_Yp5 = B9_Xp4_Yp5(AA, BB, CC);

    IACoefficients Xp5_Yp0 = B9_Xp5_Yp0(AA, BB, CC);
    IACoefficients Xp5_Yp1 = B9_Xp5_Yp1(AA, BB, CC);
    IACoefficients Xp5_Yp2 = B9_Xp5_Yp2(AA, BB, CC);
    IACoefficients Xp5_Yp3 = B9_Xp5_Yp3(AA, BB, CC);
    IACoefficients Xp5_Yp4 = B9_Xp5_Yp4(AA, BB, CC);

    IAInterval e = d0 - d1;
    IAInterval e2 = pow(e, 2);
    IAInterval d0_e = d0 / e;
    IAInterval d = b - a;
    IAInterval a_d = a / d;
    IAInterval a_d2 = a / pow(d, 2);

    IAInterval tmp0 = d0_e * (1 - d0_e);
    int tmp1 = 1;
    IAInterval tmp2 = 1 - 2 * d0_e;
    IAInterval tmp3 = 1 - 6 * tmp0;
    IAInterval tmp4 = tmp0 * tmp2;
    IAInterval tmp5 = pow(tmp0, 2);
    IAInterval tmp6 = pow(d0_e, 3) * (10 - 3 * d0_e * (5 - 2 * d0_e));

    IAInterval tmp10 = a_d * (1 + a_d);
    IAInterval tmp11 = 1 / pow(d, 5) * theta;
    IAInterval tmp12 = (1 + 2 * a_d) / pow(d, 4) * theta;
    IAInterval tmp13 = 1 + 5 * (1 + 6*tmp10) / pow(d, 3) * theta;
    IAInterval tmp14 = 1 + 10 * a_d * (1 + a_d * (3 + 2 * a_d)) / pow(d, 2) * theta;
    IAInterval tmp15 = pow(tmp10, 2) / d * theta;
    IAInterval tmp16 = pow(a_d, 3) * (10 + 3 * a_d * (5 + 2 * a_d)) * theta - value;

    if (c.Subdomain() / 10 == -1) { ///left: change some signs
      tmp1 *= -1;
      tmp3 *= -1.0;
      tmp5 *= -1.0;
    }

    for (int j = 0; j < 55; ++j) {
      V0[j] -= ((((6 * tmp1 * (5 * tmp15 * Xp5_Yp0[j] - tmp14 * Xp5_Yp1[j] + tmp13 * Xp5_Yp2[j] - 10 * tmp12 * Xp5_Yp3[j] + 5 * tmp11 * Xp5_Yp4[j]) / e
                   + 15 * tmp2 * (5 * tmp15 * Xp4_Yp0[j] - tmp14 * Xp4_Yp1[j] + tmp13 * Xp4_Yp2[j] - 10 * tmp12 * Xp4_Yp3[j] + 5 * tmp11 * Xp4_Yp4[j])) / e
                  + 10 * tmp3 * (5 * tmp15 * Xp3_Yp0[j] - tmp14 * Xp3_Yp1[j] + tmp13 * Xp3_Yp2[j] - 10 * tmp12 * Xp3_Yp3[j] + 5 * tmp11 * Xp3_Yp4[j])) / e
                 - 30 * tmp4 * (5 * tmp15 * Xp2_Yp0[j] - tmp14 * Xp2_Yp1[j] + tmp13 * Xp2_Yp2[j] - 10 * tmp12 * Xp2_Yp3[j] + 5 * tmp11 * Xp2_Yp4[j])) / e
                + 30 * tmp5 * (5 * tmp15 * Xp1_Yp0[j] - tmp14 * Xp1_Yp1[j] + tmp13 * Xp1_Yp2[j] - 10 * tmp12 * Xp1_Yp3[j] + 5 * tmp11 * Xp1_Yp4[j])) / e
               - tmp6 * (5 * tmp15 * Xp0_Yp0[j] - tmp14 * Xp0_Yp1[j] + tmp13 * Xp0_Yp2[j] - 10 * tmp12 * Xp0_Yp3[j] + 5 * tmp11 * Xp0_Yp4[j]);
      V1[j] -= 5 * ((((tmp1 * (tmp16 * Xp4_Yp0[j] - 30 * tmp15 * Xp4_Yp1[j] + 3 * tmp14 * Xp4_Yp2[j] - 2 * tmp13 * Xp4_Yp3[j] + 15 * tmp12 * Xp4_Yp4[j] - 6 * tmp11 * Xp4_Yp5[j]) / e
                       + 2 * tmp2 * (tmp16 * Xp3_Yp0[j] - 30 * tmp15 * Xp3_Yp1[j] + 3 * tmp14 * Xp3_Yp2[j] - 2 * tmp13 * Xp3_Yp3[j] + 15 * tmp12 * Xp3_Yp4[j] - 6 * tmp11 * Xp3_Yp5[j])) / e
                      + tmp3 * (tmp16 * Xp2_Yp0[j] - 30 * tmp15 * Xp2_Yp1[j] + 3 * tmp14 * Xp2_Yp2[j] - 2 * tmp13 * Xp2_Yp3[j] + 15 * tmp12 * Xp2_Yp4[j] - 6 * tmp11 * Xp2_Yp5[j])) / e
                     - 2 * tmp4 * (tmp16 * Xp1_Yp0[j] - 30 * tmp15 * Xp1_Yp1[j] + 3 * tmp14 * Xp1_Yp2[j] - 2 * tmp13 * Xp1_Yp3[j] + 15 * tmp12 * Xp1_Yp4[j] - 6 * tmp11 * Xp1_Yp5[j])) / e
                    + tmp5 * (tmp16 * Xp0_Yp0[j] - 30 * tmp15 * Xp0_Yp1[j] + 3 * tmp14 * Xp0_Yp2[j] - 2 * tmp13 * Xp0_Yp3[j] + 15 * tmp12 * Xp0_Yp4[j] - 6 * tmp11 * Xp0_Yp5[j])) / e;
    }
  }

  /**
   * Subtracts Bernstein coefficients (deg = 8) of functions if cell is contained in an
   * obstacle row, i.e., theta=const:
   * -phi'(x) * psi'(y) = -phi'(x) * (-y*(1-y))
   * -phi(x) * psi''(y) = -phi(x) * (-(1-2*y))
   * phi''(x) * psi(y)  = phi'(x) * (-y^2*(1/2-y/3) + theta / 6)
   */
  void minusB8Coefficients_DV(IAInterval d0, double d1, double theta,
                              IACoefficients &Dx_V0, IACoefficients &Dy_V0,
                              IACoefficients &Dx_V1, const Cell &c) {
    VALUES<IAInterval> AA(c[0][0], c[0][1]);
    VALUES<IAInterval> BB(c[1][0], c[1][1]);
    VALUES<IAInterval> CC(c[2][0], c[2][1]);

    IACoefficients Xp0_Yp0 = B8_Xp0_Yp0(AA, BB, CC);
    IACoefficients Xp0_Yp1 = B8_Xp0_Yp1(AA, BB, CC);
    IACoefficients Xp0_Yp2 = B8_Xp0_Yp2(AA, BB, CC);
    IACoefficients Xp0_Yp3 = B8_Xp0_Yp3(AA, BB, CC);

    IACoefficients Xp1_Yp0 = B8_Xp1_Yp0(AA, BB, CC);
    IACoefficients Xp1_Yp1 = B8_Xp1_Yp1(AA, BB, CC);
    IACoefficients Xp1_Yp2 = B8_Xp1_Yp2(AA, BB, CC);
    IACoefficients Xp1_Yp3 = B8_Xp1_Yp3(AA, BB, CC);

    IACoefficients Xp2_Yp0 = B8_Xp2_Yp0(AA, BB, CC);
    IACoefficients Xp2_Yp1 = B8_Xp2_Yp1(AA, BB, CC);
    IACoefficients Xp2_Yp2 = B8_Xp2_Yp2(AA, BB, CC);
    IACoefficients Xp2_Yp3 = B8_Xp2_Yp3(AA, BB, CC);

    IACoefficients Xp3_Yp0 = B8_Xp3_Yp0(AA, BB, CC);
    IACoefficients Xp3_Yp1 = B8_Xp3_Yp1(AA, BB, CC);
    IACoefficients Xp3_Yp2 = B8_Xp3_Yp2(AA, BB, CC);
    IACoefficients Xp3_Yp3 = B8_Xp3_Yp3(AA, BB, CC);

    IACoefficients Xp4_Yp0 = B8_Xp4_Yp0(AA, BB, CC);
    IACoefficients Xp4_Yp1 = B8_Xp4_Yp1(AA, BB, CC);
    IACoefficients Xp4_Yp2 = B8_Xp4_Yp2(AA, BB, CC);

    IACoefficients Xp5_Yp0 = B8_Xp5_Yp0(AA, BB, CC);
    IACoefficients Xp5_Yp1 = B8_Xp5_Yp1(AA, BB, CC);
    IACoefficients Xp5_Yp2 = B8_Xp5_Yp2(AA, BB, CC);

    IAInterval e = d0 - d1;
    IAInterval e2 = pow(e, 2);
    IAInterval d0_e = d0 / e;

    IAInterval tmp0 = d0_e * (1 - d0_e);
    int tmp1 = 1;
    IAInterval tmp2 = 1 - 2 * d0_e;
    IAInterval tmp3 = 1 - 6 * tmp0;
    IAInterval tmp4 = tmp0 * tmp2;
    IAInterval tmp5 = pow(tmp0, 2);
    IAInterval tmp6 = pow(d0_e, 3) * (10 - 3 * d0_e * (5 - 2 * d0_e));

    if (c.Subdomain() / 10 == -1) { ///left: change some signs
      tmp1 *= -1;
      tmp3 *= -1.0;
      tmp5 *= -1.0;
    }

    for (int j = 0; j < 45; ++j) {
      Dx_V0[j] -= -30 * ((((tmp1 * (Xp4_Yp1[j] - Xp4_Yp2[j]) / e
                            + 2 * tmp2 * (Xp3_Yp1[j] - Xp3_Yp2[j])) / e
                           + tmp3 * (Xp2_Yp1[j] - Xp2_Yp2[j])) / e
                          -2 * tmp4 * (Xp1_Yp1[j] - Xp1_Yp2[j])) / e
                         + tmp5 * (Xp0_Yp1[j] - Xp0_Yp2[j])) / e;
      Dy_V0[j] -= ((((-6 * tmp1 * (Xp5_Yp0[j] - 2 * Xp5_Yp1[j]) / e
                      - 15 * tmp2 * (Xp4_Yp0[j] - 2 * Xp4_Yp1[j])) / e
                     - 10 * tmp3 * (Xp3_Yp0[j] - 2 * Xp3_Yp1[j])) / e
                    + 30 * tmp4 * (Xp2_Yp0[j] - 2 * Xp2_Yp1[j])) / e
                   - 30 * tmp5 * (Xp1_Yp0[j] - 2 * Xp1_Yp1[j])) / e
                  + tmp6 * (Xp0_Yp0[j] - 2 * Xp0_Yp1[j]);
      Dx_V1[j] -= 10 * (((2 * tmp1 * (3 * Xp3_Yp2[j] - theta * Xp3_Yp0[j] - 2 * Xp3_Yp3[j]) / e
                          + 3 * tmp2 * (3 * Xp2_Yp2[j] - theta * Xp2_Yp0[j] - 2 * Xp2_Yp3[j])) / e
                         + tmp3 * (3 * Xp1_Yp2[j] - theta * Xp1_Yp0[j] - 2 * Xp1_Yp3[j])) / e
                        - tmp4 * (3 * Xp0_Yp2[j] - theta * Xp0_Yp0[j] - 2 * Xp0_Yp3[j])) / e2;
    }
  }

  /**
   * Subtracts Bernstein coefficients (deg = 8) of functions if cell is not contained in an
   * obstacle row, i.e., theta is not const:
   * -phi'(x) * psi'(y) = -phi'(x) * (-y*(1-y) + theta * eta'((y-a)/(b-a)) / (b-a) / 6)
   * -phi(x) * psi''(y) = -phi(x) * (-(1-2*y) + theta * eta''((y-a)/(b-a)) / (b-a)^2 / 6)
   * phi''(x) * psi(y)  = phi'(x) * (-y^2*(1/2-y/3) + value / 6 + theta * eta((y-a)/(b-a)) / 6)
   */
  void minusB8Coefficients_DV(IAInterval d0, double d1, IAInterval a, double b,
                              IACoefficients &Dx_V0, IACoefficients &Dy_V0,
                              IACoefficients &Dx_V1, const Cell &c,
                              double theta = 1.0, double value = 0.0) {
    VALUES<IAInterval> AA(c[0][0], c[0][1]);
    VALUES<IAInterval> BB(c[1][0], c[1][1]);
    VALUES<IAInterval> CC(c[2][0], c[2][1]);

    IACoefficients Xp0_Yp0 = B8_Xp0_Yp0(AA, BB, CC);
    IACoefficients Xp0_Yp1 = B8_Xp0_Yp1(AA, BB, CC);
    IACoefficients Xp0_Yp2 = B8_Xp0_Yp2(AA, BB, CC);
    IACoefficients Xp0_Yp3 = B8_Xp0_Yp3(AA, BB, CC);
    IACoefficients Xp0_Yp4 = B8_Xp0_Yp4(AA, BB, CC);
    IACoefficients Xp0_Yp5 = B8_Xp0_Yp5(AA, BB, CC);

    IACoefficients Xp1_Yp0 = B8_Xp1_Yp0(AA, BB, CC);
    IACoefficients Xp1_Yp1 = B8_Xp1_Yp1(AA, BB, CC);
    IACoefficients Xp1_Yp2 = B8_Xp1_Yp2(AA, BB, CC);
    IACoefficients Xp1_Yp3 = B8_Xp1_Yp3(AA, BB, CC);
    IACoefficients Xp1_Yp4 = B8_Xp1_Yp4(AA, BB, CC);
    IACoefficients Xp1_Yp5 = B8_Xp1_Yp5(AA, BB, CC);

    IACoefficients Xp2_Yp0 = B8_Xp2_Yp0(AA, BB, CC);
    IACoefficients Xp2_Yp1 = B8_Xp2_Yp1(AA, BB, CC);
    IACoefficients Xp2_Yp2 = B8_Xp2_Yp2(AA, BB, CC);
    IACoefficients Xp2_Yp3 = B8_Xp2_Yp3(AA, BB, CC);
    IACoefficients Xp2_Yp4 = B8_Xp2_Yp4(AA, BB, CC);
    IACoefficients Xp2_Yp5 = B8_Xp2_Yp5(AA, BB, CC);

    IACoefficients Xp3_Yp0 = B8_Xp3_Yp0(AA, BB, CC);
    IACoefficients Xp3_Yp1 = B8_Xp3_Yp1(AA, BB, CC);
    IACoefficients Xp3_Yp2 = B8_Xp3_Yp2(AA, BB, CC);
    IACoefficients Xp3_Yp3 = B8_Xp3_Yp3(AA, BB, CC);
    IACoefficients Xp3_Yp4 = B8_Xp3_Yp4(AA, BB, CC);
    IACoefficients Xp3_Yp5 = B8_Xp3_Yp5(AA, BB, CC);

    IACoefficients Xp4_Yp0 = B8_Xp4_Yp0(AA, BB, CC);
    IACoefficients Xp4_Yp1 = B8_Xp4_Yp1(AA, BB, CC);
    IACoefficients Xp4_Yp2 = B8_Xp4_Yp2(AA, BB, CC);
    IACoefficients Xp4_Yp3 = B8_Xp4_Yp3(AA, BB, CC);
    IACoefficients Xp4_Yp4 = B8_Xp4_Yp4(AA, BB, CC);

    IACoefficients Xp5_Yp0 = B8_Xp5_Yp0(AA, BB, CC);
    IACoefficients Xp5_Yp1 = B8_Xp5_Yp1(AA, BB, CC);
    IACoefficients Xp5_Yp2 = B8_Xp5_Yp2(AA, BB, CC);
    IACoefficients Xp5_Yp3 = B8_Xp5_Yp3(AA, BB, CC);

    IAInterval e = d0 - d1;
    IAInterval e2 = pow(e, 2);
    IAInterval d0_e = d0 / e;
    IAInterval d = b - a;
    IAInterval a_d = a / d;
    IAInterval a_d2 = a / pow(d, 2);

    IAInterval tmp0 = d0_e * (1 - d0_e);
    int tmp1 = 1;
    IAInterval tmp2 = 1 - 2 * d0_e;
    IAInterval tmp3 = 1 - 6 * tmp0;
    IAInterval tmp4 = tmp0 * tmp2;
    IAInterval tmp5 = pow(tmp0, 2);
    IAInterval tmp6 = pow(d0_e, 3) * (10 - 3 * d0_e * (5 - 2 * d0_e));

    IAInterval tmp10 = a_d * (1 + a_d);
    IAInterval tmp11 = theta / pow(d, 5);
    IAInterval tmp12 = (1 + 2 * a_d) / pow(d, 4) * theta;
    IAInterval tmp13 = 1 + 5 * (1 + 6*tmp10) / pow(d, 3) * theta;
    IAInterval tmp14 = 1 + 10 * a_d * (1 + a_d * (3 + 2 * a_d)) / pow(d, 2) * theta;
    IAInterval tmp15 = pow(tmp10, 2) / d * theta;
    IAInterval tmp16 = pow(a_d, 3) * (10 + 3 * a_d * (5 + 2 * a_d)) * theta - value;

    if (c.Subdomain() / 10 == -1) { ///left: change some signs
      tmp1 *= -1;
      tmp3 *= -1.0;
      tmp5 *= -1.0;
    }

    for (int j = 0; j < 45; ++j) {
      Dx_V0[j] -= 30 * ((((tmp1 * (5 * tmp15 * Xp4_Yp0[j] - tmp14 * Xp4_Yp1[j] + tmp13 * Xp4_Yp2[j] - 10 * tmp12 * Xp4_Yp3[j] + 5 * tmp11 * Xp4_Yp4[j]) / e
                           + 2*tmp2*(5 * tmp15 * Xp3_Yp0[j] - tmp14 * Xp3_Yp1[j] + tmp13 * Xp3_Yp2[j] - 10 * tmp12 * Xp3_Yp3[j] + 5 * tmp11 * Xp3_Yp4[j])) / e
                          + tmp3*(5 * tmp15 * Xp2_Yp0[j] - tmp14 * Xp2_Yp1[j] + tmp13 * Xp2_Yp2[j] - 10 * tmp12 * Xp2_Yp3[j] + 5 * tmp11 * Xp2_Yp4[j])) / e
                         - 2*tmp4*(5 * tmp15 * Xp1_Yp0[j] - tmp14 * Xp1_Yp1[j] + tmp13 * Xp1_Yp2[j] - 10 * tmp12 * Xp1_Yp3[j] + 5 * tmp11 * Xp1_Yp4[j])) / e
                        + tmp5*(5 * tmp15 * Xp0_Yp0[j] - tmp14 * Xp0_Yp1[j] + tmp13 * Xp0_Yp2[j] - 10 * tmp12 * Xp0_Yp3[j] + 5 * tmp11 * Xp0_Yp4[j])) / e;
      Dy_V0[j] -= ((((-6 * tmp1 * (tmp14 * Xp5_Yp0[j] - 2 * tmp13 * Xp5_Yp1[j] + 30 * tmp12 * Xp5_Yp2[j] - 20 * tmp11 * Xp5_Yp3[j]) / e
                      - 15 * tmp2*(tmp14 * Xp4_Yp0[j] - 2 * tmp13 * Xp4_Yp1[j] + 30 * tmp12 * Xp4_Yp2[j] - 20 * tmp11 * Xp4_Yp3[j])) / e
                     - 10 * tmp3*(tmp14 * Xp3_Yp0[j] - 2 * tmp13 * Xp3_Yp1[j] + 30 * tmp12 * Xp3_Yp2[j] - 20 * tmp11 * Xp3_Yp3[j])) / e
                    + 30 * tmp4*(tmp14 * Xp2_Yp0[j] - 2 * tmp13 * Xp2_Yp1[j] + 30 * tmp12 * Xp2_Yp2[j] - 20 * tmp11 * Xp2_Yp3[j])) / e
                   - 30 * tmp5*(tmp14 * Xp1_Yp0[j] - 2 * tmp13 * Xp1_Yp1[j] + 30 * tmp12 * Xp1_Yp2[j] - 20 * tmp11 * Xp1_Yp3[j])) / e
                  + tmp6 * (tmp14 * Xp0_Yp0[j] - 2 * tmp13 * Xp0_Yp1[j] + 30 * tmp12 * Xp0_Yp2[j] - 20 * tmp11 * Xp0_Yp3[j]);
      Dx_V1[j] -= 10 * (((2 * tmp1 * (tmp16 * Xp3_Yp0[j] - 30 * tmp15 * Xp3_Yp1[j] + 3 * tmp14 * Xp3_Yp2[j] - 2 * tmp13 * Xp3_Yp3[j] + 15 * tmp12 * Xp3_Yp4[j] - 6 * tmp11 * Xp3_Yp5[j]) / e
                          + 3 * tmp2*(tmp16 * Xp2_Yp0[j] - 30 * tmp15 * Xp2_Yp1[j] + 3 * tmp14 * Xp2_Yp2[j] - 2 * tmp13 * Xp2_Yp3[j] + 15 * tmp12 * Xp2_Yp4[j] - 6 * tmp11 * Xp2_Yp5[j])) / e
                         + tmp3 * (tmp16 * Xp1_Yp0[j] - 30 * tmp15 * Xp1_Yp1[j] + 3 * tmp14 * Xp1_Yp2[j] - 2 * tmp13 * Xp1_Yp3[j] + 15 * tmp12 * Xp1_Yp4[j] - 6 * tmp11 * Xp1_Yp5[j])) / e
                        - tmp4 * (tmp16 * Xp0_Yp0[j] - 30 * tmp15 * Xp0_Yp1[j] + 3 * tmp14 * Xp0_Yp2[j] - 2 * tmp13 * Xp0_Yp3[j] + 15 * tmp12 * Xp0_Yp4[j] - 6 * tmp11 * Xp0_Yp5[j])) / e2;
    }
  }

  /// Returns Bernstein coefficients (dim = 4) of function: theta * eta'((y-a)/(b-a)) / (b-a) / 6
  IACoefficients b4Coefficients_D_Theta_6(const Cell &c,
                                          IAInterval a, double b = 1.0, double theta = 1.0) {
    IAInterval d = b - a;
    IAInterval factor = theta / pow(d, 3);
    IAInterval a1(c[0][1]);
    IAInterval b1(c[1][1]);
    IAInterval c1(c[2][1]);
    return IACoefficients {
        5 * sqr(a - a1) * (1 + (a - a1) * (2 + (a - a1) / d) / d) * factor,
        5 * (a - a1) * ((2 * a - b1 - a1) / 2 + (a - a1) * ((4 * a - 3 * b1 - a1) / 2
                                                            + (a - a1) * (a - b1) / d) / d) * factor,
        5 * (a - a1) * ((2 * a - c1 - a1) / 2 + (a - a1) * ((4 * a - 3 * c1 - a1) / 2
                                                            + (a - a1) * (a - c1) / d) / d) * factor,
        5 * (a * (a - b1 - a1) + (b1 * (b1 + 2 * a1) + a1 * (a1 + 2 * b1)) / 6
             + (a - a1) * (a - b1) * (2 * (a - (a1 + b1) / 2) + (a - b1) * (a - a1) / d) / d) * factor,
        5 * ((3 * a * (2 * (a - a1) - c1 - b1) + c1 * (b1 + 2 * a1) + a1 * (2 * b1 + a1)) / 6
             + (a - a1) * ((a * (4 * a - 3 * c1 - 3 * b1 - 2 * a1) + c1 * (b1 + a1) + b1 * (c1 + a1)) / 2
                           + (a - a1) * (a - c1) * (a - b1) / d) / d) * factor,
        5 * (a * (a - c1 - a1) + (c1 * (c1 + 2 * a1) + a1 * (a1 + 2 * c1)) / 6
             + (a - a1) * (a - c1) * (2 * (a - (a1 + c1) / 2) + (a - c1) * (a - a1) / d) / d) * factor,
        5 * (a - b1) * ((2 * a - a1 - b1) / 2 + (a - b1) * ((4 * a - 3 * a1 - b1) / 2
                                                            + (a - b1) * (a - a1) / d) / d) * factor,
        5 * ((3 * a * (2 * (a - b1) - c1 - a1) + c1 * (a1 + 2 * b1) + b1 * (2 * a1 + b1)) / 6
             + (a - b1) * ((a * (4 * a - 3 * c1 - 3 * a1 - 2 * b1) + c1 * (a1 + b1) + a1 * (c1 + b1)) / 2
                           + (a - b1) * (a - c1) * (a - a1) / d) / d) * factor,
        5 * ((3 * a * (2 * (a - c1) - b1 - a1) + b1 * (a1 + 2 * c1) + c1 * (2 * a1 + c1)) / 6
             + (a - c1) * ((a * (4 * a - 3 * b1 - 3 * a1 - 2 * c1) + b1 * (a1 + c1) + a1 * (b1 + c1)) / 2
                           + (a - c1) * (a - b1) * (a - a1) / d) / d) * factor,
        5 * (a - c1) * ((2 * a - a1 - c1) / 2 + (a - c1) * ((4 * a - 3 * a1 - c1) / 2
                                                            + (a - c1) * (a - a1) / d) / d) * factor,
        5 * sqr(a - b1) * (1 + (a - b1) * (2 + (a - b1) / d) / d) * factor,
        5 * (a - b1) * ((2 * a - c1 - b1) / 2 + (a - b1) * ((4 * a - 3 * c1 - b1) / 2
                                                            + (a - b1) * (a - c1) / d) / d) * factor,
        5 * (a * (a - c1 - b1) + (c1 * (c1 + 2 * b1) + b1 * (b1 + 2 * c1)) / 6
             + (a - b1) * (a - c1) * (2 * (a - (b1 + c1) / 2) + (a - c1) * (a - b1) / d) / d) * factor,
        5 * (a - c1) * ((2 * a - b1 - c1) / 2 + (a - c1) * ((4 * a - 3 * b1 - c1) / 2
                                                            + (a - c1) * (a - b1) / d) / d) * factor,
        5 * sqr(a - c1) * (1 + (a - c1) * (2 + (a - c1) / d) / d) * factor
    };
  }

  /// Returns Bernstein coefficients (dim = 3) of function: theta * eta''((y-a)/(b-a)) / (b-a)^2 / 6
  IACoefficients b3Coefficients_DD_Theta_6(const Cell &c,
                                           IAInterval a, double b = 1.0, double theta = 1.0) {
    IAInterval d = b - a;
    IAInterval factor = theta / pow(d, 3);
    IAInterval a1(c[0][1]);
    IAInterval b1(c[1][1]);
    IAInterval c1(c[2][1]);
    return IACoefficients {
        10 * (a1 - a) * (1 + (a1 - a) * (-3 + 2 * (a1 - a) / d) / d) * factor,
        10 * ((2 * a1 + b1) / 3 - a + (a1 - a) * (3 * (a - (a1 + 2 * b1) / 3)
                                                  + 2 * (a1 - a) * (b1 - a) / d) / d) * factor,
        10 * ((2 * a1 + c1) / 3 - a + (a1 - a) * (3 * (a - (a1 + 2 * c1) / 3)
                                                  + 2 * (a1 - a) * (c1 - a) / d) / d) * factor,
        10 * ((2 * b1 + a1) / 3 - a + (b1 - a) * (3 * (a - (b1 + 2 * a1) / 3)
                                                  + 2 * (b1 - a) * (a1 - a) / d) / d) * factor,
        10 * ((a1 + b1 + c1) / 3 - a + ((-3 * a + 2 * (a1 + b1 + c1)) * a - (b1 + c1) * a1 - c1 * b1
                                        + 2 * (c1 - a) * (b1 - a) * (a1 - a) / d) / d) * factor,
        10 * ((2 * c1 + a1) / 3 - a + (c1 - a) * (3 * (a - (c1 + 2 * a1) / 3)
                                                  + 2 * (c1 - a) * (a1 - a) / d) / d) * factor,
        10 * (b1 - a) * (1 + (b1 - a) * (-3 + 2 * (b1 - a) / d) / d) * factor,
        10 * ((2 * b1 + c1) / 3 - a + (b1 - a) * (3 * (a - (b1 + 2 * c1) / 3)
                                                  + 2 * (b1 - a) * (c1 - a) / d) / d) * factor,
        10 * ((2 * c1 + b1) / 3 - a + (c1 - a) * (3 * (a - (c1 + 2 * b1) / 3)
                                                  + 2 * (c1 - a) * (b1 - a) / d) / d) * factor,
        10 * (c1 - a) * (1 + (c1 - a) * (-3 + 2 * (c1 - a) / d) / d) * factor
    };
  }

} //namespace navierstokes

#endif //NAVIERSTOKESPROBLEMETA_HPP
