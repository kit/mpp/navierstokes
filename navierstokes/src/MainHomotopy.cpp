#include "IAIO.hpp"
#include "m++.hpp"

#include "MeshesCreator.hpp"
#include "IAEigenvalueMethods.hpp"
#include "NavierStokesAssembleHomotpy.hpp"
#include "NavierStokesHomotopy.hpp"

template<bool VERIFIED>
int constraintHomotopy(int numEV, double sigma, double &rho) {

  std::string meshName = "UnitSquare";
  Config::Get("MeshStrip", meshName);
  int stripLevel = 2;
  Config::Get("StripLevel", stripLevel);
  int stripCLevel = 2;
  Config::Get("StripCoarseLevel", stripCLevel);
  int plevel = 1;
  Config::Get("plevelStrip", plevel);

  std::unique_ptr<Meshes> stripMeshes = MeshesCreator(meshName).
      WithPLevel(plevel).
      WithCLevel(stripCLevel).
      WithLevel(stripLevel).CreateUnique();
  stripMeshes->PrintInfo();

  navierstokes::IndependentDivergenceHomotopyStokesAssemble<VERIFIED> assemble(sigma, 3.0);

  std::shared_ptr<IDiscretization> discEV = std::move(assemble.CreateEVDisc(*stripMeshes));
  Eigenpairs EP(numEV, discEV);

  std::shared_ptr<IAIDiscretization> iadiscEV = nullptr;
  if constexpr (VERIFIED) {
    iadiscEV = std::move(assemble.CreateIAEVDisc(*stripMeshes));
    EP.SetIADisc(iadiscEV);
  }

  IAHomotopyMethod<VERIFIED> HM(0.0, 0.999998, 0.05, stripLevel - 1, "IndependentDivergence");
  HM.NearRho(0.01);
  HM.SeparationRho(0.005);
  HM.SeparationCluster(0.01);

//  double stepFinal = HM(assemble, EP, rho, true);
  double stepFinal = 0.999; rho = 67.2341;
  mout << endl;

  //Final Goerisch if n>0
  if (EP.size() > 0) {
    IAUpperEigenvalueBounds Lambda;
    Eigenfcts U(EP.size(), 0.0,  discEV);
    if constexpr (VERIFIED) {
      U.SetIADisc(iadiscEV);
    }
    HM(assemble, U, Lambda, stepFinal);

    IALowerEigenvalueBounds lambda(Lambda.size());
//    HM(assemble, EP, lambda, rho, false, stepFinal);
    HM(assemble, EP, lambda, rho, true, stepFinal);

    IAEigenvalueEnclosures enclosures(lambda, Lambda);
    mout << enclosures << endl;
  } else {
    mout << "No eigenvalues below rho(" << stepFinal << ")= " << OutputDown(rho) << endl;
  }



  mout.PrintInfo("IndependentDivergence", 2,
                 PrintInfoEntry("rho", OutputDown(rho), 2),
                 PrintInfoEntry("final step", stepFinal, 2),
                 PrintInfoEntry("num ev", EP.size(), 2)
  );

  return EP.size();
}

template<bool VERIFIED>
void domainHomotopy(int numEV, double sigma, double &rho) {

  std::string meshName = "UnitSquare";
  Config::Get("MeshStrip", meshName);
  int stripLevel = 2;
  Config::Get("StripLevel", stripLevel);
  int stripCLevel = 2;
  Config::Get("StripCoarseLevel", stripCLevel);
  int plevel = 1;
  Config::Get("plevelStrip", plevel);

//  std::unique_ptr<Meshes> stripMeshes = MeshesCreator(meshName).
//      WithPLevel(plevel).
//      WithCLevel(stripCLevel).
//      WithLevel(stripLevel).CreateUnique();
//  stripMeshes->PrintInfo();

//  navierstokes::IndependentDomainHomotopyStokesAssemble<VERIFIED> assemble(sigma, 3.0);
//
//  std::unique_ptr<IDiscretization> discEV = std::move(assemble.CreateEVDisc(*stripMeshes));
//  Eigenpairs EP(numEV, *discEV);
//
//  std::unique_ptr<IAIDiscretization> iadiscEV = nullptr;
//  if constexpr (VERIFIED) {
//    iadiscEV = std::move(assemble.CreateIAEVDisc(*stripMeshes));
//    EP.SetIADisc(*iadiscEV);
//  }
//
//  Eigenfcts U(EP.size(), 0.0,  *discEV);
//  if constexpr (VERIFIED) {
//    U.SetIADisc(*iadiscEV);
//  }

//  IARayleighRitzMethod<VERIFIED> RR;
//  IAUpperEigenvalueBounds Lambda;
//  RR(assemble, U, Lambda);

//  mout << Lambda << endl;

//  IAHomotopyMethod<VERIFIED> HM(0.0, 0.99, 0.05, stripLevel - 1, "IndependentDivergence");
//  double stepFinal = HM(assemble, EP, rho, true);
//  mout << endl;

  //Final Goerisch if n>0
//  if (EP.size() > 0) {
//    IAUpperEigenvalueBounds Lambda;
//    Eigenfcts U(EP.size(), 0.0,  *discEV);
//    if constexpr (VERIFIED) {
//      U.SetIADisc(*iadiscEV);
//    }
//    HM(assemble, U, Lambda, stepFinal);
//    IALowerEigenvalueBounds lambda;
//    HM(assemble, EP, lambda, rho, false, stepFinal);
//
//    IAEigenvalueEnclosures enclosures(lambda, Lambda);
//    mout << enclosures << endl;
//  } else {
//    mout << "No eigenvalues below rho(" << stepFinal << ")= " << OutputDown(rho) << endl;
//  }
//
//
//
//  mout.PrintInfo("IndependentDivergence", 2,
//                 PrintInfoEntry("rho", OutputDown(rho), 2),
//                 PrintInfoEntry("final step", stepFinal, 2),
//                 PrintInfoEntry("num ev", EP.size(), 2)
//  );

//  return EP.size();
}

int main(int argc, char **argv) {
  Config::SetConfPath("./../navierstokes/conf/");
  Config::SetConfigFileName("m++.conf");
  Mpp::initialize(&argc, argv);

  Config::PrintInfo();

  SetIAOutputPrecision(18, 13);

  int n_start = 14;
  double sigma = 1.0;

  IAEigenvalueEnclosures mu = navierstokes::BaseProblem(n_start + 2, 3.0, sigma);

  double eps_rho = 1e-2;
  double rho = inf(mu[n_start]) - eps_rho;

  mout << "Eigenvalues of the base problem:" << endl << mu << endl << endl;

  if (rho < sup(mu[n_start - 1])) {
    THROW("rho(0) too small")
  }

  mout << "rho(0)= " << rho << endl;

  int n = constraintHomotopy<true>(n_start, sigma, rho);

//  domainHomotopy<true>(10, sigma, rho);


//  std::string meshName = "UnitSquare";
//  Config::Get("Mesh", meshName);
//  int stripLevel = 2;
//  Config::Get("StripLevel", stripLevel);
//  int stripCLevel = 2;
//  Config::Get("StripCoarseLevel", stripCLevel);
//  int plevel = 1;
//  Config::Get("plevelStrip", plevel);
//
//  std::unique_ptr<Meshes> stripMeshes = MeshesCreator(meshName).
//      WithPLevel(plevel).
//      WithCLevel(stripCLevel).
////      WithFinalBarycentricRefinement().
//      WithLevel(stripLevel).CreateUnique();
//  stripMeshes->PrintInfo();

//  navierstokes::IndependentDomainHomotopyStokesAssemble<true> assemble(sigma, 3.0);

//  std::unique_ptr<IDiscretization> discEV = std::move(assemble.CreateEVDisc(*stripMeshes));
//  Eigenpairs EP(10, *discEV);
//
//  std::unique_ptr<IAIDiscretization> iadiscEV = nullptr;
//  if constexpr (true) {
//    iadiscEV = std::move(assemble.CreateIAEVDisc(*stripMeshes));
//    EP.SetIADisc(*iadiscEV);
//  }
//
//  Eigenfcts U(EP.size(), 0.0,  *discEV);
//  if constexpr (true) {
//    U.SetIADisc(*iadiscEV);
//  }

//  IARayleighRitzMethod<true> RR;
//  IAUpperEigenvalueBounds Lambda;
//  RR(assemble, U, Lambda);

//  mout << endl << Lambda << endl;

  return 0;
}
