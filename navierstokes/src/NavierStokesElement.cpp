#include "NavierStokesElement.hpp"

#include "bernstein/BernsteinCoefficients.hpp"

namespace navierstokes {

  template<typename T, int sDim, int tDim>
  NavierStokesElementT<T, sDim, tDim>::~NavierStokesElementT() {}

  template<typename T, int sDim, int tDim>
  void NavierStokesElementT<T, sDim, tDim>::Range(IAInterval &R0, IAInterval &R1) {
    initCoefficientsB3_B4(false);
    evalRange(coeff0, coeff1, R0, R1);
  }

  template<typename T, int sDim, int tDim>
  void NavierStokesElementT<T, sDim, tDim>::RangeD(IAInterval &R_Dx_0,
                                                   IAInterval &R_Dy_0,
                                                   IAInterval &R_Dx_1) {
    initCoefficientsB3_B4(false);
    evalRangeD(coeff0_Dx, coeff0_Dy, coeff1_Dx, R_Dx_0, R_Dy_0, R_Dx_1);
  }

  template<typename T, int sDim, int tDim>
  void NavierStokesElementT<T, sDim, tDim>::Ranges(IAInterval &W0, IAInterval &W1,
                                                   IAInterval &UOmega0, IAInterval &UOmega1,
                                                   IAInterval &Dx_UOmega0, IAInterval &Dy_UOmega0,
                                                   IAInterval &Dx_UOmega1) {
    if (problem.CellOutside(this->c)) {
      /// W = U
      initCoefficientsB3_B4(false);
      IACoefficients coeff_0 = problem.B4Coefficients_U(this->c);
      evalRange(coeff_0, W0);
      W1 = 0.0;

      for (int j = 0; j < 15; ++j) {
        coeff_0[j] += coeff0[j];
      }
      evalRange(coeff_0, coeff1, UOmega0, UOmega1);

      IACoefficients coeff_Dy_0 = problem.B3Coefficients_DU(this->c);
      for (int j = 0; j < 10; ++j) {
        coeff_Dy_0[j] += coeff0_Dy[j];
      }
      IACoefficients coeff_Dy_0_Dx_1 = coeff_Dy_0;
      for (int j = 0; j < 10; ++j) {
        coeff_Dy_0_Dx_1[j] += coeff1_Dx[j];
      }
      evalRangeD(coeff0_Dx, coeff_Dy_0, coeff1_Dx, Dx_UOmega0, Dy_UOmega0, Dx_UOmega1);
    } else if (problem.CellNearObstacle(this->c)) {
      /// W = 0
      initCoefficientsB3_B4(false);
      W0 = 0.0;
      W1 = 0.0;
      evalRange(coeff0, coeff1, UOmega0, UOmega1);
      evalRangeD(coeff0_Dx, coeff0_Dy, coeff1_Dx, Dx_UOmega0, Dy_UOmega0, Dx_UOmega1);
    } else if (problem.CellInObstacleColumn(this->c)) {
      /// phi = 1, i.e., W = (-theta'(y)/6.0, 0) -> note minus sign
      initCoefficientsB3_B4(false);
      IACoefficients coeff_0 = problem.B4Coefficients_D_Theta_6(this->c);
      evalRange(coeff_0, W0);
      W1 = 0.0;

      for (int j = 0; j < 15; ++j) {
        coeff_0[j] += coeff0[j];
      }
      evalRange(coeff_0, coeff1, UOmega0, UOmega1);

      IACoefficients coeff_Dy_0 = problem.B3Coefficients_DD_Theta_6(this->c);
      for (int j = 0; j < 10; ++j) {
        coeff_Dy_0[j] += coeff0_Dy[j];
      }
      evalRangeD(coeff0_Dx, coeff_Dy_0, coeff1_Dx, Dx_UOmega0, Dy_UOmega0, Dx_UOmega1);
    } else {
      /// W = U - (-phi(x)*psi'(y), phi'(x)*psi(y))
      initCoefficientsB3_B4(false);
      IACoefficients coeff_W0;
      IACoefficients coeff_W1;
      problem.B9Coefficients_Gamma(coeff_W0, coeff_W1, this->c);
      evalRange(coeff_W0, coeff_W1, W0, W1);

      IACoefficients coeff_0 = B4ToB9(coeff0);
      IACoefficients coeff_1 = B4ToB9(coeff1);
      for (int j = 0; j < 55; ++j) {
        coeff_0[j] += coeff_W0[j];
        coeff_1[j] += coeff_W1[j];
      }
      evalRange(coeff_0, coeff_1, UOmega0, UOmega1);

      IACoefficients coeff_Dx_0 = B3ToB8(coeff0_Dx);
      IACoefficients coeff_Dy_0 = B3ToB8(coeff0_Dy);
      IACoefficients coeff_Dx_1 = B3ToB8(coeff1_Dx);
      problem.PlusB8Coefficients_DGamma(coeff_Dx_0, coeff_Dy_0, coeff_Dx_1, this->c);
      evalRangeD(coeff_Dx_0, coeff_Dy_0, coeff_Dx_1, Dx_UOmega0, Dy_UOmega0, Dx_UOmega1);
    }
  }

  template<typename T, int sDim, int tDim>
  void NavierStokesElementT<T, sDim, tDim>::Ranges(IAInterval &UOmega0, IAInterval &UOmega1,
              IAInterval &Dx_UOmega0, IAInterval &Dy_UOmega0, IAInterval &Dx_UOmega1) {
    IAInterval W0, W1;
    Ranges(W0, W1, UOmega0, UOmega1, Dx_UOmega0, Dy_UOmega0, Dx_UOmega1);
  }

  template<typename T, int sDim, int tDim>
  void NavierStokesElementT<T, sDim, tDim>::RangesPlumSimple(IAInterval &Dx_0,
                                                             IAInterval &Dy_0_Dx_1) {
    if (problem.CellOutside(this->c)) {
      /// V = 0 (note that DU is also contained in definition of tau -> DU appears in coefficients)
      initCoefficientsB3_B4(false);
      IACoefficients coeff_Dy_0_Dx_1 = problem.B3Coefficients_DU(this->c);
      for (int j = 0; j < 10; ++j) {
        coeff_Dy_0_Dx_1[j] += coeff0_Dy[j];
        coeff_Dy_0_Dx_1[j] += coeff1_Dx[j];
      }
      evalRangeD(coeff0_Dx, coeff_Dy_0_Dx_1, Dx_0, Dy_0_Dx_1);
    } else if (problem.CellNearObstacle(this->c)) {
      /// V = U (note that DU is also contained in definition of tau -> canceling of DU!)
      initCoefficientsB3_B4(false);
      IACoefficients coeff_Dy_0_Dx_1 = coeff0_Dy;
      for (int j = 0; j < 10; ++j) {
        coeff_Dy_0_Dx_1[j] += coeff1_Dx[j];
      }
      evalRangeD(coeff0_Dx, coeff_Dy_0_Dx_1, Dx_0, Dy_0_Dx_1);
    } else if (problem.CellInObstacleColumn(this->c)) {
      /// phi = 1, i.e., V = (-psi'(y), 0) -> note minus sign
      initCoefficientsB3_B4(false);
      IACoefficients coeff_Dy_0_Dx_1 = problem.B3Coefficients_DD_Psi(this->c);
      IACoefficients coeff_Dy_U0 = problem.B3Coefficients_DU(this->c);
      for (int j = 0; j < 10; ++j) {
        coeff_Dy_0_Dx_1[j] += coeff_Dy_U0[j];
        coeff_Dy_0_Dx_1[j] += coeff0_Dy[j];
        coeff_Dy_0_Dx_1[j] += coeff1_Dx[j];
      }
      evalRangeD(coeff0_Dx, coeff_Dy_0_Dx_1, Dx_0, Dy_0_Dx_1);
    } else {
      /// V = (-phi(x)*psi'(y), phi'(x)*psi(y))
      initCoefficientsB3_B4(false);

      IACoefficients coeff_Dx_0 = B3ToB8(coeff0_Dx);
      IACoefficients coeff_Dy_0_Dx_1 = B3ToB8(coeff0_Dy);
      IACoefficients coeff_Dx_1 = B3ToB8(coeff1_Dx);
      problem.PlusB8Coefficients_DGamma(coeff_Dx_0, coeff_Dy_0_Dx_1, coeff_Dx_1, this->c);

      for (int j = 0; j < 45; ++j) {
        coeff_Dy_0_Dx_1[j] += coeff_Dx_1[j];
      }
      evalRange(coeff_Dx_0, coeff_Dy_0_Dx_1, Dx_0, Dy_0_Dx_1);
    }
  }

  template<typename T, int sDim, int tDim>
  void NavierStokesElementT<T, sDim, tDim>::RangesPlumExtended(IAInterval &Omega0,
                                                               IAInterval &Omega1,
                                                               IAInterval &Dx_Omega0,
                                                               IAInterval &Dy_Omega0,
                                                               IAInterval &Dx_Omega1,
                                                               IAInterval &Dy_0_Dx_1) {
    if (problem.CellOutside(this->c)) {
      /// V = 0
      initCoefficientsB3_B4(false);
      IACoefficients coeff_Dy_0_Dx_1 = coeff0_Dy;
      evalRange(coeff0, coeff1, Omega0, Omega1);
      for (int j = 0; j < 10; ++j) {
        coeff_Dy_0_Dx_1[j] += coeff1_Dx[j];
      }
      evalRangeD(coeff0_Dx, coeff0_Dy, coeff1_Dx, coeff_Dy_0_Dx_1, Dx_Omega0, Dy_Omega0, Dx_Omega1, Dy_0_Dx_1);
    } else if (problem.CellNearObstacle(this->c)) {
      /// V = U
      initCoefficientsB3_B4(false);
      IACoefficients coeff_0 = problem.B4Coefficients_U(this->c);
      for (int j = 0; j < 15; ++j) {
        coeff_0[j] *= -1.0;
        coeff_0[j] += coeff0[j];
      }
      evalRange(coeff_0, coeff1, Omega0, Omega1);

      IACoefficients coeff_Dy_0 = problem.B3Coefficients_DU(this->c);
      for (int j = 0; j < 10; ++j) {
        coeff_Dy_0[j] *= -1.0;
        coeff_Dy_0[j] += coeff0_Dy[j];
      }
      IACoefficients coeff_Dy_0_Dx_1 = coeff_Dy_0;
      for (int j = 0; j < 10; ++j) {
        coeff_Dy_0_Dx_1[j] += coeff1_Dx[j];
      }
      evalRangeD(coeff0_Dx, coeff_Dy_0, coeff1_Dx,
                 coeff_Dy_0_Dx_1, Dx_Omega0, Dy_Omega0, Dx_Omega1, Dy_0_Dx_1);
    } else if (problem.CellInObstacleColumn(this->c)) {
      /// phi = 1, i.e., V = (-psi'(y), 0) ->note minus sign
      initCoefficientsB3_B4(false);
      IACoefficients coeff_0 = problem.B4Coefficients_D_Psi(this->c);
      for (int j = 0; j < 15; ++j) {
        coeff_0[j] += coeff0[j];
      }
      evalRange(coeff_0, coeff1, Omega0, Omega1);

      IACoefficients coeff_Dy_0 = problem.B3Coefficients_DD_Psi(this->c);
      for (int j = 0; j < 10; ++j) {
        coeff_Dy_0[j] += coeff0_Dy[j];
      }
      IACoefficients coeff_Dy_0_Dx_1 = coeff_Dy_0;
      for (int j = 0; j < 10; ++j) {
        coeff_Dy_0_Dx_1[j] += coeff1_Dx[j];
      }
      evalRangeD(coeff0_Dx, coeff_Dy_0, coeff1_Dx,
                 coeff_Dy_0_Dx_1, Dx_Omega0, Dy_Omega0, Dx_Omega1, Dy_0_Dx_1);
    } else {
      /// V = (-phi(x)*psi'(y), phi'(x)*psi(y))
      initCoefficientsB3_B4(false);

      IACoefficients coeff_0 = B4ToB9(coeff0);
      IACoefficients coeff_1 = B4ToB9(coeff1);
      problem.MinusB9Coefficients_V(coeff_0, coeff_1, this->c);
      evalRange(coeff_0, coeff_1, Omega0, Omega1);

      IACoefficients coeff_Dx_0 = B3ToB8(coeff0_Dx);
      IACoefficients coeff_Dy_0 = B3ToB8(coeff0_Dy);
      IACoefficients coeff_Dx_1 = B3ToB8(coeff1_Dx);
      problem.MinusB8Coefficients_DV(coeff_Dx_0, coeff_Dy_0, coeff_Dx_1, this->c);

      IACoefficients coeff_Dy_0_Dx_1 = coeff_Dy_0;
      for (int j = 0; j < 45; ++j) {
        coeff_Dy_0_Dx_1[j] += coeff_Dx_1[j];
      }
      evalRangeD(coeff_Dx_0, coeff_Dy_0, coeff_Dx_1, coeff_Dy_0_Dx_1, Dx_Omega0, Dy_Omega0, Dx_Omega1, Dy_0_Dx_1);
    }
  }

  template<typename T, int sDim, int tDim>
  void NavierStokesElementT<T, sDim, tDim>::RangesDelta(IAInterval &Dx_UomegaTVT0,
                                                        IAInterval &Dy_UomegaTVT0,
                                                        IAInterval &Dx_UomegaTVT1) {
    initCoefficientsB3_B4(true);
    IACoefficients coeff_Dy_0 = problem.B3Coefficients_DU(this->c);
    for (int j = 0; j < 10; ++j)
      coeff_Dy_0[j] += coeff0_Dy[j];
    evalRangeD(coeff0_Dx, coeff_Dy_0, coeff1_Dx, Dx_UomegaTVT0, Dy_UomegaTVT0, Dx_UomegaTVT1);
  }

  template<typename T, int sDim, int tDim>
  T NavierStokesElementT<T, sDim, tDim>::vectorValue(int i, int k, bool useVec) {
    if (useVec) {
      return T(velocityVec(this->r(i), k)) - vec(this->r(i), k);
    } else {
      return velocityVec(this->r(i), k);
    }
  }

  template<typename T, int sDim, int tDim>
  IACoefficients NavierStokesElementT<T, sDim, tDim>::femCoeff(bool useVec) {
    IACoefficients c(21);
    for (int i = 0; i < 3; ++i) {
      c[6 * i] = vectorValue(i, 0, useVec);
      c[6 * i + 1] =
          this->C0[0][0] * vectorValue(i, 1, useVec)
          + this->C0[0][1] * vectorValue(i, 2, useVec);
      c[6 * i + 2] =
          this->C0[1][0] * vectorValue(i, 1, useVec)
          + this->C0[1][1] * vectorValue(i, 2, useVec);
      c[6 * i + 3] =
          this->C1[0][0] * vectorValue(i, 3, useVec)
          + this->C1[0][1] * vectorValue(i, 4, useVec)
          + this->C1[0][2] * vectorValue(i, 5, useVec);
      c[6 * i + 4] =
          this->C1[1][0] * vectorValue(i, 3, useVec)
          + this->C1[1][1] * vectorValue(i, 4, useVec)
          + this->C1[1][2] * vectorValue(i, 5, useVec);
      c[6 * i + 5] =
          this->C1[2][0] * vectorValue(i, 3, useVec)
          + this->C1[2][1] * vectorValue(i, 4, useVec)
          + this->C1[2][2] * vectorValue(i, 5, useVec);
    }
    c[18] = c[19] = c[20] = 0;
    for (int k = 0; k < 6; ++k) {
      c[18] +=
          this->C2[0][k] * vectorValue(0, k, useVec)
          + this->C3[0][k] * vectorValue(1, k, useVec);
      c[19] +=
          this->C3[1][k] * vectorValue(1, k, useVec)
          + this->C4[0][k] * vectorValue(2, k, useVec);
      c[20] +=
          this->C2[1][k] * vectorValue(0, k, useVec)
          + this->C4[1][k] * vectorValue(2, k, useVec);
    }
    for (int i = 3; i < 6; ++i)
      c[15 + i] += this->C5[i - 3] * vectorValue(i, 0, useVec);
    return c;
  }

  template<typename T, int sDim, int tDim>
  void NavierStokesElementT<T, sDim, tDim>::initCoefficientsB3_B4(bool useVec) {
    if (coeff0.size() == 15 && useVector == useVec) return;
    useVector = useVec;

    IAInterval sqrt2 = IAInterval::Sqrt2();
    IACoefficients c = femCoeff(useVec);
    const IACoefficients &tmp_x = B4_TMP_x(c, sqrt2);
    const IACoefficients &tmp_y = B4_TMP_y(c, sqrt2);
    const IACoefficients &tmp_xx = B3_TMP_xx(c, sqrt2);
    const IACoefficients &tmp_xy = B3_TMP_xy(c, sqrt2);
    const IACoefficients &tmp_yy = B3_TMP_yy(c, sqrt2);

    const TransformationT<T, sDim> &trafo = this->GetTransformation(0);
    for (int i = 0; i < tmp_x.size(); ++i) {
      coeff1.push_back(trafo[0][0] * tmp_x[i] + trafo[0][1] * tmp_y[i]);
      coeff0.push_back(-(trafo[1][0] * tmp_x[i] + trafo[1][1] * tmp_y[i]));
    }
    for (int i = 0; i < tmp_xx.size(); ++i) {
      coeff1_Dx.push_back(this->theta[0][0] * tmp_xx[i] + this->theta[0][1] * tmp_xy[i]
                          + this->theta[0][2] * tmp_yy[i]);
      coeff0_Dx.push_back(-(this->theta[1][0] * tmp_xx[i] + this->theta[1][1] * tmp_xy[i]
                            + this->theta[1][2] * tmp_yy[i]));
      coeff0_Dy.push_back(-(this->theta[2][0] * tmp_xx[i] + this->theta[2][1] * tmp_xy[i]
                            + this->theta[2][2] * tmp_yy[i]));
    }
  }

  template<typename T, int sDim, int tDim>
  void NavierStokesElementT<T, sDim, tDim>::evalRange(const IACoefficients &c,
                                                      IAInterval &range) {
    range = c[0];
    for (int j = 1; j < c.size(); ++j) {
      range = range | c[j];
    }
  }

  template<typename T, int sDim, int tDim>
  void NavierStokesElementT<T, sDim, tDim>::evalRange(const IACoefficients &c0,
                                                      const IACoefficients &c1,
                                                      IAInterval &range0, IAInterval &range1) {
    range0 = c0[0];
    range1 = c1[0];
    for (int j = 1; j < c0.size(); ++j) {
      range0 = range0 | c0[j];
      range1 = range1 | c1[j];
    }
  }

  template<typename T, int sDim, int tDim>
  void NavierStokesElementT<T, sDim, tDim>::evalRangeD(const IACoefficients &c0_Dx,
                                                       const IACoefficients &c0_Dy,
                                                       const IACoefficients &c1_Dx,
                                                       const IACoefficients &c0_Dy_1_Dx,
                                                       IAInterval &range0_Dx,
                                                       IAInterval &range0_Dy,
                                                       IAInterval &range1_Dx,
                                                       IAInterval &range0_Dy_1_Dx) {
    range0_Dx = c0_Dx[0];
    range0_Dy = c0_Dy[0];
    range1_Dx = c1_Dx[0];
    range0_Dy_1_Dx = c0_Dy_1_Dx[0];
    for (int j = 1; j < c0_Dx.size(); ++j) {
      range0_Dx = range0_Dx | c0_Dx[j];
      range0_Dy = range0_Dy | c0_Dy[j];
      range1_Dx = range1_Dx | c1_Dx[j];
      range0_Dy_1_Dx = range0_Dy_1_Dx | c0_Dy_1_Dx[j];
    }
  }

  template<typename T, int sDim, int tDim>
  void NavierStokesElementT<T, sDim, tDim>::evalRangeD(const IACoefficients &c0_Dx,
                                                       const IACoefficients &c0_Dy,
                                                       const IACoefficients &c1_Dx,
                                                       IAInterval &range0_Dx,
                                                       IAInterval &range0_Dy,
                                                       IAInterval &range1_Dx) {
    range0_Dx = c0_Dx[0];
    range0_Dy = c0_Dy[0];
    range1_Dx = c1_Dx[0];
    for (int j = 1; j < c0_Dx.size(); ++j) {
      range0_Dx = range0_Dx | c0_Dx[j];
      range0_Dy = range0_Dy | c0_Dy[j];
      range1_Dx = range1_Dx | c1_Dx[j];
    }
  }

  template<typename T, int sDim, int tDim>
  void NavierStokesElementT<T, sDim, tDim>::evalRangeD(const IACoefficients &c0_Dx,
                                                       const IACoefficients &c0_Dy_1_Dx,
                                                       IAInterval &range0_Dx,
                                                       IAInterval &range0_Dy_1_Dx) {
    range0_Dx = c0_Dx[0];
    range0_Dy_1_Dx = c0_Dy_1_Dx[0];
    for (int j = 1; j < c0_Dx.size(); ++j) {
      range0_Dx = range0_Dx | c0_Dx[j];
      range0_Dy_1_Dx = range0_Dy_1_Dx | c0_Dy_1_Dx[j];
    }
  }

  template
  class NavierStokesElementT<double, SpaceDimension, TimeDimension>;

  template
  class NavierStokesElementT<IAInterval, SpaceDimension, TimeDimension>;


  template<typename TT, int sDim, int tDim>
  TensorT<TT, sDim> NavierStokesElementDivergenceHomotopyT<TT, sDim, tDim>::W1(int q, const Vector &u) const {
    return TensorT<TT, sDim>(this->VelocityField(q, u, 0),
                             this->VelocityField(q, u, 1));
  }

  template<typename TT, int sDim, int tDim>
  VectorFieldT<TT, sDim> NavierStokesElementDivergenceHomotopyT<TT, sDim, tDim>::DivW1(int q, const Vector &u) const {
    return VectorFieldT<TT, sDim>(this->VelocityFieldDivergence(q, u, 0), this->VelocityFieldDivergence(q, u, 1));
  }

  template<typename TT, int sDim, int tDim>
  VectorFieldT<TT, sDim> NavierStokesElementDivergenceHomotopyT<TT, sDim, tDim>::W2(int q, const Vector &u) const {
    return VectorFieldT<TT, sDim>(this->Value(q, u, 1), this->Value(q, u, 2));
  }

  template<typename TT, int sDim, int tDim>
  TT NavierStokesElementDivergenceHomotopyT<TT, sDim, tDim>::W3(int q, const Vector &u) const {
    return this->Value(q, u, 0);
  }

  template<typename TT, int sDim, int tDim>
  VectorFieldT<TT, sDim> NavierStokesElementDivergenceHomotopyT<TT, sDim, tDim>::GradW3(int q, const Vector &u) const {
    return this->PressureGradient(q, u, 0);
  }

  template
  class NavierStokesElementDivergenceHomotopyT<>;

  template
  class NavierStokesElementDivergenceHomotopyT<IAInterval, SpaceDimension, TimeDimension>;

}//namespace navierstokes
