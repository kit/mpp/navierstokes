#include "NavierStokesCoarseGeometry.hpp"

namespace navierstokes {

  void addCells(CellIds &ids, int startIdx, int dimX, int dimY, int shiftRight = 0) {
    for (int x = 0; x < dimX - 1; ++x) {
      for (int y = 0; y < dimY - 1; ++y) {
        ids.emplace_back(CellId(QUADRILATERAL, 1, {startIdx + x * dimY + y,
                                                   startIdx + (x + 1) * dimY + y + shiftRight,
                                                   startIdx + (x + 1) * dimY + y + 1 + shiftRight,
                                                   startIdx + x * dimY + y + 1}));
      }
    }
  }

  CoarseGeometryData NavierStokesCoarseGeometryBottom45::createData(double radiusStrip,
                                                                    double heightStrip,
                                                                    double width,
                                                                    double radiusObstacle) {
    CoarseGeometryData data{};

    int dimX = (radiusStrip - radiusObstacle) / width + 1;
    int dimY = heightStrip / width + 1;
    double cornerX = radiusObstacle - width;
    int dimX1 = (2 * cornerX) / width + 1;
    int dimY1 = dimY - 1;

    Coordinates &coordinates = std::get<0>(data);
    //left part
    std::vector<double> xCoord = ft::linspace(-radiusStrip, -radiusObstacle, dimX);
    std::vector<double> yCoord = ft::linspace(0.0, heightStrip, dimY);
    for (int x = 0; x < xCoord.size(); ++x) {
      for (int y = 0; y < yCoord.size(); ++y) {
        coordinates.emplace_back(Point(xCoord[x], yCoord[y]));
      }
    }
    //center part
    std::vector<double> xCoord1 = ft::linspace(-cornerX, cornerX, dimX1);
    std::vector<double> yCoord1 = ft::linspace(width, heightStrip, dimY1);
    for (int x = 0; x < xCoord1.size(); ++x) {
      for (int y = 0; y < yCoord1.size(); ++y) {
        coordinates.emplace_back(Point(xCoord1[x], yCoord1[y]));
      }
    }
    //right part
    xCoord = ft::linspace(radiusObstacle, radiusStrip, dimX);
    for (int x = 0; x < xCoord.size(); ++x) {
      for (int y = 0; y < yCoord.size(); ++y) {
        coordinates.emplace_back(Point(xCoord[x], yCoord[y]));
      }
    }

    CellIds &c_ids = std::get<1>(data);
    addCells(c_ids, 0, dimX, dimY);
    int idx_tmp = (dimX - 1) * dimY;
    c_ids.emplace_back(CellId(TRIANGLE, 1, {idx_tmp, idx_tmp + dimY, idx_tmp + 1}));
    addCells(c_ids, idx_tmp + 1, 2, dimY1);
    addCells(c_ids, idx_tmp + dimY, dimX1, dimY1);
    idx_tmp += dimY + (dimX1 - 1) * dimY1;
    c_ids.emplace_back(CellId(TRIANGLE, 1, {idx_tmp, idx_tmp + dimY1, idx_tmp + dimY1 + 1}));
    addCells(c_ids, idx_tmp, 2, dimY1, 1);
    addCells(c_ids, idx_tmp + dimY1, dimX, dimY);

    FaceIds &f_ids = std::get<2>(data);
    idx_tmp = (dimX - 1) * dimY;
    f_ids.emplace_back(FaceId(OBSTACLE_BC, {idx_tmp, idx_tmp + dimY}));
    idx_tmp += dimY;
    for (int x = 0; x < dimX1 - 1; ++x) {
      f_ids.emplace_back(FaceId(OBSTACLE_BC, {idx_tmp + x * dimY1, idx_tmp + (x + 1) * dimY1}));
    }
    idx_tmp += (dimX1 - 1) * dimY1;
    f_ids.emplace_back(FaceId(OBSTACLE_BC, {idx_tmp, + idx_tmp + dimY1}));

    return data;
  }

  std::string NavierStokesCoarseGeometryBottom45::createName(double radiusStrip,
                                                             double radiusObstacle) {
    return "StripBottomObstacle45_" + std::to_string(radiusStrip) + "_"
           + std::to_string(radiusObstacle);
  }

  NavierStokesCoarseGeometryBottom45::NavierStokesCoarseGeometryBottom45(double radiusStrip,
                                                                         double heightStrip,
                                                                         double width,
                                                                         double radiusObstacle)
      : CoarseGeometry(createData(radiusStrip, heightStrip, width, radiusObstacle),
                       createName(radiusStrip, radiusObstacle)) {

  }

  CoarseGeometryData NavierStokesCoarseGeometryCenterSquare::createData(double radiusStrip,
                                                                        double heightStrip,
                                                                        double width) {
    CoarseGeometryData data{};

    int dimX = radiusStrip / width;
    int dimY = heightStrip / width + 1;
    int dimY1 = (dimY - 1) / 2;

    Coordinates &coordinates = std::get<0>(data);
    //left part
    std::vector<double> xCoord = ft::linspace(-radiusStrip, -width, dimX);
    std::vector<double> yCoord = ft::linspace(0.0, heightStrip, dimY);
    for (int x = 0; x < xCoord.size(); ++x) {
      for (int y = 0; y < yCoord.size(); ++y) {
        coordinates.emplace_back(Point(xCoord[x], yCoord[y]));
      }
    }
    //center part
    for (int y = 0; y < yCoord.size(); ++y) {
      if (yCoord[y] == 0.5) continue;
      coordinates.emplace_back(Point(0.0, yCoord[y]));
    }
    //right part
    xCoord = ft::linspace(width, radiusStrip, dimX);
    for (int x = 0; x < xCoord.size(); ++x) {
      for (int y = 0; y < yCoord.size(); ++y) {
        coordinates.emplace_back(Point(xCoord[x], yCoord[y]));
      }
    }


    CellIds &c_ids = std::get<1>(data);
    addCells(c_ids, 0, dimX, dimY);
    int idx_tmp = (dimX - 1) * dimY;
    addCells(c_ids, idx_tmp, 2, dimY1, dimY - dimY1);
    addCells(c_ids, idx_tmp + dimY1 + 1, 2, dimY1, 2);
    idx_tmp += dimY;
    addCells(c_ids, idx_tmp, 2, dimY1, dimY - dimY1 - 1);
    addCells(c_ids, idx_tmp + dimY1, 2, dimY1, 3);
    idx_tmp += dimY - 1;
    addCells(c_ids, idx_tmp, dimX, dimY);

    FaceIds &f_ids = std::get<2>(data);
    idx_tmp = (dimX - 1) * dimY + dimY1 - 1;
    f_ids.emplace_back(FaceId(OBSTACLE_BC, {idx_tmp, idx_tmp + 1}));
    f_ids.emplace_back(FaceId(OBSTACLE_BC, {idx_tmp + 1, idx_tmp + 2}));
    f_ids.emplace_back(FaceId(OBSTACLE_BC, {idx_tmp + 2, idx_tmp + dimY + 1}));
    f_ids.emplace_back(FaceId(OBSTACLE_BC, {idx_tmp + dimY + 1, idx_tmp + 2 * dimY + 1}));
    f_ids.emplace_back(FaceId(OBSTACLE_BC, {idx_tmp + 2 * dimY + 1, idx_tmp + 2 * dimY}));
    f_ids.emplace_back(FaceId(OBSTACLE_BC, {idx_tmp + 2 * dimY, idx_tmp + 2 * dimY - 1}));
    f_ids.emplace_back(FaceId(OBSTACLE_BC, {idx_tmp + 2 * dimY - 1, idx_tmp + dimY}));
    f_ids.emplace_back(FaceId(OBSTACLE_BC, {idx_tmp + dimY, idx_tmp}));

    return data;
  }

  std::string NavierStokesCoarseGeometryCenterSquare::createName(double radiusStrip) {
    return "StripCenterObstacleSquare_" + std::to_string(radiusStrip);
  }

  NavierStokesCoarseGeometryCenterSquare::NavierStokesCoarseGeometryCenterSquare(double radiusStrip,
                                                                                 double heightStrip,
                                                                                 double width)
      : CoarseGeometry(createData(radiusStrip, heightStrip, width),
                       createName(radiusStrip)) {

  }

} // namespace navierstokes