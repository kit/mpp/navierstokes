#ifndef NAVIERSTOKESCOARSEGEOMETRY_HPP
#define NAVIERSTOKESCOARSEGEOMETRY_HPP

#include "CoarseGeometry.hpp"
#include "NavierStokesAssemble.hpp"

namespace navierstokes {

  class NavierStokesCoarseGeometryBottom45 : public CoarseGeometry {
    CoarseGeometryData createData(double radiusStrip, double heightStrip, double width,
                                  double radiusObstacle);

    std::string createName(double radiusStrip, double radiusObstacle);

  public:
    NavierStokesCoarseGeometryBottom45(double radiusStrip, double heightStrip, double width,
                                       double radiusObstacle);
  };

  class NavierStokesCoarseGeometryCenterSquare : public CoarseGeometry {
    CoarseGeometryData createData(double radiusStrip, double heightStrip, double width);

    std::string createName(double radiusStrip);

  public:
    NavierStokesCoarseGeometryCenterSquare(double radiusStrip, double heightStrip, double width);
  };

} // namespace navierstokes

#endif //NAVIERSTOKESCOARSEGEOMETRY_HPP
