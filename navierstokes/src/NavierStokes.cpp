#include "NavierStokes.hpp"
#include "problem/NavierStokesProblem.hpp"

#include "NavierStokesApproximation.hpp"
#include "NavierStokesDefect.hpp"
#include "NavierStokesNormbound.hpp"
#include "NavierStokesSurjectivity.hpp"

#include "IAIO.hpp"

namespace navierstokes {

  namespace proof {

    void Velocity(NavierStokesProblem &problem) {
      if (!problem.RunPart(PartProofVelocity)) return;
      NavierStokesConstants &constants = problem.Constants();

      constants.ConstantThm() = 4 * sqr(IAInterval(constants.K<false>())) * problem.C4sqr<IAInterval>()
                       * IAInterval(constants.Delta()) * problem.Reynolds<IAInterval>();

      if (sup(constants.ConstantThm()) >= 1.0) {
        mout << "4 K^2 C_4^2 delta Re= " << constants.ConstantThm() << endl;
        ERROR("Cannot verify inequality!");
      }

      if (constants.K<true>() <= 0.0) {
        ERROR("Cannot prove surjectivity of L");
      }
      constants.Alpha() = sup(2 * IAInterval(constants.K<false>()) * constants.Delta() / (1 + sqrt(1 - constants.ConstantThm())));

      constants.AlphaUnique() = inf(2 * IAInterval(constants.K<false>()) * constants.Delta() / (1 - sqrt(1 - constants.ConstantThm())));

      int length = 91;
      int reynoldsInterval = problem.IsHomotopy() ? 2 : 999;
      mout.PrintEntries("Proof of velocity successful!", 2,
//                        PrintInfoSeparator(length, 2),
                        PrintInfoEntry("Re", problem.Re(), 2),
                        PrintInfoEntry("Re (interval)", problem.Reynolds<IAInterval>(), reynoldsInterval),
                        PrintInfoEntry("sigma", problem.Sigma(), 2),
//                        PrintInfoSeparator(length, 2),
                        PrintInfoEntry("||u*-w~||_H1 <= alpha", OutputUp(constants.Alpha()), 2),
                        PrintInfoEntry("||u*-w~||_L2", OutputUp(
                            sup(constants.Alpha() * problem.C2<IAInterval>())), 2),
                        PrintInfoEntry("||u*-w~||_L4", OutputUp(
                            sup(constants.Alpha() * problem.C4<IAInterval>())), 2),
//                        PrintInfoSeparator(length, 2),
                        PrintInfoEntry("Radius of uniqueness", OutputDown(constants.AlphaUnique()), 2),
//                        PrintInfoSeparator(length, 2),
                        PrintInfoEntry("delta", OutputUp(constants.Delta()), 2),
                        PrintInfoEntry("K", OutputUp(constants.K<false>()), 2),
                        PrintInfoEntry("K*", OutputUp(constants.K<true>()), 2),
                        PrintInfoEntry("4 K^2 C_4^2 Re delta", constants.ConstantThm(), 2)
//                        PrintInfoSeparator(length, 2)
      );

      problem.SaveData(PartProofVelocity);
    }

    void Final(NavierStokesProblem &problem) {
      if (!problem.RunPart(PartLast)) return;
      NavierStokesConstants &constants = problem.Constants();

      int verbose = 2;

      int length = 91;
      int homotopyVerbose = problem.IsHomotopy() ? 2 : 999;
      int simpleHomotopyVerbose = problem.NormboundStrategy() == NB_PlumSimple ? 2 : 999;
      int extendedHomotopyVerbose = problem.NormboundStrategy() == NB_PlumExtended ? 2 : 999;
      int reynoldsInterval = inf(problem.Reynolds<IAInterval>()) != sup(problem.Reynolds<IAInterval>()) ? 2 : 999;
      mout.PrintEntries("Proof successful!", 2,
//                        PrintInfoSeparator(length, 2),
                        PrintInfoEntry("Meshname", problem.MeshesName(), 2),
                        PrintInfoEntry("Verified", problem.Verified() ? "Yes" : "No", 2),
                        PrintInfoEntry("Normbound Strategy", problem.NormboundStrategyName(),
                                       2),
//                        PrintInfoSeparator(length, 2),
                        PrintInfoEntry("Re", problem.Re(), 2),
                        PrintInfoEntry("Re (interval)", problem.Reynolds<IAInterval>(), reynoldsInterval),
                        PrintInfoEntry("sigma", problem.Sigma(), 2),
//                        PrintInfoSeparator(length, 2),
                        PrintInfoEntry("||u*-w~||_H1 <= alpha <=", OutputUp(constants.Alpha()), 2),
                        PrintInfoEntry("||u*-w~||_L2 <=", OutputUp(
                            sup(constants.Alpha() * problem.C2<IAInterval>())), 2),
                        PrintInfoEntry("||u*-w~||_L4 <=", OutputUp(
                            sup(constants.Alpha() * problem.C4<IAInterval>())), 2),
                        PrintInfoEntry("||Dp*-Dp~||_H-1 <= alpha_p <=", OutputUp(constants.AlphaPressure()), 2),
//                        PrintInfoSeparator(length, 2),
                        PrintInfoEntry("Radius of uniqueness", OutputDown(constants.AlphaUnique()), 2),
//                        PrintInfoSeparator(length, 2),
                        PrintInfoEntry("delta <=", OutputUp(constants.Delta()), 2),
                        PrintInfoEntry("K <=", OutputUp(constants.K<false>()), 2),
                        PrintInfoEntry("K* <=", OutputUp(constants.K<true>()), 2),
                        PrintInfoEntry("4 K^2 C_4^2 Re delta", constants.ConstantThm(), 2),
//                        PrintInfoSeparator(length, 2),
                        PrintInfoEntry("R", problem.getApproximationDomainX(), 2),
                        PrintInfoEntry("R2", problem.getEigenvalueDomainX(), 2),
//                        PrintInfoSeparator(length, 2),
                        PrintInfoEntry("C2", problem.C2<IAInterval>(), 2),
                        PrintInfoEntry("C4", problem.C4<IAInterval>(), 2),
//                        PrintInfoSeparator(length, 2),
                        PrintInfoEntry("kappa >=", OutputDown(constants.Kappa()), extendedHomotopyVerbose),
                        PrintInfoEntry("tau <=", OutputUp(constants.Tau()), simpleHomotopyVerbose),
                        PrintInfoEntry("gamma0 >=", OutputDown(constants.Gamma0()), extendedHomotopyVerbose),
                        PrintInfoEntry("gamma1", constants.Gamma1<false>(), homotopyVerbose),
                        PrintInfoEntry("gamma2", constants.Gamma2<false>(), homotopyVerbose),
                        PrintInfoEntry("gamma1*", constants.Gamma1<true>(), homotopyVerbose),
                        PrintInfoEntry("gamma2*", constants.Gamma2<true>(), homotopyVerbose)
//                        PrintInfoSeparator(length, homotopyVerbose)
      );

      problem.SaveData(PartLast);
    }

  } // namespace proof

  template<bool VERIFIED>
  void run(NavierStokesProblem &problem) {
    approximation::VelocityNorms<VERIFIED>(problem);
    approximation::ApprVNorms<VERIFIED>(problem);
    defectbound::VelocityDefect<VERIFIED>(problem);
    normbound::Normbound<VERIFIED>(problem);
    surjectivity::Surjectivity<VERIFIED>(problem);

    proof::Velocity(problem);

    defectbound::PressureDefect<VERIFIED>(problem);

    proof::Final(problem);
  }
  
  void LatexOutput(NavierStokesProblem &problem) {
    NavierStokesConstants &constants = problem.Constants();

    int numOfDigits = 5;
    Config::Get("LatexDigits", numOfDigits);

    int homotopyVerbose = problem.IsHomotopy() ? 2 : 999;
    int simpleHomotopyVerbose = problem.NormboundStrategy() == NB_PlumSimple ? 2 : 999;
    int extendedHomotopyVerbose = problem.NormboundStrategy() == NB_PlumExtended ? 2 : 999;
    mout.PrintEntries("Latex output", 2,
                      PrintInfoEntry("||u*-w~||_H1 <= alpha", LatexOutputUp(constants.Alpha(), numOfDigits), 2),
                      PrintInfoEntry("||u*-w~||_L2", LatexOutputUp(sup(constants.Alpha() * problem.C2<IAInterval>()), numOfDigits), 2),
                      PrintInfoEntry("||u*-w~||_L4", LatexOutputUp(sup(constants.Alpha() * problem.C4<IAInterval>()), numOfDigits), 2),
                      PrintInfoEntry("Radius of uniqueness", LatexOutputDown(constants.AlphaUnique(), numOfDigits), 2),
                      PrintInfoEntry("delta", LatexOutputUp(constants.Delta(), numOfDigits), 2),
                      PrintInfoEntry("K", LatexOutputUp(constants.K<false>(), numOfDigits), 2),
                      PrintInfoEntry("K*", LatexOutputUp(constants.K<true>(), numOfDigits), 2),
                      PrintInfoEntry("kappa", LatexOutputDown(constants.Kappa(), numOfDigits), extendedHomotopyVerbose),
                      PrintInfoEntry("tau", LatexOutputUp(constants.Tau(), numOfDigits), simpleHomotopyVerbose),
                      PrintInfoEntry("gamma0", LatexOutputDown(constants.Gamma0(), numOfDigits), extendedHomotopyVerbose),
                      PrintInfoEntry("gamma1", LatexOutput(constants.Gamma1<false>(), numOfDigits), homotopyVerbose),
                      PrintInfoEntry("gamma2", LatexOutput(constants.Gamma2<false>(), numOfDigits), homotopyVerbose),
                      PrintInfoEntry("gamma1*", LatexOutput(constants.Gamma1<true>(), numOfDigits), homotopyVerbose),
                      PrintInfoEntry("gamma2*", LatexOutput(constants.Gamma2<true>(), numOfDigits), homotopyVerbose),
                      PrintInfoEntry("||Dp*-Dp~||_H-1 <= alpha_p", LatexOutputUp(constants.AlphaPressure(), numOfDigits), 2)
                      );


    if (problem.ContinuousReynolds()) {
      mout << "[" << LatexOutputDown(inf(problem.Reynolds<IAInterval>()), numOfDigits)
           << ",\\, " << LatexOutputUp(sup(problem.Reynolds<IAInterval>()), numOfDigits) << "] & "
           << LatexOutputUp(constants.Delta(), numOfDigits) << " & "
           << LatexOutputUp(constants.K<false>(), numOfDigits) << " & "
           << LatexOutput(constants.ConstantThm(), numOfDigits) << " & "
           << LatexOutputUp(constants.Alpha(), numOfDigits) << " & "
           << LatexOutputUp(sup(constants.Alpha() * problem.C2<IAInterval>()), numOfDigits) << " & "
           << LatexOutputUp(sup(constants.Alpha() * problem.C4<IAInterval>()), numOfDigits) << " & "
           << LatexOutputUp(constants.AlphaPressure(), numOfDigits) << " \\tabularnewline[2mm]"
           << endl
           << endl;
    } else {
      std::string approach = "Approach 1";
      if (problem.NormboundStrategy() == NB_PlumSimple)
        approach = "Approach 2 (a)";
      if (problem.NormboundStrategy() == NB_PlumExtended)
        approach = "Approach 2 (b)";
      mout << LatexOutputUp(problem.Re(), 1) << "  & "
           << LatexOutputUp(problem.Sigma(), 2) << " & "
           << LatexOutputUp(constants.Delta(), numOfDigits) << " & "
           << LatexOutput(constants.ConstantThm(), numOfDigits) << " & "
           << LatexOutputUp(constants.Alpha(), numOfDigits) << " & "
           << LatexOutputUp(sup(constants.Alpha() * problem.C2<IAInterval>()), numOfDigits) << " & "
           << LatexOutputUp(constants.AlphaPressure(), numOfDigits) << " & "
           << approach << " \\tabularnewline[0.5mm]"
           << endl
           << "         &          & "
           << LatexOutputUp(constants.K<false>(), numOfDigits) << " & "
           << "                      & "
           << LatexOutputDown(constants.AlphaUnique(), numOfDigits) << " & "
           << LatexOutputUp(sup(constants.Alpha() * problem.C4<IAInterval>()), numOfDigits) << " & "
           << "               & "
           << std::string(approach.size(), ' ') << " \\tabularnewline[2mm]"
           << endl
           << endl;
    }
  }

  void run() {
    std::unique_ptr<NavierStokesProblem> problem = CreateProblem();
    problem->PrintInfo();

    bool plotCoarseMesh = false;
    double length = -1.0;
    Config::Get("PlotCoarseMesh", plotCoarseMesh);
    Config::Get("PlotCoarseMeshStrip", length);
    problem->SET_RAPPR(length);
    if (plotCoarseMesh) {
      approximation::PlotCoarseMesh(*problem, length >= 0);
      return;
    }

    approximation::Plot(*problem);
    if (problem->Verified())
      run<true>(*problem);
    else
      run<false>(*problem);

    LatexOutput(*problem);
  }

} // namespace navierstokes
