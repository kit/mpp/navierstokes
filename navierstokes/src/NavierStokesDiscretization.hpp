#ifndef NAVIERSTOKESDISCRETIZATION_HPP
#define NAVIERSTOKESDISCRETIZATION_HPP

#include "RTLagrangeDiscretization.hpp"

template<typename T = double, int sDim = SpaceDimension, int tDim = TimeDimension>
class NavierStokesDiscDivergenceHomotopyT : public RTLagrangeDiscretizationT<T, sDim, tDim> {
public:
  NavierStokesDiscDivergenceHomotopyT(const Meshes &meshes, int rt_order, int lagrange_degree,
                                      int lagrange_size = 1)
      : RTLagrangeDiscretizationT<T, sDim, tDim>(meshes, rt_order, lagrange_degree, 2, lagrange_size) {}

  NavierStokesDiscDivergenceHomotopyT(const NonAdaptiveIDiscretizationT<T, sDim, tDim> &disc,
                                      int rt_order, int lagrange_degree, int lagrange_size = 1)
      : RTLagrangeDiscretizationT<T, sDim, tDim>(disc, rt_order, lagrange_degree, 2, lagrange_size) {}
};

using NavierStokesDiscDivergenceHomotopy = NavierStokesDiscDivergenceHomotopyT<>;

using IANavierStokesDiscDivergenceHomotopy = NavierStokesDiscDivergenceHomotopyT<IAInterval, SpaceDimension, TimeDimension>;

#endif // NAVIERSTOKESDISCRETIZATION_HPP
