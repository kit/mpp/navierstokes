#ifndef NAVIERSTOKESELEMENT_H
#define NAVIERSTOKESELEMENT_H

#include "DivergenceFreeElement.hpp"
#include "RTLagrangeElement.hpp"

#include "problem/NavierStokesProblem.hpp"

namespace navierstokes {


  template<typename T = double, int sDim = SpaceDimension, int tDim = TimeDimension>
  class NavierStokesElementT : public DivergenceFreeElementT<T, sDim, tDim> {
    const NavierStokesProblem &problem;
    const Vector &velocityVec;
    const Vector &vec;

    bool useVector = false;

    //coefficients of velocity in Bernstein basis
    IACoefficients coeff0{};
    IACoefficients coeff1{};
    //coefficients of velocity gradient in Bernstein basis
    IACoefficients coeff0_Dx{};
    IACoefficients coeff0_Dy{};
    IACoefficients coeff1_Dx{};
  public:
    template<bool coarse>
    static std::unique_ptr<NavierStokesElementT<T, sDim, tDim>> Create(const NavierStokesProblem &problem,
                                                                       const Cell &c) {
      NavierStokesElementT<T, sDim, tDim> *element
          = new NavierStokesElementT<T, sDim, tDim>(problem, problem.Velocity<coarse>(),
                                                    problem.Velocity<coarse>(), c);
      return std::unique_ptr<NavierStokesElementT<T, sDim, tDim>>(element);
    }

    template<bool coarse>
    static std::unique_ptr<NavierStokesElementT<T, sDim, tDim>> Create(const NavierStokesProblem &problem,
                                                                       const Vector &vec,
                                                                       const Cell &c) {
      NavierStokesElementT<T, sDim, tDim> *element
        = new NavierStokesElementT<T, sDim, tDim>(problem, vec, problem.Velocity<coarse>(), c);
      return std::unique_ptr<NavierStokesElementT<T, sDim, tDim>>(element);
    }

  protected:
    NavierStokesElementT(const NavierStokesProblem &problem, const Vector &vec,
                         const Vector &velocityVec, const Cell &c)
        : DivergenceFreeElementT<T, sDim, tDim>(vec, c),
          problem(problem), vec(vec), velocityVec(velocityVec) {}
  public:
    ~NavierStokesElementT();

    /// Encloses the range of the finite element solution
    void Range(IAInterval &R0, IAInterval &R1);

    /// Encloses the range of the derivative of the finite element solution
    void RangeD(IAInterval &R_Dx_0, IAInterval &R_Dy_0, IAInterval &R_Dx_1);

    void Ranges(IAInterval &W0, IAInterval &W1, IAInterval &UOmega0, IAInterval &UOmega1,
                IAInterval &Dx_UOmega0, IAInterval &Dy_UOmega0, IAInterval &Dx_UOmega1);

    void Ranges(IAInterval &UOmega0, IAInterval &UOmega1,
                IAInterval &Dx_UOmega0, IAInterval &Dy_UOmega0, IAInterval &Dx_UOmega1);

    void RangesPlumSimple(IAInterval &Dx_0, IAInterval &Dy_0_Dx_1);

    void RangesPlumExtended(IAInterval &Omega0, IAInterval &Omega1,
                            IAInterval &Dx_Omega0, IAInterval &Dy_Omega0, IAInterval &Dx_Omega1,
                            IAInterval &Dy_0_Dx_1);

    void RangesDelta(IAInterval &Dx_UomegaTVT0, IAInterval &Dy_UomegaTVT0, IAInterval &Dx_UomegaTVT1);

  private:
    T vectorValue(int i, int k, bool useVec);

    IACoefficients femCoeff(bool useVec);

    void initCoefficientsB3_B4(bool useVec);

    void evalRange(const IACoefficients &c, IAInterval &range);

    void evalRange(const IACoefficients &c0, const IACoefficients &c1,
                   IAInterval &range0, IAInterval &range1);

    void evalRangeD(const IACoefficients &c0_Dx, const IACoefficients &c0_Dy,
                    const IACoefficients &c1_Dx,
                    IAInterval &range0_Dx, IAInterval &range0_Dy,
                    IAInterval &range1_Dx);

    void evalRangeD(const IACoefficients &c0_Dx, const IACoefficients &c0_Dy,
                    const IACoefficients &c1_Dx, const IACoefficients &c0_Dy_1_Dx,
                    IAInterval &range0_Dx, IAInterval &range0_Dy,
                    IAInterval &range1_Dx, IAInterval &range0_Dy_1_Dx);

    void evalRangeD(const IACoefficients &c0_Dx, const IACoefficients &c0_Dy_1_Dx,
                    IAInterval &range0_Dx, IAInterval &range0_Dy_1_Dx);
  };

  typedef NavierStokesElementT<> NavierStokesElement;

  typedef NavierStokesElementT<IAInterval, SpaceDimension, TimeDimension>
      IANavierStokesElement;

  template<typename TT=double, int sDim = SpaceDimension, int tDim = TimeDimension>
  class NavierStokesElementDivergenceHomotopyT : public RTLagrangeElementT<TT, sDim, tDim> {
  public:
    NavierStokesElementDivergenceHomotopyT(const VectorMatrixBase &base, const Cell &c)
      : RTLagrangeElementT<TT, sDim, tDim>(base, c) {}

    const VectorFieldT<TT, sDim> &RTVectorValue(int q, int i, int k) const {
      return this->VelocityField(q, i, k);
    }

    const VectorFieldT<TT, sDim> &RTVectorValue(int q, const ShapeId &iter) const {
      return this->VelocityField(q, iter);
    }

    TT RTDivergence(int q, int i, int k) const {
      return this->VelocityFieldDivergence(q, i, k);
    }

    TT RTDivergence(int q, const ShapeId &iter) const {
      return this->VelocityFieldDivergence(q, iter);
    }

    TT LagrangeValue(int q, int i) const {
      return this->Value(q, i);
    }

    TT LagrangeValue(int q, const ShapeId &iter) const {
      return this->Value(q, iter);
    }

    const VectorFieldT<TT, sDim> &LagrangeDerivative(int q, int i) const {
      return this->PressureGradient(q, i);
    }

    const VectorFieldT<TT, sDim> &LagrangeDerivative(int q, const ShapeId &iter) const {
      return this->PressureGradient(q, iter);
    }

    TensorT<TT, sDim> W1(int q, const Vector &u) const;

    VectorFieldT<TT, sDim> DivW1(int q, const Vector &u) const;

    VectorFieldT<TT, sDim> W2(int q, const Vector &u) const;

    TT W3(int q, const Vector &u) const;

    VectorFieldT<TT, sDim> GradW3(int q, const Vector &u) const;

    constexpr int RTIndex() const { return this->VelocityIndex(); }

    int RTSize() const { return this->VelocitySize(); }

    constexpr int LagrangeIndex() const { return this->PressureIndex(); }

    int LagrangeSize() const { return this->PressureSize(); }

    int RTMaxk(int i) const { return this->VelocityMaxk(i); }

    int IndexingRT_k(int i, int k, int m) const { return this->IndexingVelocity_k(i, k, m); }
  };

  using NavierStokesElementDivergenceHomotopy = NavierStokesElementDivergenceHomotopyT<>;

  using IANavierStokesElementDivergenceHomotopy = NavierStokesElementDivergenceHomotopyT<IAInterval, SpaceDimension, TimeDimension>;
} // namespace navierstokes

#endif // NAVIERSTOKESELEMENT_H
