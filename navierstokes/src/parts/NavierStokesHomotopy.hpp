#ifndef NAVIERSTOKESHOMOTOPY_HPP
#define NAVIERSTOKESHOMOTOPY_HPP

#include "problem/NavierStokesProblem.hpp"

#include "IAEigenvalueMethods.hpp"
#include "IAIO.hpp"

namespace navierstokes {

  IAEigenvalueEnclosures BaseProblem(int n_max, int R, double sigma);

  template<bool VERIFIED, bool adjoint>
  class NavierStokesHomotopy {
    NavierStokesProblem &problem;
    IAInterval &gamma1;
    IAInterval &gamma2;
    double &K;
    double rho = -1.0;
    int numEV = -1;

    std::string partName(std::string part);

    bool baseproblem();

    void constraintHomotopy();

    void domainHomotopy();

    void coefficientHomotopy();

  public:
    NavierStokesHomotopy(NavierStokesProblem &problem)
      : problem(problem),
        gamma1(problem.Constants().Gamma1<adjoint>()),
        gamma2(problem.Constants().Gamma2<adjoint>()),
        K(problem.Constants().K<adjoint>()) {}

    void start(double rho0);
  };

} // namespace navierstokes

#endif //NAVIERSTOKESHOMOTOPY_HPP
