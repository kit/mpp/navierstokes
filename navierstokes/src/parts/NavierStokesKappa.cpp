#include "NavierStokesKappa.hpp"

#include "IASpectrum.hpp"
#include "LagrangeDiscretization.hpp"
#include "VectorFieldElement.hpp"
#include "EigenSolverCreator.hpp"
#include "MeshesCreator.hpp"


namespace navierstokes {

  namespace kappa {

    void assembleMatrix(NavierStokesProblem &problem, IASymRMatrix &A,
                        const IAInterval &xiSqr) {
      int N = A.Dim();
      for (int i = 1; i <= N; ++i) {
        IAInterval C_i = (i * i) * IAInterval::PiSqr();
        IAInterval sqrtC_i = sqrt(C_i);

        A(i - 1, i - 1) = (xiSqr - problem.Sigma() + C_i)
                          / (xiSqr + problem.Sigma() + C_i);

        for (int j = 1; j < i; ++j) {
          if ((i + j) % 2 == 0) continue;
          IAInterval sqrtC_j = sqrt((j * j) * IAInterval::PiSqr());
          A(i - 1, j - 1) = 16 * i * j * IAInterval(problem.Re()) / IAInterval::PiSqr()
                            / sqr(IAInterval(i * i) - (j * j)) / sqrtC_i / sqrtC_j;
        }
      }
    }

    template<bool unbounded>
    double theta0(NavierStokesProblem &problem, const IAInterval &xiSqr, int N);

    template<>
    double theta0<true>(NavierStokesProblem &problem, const IAInterval &xiSqr, int N) {
      IASymRMatrix A(N);
      assembleMatrix(problem, A, xiSqr);

      IAEigenvalueEnclosures Lambda;
      verifiedEVreal(A, Lambda);

      return inf(Lambda[0]);
    }

    template<>
    double theta0<false>(NavierStokesProblem &problem, const IAInterval &xiSqr_0, int N) {
      IAInterval X = xiSqr_0 + problem.Sigma();
      double t = infty;
      for (int k = 1; k <= N; ++k) {
        IAInterval sum_k{};
        IAInterval kk(k * k);
        for (int l = 1; l <= N; ++l) {
          if ((k + l) % 2 == 0) continue;
          IAInterval ll(l * l);
          sum_k += 1 / sqr(kk - ll) / sqrt(IAInterval::PiSqr() + X / kk) /
                   sqrt(IAInterval::PiSqr() + X / ll);
        }
        t = std::min(t,
                     inf(1 - 2 * IAInterval(problem.Sigma() / (X + kk * IAInterval::PiSqr()))
                         - 16 * IAInterval(problem.Re()) / IAInterval::PiSqr() * sum_k));
      }
      return t;
    }

    double theta1(NavierStokesProblem &problem, const IAInterval &xiSqr, int N) {
      if (problem.Sigma() == 0)
        return 1.0;
      return inf(1.0 - 2 * IAInterval(problem.Sigma())
                       /
                       (xiSqr + problem.Sigma() + ((N + 1) * (N + 1)) * IAInterval::PiSqr()));
    }

    double theta2(NavierStokesProblem &problem, const IAInterval &xiSqr, int N) {
      IAInterval X = xiSqr + problem.Sigma();
      IAInterval T{};
      for (int k = 1; k <= N; ++k) {
        int kk = k * k;


        IAInterval sum_l{};
        for (int l = 1; l <= N; ++l) {
          if ((k + l) % 2 == 0) continue;
          int ll = l * l;
          sum_l += (2 * ll) / pow(IAInterval(kk) - ll, 4);
        }

        T += (1 / IAInterval(3) / kk - 2 / IAInterval::PiSqr()
                                       *
                                       (1 / IAInterval(kk * kk) + 64 / IAInterval::PiSqr() * sum_l))
             / (IAInterval::PiSqr() + X / kk);
      }
      int n = (N + 1) * (N + 1);
      return sup(sqr(IAInterval(problem.Re())) / (X + n * IAInterval::PiSqr()) * T);
    }

    double theta3(NavierStokesProblem &problem, const IAInterval &xiSqr, int N) {
      IAInterval sum_k2{};
      IAInterval sum_k4{};
      IAInterval sum_k6{};
      IAInterval sum_kl{};
      for (int k = 1; k <= N; ++k) {
        int kk = k * k;
        sum_k2 += 1 / IAInterval(kk);
        sum_k4 += 1 / IAInterval(kk * kk);
        if (k % 2 == 1)
          sum_k6 += 2 / IAInterval(kk * kk * kk);

        IAInterval sum_l{};
        for (int l = 1; l <= N; ++l) {
          if ((k + l) % 2 == 0) continue;
          sum_l += 1 / pow(IAInterval(kk) - IAInterval(l * l), 4);
        }
        sum_kl += (2 * kk) * sum_l;
      }
      return sup(sqr(IAInterval(problem.Re())) /
                 (xiSqr + problem.Sigma() + ((N + 1) * (N + 1)) * IAInterval::PiSqr())
                 * (1 / IAInterval(30) - 2 / IAInterval::PiSqr()
                                         * (sum_k2 / 3 + 4 / IAInterval::PiSqr()
                                                         * (sum_k4 - 8 / IAInterval::PiSqr()
                                                                     * (sum_k6 + 2 * sum_kl)))));
    }

    template<bool bounded>
    static double theta(NavierStokesProblem &problem, const IAInterval &xiSqr, int N) {
      IAInterval t0(theta0<bounded>(problem, xiSqr, N));
      IAInterval t1(theta1(problem, xiSqr, N));
      IAInterval t2(theta2(problem, xiSqr, N));
      IAInterval t3(theta3(problem, xiSqr, N));
      return inf((t0 + t1 - sqrt(t3)) / 2 - sqrt(t2 + sqr(t1 - t0 - sqrt(t3)) / 4));
    }

    double ComputeKappa(NavierStokesProblem &problem) {
      /// considering only positive intervals of xi is sufficient since all terms are symmetric in xi
      double xi_0 = 4.0;
      int N = 8;
      double stepsize = 0.0005;

      /// compute the range of xi on each proc
      double xiMinOnProc = (double(PPM->proc()) / PPM->size()) * xi_0;
      double xiMaxOnProc = (double(PPM->proc() + 1) / PPM->size()) * xi_0;
      if (PPM->proc() == PPM->size() - 1)
        xiMaxOnProc = xi_0;

      /// compute kappa for the complement of [-xi_0, xi_0] (unbounded part) first
      double kappa = theta<false>(problem, sqr(IAInterval(xi_0)), N);

      double x = xiMinOnProc;
      double X = std::min(x + stepsize, xi_0);

      do {
        kappa = std::min(kappa, theta<true>(problem, sqr(IAInterval(x, X)), N));
        x = X;
        X = std::min(x + stepsize, xi_0);
      } while (x < xiMaxOnProc);

      return PPM->Min(kappa);
    }

    void Dirichlet(Vectors &U) {
      for (int n = 0; n < U.size(); ++n) {
        U[n] = 0.0;
        U[n].ClearDirichletFlags();
        for (cell c = U[n].cells(); c != U[n].cells_end(); ++c) {
          const Cell &C = *c;
          RowBndValues u_c(U[n], C);
          if (!u_c.onBnd()) continue;
          for (int f = 0; f < c.Faces(); ++f) {
            switch (u_c.bc(f)) {
              case 1:
                for (int i = 0; i < U[n].NumberOfNodalPointsOnFace(C, f); ++i) {
                  int j = U[n].IdOfNodalPointOnFace(C, f, i);
                  u_c(j, 0) = 0.0;
                  u_c.D(j, 0) = true;
                  u_c(j, 1) = 0.0;
                  u_c.D(j, 1) = true;
                }
                break;
              case -1:
                break;
              default:
                THROW("Boundary condition not implemented;\n");
            }
          }
        }
        U[n].DirichletConsistent();
      }
    }

    void Matrices(NavierStokesProblem &problem, Matrix &A, Matrix &B) {
      A = 0;
      B = 0;
      for (cell c = A.cells(); c != A.cells_end(); ++c) {
        const Cell &C = *c;
        VectorFieldElement E(A, C);
        RowEntries A_c(A, E);
        RowEntries B_c(B, E);
        for (int q = 0; q < E.nQ(); ++q) {
          Tensor G_U_q = problem.G_U(E.QPoint(q));
          for (int i = 0; i < E.size(); ++i) {
            for (int k = 0; k < 2; ++k) {
              VectorFieldComponent v_ik = E.VectorComponentValue(q, i, k);
              TensorRow Dv_ik = E.VectorRowGradient(q, i, k);
              for (int j = 0; j < E.size(); ++j) {
                for (int l = 0; l < 2; ++l) {
                  VectorFieldComponent v_jl = E.VectorComponentValue(q, j, l);
                  TensorRow Dv_jl = E.VectorRowGradient(q, j, l);
                  double ip = (Frobenius(Dv_ik, Dv_jl) + problem.Sigma() * (v_ik * v_jl));

                  A_c(i, j, k, l) += E.QWeight(q) * (ip + (v_ik * (G_U_q * v_jl)));
                  B_c(i, j, k, l) += E.QWeight(q) * ip;
                }
              }
            }
          }
        }
      }
      A.ClearDirichletValues();
      B.ClearDirichletValues();
    }

    double ApproximateKappa(NavierStokesProblem &problem) {
      std::string meshName = "UnitSquare";
      Config::Get("MeshStrip", meshName);
      int stripLevel = 2;
      Config::Get("StripLevel", stripLevel);
      int plevel = 1;
      Config::Get("plevelStrip", plevel);

      auto stripMeshes = MeshesCreator().
        WithMeshName(meshName).
        WithCLevel(stripLevel).
        WithLevel(stripLevel).
        WithPLevel(plevel).
        CreateUnique();

      stripMeshes->PrintInfo();

      auto discEV = std::make_shared<LagrangeDiscretization>(*stripMeshes, 2, 2);
      Vectors U(10, discEV);
      Dirichlet(U);

      Matrix A(U[0]);
      Matrix B(U[0]);
      Matrices(problem, A, B);

      IEigenSolver *esolver = EigenSolverCreator("LOBPCG2").Create();
      Eigenvalues lambda;
      (*esolver)(U, lambda, A, B);
      delete esolver;

      return lambda[0];
    }

  } // namespace kappa

} // namespace navierstokes

