#ifndef NAVIERSTOKESNORMBOUND_HPP
#define NAVIERSTOKESNORMBOUND_HPP

#include "problem/NavierStokesProblem.hpp"

namespace navierstokes {

  namespace normbound {
    template<bool VERIFIED>
    void Normbound(NavierStokesProblem &problem);
  }

}

#endif //NAVIERSTOKESNORMBOUND_HPP
