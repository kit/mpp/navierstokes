#include "NavierStokesNormbound.hpp"

#include "NavierStokesApproximation.hpp"
#include "NavierStokesHomotopy.hpp"
#include "NavierStokesKappa.hpp"

#include "IAIO.hpp"

namespace navierstokes {

  namespace normbound {

    double computeSimpleConstants(NavierStokesProblem &problem) {
      NavierStokesConstants &constants = problem.Constants();

      constants.Gamma1<false>() = 1 - problem.C2sqr<IAInterval>() *
                                 (IAInterval(2) * problem.Sigma() + problem.Re());
      constants.Gamma2<false>() = problem.Re() * (constants.Tau() - IAInterval(1));

      if (inf(constants.Gamma1<false>()) <= 0) {
        ERROR("Reynolds number too large for homotopy: gamma_1= "
              + std::to_string(inf(constants.Gamma1<false>())))
      }

      double rho_start = inf(constants.Gamma1<false>()) - 0.01;
      Config::Get("HomotopyStartRho", rho_start);
      return rho_start;
    }

    double gamma2(double x, double c1, double c2, double c3, double kappa) {
      return -c1 - (1.0 - c2 / (kappa - x)) * c3;
    }

    double Dgamma2(double x, double c1, double c2, double c3, double kappa) {
      return c2 / pow(kappa - x, 2) * c3;
    }

    double DDgamma2(double x, double c1, double c2, double c3, double kappa) {
      return 2.0 * c2 / pow(kappa - x, 3) * c3;
    }

    double Df(double x_n, double c1, double c2, double c3, double rho, double kappa) {
      double tmp = x_n - rho;
      return (Dgamma2(x_n, c1, c2, c3, kappa) - gamma2(x_n, c1, c2, c3, kappa) / tmp) / tmp;
    }

    double DDf(double x_n, double c1, double c2, double c3, double rho, double kappa) {
      double tmp = x_n - rho;
      return (DDgamma2(x_n, c1, c2, c3, kappa) -
              2.0 * (Dgamma2(x_n, c1, c2, c3, kappa) -
                     gamma2(x_n, c1, c2, c3, kappa) / tmp) / tmp) / tmp;
    }

    double newtonStep(double x_n, double c1, double c2, double c3, double rho, double kappa) {
      return x_n - Df(x_n, c1, c2, c3, rho, kappa) / DDf(x_n, c1, c2, c3, rho, kappa);
    }

    double computeConstantsViaKappa(NavierStokesProblem &problem) {
      NavierStokesConstants &constants = problem.Constants();

      constants.Kappa() = kappa::ComputeKappa(problem);

      double rho_start = constants.Kappa() - 0.1;
      Config::Get("HomotopyStartRho", rho_start);

      double tmp = constants.Kappa() - 0.05;
      Config::Get("gamma1", tmp);

      if (tmp < 0.0) {
        double c1 = problem.Re() * constants.Gamma0();
        double c2 = problem.C2sqr<double>() *
                    pow(problem.Re() / 2.0 +
                        problem.Sigma() * problem.C2<double>(), 2);
        double c3 = 4.0 * pow(problem.Re(), 2) * constants.OmegaInftySqr();

        tmp = (constants.Kappa() + rho_start) / 2.0;
        for (int i = 0; i < 20; ++i) {
          double tmp_old = tmp;
          tmp = newtonStep(tmp, c1, c2, c3, rho_start, constants.Kappa());
          if (abs(tmp_old - tmp) < 1e-10)
            break;
        }
      }

      if (rho_start >= tmp) {
        ERROR("start rho larger than gamma1")
      }
      constants.Gamma1<false>() = tmp;

      if (sup(constants.Gamma1<false>()) >= constants.Kappa()) {
        constants.Gamma1<false>() = constants.Kappa() - 0.05;
        Warning("Gamma1 to small in homotopy! Gamma1 set to " + to_string(constants.Gamma1<false>()))
      }

      IAInterval s = problem.C2sqr<IAInterval>() *
                     sqr(IAInterval(problem.Re()) / 2.0 +
                         problem.Sigma() * problem.C2<IAInterval>());

      if (inf(constants.Gamma1<false>()) < sup(constants.Kappa() - s)) {
        constants.Gamma1<false>() = sup(constants.Kappa() - s + 0.01);
        if (sup(constants.Gamma1<false>()) >= constants.Kappa()) {
          ERROR("Gamma1 to small in homotopy! Cannot resize gamma1!")
        }
        Warning("Gamma1 to small in homotopy! Gamma1 set to " + to_string(constants.Gamma1<false>()))
      }

      constants.Gamma2<false>() = -problem.Re() * IAInterval(constants.Gamma0()) +
                             (s / (constants.Kappa() - constants.Gamma1<false>()) - 1.0) * 4.0 *
                             sqr(IAInterval(problem.Re())) * constants.OmegaInftySqr();

      return rho_start;
    }

    template<bool VERIFIED>
    void modelPlum(NavierStokesProblem &problem) {
      NavierStokesConstants &constants = problem.Constants();

      approximation::InitVelocity<true>(problem);

      double rho_start;
      switch (problem.NormboundStrategy()) {
        case NB_PlumSimple:
          rho_start = computeSimpleConstants(problem);
          break;
        case NB_PlumExtended:
          rho_start = computeConstantsViaKappa(problem);
          break;
        default: ERROR("Norm bound strategy " + problem.NormboundStrategyName()
                       + " not implemented!")
      }

      problem.ClearVelocity();
      problem.ClearMeshes();

      double K_max = 1 / (2 * problem.C4<double>()
                          * sqrt(problem.Re() * constants.Delta()));

      int simpleHomotopyVerbose = problem.NormboundStrategy() == NB_PlumSimple ? 2 : 999;
      int extendedHomotopyVerbose = problem.NormboundStrategy() == NB_PlumExtended ? 2 : 999;
      mout.PrintInfo("Homotopy constants", 2,
                     PrintInfoEntry("kappa", OutputDown(constants.Kappa()), extendedHomotopyVerbose),
                     PrintInfoEntry("tau", OutputUp(constants.Tau()), simpleHomotopyVerbose),
                     PrintInfoEntry("gamma0", OutputDown(constants.Gamma0()), extendedHomotopyVerbose),
                     PrintInfoEntry("gamma1", constants.Gamma1<false>(), 2),
                     PrintInfoEntry("gamma2", constants.Gamma2<false>(), 2),
                     PrintInfoEntry("K_max", K_max, 2),
                     PrintInfoEntry("rho_min", 1 / K_max / K_max, 2)
      );

      NavierStokesHomotopy<VERIFIED, false>(problem).start(rho_start);

      mout.PrintInfo("Norm Bound (" + problem.NormboundStrategyName() + ")", 2,
                     PrintInfoEntry("K_max", K_max, 2),
                     PrintInfoEntry("K", OutputUp(constants.K<false>()), 2)
      );
    }

    void modelWieners(NavierStokesProblem &problem) {
      NavierStokesConstants &constants = problem.Constants();

      int verbose = 2;

      double K_max = 1 / (2 * problem.C4<double>()
                          * sqrt(problem.Re() * constants.Delta()));

      IAInterval tmp = problem.C2<IAInterval>()
                       * problem.Reynolds<IAInterval>() * constants.UOmegaInfty<true>();
      if (sup(tmp) > 1.0) ERROR(
          "Reynolds number too large for model Wieners: " + std::to_string(sup(tmp)));

      constants.K<false>() = sup(1 / (1 - tmp));

      mout.PrintInfo("Norm Bound (" + problem.NormboundStrategyName() + ")", verbose,
                     PrintInfoEntry("C_2 Re ||U+w||_oo", tmp, 2),
                     PrintInfoEntry("K_max", K_max, 2),
                     PrintInfoEntry("K", OutputUp(constants.K<false>()), 2)
      );

    }

    template<bool VERIFIED>
    void Normbound(NavierStokesProblem &problem) {
      if (!problem.RunPart(PartNormbound)) return;

      switch (problem.NormboundStrategy()) {
        case NB_Wieners:
          modelWieners(problem);
          break;
        case NB_PlumSimple:
        case NB_PlumExtended:
          modelPlum<VERIFIED>(problem);
          break;
        default: ERROR("Normbound model not implemented")
      }

      NavierStokesConstants &constants = problem.Constants();
      if (constants.VelocityL4Diff() != 0.0) {
        IAInterval tmp = 1 - problem.Reynolds<IAInterval>() * problem.C4<IAInterval>()
                             * constants.K<false>() * constants.VelocityL4Diff() * 2;

        if (inf(tmp) <= 0.0) ERROR("Normbound cannot be computed")

        constants.K<false>() = sup(constants.K<false>() / tmp);

        mout.PrintInfo("Norm Bound (Adapt K))", 2,
                       PrintInfoEntry("K", OutputUp(constants.K<false>()), 2)
        );
      }

      problem.SaveData(PartNormbound);
    }

    template void Normbound<true>(NavierStokesProblem &problem);

    template void Normbound<false>(NavierStokesProblem &problem);

  } // namespace normbound

} // namespace navierstokes
