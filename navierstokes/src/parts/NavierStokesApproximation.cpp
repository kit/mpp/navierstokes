#include "NavierStokesApproximation.hpp"

#include "NavierStokesAssembleApproximation.hpp"

#include "MeshesCreator.hpp"
#include "LagrangeDiscretization.hpp"
#include "ArgyrisDiscretization.hpp"
#include "THDiscretization.hpp"
#include "SaveLoad.hpp"
#include "Plotting.hpp"
#include "IAIO.hpp"
#include "GMRES.hpp"

namespace navierstokes {

  namespace approximation {

    void SaveVector(const Vector &vector, std::string &path) {
      PSaver saver(path.c_str());
      saver << vector;
      saver.close();
    }

    void LoadVector(Vector &vector, std::string &path) {
      PLoader loader(path.c_str());
      loader >> vector;
      loader.close();
    }

    int verbose = -1;

    template<bool coarse>
    bool computeVelocity = true;

    bool computeApprV = true;

    bool computePressure = true;

    template<bool coarse>
    void InitVelocity(NavierStokesProblem &problem) {
      if (problem.VelocityInitialized<coarse>()) return;

      problem.InitMeshes();

      std::string velocityPath = problem.PathVelocity<coarse>();

      /// always true if function is called first time
      if (computeVelocity<coarse> || NavierStokesProblem::SaveLoadDisabled()) {
        computeVelocity<coarse> = problem.RunPart(PartComputeVelocity) ||
                                  !SaveLoad::PFileExists(velocityPath.c_str());
      }
      problem.SetUpVelocity<coarse>();

      if (!problem.Levels().velocityLevelsDiffer && problem.VelocityInitialized<!coarse>()) {
        computeVelocity<coarse> = false;
      }

      if (!computeVelocity<coarse>) {
        LoadVector(problem.Velocity<coarse>(), velocityPath);
        return;
      }
      problem.ResetPartsToRun(PartComputeVelocity);

      if constexpr (coarse)
        mout << "Coarse velocity needs to be computed on level "
             << problem.Velocity<coarse>().SpaceLevel() << "..." << endl << endl;
      else
        mout << "Velocity needs to be computed on level "
             << problem.Velocity<coarse>().SpaceLevel() << "..." << endl << endl;
      problem.Velocity<coarse>() = 0.0;
      DivergenceFreeApproximationStokesAssemble assemble(problem);
      problem.Velocity<coarse>().GetMatrixGraph().MatrixMemoryInfo().PrintInfo();
      NewtonMethod(assemble, problem.Velocity<coarse>());
      mout << endl;

      if (!NavierStokesProblem::SaveLoadDisabled())
        SaveVector(problem.Velocity<coarse>(), velocityPath);
      computeVelocity<coarse> = false;
    }

    template void InitVelocity<false>(NavierStokesProblem &problem);

    template void InitVelocity<true>(NavierStokesProblem &problem);

    void InitPressure(NavierStokesProblem &problem) {
      if (problem.PressureInitialized()) return;

      InitVelocity<false>(problem);

      std::string pressurePath = problem.PathPressure();

      /// always true if function is called first time
      if (computePressure || NavierStokesProblem::SaveLoadDisabled()) {
        computePressure = problem.RunPart(PartComputePressure) ||
                          !SaveLoad::PFileExists(pressurePath.c_str());
      }

      if (!computePressure) {
        problem.SetUpPressure();
        LoadVector(problem.Pressure(), pressurePath);
        return;
      }
      problem.ResetPartsToRun(PartComputePressure);

      mout << "Pressure needs to be computed..." << endl << endl;

      int degreeVelocity = 2;

      auto disc = std::make_shared<THDiscretization>(problem.GetMeshes(), degreeVelocity, 14, 1);
      Vector u(disc, problem.Levels().pressureLevel);
      u = 0;

      {
        std::string solverName = "GMRES";
        Config::Get("PressureInitLinearSolver", solverName);
        MixedInitApproximationStokesAssemble assemble(problem);
        u.GetMatrixGraph().MatrixMemoryInfo().PrintInfo();
        ApplyLinearSolver(assemble, u, solverName);
        mout << endl;
      }
      {
        MixedApproximationStokesAssemble assemble(problem);
        u.GetMatrixGraph().MatrixMemoryInfo().PrintInfo();
        NewtonMethod(assemble, u);
        mout << endl;
      }

      problem.SetUpPressure();
      for (row r = problem.Pressure().rows(); r != problem.Pressure().rows_end(); ++r) {
        problem.Pressure()(r, 0) = u(r(), 2);
      }

      if (!NavierStokesProblem::SaveLoadDisabled())
        SaveVector(problem.Pressure(), pressurePath);
      computePressure = false;
    }

    void InitApproximationV(NavierStokesProblem &problem) {
      if (problem.ApprVInitialized()) return;

      InitVelocity<false>(problem);

      std::string vPath = problem.PathApprV();

      if (computeApprV || NavierStokesProblem::SaveLoadDisabled()) { /// always true if function is called first time
        computeApprV = problem.RunPart(PartComputeApprV) ||
                       !SaveLoad::PFileExists(vPath.c_str());
      }
      problem.SetUpApprV();

      if (!computeApprV) {
        LoadVector(problem.ApprV(), vPath);
        return;
      }

      std::string solverName = "GMRES";
      Config::Get("ApprVLinearSolver", solverName);
      mout << "Approximation V needs to be computed..." << endl << endl;
      problem.ApprV() = 0.0;
      VApproximationStokesAssemble assemble(problem);
      problem.ApprV().GetMatrixGraph().MatrixMemoryInfo().PrintInfo();
      ApplyLinearSolver(assemble, problem.ApprV(), solverName);
      mout << endl;

      if (!NavierStokesProblem::SaveLoadDisabled())
        SaveVector(problem.ApprV(), vPath);
      computeApprV = false;
    }

    template<bool VERIFIED>
    void velocityL4Diff(const NavierStokesProblem &problem,
                        DivergenceFreeElementT<IAEigenvalueType<VERIFIED>> &E_coarse,
                        IAEigenvalueType<VERIFIED> &s0, IAEigenvalueType<VERIFIED> &s1,
                        const Cell &C, int level) {
      using T = IAEigenvalueType<VERIFIED>;

      if (level < problem.Levels().velocityLevel) {
        for (int i = 0; i < C.Children(); ++i) {
          auto childCell = problem.GetMesh(level + 1).find_cell(C.Child(i));
          if (childCell == problem.GetMesh(level + 1).cells_end()) THROW("Child not found");
          velocityL4Diff<VERIFIED>(problem, E_coarse, s0, s1, *childCell, level + 1);
        }
      } else {
        DivergenceFreeElementT<T> E_fine(problem.Velocity<false>(), C);
        for (int q = 0; q < E_fine.nQ(); ++q) {
          PointT<T> qPointLocal = E_coarse.GlobalToLocal(E_fine.QPoint(q));
          VectorFieldT<T> u_c = E_coarse.VelocityField(qPointLocal, problem.Velocity<true>());
          VectorFieldT<T> u_f = E_fine.VelocityField(q, problem.Velocity<false>());

          s0 += E_fine.QWeight(q) * pow(u_c[0] - u_f[0], 4);
          s1 += E_fine.QWeight(q) * pow(u_c[1] - u_f[1], 4);
        }
      }
    }

    template<bool VERIFIED, NBStrategy STRAT>
    void VelocityNormsT(NavierStokesProblem &problem) {
      NavierStokesConstants &constants = problem.Constants();
      using T = IAEigenvalueType<VERIFIED>;

      InitVelocity<false>(problem);
      InitVelocity<true>(problem);

      mout.StartBlock("Velocity norms");
      mout << "Computing norms" << endl;

      verbose = 2;

      double max_G0 = 0.0;
      double max_G1 = 0.0;
      double max_UOmega0 = 0.0;
      double max_UOmega1 = 0.0;
      double max_Dx_UOmega0 = 0.0;
      double max_Dy_UOmega0 = 0.0;
      double max_Dx_UOmega1 = 0.0;
      double max_Omega0 = 0.0;
      double max_Omega1 = 0.0;
      double max_Dx_Omega0 = 0.0;
      double max_Dy_Omega0 = 0.0;
      double max_Dx_Omega1 = 0.0;

      if constexpr (STRAT == NB_PlumSimple) {
        constants.Tau() = 0.0;
      }

      if constexpr (STRAT == NB_PlumExtended) {
        constants.Gamma0() = infty;
      }

      for (cell c = problem.Velocity<true>().cells(); c != problem.Velocity<true>().cells_end(); ++c) {
        const Cell &C = *c;
        auto E = NavierStokesElementT<T>::template Create<true>(problem, C);

        IAInterval G0, G1, UOmega0, UOmega1, Dx_UOmega0, Dy_UOmega0, Dx_UOmega1;
        E->Ranges(G0, G1, UOmega0, UOmega1, Dx_UOmega0, Dy_UOmega0, Dx_UOmega1);
        max_G0 = std::max(max_G0, sup(abs(G0)));
        max_G1 = std::max(max_G1, sup(abs(G1)));
        max_UOmega0 = std::max(max_UOmega0, sup(abs(UOmega0)));
        max_UOmega1 = std::max(max_UOmega1, sup(abs(UOmega1)));
        max_Dx_UOmega0 = std::max(max_Dx_UOmega0, sup(abs(Dx_UOmega0)));
        max_Dy_UOmega0 = std::max(max_Dy_UOmega0, sup(abs(Dy_UOmega0)));
        max_Dx_UOmega1 = std::max(max_Dx_UOmega1, sup(abs(Dx_UOmega1)));

        if constexpr (STRAT == NB_PlumSimple) {
          IAInterval Dx_0, Dy_0_Dx_1;
          E->RangesPlumSimple(Dx_0, Dy_0_Dx_1);
          constants.Tau() = std::max(constants.Tau(),
                                     sup(sqrt(4 * sqr(Dx_0) + sqr(Dy_0_Dx_1))));
        }

        if constexpr (STRAT == NB_PlumExtended) {
          IAInterval Omega0, Omega1, Dx_Omega0, Dy_Omega0, Dx_Omega1, Dy_0_Dx_1;
          E->RangesPlumExtended(Omega0, Omega1, Dx_Omega0, Dy_Omega0, Dx_Omega1, Dy_0_Dx_1);
          max_Omega0 = std::max(max_Omega0, sup(abs(Omega0)));
          max_Omega1 = std::max(max_Omega1, sup(abs(Omega1)));
          max_Dx_Omega0 = std::max(max_Dx_Omega0, sup(abs(Dx_Omega0)));
          max_Dy_Omega0 = std::max(max_Dy_Omega0, sup(abs(Dy_Omega0)));
          max_Dx_Omega1 = std::max(max_Dx_Omega1, sup(abs(Dx_Omega1)));
          constants.Gamma0() =
              std::min(constants.Gamma0(),
                       inf(-sqrt(4 * sqr(Dx_Omega0) + sqr(Dy_0_Dx_1))));
        }
      }

      max_G0 = PPM->Max(max_G0);
      max_G1 = PPM->Max(max_G1);
      max_UOmega0 = PPM->Max(max_UOmega0);
      max_UOmega1 = PPM->Max(max_UOmega1);
      max_Dx_UOmega0 = PPM->Max(max_Dx_UOmega0);
      max_Dy_UOmega0 = PPM->Max(max_Dy_UOmega0);
      max_Dx_UOmega1 = PPM->Max(max_Dx_UOmega1);

      constants.GammaInftySqr() = sup(sqr(IAInterval(max_G0)) + sqr(IAInterval(max_G1)));
      constants.UOmegaInftySqr<true>() = sup(
          sqr(IAInterval(max_UOmega0)) + sqr(IAInterval(max_UOmega1)));
      constants.D_UOmegaInftySqr<true>() = sup(2 * sqr(IAInterval(max_Dx_UOmega0))
                                          + sqr(IAInterval(max_Dy_UOmega0)) +
                                          sqr(IAInterval(max_Dx_UOmega1)));

      if (problem.Levels().velocityLevelsDiffer) {
        max_UOmega0 = 0.0;
        max_UOmega1 = 0.0;
        max_Dx_UOmega0 = 0.0;
        max_Dy_UOmega0 = 0.0;
        max_Dx_UOmega1 = 0.0;
        for (cell c = problem.Velocity<false>().cells();
             c != problem.Velocity<false>().cells_end(); ++c) {
          const Cell &C = *c;
          auto E = NavierStokesElementT<T>::template Create<false>(problem, C);

          IAInterval UOmega0, UOmega1, Dx_UOmega0, Dy_UOmega0, Dx_UOmega1;
          E->Ranges(UOmega0, UOmega1, Dx_UOmega0, Dy_UOmega0, Dx_UOmega1);
          max_UOmega0 = std::max(max_UOmega0, sup(abs(UOmega0)));
          max_UOmega1 = std::max(max_UOmega1, sup(abs(UOmega1)));
          max_Dx_UOmega0 = std::max(max_Dx_UOmega0, sup(abs(Dx_UOmega0)));
          max_Dy_UOmega0 = std::max(max_Dy_UOmega0, sup(abs(Dy_UOmega0)));
          max_Dx_UOmega1 = std::max(max_Dx_UOmega1, sup(abs(Dx_UOmega1)));
        }
        max_UOmega0 = PPM->Max(max_UOmega0);
        max_UOmega1 = PPM->Max(max_UOmega1);
        max_Dx_UOmega0 = PPM->Max(max_Dx_UOmega0);
        max_Dy_UOmega0 = PPM->Max(max_Dy_UOmega0);
        max_Dx_UOmega1 = PPM->Max(max_Dx_UOmega1);

        constants.UOmegaInftySqr<false>() = sup(
            sqr(IAInterval(max_UOmega0)) + sqr(IAInterval(max_UOmega1)));
        constants.D_UOmegaInftySqr<false>() = sup(2 * sqr(IAInterval(max_Dx_UOmega0))
                                             + sqr(IAInterval(max_Dy_UOmega0)) +
                                             sqr(IAInterval(max_Dx_UOmega1)));

        T s0{}, s1{};
        for (cell c = problem.Velocity<true>().cells();
             c != problem.Velocity<true>().cells_end(); ++c) {
          const Cell &C = *c;
          DivergenceFreeElementT<T> E_coarse(problem.Velocity<true>(), C);
          velocityL4Diff<VERIFIED>(problem, E_coarse, s0, s1, C, problem.Levels().velocityCoarseLevel);
        }
        constants.VelocityL4Diff() = sqrt(sqrt(PPM->Sum(s0)) + sqrt(PPM->Sum(s1)));
      } else {
        constants.UOmegaInftySqr<false>() = constants.UOmegaInftySqr<true>();
        constants.D_UOmegaInftySqr<false>() = constants.D_UOmegaInftySqr<true>();
        constants.VelocityL4Diff() = 0.0;
      }

      mout.EndBlock();
      mout << endl;

      if constexpr (STRAT == NB_Wieners) {
        mout.PrintInfo("Velocity Norms", verbose,
                       PrintInfoEntry("||Gamma||_oo", OutputUp(sup(constants.GammaInfty())), 2),
                       PrintInfoEntry("||U + w||_oo", OutputUp(sup(constants.UOmegaInfty<false>())), 2),
                       PrintInfoEntry("||D(U + w)||_oo", OutputUp(sup(constants.D_UOmegaInfty<false>())),
                                      2),
                       PrintInfoEntry("||U + w_c||_oo", OutputUp(sup(constants.UOmegaInfty<true>())), 2),
                       PrintInfoEntry("||D(U + w_c)||_oo",
                                      OutputUp(sup(constants.D_UOmegaInfty<true>())), 2),
                       PrintInfoEntry("||w - w_c||_L4", constants.VelocityL4Diff(), 2)
        );
      }

      if constexpr (STRAT == NB_PlumSimple) {
        constants.Tau() = PPM->Max(constants.Tau());

        mout.PrintInfo("Velocity Norms", verbose,
                       PrintInfoEntry("||Gamma||_oo", OutputUp(sup(constants.GammaInfty())), 2),
                       PrintInfoEntry("||U + w||_oo", OutputUp(sup(constants.UOmegaInfty<false>())), 2),
                       PrintInfoEntry("||D(U + w)||_oo", OutputUp(sup(constants.D_UOmegaInfty<false>())),
                                      2),
                       PrintInfoEntry("||U + w_c||_oo", OutputUp(sup(constants.UOmegaInfty<true>())), 2),
                       PrintInfoEntry("||D(U + w_c)||_oo",
                                      OutputUp(sup(constants.D_UOmegaInfty<true>())), 2),
                       PrintInfoEntry("||w - w_c||_L4", constants.VelocityL4Diff(), 2),
                       PrintInfoEntry("tau", OutputUp(constants.Tau()), 2)
        );
      }

      if constexpr (STRAT == NB_PlumExtended) {
        max_Omega0 = PPM->Max(max_Omega0);
        max_Omega1 = PPM->Max(max_Omega1);
        max_Dx_Omega0 = PPM->Max(max_Dx_Omega0);
        max_Dy_Omega0 = PPM->Max(max_Dy_Omega0);
        max_Dx_Omega1 = PPM->Max(max_Dx_Omega1);
        constants.Gamma0() = PPM->Min(constants.Gamma0());

        constants.OmegaInftySqr() = sup(sqr(IAInterval(max_Omega0)) + sqr(IAInterval(max_Omega1)));
        constants.D_OmegaInftySqr() = sup(2 * sqr(IAInterval(max_Dx_Omega0))
                                     + sqr(IAInterval(max_Dy_Omega0)) +
                                     sqr(IAInterval(max_Dx_Omega1)));

        mout.PrintInfo("Velocity Norms", verbose,
                       PrintInfoEntry("||Gamma||_oo", OutputUp(sup(constants.GammaInfty())), 2),
                       PrintInfoEntry("||U + w||_oo", OutputUp(sup(constants.UOmegaInfty<false>())), 2),
                       PrintInfoEntry("||D(U + w)||_oo", OutputUp(sup(constants.D_UOmegaInfty<false>())),
                                      2),
                       PrintInfoEntry("||U + w_c||_oo", OutputUp(sup(constants.UOmegaInfty<true>())), 2),
                       PrintInfoEntry("||D(U + w_c)||_oo",
                                      OutputUp(sup(constants.D_UOmegaInfty<true>())), 2),
                       PrintInfoEntry("||w_c||_oo", OutputUp(sup(constants.OmegaInfty())), 2),
                       PrintInfoEntry("||Dw_c||_oo", OutputUp(sup(constants.D_OmegaInfty())), 2),
                       PrintInfoEntry("||w - w_c||_L4", constants.VelocityL4Diff(), 2),
                       PrintInfoEntry("gamma0", OutputDown(constants.Gamma0()))
        );
      }

      problem.ClearVelocity();
    }

    template<bool VERIFIED>
    void VelocityNorms(NavierStokesProblem &problem) {
      if (!problem.RunPart(PartVelocityNorms)) return;

      switch (problem.NormboundStrategy()) {
        case NB_Wieners:
          VelocityNormsT<VERIFIED, NB_Wieners>(problem);
          break;
        case NB_PlumSimple:
          VelocityNormsT<VERIFIED, NB_PlumSimple>(problem);
          break;
        case NB_PlumExtended:
          VelocityNormsT<VERIFIED, NB_PlumExtended>(problem);
          break;
        default: ERROR("Normbound strategy not implemented")
      }

      problem.SaveData(PartVelocityNorms);
    }

    template void VelocityNorms<true>(NavierStokesProblem &problem);

    template void VelocityNorms<false>(NavierStokesProblem &problem);

    template<bool VERIFIED>
    void ApprVNorms(NavierStokesProblem &problem) {
      NavierStokesConstants &constants = problem.Constants();
      using T = IAEigenvalueType<VERIFIED>;

      if (!problem.RunPart(PartApprVNorms)) return;

      InitApproximationV(problem);

      mout.StartBlock("Approximation V norms");
      mout << "Computing norms" << endl;

      verbose = 2;

      double max_Dx_0 = 0.0;
      double max_Dy_0 = 0.0;
      double max_Dx_1 = 0.0;

      T nL{};
      T nDL{};
      for (cell c = problem.ApprV().cells(); c != problem.ApprV().cells_end(); ++c) {
        const Cell &C = *c;
        auto E = NavierStokesElementT<T>::template Create<false>(problem, problem.ApprV(), C);

        IAInterval Dx_UomegaTVT0, Dy_UomegaTVT0, Dx_UomegaTVT1;
        E->RangesDelta(Dx_UomegaTVT0, Dy_UomegaTVT0, Dx_UomegaTVT1);
        max_Dx_0 = std::max(max_Dx_0, sup(abs(Dx_UomegaTVT0)));
        max_Dy_0 = std::max(max_Dy_0, sup(abs(Dy_UomegaTVT0)));
        max_Dx_1 = std::max(max_Dx_1, sup(abs(Dx_UomegaTVT1)));

        for (int q = 0; q < E->nQ(); ++q) {
          TensorT<T> DV = E->VelocityFieldGradient(q, problem.ApprV())
                          - problem.VFG_V(E->QPoint(q), C);
          VectorFieldT<T> V = E->VelocityField(q, problem.ApprV())
                              - problem.VF_V(E->QPoint(q), C);

          nL += E->QWeight(q) * (V * V);
          nDL += E->QWeight(q) * Frobenius(DV, DV);
        }
      }
      if constexpr (VERIFIED) {
        constants.NormApprV() = sup(sqrt(PPM->Sum(abs(nL))));
        constants.NormDApprV() = sup(sqrt(PPM->Sum(abs(nDL))));
      } else {
        constants.NormApprV() = sqrt(PPM->Sum(abs(nL)));
        constants.NormDApprV() = sqrt(PPM->Sum(abs(nDL)));
      }
      max_Dx_0 = PPM->Max(max_Dx_0);
      max_Dy_0 = PPM->Max(max_Dy_0);
      max_Dx_1 = PPM->Max(max_Dx_1);
      constants.NormDApprVInfty() = sup(sqrt(2 * sqr(IAInterval(max_Dx_0)) + sqr(IAInterval(max_Dy_0))
                                   + sqr(IAInterval(max_Dx_1))));

      problem.ClearVelocity();

      mout.EndBlock();
      mout << endl;

      mout.PrintInfo("Approximation V Norms", verbose,
                     PrintInfoEntry("||V-V~||_L2", OutputUp(constants.NormApprV()), 2),
                     PrintInfoEntry("||D(V-V~)||_L2", OutputUp(constants.NormDApprV()), 2),
                     PrintInfoEntry("||D(U+w~-V~)||_oo", OutputUp(constants.NormDApprVInfty()), 2)
      );

      problem.SaveData(PartApprVNorms);
    }

    template void ApprVNorms<true>(NavierStokesProblem &problem);

    template void ApprVNorms<false>(NavierStokesProblem &problem);

    void Plot(NavierStokesProblem &problem) {
      bool plotted = false;
      if (problem.RunPart(PartPlotVelocity)) {
        InitVelocity<false>(problem);

        /// Add auxiliary function to velocity
        std::list<Point> changed{};
        for (cell c = problem.Velocity<false>().cells(); c != problem.Velocity<false>().cells_end(); ++c) {
          for (int i = 0; i < c.Corners(); ++i) {
            const Point &p = c.Corner(i);
            if (std::find(changed.begin(), changed.end(), p) == changed.end()) {
              changed.emplace_back(p);
              VectorField G = problem.VF_Gamma(p, *c);
              row r = problem.Velocity<false>().find_row(p);
              problem.Velocity<false>()(r, 1) += G[1];
              problem.Velocity<false>()(r, 2) -= G[0];
            }
          }
        }

        mpp::plot("Approximation").AddData("velocity", problem.Velocity<false>());
        problem.ClearVelocity();
        plotted = true;
      }
      if (problem.RunPart(PartPlotPressure)) {
        InitPressure(problem);

        for (row r = problem.Pressure().rows(); r != problem.Pressure().rows_end(); ++r) {
          problem.Pressure()(r, 0) += problem.PressureP(r());
        }

        mpp::plot("Approximation").AddData("pressure", problem.Pressure());
        problem.ClearPressure();
        plotted = true;
      }
      if (plotted)
        mpp::plot("Approximation").PlotFile(problem.PlotName());
    }

    void PlotCoarseMesh(NavierStokesProblem &problem, bool strip) {
      std::string name = problem.MeshesName();

      MeshesCreator creator = MeshesCreator(name).
          WithPLevel(0).
          WithCLevel(0).
          WithLevel(0);
      if (strip) {
        creator.WithSubdomainSetter(problem.GetSdSetterStrip());
      } else {
        creator.WithSubdomainSetter(problem.GetSdSetter());
      }
      std::unique_ptr<Meshes> coarseMesh = creator.CreateUnique();

      mpp::plot_mesh(coarseMesh->fine());
    }

  } // namespace approximation

} // namespace navierstokes
