#ifndef NAVIERSTOKESAPPROXIMATION_HPP
#define NAVIERSTOKESAPPROXIMATION_HPP

#include "problem/NavierStokesProblem.hpp"

namespace navierstokes {

  namespace approximation {
      template<bool coarse>
      void InitVelocity(NavierStokesProblem &problem);

      void InitPressure(NavierStokesProblem &problem);

      void InitApproximationV(NavierStokesProblem &problem);

      template<bool VERIFIED>
      void VelocityNorms(NavierStokesProblem &problem);

      template<bool VERIFIED>
      void ApprVNorms(NavierStokesProblem &problem);

      void Plot(NavierStokesProblem &problem);

      void PlotCoarseMesh(NavierStokesProblem &problem, bool strip=false);
  }

}

#endif //NAVIERSTOKESAPPROXIMATION_HPP
