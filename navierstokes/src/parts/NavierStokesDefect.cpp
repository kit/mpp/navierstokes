#include "NavierStokesDefect.hpp"
#include "NavierStokesApproximation.hpp"
#include "assemble/NavierStokesAssembleDefect.hpp"

#include "RTDiscretization.hpp"
#include "IAIO.hpp"
#include "GMRES.hpp"

namespace navierstokes {

  namespace defectbound {
    template<bool VERIFIED>
    void VelocityDefect(NavierStokesProblem &problem) {
      if (!problem.RunPart(PartDefectVelocity)) return;

      std::string discDefectName = "RT2";
      Config::Get("VelocityDefectDiscretization", discDefectName);
      std::unique_ptr<DefectStokesAssemble> assemble = nullptr;
      if (discDefectName.substr(0, 2) == "RT") {
        int order = std::stoi(discDefectName.substr(2));
        if (problem.Levels().velocityLevel < problem.Levels().velocityDefectLevel) {
          assemble = std::make_unique<RTVelocityDefectStokesAssembleDiffer<VERIFIED>>(problem, order);
        } else if (problem.Levels().velocityLevel > problem.Levels().velocityDefectLevel) {
          ERROR("Defect level to small!")
        } else {
          assemble = std::make_unique<RTVelocityDefectStokesAssembleCoincide<VERIFIED>>(problem, order);
        }
      } else if (discDefectName.substr(0, 1) == "P") {
        int degree = std::stoi(discDefectName.substr(1));
        if (problem.Levels().velocityLevel < problem.Levels().velocityDefectLevel) {
          assemble = std::make_unique<LagrangeVelocityDefectStokesAssembleDiffer<VERIFIED>>(problem, degree);
        } else if (problem.Levels().velocityLevel > problem.Levels().velocityDefectLevel) {
          ERROR("Defect level to small!")
        } else {
          assemble = std::make_unique<LagrangeVelocityDefectStokesAssembleCoincide<VERIFIED>>(problem, degree);
        }
      } else {
        ERROR("Velocity defect assemble not implemented")
      }

      NavierStokesConstants &constants = problem.Constants();

      approximation::InitApproximationV(problem);
      approximation::InitVelocity<false>(problem);
      approximation::InitPressure(problem);
      int verbose = 2;

      mout << "Computing approximations for velocity defect..." << endl << endl;

      std::shared_ptr<NonAdaptiveIDiscretization> discDefect
          = std::move(assemble->CreateDefectDisc(problem.DiscVelocity<double>()));
      std::shared_ptr<IANonAdaptiveIDiscretization> IAdiscDefect
          = std::move(assemble->CreateDefectDisc(problem.DiscVelocity<IAInterval>()));

      std::string solverName = "GMRES";
      Config::Get("VelocityDefectLinearSolver", solverName);
      Vectors rho(2, discDefect, problem.Levels().velocityDefectLevel);
      rho.SetIADisc(IAdiscDefect);
//      rho.GetMatrixGraph().MatrixMemoryInfo().PrintInfo();
      ApplyLinearSolver(*assemble, rho, solverName);

      mout << endl;
      mout.StartBlock("Velocity Defect");
      mout << "Computing defect" << endl;

      double delta1, delta2;
      assemble->Defect(delta1, delta2, rho);

      constants.Delta3() = sup(
          constants.NormDApprV() * constants.UOmegaInfty<false>() + constants.NormApprV() * constants.NormDApprVInfty());

      if constexpr (VERIFIED) {
        constants.Delta() = sup(IAInterval(delta1) + problem.C2<IAInterval>()
                                                     * (delta2 + IAInterval(problem.Re()) * constants.Delta3()));
      } else {
        constants.Delta() =
            delta1 + problem.C2<double>() * (delta2 + problem.Re() * constants.Delta3());
      }
      problem.ClearVelocity();
      problem.ClearPressure();
      mout.EndBlock();
      mout << endl;

      mout.PrintInfo("Velocity Defect", verbose,
                     PrintInfoEntry("delta_1", OutputUp(delta1), 2),
                     PrintInfoEntry("delta_2", OutputUp(delta2), 2),
                     PrintInfoEntry("delta_3", OutputUp(constants.Delta3()), 2),
                     PrintInfoEntry("delta", OutputUp(constants.Delta()), 1)
      );

      problem.SaveData(PartDefectVelocity);
    }

    template void VelocityDefect<true>(NavierStokesProblem &problem);

    template void VelocityDefect<false>(NavierStokesProblem &problem);

    template<bool VERIFIED>
    void PressureDefect(NavierStokesProblem &problem) {
      if (!problem.RunPart(PartDefectPressure)) return;

      std::string discDefectName = "RT2";
      Config::Get("PressureDefectDiscretization", discDefectName);
      std::unique_ptr<DefectStokesAssemble> assemble = nullptr;
      if (discDefectName.substr(0, 2) == "RT") {
        int order = std::stoi(discDefectName.substr(2));
        if (problem.Levels().velocityLevel < problem.Levels().velocityDefectLevel) {
          assemble = std::make_unique<RTPressureDefectStokesAssembleDiffer<VERIFIED>>(problem, order);
        } else if (problem.Levels().velocityLevel > problem.Levels().velocityDefectLevel) {
          ERROR("Defect level to small!")
        } else {
          assemble = std::make_unique<RTPressureDefectStokesAssembleCoincide<VERIFIED>>(problem, order);
        }
      } else if (discDefectName.substr(0, 1) == "P") {
        int degree = std::stoi(discDefectName.substr(1));
        if (problem.Levels().velocityLevel < problem.Levels().velocityDefectLevel) {
          assemble = std::make_unique<LagrangePressureDefectStokesAssembleDiffer<VERIFIED>>(problem, degree);
        } else if (problem.Levels().velocityLevel > problem.Levels().velocityDefectLevel) {
          ERROR("Defect level to small!")
        } else {
          assemble = std::make_unique<LagrangePressureDefectStokesAssembleCoincide<VERIFIED>>(problem, degree);
        }
      } else {
        ERROR("Velocity defect assemble not implemented")
      }

      NavierStokesConstants &constants = problem.Constants();

      approximation::InitApproximationV(problem);
      approximation::InitPressure(problem);

      int verbose = 2;

      mout << "Computing approximations for pressure defect..." << endl << endl;

      std::shared_ptr<NonAdaptiveIDiscretization> discDefect
          = std::move(assemble->CreateDefectDisc(problem.DiscPressure<double>()));
      std::shared_ptr<IANonAdaptiveIDiscretization> IAdiscDefect
          = std::move(assemble->CreateDefectDisc(problem.DiscPressure<IAInterval>()));

      std::string solverName = "GMRES";
      Config::Get("PressureDefectLinearSolver", solverName);

      Vectors rho(2, discDefect, problem.Levels().pressureDefectLevel);
      rho.SetIADisc(IAdiscDefect);
//      rho.GetMatrixGraph().MatrixMemoryInfo().PrintInfo();
      ApplyLinearSolver(*assemble, rho, solverName);

      mout << endl;
      mout.StartBlock("Pressure Defect");
      mout << "Computing defect" << endl;

      double alpha1, alpha2;
      assemble->Defect(alpha1, alpha2, rho);

      if constexpr (VERIFIED)
        constants.AlphaPressure() = sup((IAInterval(alpha1) + problem.C2<IAInterval>() * alpha2) / problem.Re()
                                        + problem.C2<IAInterval>() * constants.Delta3()
                                        + constants.Alpha() * (IAInterval(1.0) / problem.Re()
                                                               + problem.C4sqr<IAInterval>() * constants.Alpha()
                                                               + problem.C2sqr<IAInterval>() * sqrt(IAInterval(constants.D_UOmegaInftySqr<false>()))
                                                               + problem.C2<IAInterval>() * sqrt(IAInterval(constants.UOmegaInftySqr<false>()))));
      else
        constants.AlphaPressure() = (alpha1 + problem.C2<double>() * alpha2) / problem.Re()
                                    + problem.C2<double>() * constants.Delta3()
                                    + constants.Alpha() * (1.0 / problem.Re()
                                                           + problem.C4sqr<double>() * constants.Alpha()
                                                           + problem.C2sqr<double>() * sqrt(constants.D_UOmegaInftySqr<false>())
                                                           + problem.C2<double>() * sqrt(constants.UOmegaInftySqr<false>()));

      problem.ClearPressure();
      problem.ClearVelocity();
      mout.EndBlock();
      mout << endl;

      mout.PrintInfo("Pressure Defect", verbose,
                     PrintInfoEntry("alpha_1", OutputUp(alpha1), 2),
                     PrintInfoEntry("alpha_2", OutputUp(alpha2), 2),
                     PrintInfoEntry("alpha_p", OutputUp(constants.AlphaPressure()), 1)
      );

      problem.SaveData(PartDefectPressure);
    }

    template void PressureDefect<true>(NavierStokesProblem &problem);

    template void PressureDefect<false>(NavierStokesProblem &problem);

  } // namespace defectbound

} // namespace navierstokes
