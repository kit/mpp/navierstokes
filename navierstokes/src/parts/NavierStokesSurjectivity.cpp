#include "NavierStokesSurjectivity.hpp"

#include "NavierStokesApproximation.hpp"
#include "NavierStokesHomotopy.hpp"
#include "NavierStokesElement.hpp"

#include "IAIO.hpp"

namespace navierstokes {

  namespace surjectivity {

    void modelWieners(NavierStokesProblem &problem) {
      problem.Constants().K<true>() = problem.Constants().K<false>();

      int verbose = 2;
      mout.PrintInfo("Surjectivity (Wieners)", verbose,
                     PrintInfoEntry("K*", OutputUp(problem.Constants().K<true>()), 2)
      );
    }

    double computeSimpleConstants(NavierStokesProblem &problem) {
      problem.Constants().Gamma1<true>() = problem.Constants().Gamma1<false>();
      problem.Constants().Gamma2<true>() = problem.Constants().Gamma2<false>();

      double rho_start = inf(problem.Constants().Gamma1<false>()) - 0.01;
      Config::Get("AdjointHomotopyStartRho", rho_start);
      return rho_start;
    }

    double gamma2(double x, double c1, double c2, double c3, double kappa) {
      return -c1 - (1.0 - c2 / (kappa - x)) * c3;
    }

    double Dgamma2(double x, double c1, double c2, double c3, double kappa) {
      return c2 / pow(kappa - x, 2) * c3;
    }

    double DDgamma2(double x, double c1, double c2, double c3, double kappa) {
      return 2.0 * c2 / pow(kappa - x, 3) * c3;
    }

    double Df(double x_n, double c1, double c2, double c3, double rho, double kappa) {
      double tmp = x_n - rho;
      return (Dgamma2(x_n, c1, c2, c3, kappa) - gamma2(x_n, c1, c2, c3, kappa) / tmp) / tmp;
    }

    double DDf(double x_n, double c1, double c2, double c3, double rho, double kappa) {
      double tmp = x_n - rho;
      return (DDgamma2(x_n, c1, c2, c3, kappa) -
              2.0 * (Dgamma2(x_n, c1, c2, c3, kappa) -
                     gamma2(x_n, c1, c2, c3, kappa) / tmp) / tmp) / tmp;
    }

    double newtonStep(double x_n, double c1, double c2, double c3, double rho, double kappa) {
      return x_n - Df(x_n, c1, c2, c3, rho, kappa) / DDf(x_n, c1, c2, c3, rho, kappa);
    }

    double computeConstantsViaKappa(NavierStokesProblem &problem) {
      NavierStokesConstants &constants = problem.Constants();

      double rho_start = constants.Kappa() - 0.1;
      Config::Get("AdjointHomotopyStartRho", rho_start);

      double tmp = constants.Kappa() - 0.05;
      Config::Get("Adjointgamma1", tmp);

      if (tmp < 0.0) {
        double c1 = problem.Re() * constants.Gamma0();
        double c2 = problem.C2sqr<double>() *
                    pow((problem.C2<double>() + 0.25) * problem.Re() +
                        problem.Sigma() * problem.C2<double>(), 2);
        double c3 = pow(problem.Re() * (mid(constants.OmegaInfty()) + problem.C2<double>() * mid(constants.D_OmegaInfty())), 2);

        tmp = (constants.Kappa() + rho_start) / 2.0;
        for (int i = 0; i < 20; ++i) {
          double tmp_old = tmp;
          tmp = newtonStep(tmp, c1, c2, c3, rho_start, constants.Kappa());
          if (abs(tmp_old - tmp) < 1e-10)
            break;
        }
      }

      if (rho_start >= tmp) {
        ERROR("start rho larger than gamma1*")
      }
      constants.Gamma1<true>() = tmp;


      if (sup(constants.Gamma1<true>()) >= constants.Kappa()) {
        constants.Gamma1<true>() = constants.Kappa() - 0.05;
        Warning("Gamma1* to small in homotopy! Gamma1* set to " + to_string(constants.Gamma1<true>()))
      }

      IAInterval s = problem.C2sqr<IAInterval>() *
                     sqr(problem.Re() * (problem.C2<IAInterval>() + 0.25) +
                         problem.Sigma() * problem.C2<IAInterval>());

      if (inf(constants.Gamma1<true>()) < sup(constants.Kappa() - s)) {
        constants.Gamma1<true>() = sup(constants.Kappa() - s + 0.01);
        if (sup(constants.Gamma1<true>()) >= constants.Kappa()) {
          ERROR("Gamma1* to small in homotopy! Cannot resize gamma1*!")
        }
        Warning("Gamma1* to small in homotopy! Gamma1* set to " + to_string(constants.Gamma1<true>()))
      }

      constants.Gamma2<true>() = -problem.Re() * IAInterval(constants.Gamma0()) +
                            (s / (constants.Kappa() - constants.Gamma1<true>()) - 1.0) *
                            sqr(IAInterval(problem.Re()) * (constants.OmegaInfty() +
                                                                   problem.C2<IAInterval>() *
                                                                       constants.D_OmegaInfty()));

      return rho_start;
    }

    template<bool VERIFIED>
    void modelPlum(NavierStokesProblem &problem) {
      double rho_start;
      switch (problem.NormboundStrategy()) {
        case NB_PlumSimple:
          rho_start = computeSimpleConstants(problem);
          break;
        case NB_PlumExtended:
          rho_start = computeConstantsViaKappa(problem);
          break;
        default: ERROR("Norm bound strategy " + problem.NormboundStrategyName()
                       + " not implemented!")
      }

      NavierStokesConstants &constants = problem.Constants();

      double K_max = 1 / (2 * problem.C4<double>()
                          * sqrt(problem.Re() * constants.Delta()));

      int simpleHomotopyVerbose = problem.NormboundStrategy() == NB_PlumSimple ? 2 : 999;
      int extendedHomotopyVerbose = problem.NormboundStrategy() == NB_PlumExtended ? 2 : 999;
      mout.PrintInfo("Homotopy constants (adjoint)", 2,
                     PrintInfoEntry("kappa", OutputDown(constants.Kappa()), extendedHomotopyVerbose),
                     PrintInfoEntry("tau", OutputUp(constants.Tau()), simpleHomotopyVerbose),
                     PrintInfoEntry("gamma0", OutputDown(constants.Gamma0()), extendedHomotopyVerbose),
                     PrintInfoEntry("gamma1*", constants.Gamma1<true>(), 2),
                     PrintInfoEntry("gamma2*", constants.Gamma2<true>(), 2),
                     PrintInfoEntry("K*_max", K_max, 2),
                     PrintInfoEntry("rho_min", 1 / K_max / K_max, 2)
      );

      NavierStokesHomotopy<VERIFIED, true>(problem).start(rho_start);

      mout.PrintInfo("Surjectivity ("+problem.NormboundStrategyName()+")", 2,
                     PrintInfoEntry("K*_max", K_max, 2),
                     PrintInfoEntry("K*", OutputUp(constants.K<true>()), 2)
      );
    }

    template<bool VERIFIED>
    void Surjectivity(NavierStokesProblem &problem) {
      if (!problem.RunPart(PartSurjectivity)) return;

      switch (problem.NormboundStrategy()) {
        case NB_Wieners:
          modelWieners(problem);
          break;
        case NB_PlumSimple:
        case NB_PlumExtended:
          modelPlum<VERIFIED>(problem);
          break;
        default: ERROR("Normbound model not implemented")
      }

      NavierStokesConstants &constants = problem.Constants();
      if (constants.VelocityL4Diff() != 0.0) {
        IAInterval tmp = 1 - problem.Re() * problem.C4<IAInterval>()
                             * constants.K<true>() * constants.VelocityL4Diff() * 2;

        if (inf(tmp) <= 0.0) ERROR("Surjectivity cannot be proven")

        constants.K<true>() = sup(constants.K<true>() / tmp);

        mout.PrintInfo("Surjectivity (Adapt K*))", 2,
                       PrintInfoEntry("K*", OutputUp(constants.K<true>()), 2)
        );
      }

      problem.SaveData(PartSurjectivity);
    }

    template void Surjectivity<true>(NavierStokesProblem &problem);

    template void Surjectivity<false>(NavierStokesProblem &problem);

  } // namespace surjectivity

} // namespace navierstokes
