#include "NavierStokesHomotopy.hpp"

#include "NavierStokesAssembleHomotpy.hpp"

#include "IAEigenvalueMethods.hpp"
#include "IAIO.hpp"
#include "MeshesCreator.hpp"

namespace navierstokes {

  inline IAInterval f_sym(const double &x, int n, const double &R, const double &sigma) {
    IAInterval y(x);
    return tan(y) - sqrt((n * n) * IAInterval::PiSqr() + sigma) * R / y;
  }

  inline IAInterval Df_sym(const IAInterval &x,
                           int n,
                           const double &R,
                           const double &sigma) {
    return 1 / sqr(cos(x))
           + sqrt((n * n) * IAInterval::PiSqr() + sigma) * R / sqr(x);
  }

  inline bool crit_sym(const IAInterval &x, int n, const double &R, const double &sigma) {
    return sup(f_sym(inf(x), n, R, sigma) * f_sym(sup(x), n, R, sigma)) < 0.0
           && !(0.0 <= Df_sym(x, n, R, sigma));
  }

  void findZero_sym(IAInterval &x, int n, const double &R, const double &sigma) {
    IAInterval xOld;
    if (crit_sym(x, n, R, sigma)) {
      do {
        xOld = x;
        x = (mid(x) - f_sym(mid(x), n, R, sigma) / Df_sym(x, n, R, sigma)) & x;
      } while (x != xOld);
      x = (n * n) * IAInterval::PiSqr() + sigma + sqr(x / R);
    } else {
      x = IAInterval(infty);
      ERROR("Criterion not satisfied!");
    }
  }

  inline IAInterval f_asym(const double &x, int n, const double &R, const double &sigma) {
    IAInterval y(x);
    return tan(y) + y / sqrt((n * n) * IAInterval::PiSqr() + sigma) / R;
  }

  inline IAInterval Df_asym(const IAInterval &x,
                            int n,
                            const double &R,
                            const double &sigma) {
    return 1 / sqr(cos(x))
           + 1 / sqrt((n * n) * IAInterval::PiSqr() + sigma) / R;
  }

  inline bool crit_asym(const IAInterval &x, int n, const double &R, const double &sigma) {
    return sup(f_asym(inf(x), n, R, sigma) * f_asym(sup(x), n, R, sigma)) < 0.0
           && !(0.0 <= Df_asym(x, n, R, sigma));
  }

  void findZero_asym(IAInterval &x, int n, const double &R, const double &sigma) {
    IAInterval xOld;
    if (crit_asym(x, n, R, sigma)) {
      do {
        xOld = x;
        x = (mid(x) - f_asym(mid(x), n, R, sigma) / Df_asym(x, n, R, sigma)) & x;
      } while (x != xOld);
      x = (n * n) * IAInterval::PiSqr() + sigma + sqr(x / R);
    } else {
      x = IAInterval(infty);
      ERROR("Criterion not satisfied!");
    }
  }

  IAEigenvalueEnclosures BaseProblem(int n_max, int R, double sigma) {
    if (n_max % 2 != 0) {
      THROW("n_max is not even")
    }

    int n;
    IAInterval x;
    std::vector<std::pair<double, int>> idx;
    std::vector<IAInterval> mu_tmp;

    for (n = 1; n <= n_max / 2; ++n) {
      // symmetric
      x = IAInterval(1e-8, inf(IAInterval::Pid2()));

      findZero_sym(x, n, R, sigma);
      mu_tmp.push_back(x);
      idx.push_back(std::pair<double, int>(mid(x), idx.size()));

      for (int k = 0; k < n_max / 4; ++k) {
        IAInterval x0(sup(IAInterval::Pid2() + k * IAInterval::Pi()),
                      inf(IAInterval::Pid2() + (k + 1) * IAInterval::Pi()));

        // antisymmetric
        x = x0;
        findZero_asym(x, n, R, sigma);
        mu_tmp.push_back(x);
        idx.push_back(std::pair<double, int>(mid(x), idx.size()));

        // symmetric
        x = x0;
        findZero_sym(x, n, R, sigma);
        mu_tmp.push_back(x);
        idx.push_back(std::pair<double, int>(mid(x), idx.size()));
      }
    }
    std::sort(idx.begin(), idx.end());

    IAEigenvalueEnclosures mu(n_max);
    for (int i = 0; i < n_max / 2; ++i)
      mu[2 * i] = mu[2 * i + 1] = mu_tmp[idx[i].second];

    return mu;
  }

  template<>
  std::string NavierStokesHomotopy<true, false>::partName(std::string part) {
    return part;
  }

  template<>
  std::string NavierStokesHomotopy<false, false>::partName(std::string part) {
    return part;
  }

  template<>
  std::string NavierStokesHomotopy<true, true>::partName(std::string part) {
    return part + " (adjoint)";
  }

  template<>
  std::string NavierStokesHomotopy<false, true>::partName(std::string part) {
    return part + " (adjoint)";
  }

  template<bool VERIFIED, bool adjoint>
  bool NavierStokesHomotopy<VERIFIED, adjoint>::baseproblem() {
    double bound = sup(gamma2 / (gamma1 - rho));
    int n_max = int(floor(sup(sqrt((IAInterval(bound) - problem.Sigma())
                                   / IAInterval::PiSqr()))));

    if (n_max <= 0) {
      numEV = 0;
      K = sup(1 / sqrt(IAInterval(rho)));
      mout.PrintInfo(partName("Base Problem"), 2,
                     PrintInfoEntry("rho_0", OutputDown(rho), 2),
                     PrintInfoEntry("Eigenvalues below rho_0", numEV, 2),
                     PrintInfoEntry("R", problem.getApproximationDomainX(), 2)
      );
      return true;
    }

    int n;
    double ev;
    IAInterval x;
    std::vector<std::pair<double, int>> idx;
    std::vector<IAInterval> mu_tmp;

    for (n = 1; n <= n_max; ++n) {
      // symmetric
      x = IAInterval(1e-8, inf(IAInterval::Pid2()));

      findZero_sym(x, n,
                   problem.getApproximationDomainX(),
                   problem.Sigma());

      ev = mid(x);
      x = gamma1 - (gamma2 / x);
      if (inf(x) >= rho) continue;
      mu_tmp.push_back(x);
      idx.push_back(std::pair<double, int>(mid(x), idx.size()));

      for (int k = 0;; ++k) {
        IAInterval x0(sup(IAInterval::Pid2() + k * IAInterval::Pi()),
                      inf(IAInterval::Pid2() + (k + 1) * IAInterval::Pi()));

        // antisymmetric
        x = x0;
        findZero_asym(x, n,
                      problem.getApproximationDomainX(),
                      problem.Sigma());

        ev = mid(x);
        x = gamma1 - (gamma2 / x);
        if (inf(x) >= rho) break;
        mu_tmp.push_back(x);
        idx.push_back(std::pair<double, int>(mid(x), idx.size()));

        // symmetric
        x = x0;
        findZero_sym(x, n,
                     problem.getApproximationDomainX(),
                     problem.Sigma());

        ev = mid(x);
        x = gamma1 - (gamma2 / x);
        if (inf(x) >= rho) break;
        mu_tmp.push_back(x);
        idx.push_back(std::pair<double, int>(mid(x), idx.size()));
      }
    }
    std::sort(idx.begin(), idx.end());

    IAEigenvalueEnclosures mu(2 * idx.size());
    for (int i = 0; i < idx.size(); ++i)
      mu[2 * i] = mu[2 * i + 1] = mu_tmp[idx[i].second];

    double separationRho = 0.003;
    Config::Get("BaseproblemSeparationRho", separationRho);
    while (sup(abs(rho - mu[mu.size() - 1])) < separationRho) {
      rho = inf(mu[mu.size() - 1]);
      mu.removeLast();
      mu.removeLast();
    }

    numEV = mu.size();
    if (inf(mu[0]) > 0.0) {
      K = sup(1 / sqrt(mu[0]));
    } else {
      K = -1.0;
    }
    mout.PrintInfo(partName("Base Problem"), 2,
                   PrintInfoEntry("rho_0", OutputDown(rho), 2),
                   PrintInfoEntry("Eigenvalues below rho_0", numEV, 2),
                   PrintInfoEntry("R", problem.getApproximationDomainX(), 2)
    );

    int verbose = 4;

    int length = 8 + (int)(log10(mu.size()));
    vout(4) << "Eigenvalues of the base problem" << endl;
    for (int i = 0; i < mu.size(); ++i) {
      std::string out = "  mu_" + std::to_string(i + 1) + "=";
      out += std::string(length - out.size(), ' ');
      vout(4) << out << mu[i] << endl;
    }
    vout(4) << endl;

    if (inf(mu[0]) > 0.0)
      return true;
    return false;
  }

  template<bool VERIFIED, bool adjoint>
  void NavierStokesHomotopy<VERIFIED, adjoint>::constraintHomotopy() {
    if (numEV == 0) return;

    std::string meshName = "UnitSquare";
    Config::Get("MeshStrip", meshName);
    int stripLevel = 2;
    Config::Get("StripLevel", stripLevel);
    int stripCLevel = 2;
    Config::Get("StripCoarseLevel", stripCLevel);
    int plevel = 1;
    Config::Get("plevelStrip", plevel);

    std::unique_ptr<Meshes> stripMeshes = MeshesCreator(meshName).
        WithPLevel(plevel).
        WithCLevel(stripCLevel).
        WithLevel(stripLevel).
        WithSubdomainSetter(problem.GetSdSetterStrip()).
        CreateUnique();
    stripMeshes->PrintInfo();
    problem.ComputeRadiusStrip(*stripMeshes);

    double shift = 1 + std::ceil(sup(1 + gamma2 / problem.Sigma() - gamma1));
    std::unique_ptr<DivergenceHomotopyStokesAssemble<VERIFIED, adjoint>> assemble
        = createDivergenceHomotopyStokesAssemble<VERIFIED, adjoint>(problem, shift);

    std::shared_ptr<IDiscretization> discEV = std::move(assemble->CreateEVDisc(*stripMeshes));
    Eigenpairs EP(numEV, discEV);

    std::shared_ptr<IAIDiscretization> iadiscEV = nullptr;
    if constexpr (VERIFIED) {
      iadiscEV = std::move(assemble->CreateIAEVDisc(*stripMeshes));
      EP.SetIADisc(iadiscEV);
    }

    IAHomotopyMethod<VERIFIED> HM(stripCLevel, adjoint ? "AdjointDivergence" : "Divergence");
    double stepFinal = HM(*assemble, EP, rho, true);
    mout << endl;

    //Final Goerisch if n>0
    if (EP.size() > 0) {
      IALowerEigenvalueBounds lambda;
      HM(*assemble, EP, lambda, rho, false, stepFinal);
      if constexpr (VERIFIED)
        K = sup(1 / sqrt(IAInterval(lambda[0])));
      else
        K = 1 / sqrt(lambda[0]);
      numEV = lambda.size();
      mout << endl;
    } else {
      if constexpr (VERIFIED)
        K = sup(1 / sqrt(IAInterval(rho)));
      else
        K = 1 / sqrt(rho);
      numEV = 0;
    }

    mout.PrintInfo(partName("Constraint homotopy"), 2,
                   PrintInfoEntry("rho", OutputDown(rho), 2),
                   PrintInfoEntry("Eigenvalues below rho", numEV, 2)
    );
  }

  template<bool VERIFIED, bool adjoint>
  void NavierStokesHomotopy<VERIFIED, adjoint>::domainHomotopy() {
    if (numEV == 0) return;

    mout.PrintInfo(partName("Domain homotopy"), 2,
                   PrintInfoEntry("rho", OutputDown(rho), 2),
                   PrintInfoEntry("Eigenvalues below rho", numEV, 2)
    );
  }

  template<bool VERIFIED, bool adjoint>
  void NavierStokesHomotopy<VERIFIED, adjoint>::coefficientHomotopy() {
    if (numEV == 0) return;

    mout.PrintInfo(partName("Coefficient homotopy"), 2,
                   PrintInfoEntry("rho", OutputDown(rho), 2),
                   PrintInfoEntry("Eigenvalues below rho", numEV, 2)
    );
  }

  template<bool VERIFIED, bool adjoint>
  void NavierStokesHomotopy<VERIFIED, adjoint>::start(double rho0) {
    if (!adjoint || problem.NormboundStrategy() == NB_PlumExtended) {
      rho = rho0;

      if (rho >= inf(problem.Constants().Gamma1<adjoint>()))
        ERROR("HomotopyStartRho too small!")

      bool firstEigenvaluePositive = baseproblem();

      if (!adjoint || !firstEigenvaluePositive) {
        constraintHomotopy();
        domainHomotopy();
      }

      if constexpr (!adjoint) {
        problem.Constants().RhoAfterDomainHomotopy() = rho;
        problem.Constants().KAfterDomainHomotopy() = K;
        problem.Constants().NumEVAfterDomainHomotopy() = numEV;
      } else {

      }
    } else {
      rho = problem.Constants().RhoAfterDomainHomotopy();
      K = problem.Constants().KAfterDomainHomotopy();
      numEV = problem.Constants().NumEVAfterDomainHomotopy();
    }
    coefficientHomotopy();
    problem.Constants().K<adjoint>() = K;

    mout.PrintInfo(partName("Homotopy"), 2,
                   PrintInfoEntry("rho", OutputDown(rho), 2),
                   PrintInfoEntry("Eigenvalues below rho", numEV, 2),
                   PrintInfoEntry(adjoint ? "K*" : "K", K, 2)
    );
  }

  template
  class NavierStokesHomotopy<false, false>;

  template
  class NavierStokesHomotopy<true, false>;

  template
  class NavierStokesHomotopy<false, true>;

  template
  class NavierStokesHomotopy<true, true>;
} // namespace navierstokes

