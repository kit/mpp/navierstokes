#ifndef NAVIERSTOKESKAPPA_HPP
#define NAVIERSTOKESKAPPA_HPP

#include "problem/NavierStokesProblem.hpp"
#include "SymRMatrix.hpp"

namespace navierstokes {

  namespace kappa {
      double ComputeKappa(NavierStokesProblem &problem);

      double ApproximateKappa(NavierStokesProblem &problem);
  }

} // namespace navierstokes

#endif //NAVIERSTOKESKAPPA_HPP
