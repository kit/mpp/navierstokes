#ifndef NAVIERSTOKESSURJECTIVITY_HPP
#define NAVIERSTOKESSURJECTIVITY_HPP

#include "problem/NavierStokesProblem.hpp"

namespace navierstokes {

  namespace surjectivity {
    template<bool VERIFIED>
    void Surjectivity(NavierStokesProblem &problem);
  }

}

#endif //NAVIERSTOKESSURJECTIVITY_HPP
