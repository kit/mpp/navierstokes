#ifndef NAVIERSTOKESDEFECT_HPP
#define NAVIERSTOKESDEFECT_HPP

#include "problem/NavierStokesProblem.hpp"

namespace navierstokes {

  namespace defectbound {
    template<bool VERIFIED>
    void VelocityDefect(NavierStokesProblem &problem);

    template<bool VERIFIED>
    void PressureDefect(NavierStokesProblem &problem);
  }

}

#endif //NAVIERSTOKESDEFECT_HPP
