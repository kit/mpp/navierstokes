## What is M++?
A C++ library providing Finite Element Methods (FEM) to solve Partial Differential Equations (PDE).
It uses MPI to distribute the computations on parallel processes and nodes.

More information:
- https://www.math.kit.edu/ianm3/page/mplusplus

## Getting Started
M++ is a parallel PDE solving software requiring some preinstalled packages. This guide hopefully walks you through the installation process.

Simply try

```./init.sh```

which works if make and cmake is installed.

### Prerequisites
This version of M++ uses CMake as building tool. If you haven't installed CMake Version 3.15 or higher, find out about
the installation process on https://cmake.org/download/.

Furthermore, you need the packages BLAS (Basic Linear Algebra Subroutines) and LAPACK (Linear Algebra PACKage). Find out
more on https://www.netlib.org/blas/ and https://www.netlib.org/lapack/.

Most importantly, M++ is a is a parallel PDE solving software. In order to distribute your computations on parallel processes you need to have Open MPI (Message Passing Interface) installed.
If not installed, get a version of Open MPI (e.g. 2.1) on https://www.open-mpi.org/software/ompi/v2.1/ and follow the installation instructions.

#### On Unix
If you are using a Unix based system, you can simply run:

```sudo apt install cmake```

```sudo apt install libblas-dev liblapack-dev```

```sudo apt install openmpi-bin libopenmpi-dev```

### Installing M++
This Project uses the M++ core. To install it and the M++ core, use the command

```git clone --recurse-submodules https://gitlab.kit.edu/kit/mpp/navierstokes.git```

and then, change the branch:

```cd navierstokes```  
```git checkout 22-gelfand```

Then change into the project root, create a new build folder with ```mkdir build```, go into this folder and run ```cmake ..```.
CMake generates a Makefile tailored to your system. Run ```make -j``` to finally build the project.

### Running M++
You can run the predefined code for the Gelfand equation by using the command 

```./Gelfand```

or, to use multiple cores, 

```mpirun -n N Gelfand```,

where N is the number of cores you want to use.

The result is in log/logfile.log, and 

```paraview data/vtu/u.vtu```

can be used for plotting. Different solutions are computed by 

```mpirun -n 8 Gelfand initvalue_left=1 initvalue_right=1```  
```mpirun -n 8 Gelfand initvalue_left=6 initvalue_right=1```  
```mpirun -n 8 Gelfand initvalue_left=1 initvalue_right=6```  
```mpirun -n 8 Gelfand initvalue_left=6 initvalue_right=6```  

or changing the options in navierstokes/conf/gelfand.conf.

### Updating M++
If you want to update the project to its latest release, simply pull the repository by using

```git pull --recurse-submodules```

This will automatically update the M++ core as well as all used submodules as well.
